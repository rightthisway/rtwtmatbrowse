package com.admitone.tmat.eventfetcher;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.apache.xalan.xsltc.trax.SAX2DOM;
import org.apache.xpath.XPathAPI;
import org.ccil.cowan.tagsoup.Parser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.admitone.tmat.data.Site;
import com.admitone.tmat.utils.QueryUtil;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;

public class TicketSolutionsEventListingFetcher extends EventListingFetcher {
	private Logger logger = LoggerFactory.getLogger(TicketSolutionsEventListingFetcher.class);
	private Pattern datePattern = Pattern.compile("(\\d{1,2}/\\d{1,2}/\\d{4})");
	private Pattern timePattern = Pattern.compile("(\\d{1,2}:\\d{1,2} [AP]M)");
	private Pattern eventIdPattern = Pattern.compile("eventid=(\\d+)");

	public Collection<EventHit> getEventList(String keywords, String location, Date fromDate, Date toDate) throws Exception {
		DateFormat dateFormat = new SimpleDateFormat("M/d/yyyy");
		DateFormat timeFormat = new SimpleDateFormat("h:mm aa");
		
		Collection<EventHit> eventHits = new ArrayList<EventHit>();
		Collection<String> eventIds = new ArrayList<String>();

		keywords = keywords.replace('+', ' ');
		for(String keyword : keywords.split(" ")){

		Calendar fromCal = new GregorianCalendar();
		fromCal.setTime(fromDate);

		Calendar toCal = new GregorianCalendar();
		toCal.setTime(toDate);

		if (location == null) {
			location = "";
		}
		
		SimpleHttpClient httpClient = null;		

		String eventUrl = "http://www.ticketsolutions.com/searchresults.asp";
			
		try {
			httpClient = HttpClientStore.createHttpClient();
			
			HttpPost post = new HttpPost(eventUrl);
			
//			post.setHeader("Accept", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
//			post.setHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
			
            List <NameValuePair> nvps = new ArrayList <NameValuePair>();
            nvps.add(new BasicNameValuePair("HeadlinerName", keyword));
            nvps.add(new BasicNameValuePair("VenueName", ""));
            // note: do the filtering after
//            nvps.add(new BasicNameValuePair("CityName", location));
            nvps.add(new BasicNameValuePair("CityName", ""));
            nvps.add(new BasicNameValuePair("StateName", ""));
            nvps.add(new BasicNameValuePair("DateMonth", ""));
            nvps.add(new BasicNameValuePair("DateDay", ""));
            nvps.add(new BasicNameValuePair("DateYear", ""));
            nvps.add(new BasicNameValuePair("DateGroup", "DateRange"));
            nvps.add(new BasicNameValuePair("StartDateDay", fromCal.get(Calendar.DATE) + ""));
            nvps.add(new BasicNameValuePair("StartDateYear", fromCal.get(Calendar.YEAR) + ""));
            nvps.add(new BasicNameValuePair("StartDateMonth", (fromCal.get(Calendar.MONTH) + 1) + ""));
            nvps.add(new BasicNameValuePair("EndDateMonth", (toCal.get(Calendar.MONTH) + 1) + ""));
            nvps.add(new BasicNameValuePair("EndDateDay", toCal.get(Calendar.DATE) + ""));
            nvps.add(new BasicNameValuePair("EndDateYear", toCal.get(Calendar.YEAR) + ""));
            nvps.add(new BasicNameValuePair("Submit1", "Search Events"));
            post.setEntity(new UrlEncodedFormEntity(nvps, HTTP.UTF_8));

            HttpResponse response = httpClient.execute(post);
			Parser parser = new Parser();
			parser.setFeature("http://xml.org/sax/features/namespace-prefixes", true);
			// to define the html: prefix (off by default)
			SAX2DOM sax2dom = new SAX2DOM();
			parser.setContentHandler(sax2dom);
				
			// System.out.println("CONTENT=" + EntityUtils.toString(response.getEntity()));
			
			parser.parse(new InputSource(response.getEntity().getContent()));
			Node document = sax2dom.getDOM();

			// check if no result
			if (XPathAPI.selectSingleNode(document, "//html:b[contains(text(), 'No Events Were Found!')]") != null) {
				return null; 
			}
			
			// note: it seems that all search results contains this string now

			if (XPathAPI.selectSingleNode(document, "//html:h1[contains(text(), 'The Search You Requested Did Not Send Back Exact Results')]") != null) {
				return null; 
			}
			
			
			NodeList list = XPathAPI.selectNodeList(document, "//html:tr[@class='GridBodyNormal' or @class='GridBodyAlternative']");
			for (int i = 0; i < list.getLength(); i++) {
				Node node = list.item(i);
				Element element = (Element) node;
				NodeList tdList = element.getElementsByTagName("td");
				String eventName = tdList.item(0).getTextContent();
				String dateTimeStr = tdList.item(1).getTextContent();
				String eventLocation = tdList.item(2).getTextContent().replaceAll("\\s+", " ");
				String url = "http://go.ticketsolutions.com/" + ((Element)tdList.item(3).getFirstChild()).getAttribute("href");
				
				System.out.println("LOCATION=" + location);
				System.out.println("EVENT LOCATION=" + eventLocation);
				if (location != null && location.length() > 0 && !QueryUtil.matchAllTerms(location, eventLocation)) {
					continue;
				}
				
				java.sql.Date date = null; 
				java.sql.Time time = null;
				
				Matcher dateMatcher = datePattern.matcher(dateTimeStr);
				if (dateMatcher.find()) {
					String dateStr = dateMatcher.group(1);
					
					try {
						date = new java.sql.Date(dateFormat.parse(dateStr).getTime());
						Matcher timeMatcher = timePattern.matcher(dateTimeStr);
						if (timeMatcher.find()) {
							String timeStr = timeMatcher.group(1);
							time = new java.sql.Time(timeFormat.parse(timeStr).getTime());
						}
						
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
	
				Matcher matcher = eventIdPattern.matcher(url);
				if (!matcher.find()) {
					continue;
				}
				String eventId = matcher.group(1);
				// System.out.println(url);
				// System.out.println(eventName + " " + dateTimeStr + " " + eventLocation);
				if(!eventIds.contains(eventId)) {
					EventHit eventHit = new EventHit(eventId, eventName, date, time, eventLocation, Site.TICKET_SOLUTIONS, url);
					eventHits.add(eventHit);
					eventIds.add(eventId);
				}
			}
		} finally {
			HttpClientStore.releaseHttpClient(httpClient);
		}
		}
		return eventHits;
	}	
}
