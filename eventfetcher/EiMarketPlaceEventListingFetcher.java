package com.admitone.tmat.eventfetcher;

import java.io.StringReader;
import java.net.URI;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ProtocolException;
import org.apache.http.client.RedirectHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.cookie.BasicClientCookie2;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.apache.xalan.xsltc.trax.SAX2DOM;
import org.apache.xpath.XPathAPI;
import org.ccil.cowan.tagsoup.Parser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.utils.QueryUtil;
import com.admitone.tmat.utils.TextUtil;
import com.admitone.tmat.utils.eimarketplace.EIMPCredential;
import com.admitone.tmat.utils.eimarketplace.EIMPRedirectHandler;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;

/**
 * EiMarketPlace Event Fetcher.
 * When doing a search on eimarketplace, we get a page with the list of artist, then we get a page
 * with the list of venue and finally a page with the list of date.
 * 
 *  However if in one page, there is only one choice, we are automatically redirect to the next page.
 *  (can be redirected up to the event ticket listing page).
 *  
 * when doing a post, set the header with the following value:
 * 			httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");
 */
public class EiMarketPlaceEventListingFetcher extends EventListingFetcher {
	private Logger logger = LoggerFactory.getLogger(EiMarketPlaceEventListingFetcher.class);
	
	// tour pattern
	//                                                                                           HREF                       EVENT NAME       ROW TYPE                        DATE
	private Pattern tourPattern = Pattern.compile("<tr class='SearchResult.*?Row'>.*?<a.*?href='(.*?)'.*?SearchResult.*?Text'>(.*?)<.*?<Div.*?>(.*?)<.*?SearchResultBlankText'>(.*?)<", Pattern.DOTALL);
	
	 // Tue, May. 26th, 2009 07:00 PM 
	private Pattern datePattern1 = Pattern.compile("\\w+, (\\w+). (\\d+)\\w+, (\\d+)+ (\\d+):(\\d)+ (\\w+)");

	// Tue, Jul. 7th, 2009 TBD 
	private Pattern datePattern2 = Pattern.compile("\\w+, (\\w+). (\\d+)\\w+, (\\d+)+ TBD");

	// http://www.eimarketplace.com/MarketPlace/Inventory/TicketList.aspx?PID=820766
	private Pattern eventIdPattern = Pattern.compile("PID=(\\d+)");

	// searchId => 
	private Map<String, EventHitCacheEntry> eventHitCacheMap = new HashMap<String, EventHitCacheEntry>();
	private Map<String, Semaphore> lockMap = new HashMap<String, Semaphore>();

	private EIMPCredential credential;
	
	@Override
	public Collection<EventHit> getEventList(String keywords, String location, Date fromDate, Date toDate,List<Event> eventList,boolean isVenue) throws Exception {
		Collection<EventHit> eventHits = new ArrayList<EventHit>();
		SimpleDateFormat cacheDateFormat = new SimpleDateFormat("yyyyMMdd");
		if(keywords == null || keywords.isEmpty()) {
		String cacheKey = keywords + location + cacheDateFormat.format(fromDate) + cacheDateFormat.format(toDate);
		} else {
			for(String keyword : keywords.split(" ")){
				String cacheKey = keyword + location + cacheDateFormat.format(fromDate) + cacheDateFormat.format(toDate);
			
		
		
		//logger.info("EIMP keywords=" + keywords + " location=" + location);
		
			Semaphore lock;
			synchronized (this) {
				// remove old entries in cache
				Date now = new Date();
				Map<String, EventHitCacheEntry> newEventHitCacheMap = new HashMap<String, EventHitCacheEntry>();
				for(Map.Entry<String, EventHitCacheEntry> entry: eventHitCacheMap.entrySet()) {
					if (now.getTime() - entry.getValue().getInsertionDate().getTime() < 5L * 60L * 1000L) {
						newEventHitCacheMap.put(entry.getKey(), entry.getValue());
					}
				}
				eventHitCacheMap = newEventHitCacheMap;			
				//logger.info("CACHE KEY=" + cacheKey);

				// don't allow two searches with same keywords, location and date to run simultaneously
				lock = lockMap.get(cacheKey);
				if (lock == null) {
					lock = new Semaphore(1);
					lockMap.put(cacheKey, lock);
				}
			}
		

			SimpleHttpClient httpClient = null;
			try {
						
				lock.acquire();		
				synchronized (this) {
					EventHitCacheEntry cacheEntry = eventHitCacheMap.get(cacheKey);
					logger.info("FOUND CACHE ENTRY=" + cacheEntry);
					if (cacheEntry != null) {
						logger.info("FOUND CACHE ENTRIES=" + cacheEntry.getEventHits());
						return cacheEntry.getEventHits();
					}
				}			
			
				httpClient = HttpClientStore.createHttpClient(Site.EI_MARKETPLACE, credential.getUsername(), credential.getPassword());
				if (httpClient == null) {
					throw new Exception("Cannot log in");				
				}
			
				//
				// get matching tours
				//
			
				httpClient.setRedirectHandler(new EIMPRedirectHandler());

				((EIMPRedirectHandler)httpClient.getRedirectHandler()).reset();
				String searchUrl = "https://www.eimarketplace.com/MarketPlace/Inventory/SearchResults.aspx?STY=SS&WHAT=" + keywords.replace(" ", "+")+ "&sar=0";
				HttpGet httpGet = new HttpGet(searchUrl);
				httpGet.addHeader("Host", "www.eimarketplace.com");
//				httpGet.addHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
				httpGet.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
				httpGet.addHeader("Accept-Language", "en-us,en;q=0.5");
				httpGet.addHeader("Accept-Encoding", "gzip,deflate");
				httpGet.addHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
				httpGet.addHeader("Referer", "https://www.eimarketplace.com/members/index.cfm");
				HttpResponse response = httpClient.execute(httpGet);
			
				String currentUrl = searchUrl;						
				String redirectUrl = ((EIMPRedirectHandler)httpClient.getRedirectHandler()).getRedirectUrl();
			
				if (redirectUrl != null) {
					if (!redirectUrl.startsWith("http")) {
						currentUrl = "https://www.eimarketplace.com" + redirectUrl;
					} else {
						currentUrl = redirectUrl;
					}
				}

				Collection<EventHit> addEventHits = null;
			
				if (currentUrl.contains("SearchResults.aspx")) {
					addEventHits = parseTourPage(httpClient, response.getEntity(), currentUrl, keywords, location, fromDate, toDate);
				} else if (currentUrl.contains("EventLanding.aspx")) {
					addEventHits = parseVenuePage(httpClient, response.getEntity(), currentUrl, keywords, location, fromDate, toDate);				
				} else {
					addEventHits = parseEventPage(httpClient, response.getEntity(), currentUrl, keywords, location, fromDate, toDate);
				}
			
				// remove duplicate
				Map<String, EventHit> eventHitMap = new HashMap<String, EventHit>();
				for(EventHit eventHit: addEventHits) {
					eventHitMap.put(eventHit.getEventId(), eventHit);
				}

				synchronized (this) {
					eventHitCacheMap.put(cacheKey, new EventHitCacheEntry(eventHitMap.values()));
				}
				eventHits.addAll(eventHitMap.values());
			} finally {
				if (httpClient != null) {
					HttpClientStore.releaseHttpClient(httpClient);
				}
				lock.release();
			}
			}
		}
		return eventHits;
	}

	private Collection<EventHit> parseTourPage(DefaultHttpClient httpClient, HttpEntity entity, 
			String refererUrl, String keywords, String location, Date fromDate, Date toDate) throws Exception {
		HttpEntity uncompressedEntity = HttpEntityHelper.getUncompressedEntity(entity);
		Collection<EventHit> eventHits = new ArrayList<EventHit>();
		
		String content = EntityUtils.toString(uncompressedEntity);
		

		// Tue, Jul. 7th, 2009 TBD 
		SimpleDateFormat dateFormat3 = new SimpleDateFormat("MMM dd, yyyy");

		
		Matcher tourMatcher = tourPattern.matcher(content);
		
		while (tourMatcher.find()) {
			System.out.println("HREF=" + tourMatcher.group(1));
			System.out.println("NAME=" + tourMatcher.group(2));
			System.out.println("TYPE=" + tourMatcher.group(3));
			System.out.println("DATE=" + tourMatcher.group(4));
			System.out.println("---");
			String rowType = tourMatcher.group(3);
			
			if (rowType.equals("Venue")) {
				// it is a venue result, skip it
				continue;
			}
			
			String tourName = tourMatcher.group(2);
			if (!QueryUtil.matchAnyTerms(keywords, tourName)) {
				continue;
			}
			
			String tourDateString = tourMatcher.group(4);
			String[] tourDateTokens = tourDateString.split("-");

			Date startDate = dateFormat3.parse(tourDateTokens[0].trim());
			Date endDate = dateFormat3.parse(tourDateTokens[1].trim());
			
			if (endDate.before(fromDate) || startDate.after(toDate)) {
				continue;
			}
			

			
			if (true) {
				continue;
			}
			
			// the href contains some spaces
			String tourHref = tourMatcher.group(1).replace(" ", "+");				
			HttpGet httpGet = new HttpGet(tourHref);
			
			httpGet.addHeader("Host", "www.eimarketplace.com");
			httpGet.addHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
			httpGet.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
			httpGet.addHeader("Accept-Language", "en-us,en;q=0.5");
			httpGet.addHeader("Accept-Encoding", "gzip,deflate");
			httpGet.addHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
			// httpGet.addHeader("Keep-Alive", "300");
			// httpGet.addHeader("Connection", "keep-alive");
			httpGet.addHeader("Referer", "https://www.eimarketplace.com/members/index.cfm");
			HttpResponse response = httpClient.execute(httpGet);

			String currentUrl = tourHref;
			if (((EIMPRedirectHandler)httpClient.getRedirectHandler()).getRedirectUrl() != null) {
				currentUrl = ((EIMPRedirectHandler)httpClient.getRedirectHandler()).getRedirectUrl();
			}
			
			Collection<EventHit> addEventHits = null;
			if (currentUrl.contains("EventLanding.aspx")) {
				addEventHits = parseVenuePage(httpClient, response.getEntity(), currentUrl, keywords, location, fromDate, toDate);
			} else {
				addEventHits = parseEventPage(httpClient, response.getEntity(), currentUrl, keywords, location, fromDate, toDate);
			}
			
			if (addEventHits != null) {
				eventHits.addAll(addEventHits);
			}

		}
		
		return eventHits;		
	}

	private Collection<EventHit> parseVenuePage(DefaultHttpClient httpClient, HttpEntity entity, 
			String refererUrl, String keywords, String location, Date fromDate, Date toDate) throws Exception {
		
		Collection<EventHit> eventHits = new ArrayList<EventHit>();
		Collection<String> eventIds = new ArrayList<String>();

		Parser parser = new Parser();
		parser.setFeature("http://xml.org/sax/features/namespace-prefixes", true);
		// to define the html: prefix (off by default)
		SAX2DOM sax2dom = new SAX2DOM();
		parser.setContentHandler(sax2dom);
		HttpEntity uncompressedEntity = HttpEntityHelper.getUncompressedEntity(entity);
		parser.parse(new InputSource(uncompressedEntity.getContent()));
		
		Node document = sax2dom.getDOM();
		
		// 5/26/10
		SimpleDateFormat venueDateFormat = new SimpleDateFormat("MM/dd/yy");
		
		NodeList rowNodeList = XPathAPI.selectNodeList(document, "//html:table[@id='ucEventLanding_gvEvents']/html:tr");
		for (int i = 1 ; i < rowNodeList.getLength() ; i++) {
			Node rowNode = rowNodeList.item(i);
			NodeList rowNodeChildNodes = rowNode.getChildNodes();
			
			String venueLocation = TextUtil.removeExtraWhitespaces(rowNodeChildNodes.item(0).getTextContent() + " " + rowNodeChildNodes.item(1).getTextContent()).trim();
			if (!QueryUtil.matchAllTerms(location, venueLocation)) {
				continue;
			}
						
			String venueDateString =  rowNodeChildNodes.item(2).getTextContent();
			
			String[] dateTokens = venueDateString.split("-");
			Date startDate = venueDateFormat.parse(dateTokens[0].trim());
			Date endDate = venueDateFormat.parse(dateTokens[1].trim());

			// System.out.println("VENUE LOCATION=" + venueLocation);
			// System.out.println("VENUE DATE=" + venueDateString);
			// System.out.println("VENUE FROM DATE=" + startDate + "/" + fromDate + "/" + fromDate.before(startDate));
			// System.out.println("VENUE END DATE=" + endDate + "/" + toDate + "/" + toDate.after(endDate));

			if (endDate.before(fromDate) || startDate.after(toDate)) {
				continue;
			}
			
			String eventListHref = XPathAPI.selectSingleNode(rowNodeChildNodes.item(0), "html:a/@href").getTextContent(); 
			
			HttpGet httpGet = new HttpGet(eventListHref);			
			httpGet.addHeader("Host", "www.eimarketplace.com");
			httpGet.addHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
			httpGet.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
			httpGet.addHeader("Accept-Language", "en-us,en;q=0.5");
			httpGet.addHeader("Accept-Encoding", "gzip,deflate");
			httpGet.addHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
			httpGet.addHeader("Referer", refererUrl);
			HttpResponse response = httpClient.execute(httpGet);

			Collection<EventHit> addEventHits = parseEventPage(httpClient, response.getEntity(), eventListHref, keywords, venueLocation, fromDate, toDate);
			if (addEventHits != null) {
				for(EventHit eHit : addEventHits){
					if(!eventIds.contains(eHit.getId())){
						eventHits.add(eHit);	
						eventIds.add(eHit.getId());
					}
				}			
			}
			
		}
		
		return eventHits;
		
	}

	private Collection<EventHit> parseEventPage(DefaultHttpClient httpClient, HttpEntity entity, 
			String currentUrl, String keywords, String location, Date fromDate, Date toDate) throws Exception {

		Collection<EventHit> eventHits = new ArrayList<EventHit>();

		Parser parser = new Parser();
		parser.setFeature("http://xml.org/sax/features/namespace-prefixes", true);
		// to define the html: prefix (off by default)
		SAX2DOM sax2dom = new SAX2DOM();
		parser.setContentHandler(sax2dom);
		HttpEntity uncompressedEntity = HttpEntityHelper.getUncompressedEntity(entity);
		parser.parse(new InputSource(uncompressedEntity.getContent()));
		
		Node document = sax2dom.getDOM();
		
		if (XPathAPI.selectSingleNode(document, "//html:table[@id='ucEventsList_obtnSports']") != null) {
			String viewState = XPathAPI.selectSingleNode(document, "//html:input[@name='__VIEWSTATE']/@value").getTextContent();
			// that means that we have a home and away checkbox
			// we need to reexecute the request with a POST to force to display both home and away events
			HttpPost httpPost = new HttpPost(currentUrl);			
			httpPost.addHeader("Host", "www.eimarketplace.com");
			httpPost.addHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
			httpPost.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
			httpPost.addHeader("Accept-Language", "en-us,en;q=0.5");
			httpPost.addHeader("Accept-Encoding", "gzip,deflate");
			httpPost.addHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
			// httpGet.addHeader("Keep-Alive", "300");
			// httpGet.addHeader("Connection", "keep-alive");
			httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");			
			httpPost.addHeader("Referer", currentUrl);
			
			String postData = "txtSearch=&__EVENTTARGET=ucEventsList%24obtnSports%241&__EVENTARGUMENT=&__LASTFOCUS=&__VIEWSTATE="
				+ URLEncoder.encode(viewState, "ISO-8859-1") 
				+ "&ucEventsList%24ddlDateRangeFilter=Full+Schedule&ucEventsList%24obtnSports=A";

			// System.out.println("POSTDATA=" + postData);
			httpPost.setEntity(new StringEntity(postData));
			HttpResponse response = httpClient.execute(httpPost);

			parser = new Parser();
			parser.setFeature("http://xml.org/sax/features/namespace-prefixes", true);
			// to define the html: prefix (off by default)
			sax2dom = new SAX2DOM();
			parser.setContentHandler(sax2dom);
			uncompressedEntity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
			parser.parse(new InputSource(uncompressedEntity.getContent()));
			
			document = sax2dom.getDOM();
		}
		
		 // Tue, May. 26th, 2009 07:00 PM
		SimpleDateFormat dateFormat1 = new SimpleDateFormat("MMM dd yyyy hh:mm aa");

		// Tue, Jul. 7th, 2009 TBD 
		SimpleDateFormat dateFormat2 = new SimpleDateFormat("MMM dd yyyy");				

		NodeList rowNodeList = XPathAPI.selectNodeList(document, "//html:div[@class='row' or @class='rowAlt']");
		for (int j = 0 ; j < rowNodeList.getLength() ; j++) {
			Node rowNode = rowNodeList.item(j);
			
			Node eventAnchor = XPathAPI.selectSingleNode(rowNode, "html:div[@class='c1']/html:a");
			
			String eventName = TextUtil.removeExtraWhitespaces(eventAnchor.getTextContent()).trim();
			
			// http://www.eimarketplace.com/MarketPlace/Inventory/TicketList.aspx?PID=820766
			String eventHref = eventAnchor.getAttributes().getNamedItem("href").getTextContent();
			Matcher eventIdMatcher = eventIdPattern.matcher(eventHref);
			eventIdMatcher.find();
			String eventId = eventIdMatcher.group(1);					
			
			String eventDateString = XPathAPI.selectSingleNode(rowNode, "html:div[@class='c2']/html:a").getTextContent();					
			// Tue, Jul. 7th, 2009 TBD
			// Tue, May. 26th, 2009 07:00 PM

			java.sql.Date eventSqlDate;
			java.sql.Time eventSqlTime;
			Date eventDate;
			Matcher dateMatcher = datePattern1.matcher(eventDateString);
			if (dateMatcher.find()) {
				eventDate = dateFormat1.parse(
						dateMatcher.group(1)
						+ " " + dateMatcher.group(2)
						+ " " + dateMatcher.group(3)
						+ " " + dateMatcher.group(4)
						+ ":" + dateMatcher.group(5)
						+ " " + dateMatcher.group(6)
				);
				eventSqlDate = new java.sql.Date(eventDate.getTime());
				eventSqlTime = new java.sql.Time(eventDate.getTime());
			} else {
				dateMatcher = datePattern2.matcher(eventDateString);
				dateMatcher.find();

				eventDate = dateFormat2.parse(
						dateMatcher.group(1)
						+ " " + dateMatcher.group(2)
						+ " " + dateMatcher.group(3)
				);					
				eventSqlDate = new java.sql.Date(eventDate.getTime());
				eventSqlTime = null;
			}
			
			if (eventDate.before(fromDate) || eventDate.after(toDate)) {
				continue;
			}
									
			String eventLocation = TextUtil.removeExtraWhitespaces(XPathAPI.selectSingleNode(rowNode, "html:div[@class='c3']/html:a").getTextContent()).trim();
			
			EventHit eventHit = new EventHit(eventId, eventName, eventSqlDate, eventSqlTime, eventLocation, Site.EI_MARKETPLACE, eventHref);
			eventHits.add(eventHit);
		}
		
		return eventHits;

	}

	public void setCredential(EIMPCredential credential) {
		this.credential = credential;
	}
	
	class EventHitCacheEntry {
		private Date insertionDate;
		private Collection<EventHit> eventHits;
		
		public EventHitCacheEntry(Collection<EventHit> eventHits) {
			insertionDate = new Date();
			this.eventHits = eventHits;
		}

		public Date getInsertionDate() {
			return insertionDate;
		}

		public Collection<EventHit> getEventHits() {
			return eventHits;
		}
	}
}
