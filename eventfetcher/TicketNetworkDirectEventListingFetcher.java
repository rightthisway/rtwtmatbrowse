package com.admitone.tmat.eventfetcher;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.ticketfetcher.TicketNetworkDirectTicketListingFetcher;
import com.admitone.tmat.utils.QueryUtil;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;
import com.admitone.tmat.utils.ticketnetwork.TicketNetworkCategory;
import com.admitone.tmat.utils.ticketnetwork.TicketNetworkEvent;
import com.admitone.tmat.utils.ticketnetwork.TicketNetworkTicketGroup;
import com.admitone.tmat.utils.ticketnetwork.TicketNetworkWebServiceClient;

/**
 * TicketNetworkDirect Event Fetcher.
 */

public class TicketNetworkDirectEventListingFetcher extends EventListingFetcher {

	@Override
	public Collection<EventHit> getEventList(String keywords, String location, Date fromDate, Date toDate,List<Event> eventList,boolean isVenue) throws Exception {
		SimpleHttpClient httpClient = null;		
		try {
			httpClient = HttpClientStore.createHttpClient();
			
			TicketNetworkWebServiceClient client = new TicketNetworkWebServiceClient();
			List<TicketNetworkEvent> events = new ArrayList<TicketNetworkEvent>();
			StringTokenizer keywordList = new StringTokenizer(keywords);
			
			Set<String> usedEventIds = new HashSet<String>();
			if(!isVenue){
				while(keywordList.hasMoreTokens()){
					String symbol = keywordList.nextToken();
					List<TicketNetworkEvent> tempEvents = client.getEvents(httpClient, symbol, fromDate, toDate, null, null, null,location,eventList,isVenue);
					if(tempEvents != null && !tempEvents.isEmpty()) {
						TicketNetworkEvent tempEvent = null;
						for(int index = 0 ; index < tempEvents.size(); index++){
							tempEvent = tempEvents.get(index);
							if (!QueryUtil.matchAtleastHalfTerms(keywords.replaceAll("\\s+", " "), tempEvent.getEventName())) {
								continue;
							}
							
							if (!usedEventIds.contains(tempEvent.getEventID())) {
								events.add(tempEvent);
								usedEventIds.add(tempEvent.getEventID());
							}
							
	// FIXME: this test is not correct: some games have the same name
	// and occurs at different date						
	//						boolean acceptEvent = true;
	//						for(int i = 0; (i < events.size()) && acceptEvent ; i++){
	//							if(tempEvent.getEventName().equals(events.get(i).getEventName())) {
	//								acceptEvent = false;
	//							}
	//						}
	//						if(acceptEvent){
	//							events.add(tempEvent);
	//						}
						}
					}
				}
			}else{
				List<TicketNetworkEvent> tempEvents = client.getEvents(httpClient, keywords, fromDate, toDate, null, null, null,location,eventList,isVenue);
				if(tempEvents != null && !tempEvents.isEmpty()) {
					TicketNetworkEvent tempEvent = null;
					for(int index = 0 ; index < tempEvents.size(); index++){
						tempEvent = tempEvents.get(index);
						if(!QueryUtil.matchAtleastHalfTerms(keywords.replaceAll("\\s+", " "),tempEvent.getVenue())){
							continue;
						}
						
						if (!usedEventIds.contains(tempEvent.getEventID())) {
							events.add(tempEvent);
							usedEventIds.add(tempEvent.getEventID());
						}
						
// FIXME: this test is not correct: some games have the same name
// and occurs at different date						
//						boolean acceptEvent = true;
//						for(int i = 0; (i < events.size()) && acceptEvent ; i++){
//							if(tempEvent.getEventName().equals(events.get(i).getEventName())) {
//								acceptEvent = false;
//							}
//						}
//						if(acceptEvent){
//							events.add(tempEvent);
//						}
					}
				}
			}
			Collection<EventHit> eventHits = new ArrayList<EventHit>();
			for (TicketNetworkEvent event: events) {
				eventHits.add(new EventHit(event.getEventID(), event.getEventName(),
						new java.sql.Date(event.getEventDate().getTime()), new java.sql.Time(event.getEventDate().getTime()),
						event.getVenue() + ", " + event.getCity() + ", " + event.getState(), Site.TICKET_NETWORK_DIRECT, "http://www.ticketnetwork.com/tix/tix-"
						+ event.getEventID() + ".aspx"));
				System.out.println("===>" + event);
			}
						
			return eventHits;
		} finally {
			HttpClientStore.releaseHttpClient(httpClient);
		}
	}
	
	public static void main(String[] args) throws Exception {
		TicketNetworkDirectEventListingFetcher fetcher = new TicketNetworkDirectEventListingFetcher();
		fetcher.getEventList("giants", "", new Date(), new Date(new Date().getTime() + 365L * 24L * 3600L * 1000L),null,false);
		
		TicketNetworkDirectTicketListingFetcher tixFetcher = new TicketNetworkDirectTicketListingFetcher();
		List<TicketNetworkTicketGroup> groups = TicketNetworkWebServiceClient.getTicketNetworkTicketGroups(HttpClientStore.createHttpClient(), "1214218");
		for (TicketNetworkTicketGroup group: groups) {
			System.out.println("===> " + group);
		}
		
		for (TicketNetworkCategory cat: TicketNetworkWebServiceClient.getAllCategories(HttpClientStore.createHttpClient())) {
			System.out.println("CAT ===> " + cat);
		}
	}
}