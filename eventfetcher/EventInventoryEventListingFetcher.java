package com.admitone.tmat.eventfetcher;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import com.admitone.tmat.data.Site;

public class EventInventoryEventListingFetcher extends EventListingFetcher {
	private EventListingFetcher eiMarketPlaceEventListingFetcher;
	
	public Collection<EventHit> getEventList(String keywords, String location, Date fromDate, Date toDate) throws Exception{
		Collection<EventHit> eimpEventHits = new ArrayList<EventHit>();
		for (String keyword : keywords.split(" ")) {
			eimpEventHits.addAll(eiMarketPlaceEventListingFetcher.getEventList(keyword, location, fromDate, toDate,null,false));
		}
		/*if (eimpEventHits == null ) {
			return null;
		}*/
		if (eimpEventHits.isEmpty()) {
			return null;
		}
		
		Collection<EventHit> eventHits = new ArrayList<EventHit>();

		for(EventHit eventHit: eimpEventHits) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(eventHit.getDate());
			int year = calendar.get(Calendar.YEAR);
			int month = calendar.get(Calendar.MONTH) + 1;
			int day = calendar.get(Calendar.DAY_OF_MONTH);

			String url = "http://www.eventinventory.com/search/results.cfm?restart=yes&client=1337&e=4167&v=1157&s=1&month=" + month + "&day=" + day + "&year=" + year + "&p=" + eventHit.getEventId() + "&cfid=125924502&cftoken=1b577d5-a6a38e98-2780-4bd6-9fb9-54bbb1e42517&cfuser=C0F508B7-4DBC-4C47-972ED33A42D06ACF&RefList=#search_top";
			eventHits.add(new EventHit(eventHit.getEventId(), eventHit.getName(), eventHit.getDate(),
					eventHit.getTime(), eventHit.getLocation(), Site.EVENT_INVENTORY, url));
			
		}
		return eventHits;
	}
	
	public void setEiMarketPlaceEventListingFetcher(EventListingFetcher eiMarketPlaceEventListingFetcher) {
		this.eiMarketPlaceEventListingFetcher = eiMarketPlaceEventListingFetcher;
	}
}
