package com.admitone.tmat.eventfetcher;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.SeatWaveEvent;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.utils.QueryUtil;

/**
 * SeatWave Event Fetcher.
 * When doing a search on seatwave, we get the list of tours matching the query.
 * We need to fetch the tour pages to get the event lists for each tour.
 * Then for each event, we need to fetch the event page to get the time of the event.
 * 
 */
public class SeatWaveWebServicesEventListingFetcher extends EventListingFetcher {

	// private static final String authId = "00000000011";
	// private static Pattern datePattern = Pattern.compile("(\\d+)\\+");

	@Override
	public Collection<EventHit> getEventList(String keywords, String location, Date fromDate, Date toDate,List<Event> eventList,boolean isVenue) throws Exception{
		Collection<EventHit> eventHits = new ArrayList<EventHit>();
		
		Collection<SeatWaveEvent> events = DAORegistry.getSeatWaveEventDAO().getSeatWaveEvents(fromDate, toDate);
		for (SeatWaveEvent event: events) {
			String venueLocation = event.getVenueName() + ", " + event.getVenueTown() + ", " + event.getVenueCountryCode();

			if (keywords != null && keywords.trim().length() > 0 && !QueryUtil.matchAnyTerms(keywords, event.getName())) {
				continue;
			}

			if (!QueryUtil.matchAnyTerms(location, venueLocation)) {
				continue;
			}
			
			EventHit eventHit = new EventHit("" + event.getId(), 
					event.getName(),
					new java.sql.Date(event.getEventDate().getTime()),
					new java.sql.Time(event.getEventDate().getTime()),
					venueLocation,
					Site.SEATWAVE,
					event.getUrl());
			eventHits.add(eventHit);					
		}
		return eventHits;
/*
		SimpleHttpClient httpClient = null;			

		try {

			String searchURL = String.format("http://api.seatwave.com/Service.svc/v1/json/%1$s/names?term=%2$s&ent=1", 
					authId, keywords.replace(" ", "+"));		

			HttpGet httpGet = new HttpGet(searchURL);
			httpGet.addHeader("Host", "api.seatwave.com");
			httpGet.addHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
			httpGet.addHeader("Accept-Language", "en-us,en;q=0.5");
			httpGet.addHeader("Accept-Encoding", "gzip,deflate");
			httpGet.addHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");

			
			httpClient = HttpClientStore.createHttpClient();			

			//
			// FETCH SEASON(ARTIST) RESULT
			//

			HttpResponse response = httpClient.execute(httpGet);
			HttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());			
			String content = EntityUtils.toString(entity);

			JSONArray seasonJsonArray = new JSONArray(content);
			
			for(int i = 0 ; i < seasonJsonArray.length() ; i++) {
				JSONObject seasonJSON = seasonJsonArray.getJSONObject(i);
				
				int seasonID = seasonJSON.getInt("ID");
				String getPerformancesURL = String.format("http://api.seatwave.com/Service.svc/v1/json/%1$s/seasons/%2$s/performances?size=1000",
						authId, seasonID);
				String seasonName = seasonJSON.getString("Name");
				
				// check if seasonname matches the query
				if (!QueryUtil.matchAnyTerms(keywords, seasonName)) {
					continue;
				}

				httpGet = new HttpGet(getPerformancesURL);
				response = httpClient.execute(httpGet);
				entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());			
				content = EntityUtils.toString(entity);
								
				JSONArray performanceJsonArray = new JSONArray(content);
				
				for(int j = 0 ; j < performanceJsonArray.length() ; j++) {
					JSONObject performanceJSON = performanceJsonArray.getJSONObject(j);
					
					String performanceId = performanceJSON.getString("ID");
					
					String performanceUrl = performanceJSON.getString("URL");

					//Date(1264968000000+0000);
					String dateString = performanceJSON.getString("Date");
					
					Matcher dateMatcher = datePattern.matcher(dateString);
					dateMatcher.find();
					
					// the date is actually local but with an offset of -5
					Long dateInSeconds = Long.parseLong(dateMatcher.group(1)) + 5L * 3600000L;
					
					Date performanceDate = new Date(dateInSeconds);
					
					System.out.println("DATE=" + performanceDate);
					System.out.println("FROM DATE=" + fromDate);
					System.out.println("TO DATE=" + toDate);
					System.out.println();
										
					// check that the date is matching the query
					
					if (toDate.before(performanceDate) || fromDate.after(performanceDate)) {
						continue;
					}
					
					JSONObject venueJSON = performanceJSON.getJSONObject("venue");
					String venueName = venueJSON.getString("Name");
					String venueTown = venueJSON.getString("Town");
					String venueCountry = venueJSON.getString("Country");
					
					String venueLocation = venueName + ", " + venueTown + ", " + venueCountry;
								
					if (!QueryUtil.matchAnyTerms(location, venueLocation)) {
						continue;
					}
					
					EventHit eventHit = new EventHit(performanceId, 
							seasonName,
							new java.sql.Date(performanceDate.getTime()),
							new java.sql.Time(performanceDate.getTime()),
							venueLocation,
							Site.SEATWAVE,
							performanceUrl);
					eventHits.add(eventHit);					
				}

			}
			
			

		} finally {
			HttpClientStore.releaseHttpClient(httpClient);
		}
		return eventHits;
*/
	}

}