package com.admitone.tmat.eventfetcher;

import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.sql.*;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admitone.tmat.data.Site;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.UncompressedHttpEntity;

public class AOEventListingFetcher extends EventListingFetcher {
	private final Logger logger = LoggerFactory.getLogger(AOEventListingFetcher.class);
	private Pattern tourIdPattern = null;
	private Pattern parameterPattern=null;
	public java.util.Collection<EventHit> getEventList(String keywords, String location, java.util.Date fromDate, java.util.Date toDate) throws Exception {
		String eventId="",name="";
		Date date=null;
		Time time=null;
		String patternParameters[]=keywords.split("vs");
		
		String temp="";
		for(String keyword:patternParameters[0].split(" ")){
			temp+= keyword + " " ;   //toCamelCase(keyword)+" ";
		}
		String pattern="=\"(\\d++)\">(.*?)<";
		tourIdPattern = Pattern.compile(pattern);
		parameterPattern=Pattern.compile("cfid(.*?)=\"");
		HttpClient hc = HttpClientStore.createHttpClient(Site.AOP);
		Object obj=hc.getParams().getParameter(ConnRoutePNames.DEFAULT_PROXY);
		if(obj!=null){
			System.out.println("Proxy used to create AO Event crawl:" + obj.toString());
		}
		String finalUrl="";
		HttpPost hp = new HttpPost("http://www.eventinventory.com/search/byevent.cfm");
		NameValuePair nameValuePair1 = new BasicNameValuePair("restart", "Interface4");
		NameValuePair nameValuePair2= new BasicNameValuePair("next", "byevent.cfm");
		NameValuePair nameValuePair3 = new BasicNameValuePair("ref", "");
		NameValuePair nameValuePair4 = new BasicNameValuePair("id", "-1");
		NameValuePair nameValuePair5 = new BasicNameValuePair("client", "4583");
		NameValuePair nameValuePair6 = new BasicNameValuePair("k", keywords);
		
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(nameValuePair1);
		parameters.add(nameValuePair2);
		parameters.add(nameValuePair3);
		parameters.add(nameValuePair4);
		parameters.add(nameValuePair5);
		parameters.add(nameValuePair6);
		
		UrlEncodedFormEntity entity = new UrlEncodedFormEntity(parameters);
		hp.setEntity(entity);
		HttpResponse response = hc.execute(hp);
		UncompressedHttpEntity entityResult = HttpEntityHelper.getUncompressedEntity(response.getEntity());			
		String content = EntityUtils.toString(entityResult).replaceAll("\\s++", " ");
//		System.err.println(content);
		logger.info(content);
		Matcher tourIdMatcher = tourIdPattern.matcher(content);
		String stringTourId=null;
		Integer tourId=null;
		Collection<EventHit> eventHits = new ArrayList<EventHit>();
		DateFormat timeFormat = new SimpleDateFormat("HH:mm aa");
		boolean outerFalg= false;
		
		while(tourIdMatcher.find()){
//			System.err.println(tourIdMatcher.group(0).toLowerCase());
			String tempKeywords[] =temp.split(" ");
			int ii=0;
			while(ii<tempKeywords.length){
				if(!(tourIdMatcher.group(0).toLowerCase()).contains(tempKeywords[ii].trim().toLowerCase())){
					outerFalg=true;
					break;
				}
				ii++;
			}
			if(outerFalg){
				outerFalg=false;
				continue;
			}
			try{
//				System.out.println(tourIdMatcher.group(1));
				stringTourId=tourIdMatcher.group(1);
//				System.out.println(stringTourId);
				tourId=Integer.parseInt(stringTourId);
				if(tourId!=null){
					Matcher parameterMatcher = parameterPattern.matcher(content);
					if(parameterMatcher.find()){
//						System.out.println(parameterMatcher.group(0));
//						String url="http://www.eventinventory.com/search/pubsearch.cfm?" + parameterMatcher.group(0) + "&client=4583&id=10&RefList=&e="+tourId;
//						System.out.println(url);
						//cfid=191771745&cftoken=4ecece0-8cce2544-e12c-42e4-8f1b-9bb5b640295b&cfuser=659D2621-A558-42FA-9F37FF648B15DB3F&RefList="" +
						String cfid="",cftoken="",cfuser="";//,reflist="";
						for(String tempParam:parameterMatcher.group(0).split("&")){
							if(tempParam.contains("cfid")){
								cfid=tempParam.split("=")[1];
							}else if(tempParam.contains("cftoken")){
								cftoken =tempParam.split("=")[1];
							}else if(tempParam.contains("cfuser")){
								cfuser =tempParam.split("=")[1];
							}
						}
//						HttpClient hc1 = new DefaultHttpClient();
						HttpPost hp1 = new HttpPost("http://www.eventinventory.com/search/pubsearch.cfm");
						NameValuePair nameValuePair7 = new BasicNameValuePair("client", "4583");
						NameValuePair nameValuePair8= new BasicNameValuePair("id", "10");
						NameValuePair nameValuePair9 = new BasicNameValuePair("e", stringTourId);
						NameValuePair nameValuePair10 = new BasicNameValuePair("cfid", cfid);
						NameValuePair nameValuePair11 = new BasicNameValuePair("cftoken", cftoken);
						NameValuePair nameValuePair12 = new BasicNameValuePair("cfuser", cfuser);
						
						List<NameValuePair> parametersList = new ArrayList<NameValuePair>();
						parametersList.add(nameValuePair7);
						parametersList.add(nameValuePair8);
						parametersList.add(nameValuePair9);
						parametersList.add(nameValuePair10);
						parametersList.add(nameValuePair11);
						parametersList.add(nameValuePair12);
						UrlEncodedFormEntity entityTickets = new UrlEncodedFormEntity(parametersList);
						hp1.setEntity(entityTickets);
						HttpResponse response1 = hc.execute(hp1);
						UncompressedHttpEntity entityTicketsResult = HttpEntityHelper.getUncompressedEntity(response1.getEntity());			
						String content1 = EntityUtils.toString(entityTicketsResult).replaceAll("\\s++", " ");
//						System.err.println(content1);
//						String abc="<table width=\"100%\" cellspacing=\"1\" aa table>";
						Pattern tablePattern= Pattern.compile("<table width=\"100%\" cellspacing=\"1\"(.*?)table>");
//						String abc="<table>aa<\\table>";
//						Pattern eventPattern= Pattern.compile("<table(.*?)table>");
						
						if(content1.contains("Please SELECT A THEATER from the list below")){
							Pattern venuePattern = Pattern.compile("href(.*?)font>");
							if(location==null){
								location="location not specified..";
							}
							boolean found=false;
							Matcher venueMatcher=venuePattern.matcher(content1);
							while(venueMatcher.find()){
								if((venueMatcher.group(0).toLowerCase()).contains(location.toLowerCase())){
									List<NameValuePair> pList = new ArrayList<NameValuePair>();
									NameValuePair nvp;
									HttpPost hp2 = new HttpPost("http://www.eventinventory.com/search/pubsearch.cfm");
									Pattern urlPattern= Pattern.compile("\\?(.*?)\"");
									Matcher urlMatcher=urlPattern.matcher(venueMatcher.group());
									while(urlMatcher.find()){
										String[] params=urlMatcher.group(1).split("&");
										for(String param:params){
											String[] tempParam=param.split("=");
											if(tempParam.length==2){
												nvp = new BasicNameValuePair(tempParam[0], tempParam[1]);
											}else{
												nvp = new BasicNameValuePair(tempParam[0], "");
											}
											pList.add(nvp);
										}
										UrlEncodedFormEntity urlEntity = new UrlEncodedFormEntity(pList);
										hp2.setEntity(urlEntity);
										HttpResponse response2 = hc.execute(hp2);
										UncompressedHttpEntity urlResult = HttpEntityHelper.getUncompressedEntity(response2.getEntity());			
										content1 = EntityUtils.toString(urlResult).replaceAll("\\s++", " ");
										found=true;
										break;
									}
								}
								if(found){
									break;
								}
							}
						}
						Matcher tableMatcher=tablePattern.matcher(content1);
//						System.out.println(content1);
						if(tableMatcher.find()){
							System.out.println( tableMatcher.group());
							Pattern rowPattern= Pattern.compile("<tr(.*?)tr>");
							Matcher rowMatcher=rowPattern.matcher(tableMatcher.group());
							DateFormat df = new SimpleDateFormat("MMMMMM d, yyyy");
							String dff = df.format(toDate);
							
							while(rowMatcher.find()){
//								System.out.println(rowMatcher.group());
								if (rowMatcher.group().contains(dff)){
//									System.err.println(rowMatcher.group());
									Pattern urlPattern= Pattern.compile("href=\"(.*?)\"");
									Matcher urlMatcher=urlPattern.matcher(rowMatcher.group());
									if(urlMatcher.find()){
//										System.out.println("ULalalla..we found it:" + urlMatcher.group());
										finalUrl= "http://www.eventinventory.com"+urlMatcher.group().replaceAll("href=\"", "");
										eventId="";
										String[] paramPair = urlMatcher.group().split("&");
										for(String param:paramPair){
											if(param.contains("e=")){
												eventId=param.split("=")[1];
												break;
											}
										}
										Pattern timePattern = Pattern.compile("\\d++:\\d++ [AMP]{2}");
										Matcher timeMatcher = timePattern.matcher(rowMatcher.group());
										
//										time=new Time(timeFormat.parse("Sunday 12:00 AM").getTime());
										if(timeMatcher.find()){
//											System.out.println(timeMatcher.group());
											time=new Time(timeFormat.parse(timeMatcher.group()).getTime());
										}
										Pattern columnPattern = Pattern.compile("<td(.*?)td>");
										Matcher columnMatcher = columnPattern.matcher(rowMatcher.group());
//										System.out.println(rowMatcher.group());
										int i=0;
										while(columnMatcher.find()){
											if(i++<2){
												continue;
											}
//											System.out.println(columnMatcher.group(1)); 
											Pattern locationPattern = Pattern.compile("fineprint\">(.*?)<");
											Pattern locationPattern2 = Pattern.compile("fieldinfo\"><strong>(.*?)<");
											Matcher locationMatcher = locationPattern.matcher(columnMatcher.group(1));
											Matcher locationMatcher2 = locationPattern2.matcher(columnMatcher.group(1));
											location="";
											if(locationMatcher2.find()){
												location = locationMatcher2.group().replaceAll("fieldinfo", "").replaceAll("strong", "").replaceAll("[\"<>]", "");
											}
											if(locationMatcher.find()){
												location+= " " + locationMatcher.group().replaceAll("fineprint", "").replaceAll("[\"<>]", "");
											}
											
											break;
										}
//										if(!finalUrl.equals("")){
											name = keywords + " Tickets";
											date=new Date(toDate.getTime());
											if(location==null){
												location="";
											}
											EventHit eventHit = new EventHit(eventId, name, date, time, location, Site.AOP, finalUrl);
											eventHits.add(eventHit);
//										}
//										break;
									}
								}
							}
							
						}
						
					}
				}
			}catch(Exception ex){
				System.out.println(ex.fillInStackTrace());
			}
			/*if(!finalUrl.equals("")){
				name = keywords + " Tickets";
				date=new Date(toDate.getTime());
				if(location==null){
					location="";
				}
				EventHit eventHit = new EventHit(eventId, name, date, time, location, Site.AOP, finalUrl);
				eventHits.add(eventHit);
			}*/
		}
		
		
		return eventHits;
		
	};
	public String toCamelCase(String keyword)
	{
//	    String[] words = xmlMethodName.split("-"); // split on "-"
	    StringBuilder keywordBuilder = new StringBuilder(keyword.length());
//	    nameBuilder.append(words[0]);
	    	keywordBuilder.append(keyword.substring(0, 1).toUpperCase());
	    	keywordBuilder.append(keyword.substring(1));
	    return keywordBuilder.toString(); // join
	}

	public static void main(String[] args) throws Exception {
		AOEventListingFetcher aoEventListingFetcher = new AOEventListingFetcher();
		System.out.println(aoEventListingFetcher.getEventList("The book of mormon", "eugene", new Date(111, 7, 11),new Date(111, 7, 11)));
	}
}
