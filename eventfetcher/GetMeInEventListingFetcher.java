package com.admitone.tmat.eventfetcher;

import java.net.URLEncoder;
import java.util.Date;
import java.text.DateFormat;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;

import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Site;

import com.admitone.tmat.utils.QueryUtil;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;

/**
 * GetMeIn Event Listing Fetcher.
 */
public class GetMeInEventListingFetcher extends EventListingFetcher {	
	
	private Pattern eventIdPattern = Pattern.compile("-([0-9]+)\\.html");
	
	private Pattern eventTargetPattern = Pattern.compile("id=\"__EVENTTARGET\" value=\"(.*?)\"");
	private Pattern eventArgumentPattern = Pattern.compile("id=\"__EVENTARGUMENT\" value=\"(.*?)\"");
	private Pattern viewStatePattern = Pattern.compile("id=\"__VIEWSTATE\" value=\"(.*?)\"");
	private Pattern eventValidationPattern = Pattern.compile("id=\"__EVENTVALIDATION\" value=\"(.*?)\"");

	private Pattern pageCountPattern = Pattern.compile("<span id=\"ctl00_centerContent_eventsBox_lblPageCount\">.*?Page 1 of (\\d*?)</span>");
	
	private Pattern eventNamePattern = Pattern.compile("^(.*?)Tickets$");
	
	//                                                                                       URL  TOUR NAME                                   TOUR DATES 
	private Pattern tourResultPattern = Pattern.compile("<div class=\"result\">.*?<a href='(.*?)'>(.*?)</a>.*?</div>.*?<div.*?</div>.*?<div.*?>(.*?)</div>", Pattern.DOTALL);
	
	// for the date, we can have: Sat, 15 May 2010
	// or Sun, 9 May 2010<br />12:00          
	//                                                                                       EVENT DATE                    EVENT URL    EVENT NAME                         VENUE NAME       CITY
	private Pattern eventResultPattern = Pattern.compile("<tr class=\"listingrow\">.*?<td.*?>(.*?)</td>.*?<td.*?>.*?<a href=\"(.*?)\".*?>(.*?)</a>.*?</td>.*?<td.*?<a.*?>(.*?)</a>.*?<a.*?>(.*?)</a>.*?</td>.*?</tr>", Pattern.DOTALL);

	// 30 Jun 2009
	private DateFormat tourDateFormat = new SimpleDateFormat("dd MMM yyyy");

	@Override
	public Collection<EventHit> getEventList(String keywords, String location, Date fromDate, Date toDate,List<Event> eventList,boolean isVenue) throws Exception{
		SimpleHttpClient httpClient = null;
	

		try {
			httpClient = HttpClientStore.createHttpClient();

			if (keywords == null || keywords.trim().length() == 0) {
				return getEventHitsByDate(httpClient, location, fromDate, toDate);
			}
			keywords = keywords.replaceAll(" +", " ");
			
	
			// map to remove duplicate
			Map<String, EventHit> eventHitMap = new HashMap<String, EventHit>();
			Collection<EventHit> eventHits = new ArrayList<EventHit>();
			Collection<String> eventIds = new ArrayList<String>();
			
			for(String keyword : keywords.split(" ")) {		
			HttpGet httpGet = new HttpGet("http://www.getmein.com/search.html");			
			httpGet.addHeader("Host", "www.getmein.com");		
			httpGet.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
			httpGet.addHeader("Accept-Encoding", "gzip,deflate");

			HttpResponse response = httpClient.execute(httpGet);
			HttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
			
			String content = EntityUtils.toString(entity);
			
			Matcher eventTargetMatcher = eventTargetPattern.matcher(content);
			eventTargetMatcher.find();
			String eventTarget = eventTargetMatcher.group(1);

			Matcher eventArgumentMatcher = eventArgumentPattern.matcher(content);
			eventArgumentMatcher.find();
			String eventArgument = eventArgumentMatcher.group(1);

			Matcher viewStateMatcher = viewStatePattern.matcher(content);
			viewStateMatcher.find();
			String viewState = viewStateMatcher.group(1);

			Matcher eventValidationMatcher = eventValidationPattern.matcher(content);
			eventValidationMatcher.find();
			String eventValidation = eventValidationMatcher.group(1);

			HttpPost httpPost = new HttpPost("http://www.getmein.com/search.html");			
			httpPost.addHeader("Host", "www.getmein.com");		
			httpPost.addHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
			httpPost.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
			httpPost.addHeader("Accept-Language", "en-us,en;q=0.5");
			httpPost.addHeader("Accept-Encoding", "gzip,deflate");
			httpPost.addHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
			httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");			
			httpPost.addHeader("Referer", "http://www.getmein.com/search.html");

			String postData = "__EVENTTARGET=" + eventTarget
				+ "&__EVENTARGUMENT=" + URLEncoder.encode(eventArgument, "ISO-8859-1")
				+ "&__VIEWSTATE=" + URLEncoder.encode(viewState, "ISO-8859-1")
				+ "&ctl00%24ucHeader%24txtSearch="
				+ "&ctl00%24centerContent%24AdvSearchBox%24txtKeywords=" + keyword
				+ "&ctl00%24centerContent%24AdvSearchBox%24cboCategories=0"
				+ "&ctl00%24centerContent%24AdvSearchBox%24A=optAllEvents"
				+ "&ctl00%24centerContent%24AdvSearchBox%24btnShowEvents.x=0"
				+ "&ctl00%24centerContent%24AdvSearchBox%24btnShowEvents.y=0"
				+ "&__EVENTVALIDATION=" + URLEncoder.encode(eventValidation, "ISO-8859-1");
						
			httpPost.setEntity(new StringEntity(postData));
			
			response = httpClient.execute(httpPost);
			entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());		
			
			
			String tourContent = EntityUtils.toString(entity);
			
			Matcher tourResultMatcher = tourResultPattern.matcher(tourContent);
			
			while (tourResultMatcher.find()) {
				String tourUrl = tourResultMatcher.group(1);
				String tourName = tourResultMatcher.group(2);
				String tourDateString = tourResultMatcher.group(3);
				
				if (tourDateString.contains("-")) {
					String[] dateTokens = tourDateString.split("-");
					Date startDate = tourDateFormat.parse(dateTokens[0]);
					Date endDate = tourDateFormat.parse(dateTokens[1]);
					if (toDate.before(startDate) || fromDate.after(toDate)) {
						continue;
					}
				} else {
					Date date = tourDateFormat.parse(tourDateString);
					if (toDate.before(date) || fromDate.after(date)) {
						continue;
					}					
				}
				
				Collection<EventHit> fetchedEventHits = getEventHits(httpClient, tourUrl);
				for(EventHit eventHit: fetchedEventHits) {
					if (!QueryUtil.matchAnyTerms(location, eventHit.getLocation())) {
						continue;
					}
					
					if (eventHitMap.containsKey(eventHit.getEventId())) {
						continue;						
					}
					
					Date eventDate = new Date(eventHit.getDate().getTime());
					eventDate.setHours(eventHit.getTime().getHours());
					eventDate.setMinutes(eventHit.getTime().getMinutes());
					eventDate.setSeconds(eventHit.getTime().getSeconds());
					System.out.println("EVENT DATE=" + eventDate);

					if (toDate.before(eventDate) || fromDate.after(eventDate)) {
						continue;
					}
					if(!eventIds.contains(eventHit.getEventId())) {
						eventHitMap.put(eventHit.getEventId(), eventHit);
						eventHits.add(eventHit);
						eventIds.add(eventHit.getEventId());
					}
				}
			}
		}
			return eventHits;
			
		} finally {
			if (httpClient != null) {
				HttpClientStore.releaseHttpClient(httpClient);
			}
		}
	}

	private Collection<EventHit> getEventHitsByDate(SimpleHttpClient httpClient, String location, Date fromDate, Date toDate) throws Exception {
		DateFormat eventDateFormat = new SimpleDateFormat("dd-MM-yyyy");
		
		Date fromDate0 = new Date(fromDate.getTime());
		fromDate0.setHours(0);
		fromDate0.setMinutes(0);
		fromDate0.setSeconds(0);
		
		Date toDate0 = new Date(toDate.getTime());
		toDate0.setHours(1);
		toDate0.setMinutes(0);
		toDate0.setSeconds(0);
		
		Map<String, EventHit> eventHitMap = new HashMap<String, EventHit>();
		Collection<EventHit> eventHits = new ArrayList<EventHit>();
		
		for(long time = fromDate0.getTime() ; time < toDate0.getTime() ; time += 24L * 60L * 60L * 1000L) {
			String url = "/calendar/" + eventDateFormat.format(new Date(time)) + ".html";
			System.out.println("URL=" + url);
			Collection<EventHit> fetchedEventHits = getEventHits(httpClient, url);
			
			for(EventHit eventHit: fetchedEventHits) {
				
				if (!QueryUtil.matchAnyTerms(location, eventHit.getLocation())) {
					continue;
				}
				
				if (eventHitMap.containsKey(eventHit.getEventId())) {
					continue;
				}
				eventHitMap.put(eventHit.getEventId(), eventHit);
				eventHits.add(eventHit);
			}
		}
		
		return eventHits;
	}
	
	private Collection<EventHit> getEventHits(SimpleHttpClient httpClient, String url) throws Exception {
		Collection<EventHit> eventHits = new ArrayList<EventHit>();
		
		int page = 0;
		int pageCount = 1;
				
		do {
			// fetch tour page
			String listingUrl;
			if (page == 0) {
				listingUrl = "http://www.getmein.com" + url;
			} else {
				listingUrl = "http://www.getmein.com" + url + "?eventsBoxevpage=" + page + "&evfilter=-1&evsort=";
			}
			
			HttpGet httpGet = new HttpGet(listingUrl);		
			System.out.println("URL=" + listingUrl);
			httpGet.addHeader("Host", "www.getmein.com");		
			httpGet.addHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
			httpGet.addHeader("Accept-Language", "en-us,en;q=0.5");
			httpGet.addHeader("Accept-Encoding", "gzip,deflate");
			httpGet.addHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
			httpGet.addHeader("Referer", "http://www.getmein.com/search.html");
			
			HttpResponse response = httpClient.execute(httpGet);
			HttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
					
			String eventContent = EntityUtils.toString(entity);
			
			if (page == 0) {
				Matcher pageCountMatcher = pageCountPattern.matcher(eventContent);
				if (pageCountMatcher.find()) {
					pageCount = Integer.parseInt(pageCountMatcher.group(1));
				} else {
					pageCount = 1;			
				}
				System.out.println("PAGE COUNT=" + pageCount);
			}
			
			
			// System.out.println("EVENT CONTENT=" + eventContent);
		
			// Fri, 14 Aug 2009 19:00
			DateFormat eventDateFormat1 = new SimpleDateFormat("EEE, dd MMM yyyy");
			
			// Fri, 14 Aug 2009
			DateFormat eventDateFormat2 = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm");
	
			Matcher eventResultMatcher = eventResultPattern.matcher(eventContent);
			
			while(eventResultMatcher.find()) {
	
				String eventDateString = eventResultMatcher.group(1);
				String eventUrl = "http://www.getmein.com" + eventResultMatcher.group(2);
				String eventName = eventResultMatcher.group(3);
				Matcher eventNameMatcher = eventNamePattern.matcher(eventName);
				eventNameMatcher.find();
				eventName = eventNameMatcher.group(1);
				
				String venueName = eventResultMatcher.group(4);
				String venueCity = eventResultMatcher.group(5);
				
				// System.out.println("EVENT DATE=" + eventDateString + "URL=" + eventUrl + " / " + eventName + " / " + venueName + " / " + venueCity);
	
				String[] eventDateTokens = eventDateString.split("<br />");
				
				Date eventDate;
				if (eventDateTokens.length == 1) {
					eventDateString = eventDateTokens[0];
					eventDate = eventDateFormat1.parse(eventDateString);
				} else {
					eventDateString = eventDateTokens[0] + " " + eventDateTokens[1];												
					eventDate = eventDateFormat2.parse(eventDateString);
				}
	
	
				Matcher eventIdMatcher = eventIdPattern.matcher(eventUrl);
				eventIdMatcher.find();
				
				String eventId = eventIdMatcher.group(1);
	
				
				String eventLocation = venueName + " - " + venueCity;

				
				EventHit eventHit = new EventHit(eventId, 
						eventName,
						new java.sql.Date(eventDate.getTime()),
						new java.sql.Time(eventDate.getTime()),
						eventLocation,
						Site.GET_ME_IN,
						eventUrl);
				eventHits.add(eventHit);
			}
			page++;
		} while (page < pageCount);
		
		return eventHits;
	}


}
