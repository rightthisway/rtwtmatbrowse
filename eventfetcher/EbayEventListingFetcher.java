package com.admitone.tmat.eventfetcher;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Site;

/**
 * Ebay Event Fetcher.
 */

public class EbayEventListingFetcher extends EventListingFetcher {
	//private final static String ebayListUrlWithDate = "http://tickets.shop.ebay.com/items/Tickets__%KEYWORDS%?_catref=1&_dmpt=US_Tickets_all_in_one&_fln=1&_sacat=1305&_ssov=1&_trksid=p3286.c0.m282&_rnglo_Event%2520Date=%MONTH%%2F%DAY%%2F%YEAR%&_rnghi_Event%2520Date=%MONTH%%2F%DAY%%2F%YEAR%&_pgn=%PAGE%";
	private final static String ebayListUrlWithDate = "http://tickets.shop.ebay.com/Tickets-/1305/i.html?_nkw=%KEYWORDS%&_dmpt=US_Tickets_all_in_one&_trksid=p3286.c0.m282&_sacat=1305";
	private final static Pattern ticketCountPattern = Pattern.compile("class='countClass'>(\\d+)</span"); 
	private Logger logger = LoggerFactory.getLogger(EbayEventListingFetcher.class);
	
	public Collection<EventHit> getEventList(String keywords, String location, Date fromDate, Date toDate) throws Exception {
		Collection<EventHit> eventHits = new ArrayList<EventHit>();
		String query = keywords.trim().replaceAll(" ", "+");

		Calendar cal = new GregorianCalendar();
		cal.setTime(fromDate);
		HttpClient httpClient = new DefaultHttpClient();
		while (cal.getTime().getTime() < toDate.getTime()) {
			String url = ebayListUrlWithDate;
			url = url.replaceAll("%YEAR%", cal.get(Calendar.YEAR) + "");
			url = url.replaceAll("%MONTH%", (cal.get(Calendar.MONTH) + 1) + "");
			url = url.replaceAll("%DAY%", cal.get(Calendar.DAY_OF_MONTH) + "");
			url = url.replaceAll("%PAGE%", "1");
			
			url = url.replaceAll("%KEYWORDS%", query);
			HttpGet httpGet = new HttpGet(url); 
			httpGet.addHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
			try {
				for(String keyword : query.split(" ")){
					String response = httpClient.execute(httpGet, new BasicResponseHandler());
					Matcher matcher = ticketCountPattern.matcher(response);
					if (!matcher.find()) {
						continue;
					}
					
					Integer ticketCount = Integer.parseInt(matcher.group(1));
					if (ticketCount == 0) {
						continue;
					}
				
					EventHit eventHit = new EventHit(keyword, keyword + " (" + ticketCount + " entries) [auto-generated]", new java.sql.Date(cal.getTime().getTime()), null, "", Site.EBAY, url);
					eventHit.setShortName(keyword);
					String extraParameters = "queryString=" + keyword
										+ "\nqueryYear=" + cal.get(Calendar.YEAR)
										+ "\nqueryMonth=" + (cal.get(Calendar.MONTH) + 1)
										+ "\nqueryDay=" + cal.get(Calendar.DATE) + "\n";

					eventHit.setUrl(url);
					eventHit.setExtraParameters(extraParameters);
					eventHits.add(eventHit);
				}
			} catch (Exception e) {
				logger.warn("Error while trying to get event list", e);
			} finally {
				cal.add(Calendar.DATE, 1);
			}
		}

		return eventHits;
	}
}
