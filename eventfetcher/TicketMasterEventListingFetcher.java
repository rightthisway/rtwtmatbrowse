package com.admitone.tmat.eventfetcher;

import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.utils.QueryUtil;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;

/**
 * Ticket Master Event Listing Fetcher. 
 * https://teamexchange.ticketmaster.com
 * As Ticket Master do not provide a search feature, we have to crawl all the event listing, put them into a cache.
 * Then whenever we search an event, we look into the cache for events matching the query.
 * 
 * We refresh the event cache every day.
 */
public class TicketMasterEventListingFetcher extends EventListingFetcher {
	private Logger logger = LoggerFactory.getLogger(TicketMasterEventListingFetcher.class);
	
	//<a href="https://teamexchange.ticketmaster.com/html/outsider.htmI?CAMEFROM=TM_SPORTSTE_HAWKS&GOTO=https%3A%2F%2Fteamexchange.ticketmaster.com%2Fhtml%2Feventlist.htmI%3Fl%3DEN%26team%3Dhawks">Atlanta Hawks</a>
	private Pattern tourListLink = Pattern.compile("https://teamexchange.ticketmaster.com/html/outsider.htmI.*?GOTO=(.*?)\">(.*)</a>");

	// <table border="0" cellpadding="0" cellspacing="0" width="100%" class="tableListing" id="tableListing">
	private Pattern eventTablePattern = Pattern.compile("<table.*?id=\"tableListing\".*?>(.*?)</table>", Pattern.DOTALL);
	
    // <tr>
    //   <td class="left">Tue, September 8, 2009<br> 8:00 PM</td>
	//   <td class="center"><span class="tableListing-act">THE PHANTOM OF THE OPERA</a></td>
    //   <td class="center"><span class="tableListing-venue"><a href="JAVASCRIPT:newVenueWin2('/teamimages/fisher/14343.gif',800,550)" id="venue_Opera House">Opera House</a></span></td>
    //   <td class="right">
	//     <a href="https://teamexchange.ticketmaster.com/html/seatlist.htmI?l=EN&team=fisher&EVNT=EDO0908&CNTX=" style="display: inline;" id="buy_EDO0908"><!--ID TU_Exchange0013 EN ESC-->Buy<!--/ID TU_Exchange0013 EN--></a> |                                                                                                                                                     <a href="https://oss.ticketmaster.com/html/credentials.php?l=EN&team=fisher&EVNT=EDO0908&cf=TEXSELL" style="display: inline;" id="sell"><!--ID TU_Exchange0014 EN ESC-->Sell<!--/ID TU_Exchange0014 EN--></a>
    //   </td>
    // </tr>
	
	//                                                                                          DATE                    EVENT NAME (buggy a closing tag)          VENUE                                              EVENT LINK
	private Pattern eventPattern = Pattern.compile("<tr>.*?<td.*?>(.*?)</td>.*?<td.*?><span.*?>(.*?)</a>.*?</td>.*?<td.*?>(.*?)</td>.*?<td.*?>.*?<a href=\"(.*?)\".*?</td>.*?</tr>", Pattern.DOTALL);
	
	// https://teamexchange.ticketmaster.com/html/seatlist.htmI?l=EN&EVNT=H090717E&team=dsm
	private Pattern eventIdPattern = Pattern.compile("EVNT=(.*?)&");

	private Pattern venueInTagPattern = Pattern.compile(">([^><]*)</");

	private Date lastCacheUpdate = null;
	private Collection<EventHit> cachedEventHits = null;
	
	public TicketMasterEventListingFetcher() {
		// IMPORTANT: commented the prefetch of events as TicketMaster is not in use now.
		/*
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				refreshCachedEventHits();
			}			
		}, 0);
		*/
	}

	/**
	 * Crawl all the event listing page and put the cached event in cachedEventHits. 
	 */
	private synchronized void refreshCachedEventHits() {
		if (lastCacheUpdate != null && (lastCacheUpdate.getTime() - new Date().getTime() < 24L * 60L * 60L * 1000L)) {
			return;
		}
		
		Collection<EventHit> newCachedEventHits = new ArrayList<EventHit>();
		
		SimpleHttpClient httpClient = null;		

		try {
			httpClient = HttpClientStore.createHttpClient();
			
			HttpGet httpGet = new HttpGet("https://teamexchange.ticketmaster.com/html/tmt_home.htmI?l=EN");
			HttpResponse response = httpClient.execute(httpGet);
			String content = EntityUtils.toString(HttpEntityHelper.getUncompressedEntity(response.getEntity()));
			
			Matcher tourListLinkMatcher = tourListLink.matcher(content);
			int tourCount = 0;
			while(tourListLinkMatcher.find()) {
				tourCount++;
				String eventListLinkHref = URLDecoder.decode(tourListLinkMatcher.group(1), "ISO-8859-1");
				// System.out.println("LINK=" + eventListLinkHref);
				
				// only consider link to event listing (not to ticket listing)
				if (!eventListLinkHref.contains("eventlist.htmI")) {
					continue;
				}
				
				String tourName = tourListLinkMatcher.group(2);
				// System.out.println("TOUR NAME=" + tourName);
				
				// try 3 times to fetch the page
				int retryCount = 0;
				content = null;
				while(retryCount < 3) {
					try {
						httpGet = new HttpGet(eventListLinkHref + "&showAll=1");
						response = httpClient.execute(httpGet);
						content = EntityUtils.toString(HttpEntityHelper.getUncompressedEntity(response.getEntity()));
						break;
					} catch (Exception e) {
					//	e.printStackTrace();
					}
					retryCount++;
				}
				if (content == null) {
					continue;
				}

				Matcher eventTableMatcher = eventTablePattern.matcher(content);
				if (!eventTableMatcher.find()) {
					// sometimes, we can have a message like:
					// An unusually large number of visitors are accessing our site. You've been placed in line
					// and will be granted access to the site in the order in which you arrived. 
					continue;
				}

				String eventTable = eventTableMatcher.group(1);
				
				Matcher eventMatcher = eventPattern.matcher(eventTable);
				
				while(eventMatcher.find()) {
					
					String eventDateString = eventMatcher.group(1);

					// can either be
					// Sun, October 18, 2009<br> 7:00 PM
					// or Sun, October 18, 2009
					String[] eventDateTokens = eventDateString.split("<br>");
					
					String normalizedEventDate;
					
					if (eventDateTokens.length == 1) {
						normalizedEventDate = eventDateTokens[0].trim() + " " + "12:00 am";						
					} else {
						normalizedEventDate = eventDateTokens[0].trim() + " " + eventDateTokens[1].trim();
					}
					SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, MMMMMMMMM dd, yyyy kk:mm aa");
					
					Date eventDate = null;
					try {
						eventDate = dateFormat.parse(normalizedEventDate);
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					// System.out.println("EVENT DATE STRING=" + eventDate);
					
					
					String eventName = eventMatcher.group(2);
					// System.out.println("EVENT NAME=" + eventName);

					String venueName = eventMatcher.group(3);
					// the venue can either be in an anchor/span tag or not
					Matcher venueInAnchorMatcher = venueInTagPattern.matcher(venueName);
					if (venueInAnchorMatcher.find()) {
						venueName = venueInAnchorMatcher.group(1);
					}					

					// System.out.println("VENUE NAME=" + venueName);

					String eventUrl = eventMatcher.group(4);
					// System.out.println("EVENT URL=" + eventUrl);
					
					Matcher eventIdMatcher = eventIdPattern.matcher(eventUrl);
					eventIdMatcher.find();
					String eventId = eventIdMatcher.group(1);

					// System.out.println("EVENT ID=" + eventId);

					EventHit eventHit = new EventHit(eventId, tourName + " - " + eventName, new java.sql.Date(eventDate.getTime()), new java.sql.Time(eventDate.getTime()), venueName, Site.TICKET_MASTER, eventUrl);
					newCachedEventHits.add(eventHit);
				}
			}
			// System.out.println("TOUR COUNT=" + tourCount);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			HttpClientStore.releaseHttpClient(httpClient);
		}
		
		cachedEventHits = newCachedEventHits;
		lastCacheUpdate = new Date();
	}
	
	private Collection<EventHit> getCachedEventHits() {
		if (cachedEventHits == null) {
			refreshCachedEventHits();
		}
		return cachedEventHits;
	}
	
	@Override
	public Collection<EventHit> getEventList(String keywords, String location, Date fromDate, Date toDate,List<Event> eventList,boolean isVenue) throws Exception {
		Collection<EventHit> eventHits = getCachedEventHits();
		
		Collection<EventHit> eventHitsResult = new ArrayList<EventHit>();
		
		for(EventHit eventHit: eventHits) {
			// remove this after the testing is done
			if (QueryUtil.matchAnyTerms(keywords, eventHit.getName())
					&& eventHit.getDate().after(fromDate) && eventHit.getDate().before(toDate)
					&& QueryUtil.matchAnyTerms(location, eventHit.getLocation())) {
				eventHitsResult.add(eventHit);
			}
		}
		
		return eventHitsResult;
	}	
}
