package com.admitone.tmat.eventfetcher;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.utils.TextUtil;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.http.util.EntityUtils;
import org.apache.commons.codec.binary.Base64;



/**
 * TicketEvolution Event Fetcher.
 * When doing a search on TicketEvolution, we get a page with the list of artist, then we get a page
 * with the list of venue and finally a page with the list of date.
 * 
 *  However if in one page, there is only one choice, we are automatically redirect to the next page.
 *  (can be redirected up to the event ticket listing page).
 *  
 * when doing a post, set the header with the following value:
 * 			httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");
 */
public class TicketEvolutionEventListingFetcher extends EventListingFetcher {
	private  String token="c517a07c3875093fed0c789df1aa0460";
	private String secrete="5nSPDnk4chgthfVsQDNO9vUqL+JFG3rjfqk+G5ME";
	
	@Override
	public Collection<EventHit> getEventList(String keywords, String location, Date fromDate, Date toDate,List<Event> eventList,boolean isVenue) throws Exception {
//		Collection<EventHit> eventHits = new ArrayList<EventHit>();	
		Collection<EventHit> eventHitsLocal = new ArrayList<EventHit>();
//		Collection<EventHit> tempEventHits = new ArrayList<EventHit>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String baseURL="https://api.ticketevolution.com/v9";
		ArrayList<String> performerIds = new ArrayList<String>();
		String perfomerList[] = keywords.split("vs");
		String performer="";
		
		if(perfomerList!=null && perfomerList.length==2){
			performer=perfomerList[0].trim();
		}else{
			performer=keywords;
		}
		if(keywords == null || keywords.isEmpty()) {
		} else {			
			HttpClient httpClient = HttpClientStore.createHttpClient(Site.TICKET_EVOLUTION);
			String finalContent = "";
//			String performerConent="";
//			String perfomerUrl = "https://api.ticketevolution.com/performers/search?q=";//uMJlA4oZxQWvPeBxNQiLX/xHl6U5o2veejd1b1mSt5c=
//			StringBuffer resuBuffer = new StringBuffer();
			String originalKeywords = keywords.replaceAll("\\+", " ").replaceAll("\\s+", " ");
			keywords= keywords.trim().toLowerCase().replaceAll("\\s+", "\\+");
			
			
			/****************************/
			Map<String,Boolean> dateMap = new HashMap<String, Boolean>();
			Map<String,Boolean> locationMap = new HashMap<String, Boolean>();
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			for(Event event:eventList){
				if(event.getDate()==null){
					dateMap.put("TBD", true);
					locationMap.put(event.getVenue().getBuilding().replaceAll("\\s+", " ") + ":-:" + event.getVenue().getCity().replaceAll("\\s+", " ") + ":-:" + event.getVenue().getState().replaceAll("\\s+", " ") , true);
					continue;
				}
				dateMap.put(dateFormat.format(event.getDate()), true);
			}
//			String url="";
//			JSONArray finalEventArray = new JSONArray();
			if(isVenue){
				String venueUrl=baseURL + "/venues/search?q="+keywords;
				String venues = hitTicketEvolution(httpClient,venueUrl);
				System.out.println(venues);
				JSONObject venueObj= new JSONObject(venues);
				JSONArray venueArray = venueObj.getJSONArray("venues");
				List<Integer> venueIdsList = new ArrayList<Integer>();
				if(venueArray != null && venueArray.length()>0){
					for(int index=0;index<venueArray.length();index++){
						JSONObject obj = venueArray.getJSONObject(index);
						String name = obj.getString("name");
						System.out.println(name);
						if((originalKeywords.toUpperCase().trim().equals(name.toUpperCase().trim()))){
							venueIdsList.clear();
							venueIdsList.add(obj.getInt("id"));
							break;
						}
						if(isMatch(removeIgnoreWordsList(keywords),name) || (name.toUpperCase().trim().contains(originalKeywords.toUpperCase().trim()))){
							if(!venueIdsList.contains(obj.getInt("id"))){
								venueIdsList.add(obj.getInt("id"));		
							}
						}
					}
				}
//				venueIdsList.add(5725);
//				venueIdsList.add(896);
				
//				String finalResult="";
				for(Integer id:venueIdsList){
					String eventUrl=baseURL + "/events?per_page=500&venue_id="+ id;
					String events = hitTicketEvolution(httpClient,eventUrl);
					JSONObject eventObj= new JSONObject(events);
					Collection<EventHit> hits=null;
					System.out.println(eventObj.get("total_entries"));
					System.out.println(eventObj.get("per_page"));
					int totalEvent = eventObj.getInt("total_entries")/500 + 1;
					hits = getEventsFromJSON(eventObj,sdf,baseURL,location,eventList);
					eventHitsLocal.addAll(hits);
					//if total events are more than 100 fetch regcords by page no
					for(int i=2;i<totalEvent;i++){
						eventUrl=baseURL + "/events?per_page=500&venue_id="+ id + "&page=" + i;
						events = hitTicketEvolution(httpClient,eventUrl);
						eventObj= new JSONObject(events);
						hits=null;
						System.out.println(eventObj.get("total_entries"));
						hits = getEventsFromJSON(eventObj,sdf,baseURL,location,eventList);
						eventHitsLocal.addAll(hits);
					}
//					JSONArray eventArray =  eventObj.getJSONArray("events");
//					for(int index=0;index<eventArray.length();index++){
//						finalEventArray.put(eventArray.get(index));
//					}
//					String tempEvent = eventArray.toString();
//					System.out.println(eventArray.length());
//					System.out.println(tempEvent);
//					finalResult+=tempEvent.substring(0,tempEvent.length()-1)+",";
				}
			}else{
				String performerConent="";
				String perfomerUrl = baseURL + "/performers/search?q="+performer.toLowerCase().replaceAll("\\s+", "\\+");//uMJlA4oZxQWvPeBxNQiLX/xHl6U5o2veejd1b1mSt5c=
				performerConent = hitTicketEvolution(httpClient,perfomerUrl);
				JSONObject performerJson = new JSONObject(performerConent);
				try{		
					if(!performerJson.isNull("performers")){
						JSONArray performerArray =performerJson.getJSONArray("performers");				
						if(performerArray!=null && performerArray.length()>0){
							for(int index=0;index<performerArray.length();index++){
								JSONObject jsonObject= (JSONObject)performerArray.get(index);
								//System.out.println(jsonObject.get("id")+"\t"+jsonObject.getString("name"));
								if((performer.toUpperCase().trim().equals(jsonObject.getString("name").toUpperCase().trim()))){
									performerIds.clear();
									performerIds.add(jsonObject.getString("id"));
									break;
								}
								if(isMatch(removeIgnoreWordsList(keywords),jsonObject.getString("name")) || (jsonObject.getString("name").toUpperCase().trim().contains(originalKeywords.toUpperCase().trim()))){
									if(!performerIds.contains(jsonObject.getString("id"))){
									performerIds.add(jsonObject.getString("id"));		
									}
								}
								
							}
						}
					}
				}catch (Exception e) {
						e.printStackTrace();
				}
				//performances%5Bperformer_id%5D=15533
				try{
				String searchUrl = baseURL + "/events?";//uMJlA4oZxQWvPeBxNQiLX/xHl6U5o2veejd1b1mSt5c=
				if(performerIds!=null && !performerIds.isEmpty() && performerIds.size()!=0){
					for(String perString:performerIds){
					//searchUrl+=	"page=1&performances%5Bperformer_id%5D="+perString+"";
						DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
						String occursAt="occurs_at" + df.format(fromDate) + "&";
						if(toDate != null){
							finalContent = hitTicketEvolution(httpClient,searchUrl+"per_page=500&performances%5Bperformer_id%5D="+perString);
						}else{
							finalContent = hitTicketEvolution(httpClient,searchUrl+occursAt.replaceAll(":", "%3A") + "performances%5Bperformer_id%5D="+perString+"&per_page=500");
						}
						//System.out.println(finalContent);
						JSONObject json = new JSONObject(finalContent);
						//int totalEvent = json.getInt("total_entries")/100 + 1;
						/*tempEventHits = getEventsFromJSON(json,eventHits, sdf, baseURL, finalContent,fromDate,toDate,location,eventList);
						if(tempEventHits.size()>0){
							eventHits.addAll(tempEventHits);								
						}*/			
						/*if(totalEvent>0 && !(tempEventHits.size()>0)){
							for(int i=2;i<=totalEvent+1;i++){
								tempEventHits.removeAll(tempEventHits);
								//searchUrl = searchUrl.replace("page="+Integer.toString(i), "page="+Integer.toString(i+1));
								finalContent = hitTicketEvolution(httpClient,searchUrl+"page="+Integer.toString(i)+"&performances%5Bperformer_id%5D="+perString+"");
								Collection<EventHit> hits=null;
								hits = getEventsFromJSON(json,sdf, baseURL,location,eventList);
								eventHitsLocal.addAll(hits);
								/*if(tempEventHits.size()>0){
//									eventHits.addAll(tempEventHits);
									tempEventHits.removeAll(tempEventHits);
									break;
								}* /
							}
						}*/
						Collection<EventHit> hits=null;
						hits = getEventsFromJSON(json,sdf, baseURL,location,eventList);
						eventHitsLocal.addAll(hits);
					}
				}
				}catch (Exception e) {
					e.printStackTrace();
				}
//				System.out.println(eventNames);
//				JSONObject eventObj= new JSONObject(eventNames);
//				JSONArray eventArray = eventObj.getJSONArray("events");
//				List<Integer> venueIdsList = new ArrayList<Integer>();
//				if(eventArray != null && eventArray.length()>0){
//					for(int index=0;index<eventArray.length();index++){
//						finalEventArray.put(eventArray.get(index));
//					}
//				}
			}
			
			/*
			for(int index=0;index<finalEventArray.length();index++){
				JSONObject jsonObject= (JSONObject)finalEventArray.get(index);
				//System.out.println(jsonObject.get("id")+"\t"+jsonObject.get("occurs_at")+"\t"+jsonObject.get("name")+"\t"+jsonObject.get("url"));
				JSONObject venueJSON =jsonObject.getJSONObject("venue");
				String dateString = jsonObject.getString("occurs_at").replace('T', ' ').replace('Z', ' ').trim();				
//				if(sdf.parse(dateString).getDate()==fromDate.getDate() && sdf.parse(dateString).getMonth() == fromDate.getMonth())
//					eventHitsLocal.add(new EventHit(jsonObject.get("id").toString(), jsonObject.get("name").toString(), new java.sql.Date(sdf.parse(dateString).getTime()), new java.sql.Time(sdf.parse(dateString).getTime()) , venueJSON.getString("name")+" ,"+venueJSON.getString("location"), "ticketevolution", baseURL+jsonObject.get("url").toString())); 
//				}
				String locationName="";
				try{
					locationName=venueJSON.getString("name") ;
				}catch (Exception e) {
					location="";
				}
				
				String street="";
				try{
					street=venueJSON.getString("street_address") ;
				}catch (Exception e) {
					street="";
				}
				String cityState="";
				try{
					cityState=venueJSON.getString("location") ;
				}catch (Exception e) {
					cityState="";
				}
				String country="";
				try{
					country=venueJSON.getString("country_code") ;
				}catch (Exception e) {
					country="";
				}
				;
				String completeLocation  = (locationName==""?"":locationName+",")+(street==""?"":street+",")+(cityState==""?"":cityState+",")+(country);
				if(location!=null && !location.isEmpty() && !completeLocation.contains(location)){
					continue;
				}
					
				/*if(sdf.parse(dateString).after(fromDate) && sdf.parse(dateString).before(toDate))
					eventHitsLocal.add(new EventHit(jsonObject.get("id").toString(), jsonObject.get("name").toString(), new java.sql.Date(sdf.parse(dateString).getTime()), new java.sql.Time(sdf.parse(dateString).getTime()) , venueJSON.getString("name")+" ,"+venueJSON.getString("location"), "ticketevolution", baseURL+jsonObject.get("url").toString()));
					** /
				if(jsonObject.get("name").toString().contains("angers")){
					System.out.println(jsonObject.get("name").toString()  + ":" + dateString);
				}
				boolean flag=false;
				if(!dateMap.containsKey(dateFormat.format(sdf.parse(dateString)))){
					for(String key:locationMap.keySet()){
						String tokens[] = key.split(":-:");
						String building = tokens[0].trim();
						String city = tokens[1].trim();
						String state = tokens[2].trim();
						
						if(cityState.contains(city.trim())&& cityState.contains(state.trim()) && (TextUtil.isSimilarVenue(building, locationName, 1))){
							flag=true;
							break;
						}
        			}
//					if(!locationMap.containsKey(street.toUpperCase().replaceAll("\\s+", " "))){
//						continue;
//					}
				}else{
					flag=true;
				}
//				if(dateMap.containsKey(dateFormat.format(sdf.parse(dateString)))){
				if(flag){
					eventHitsLocal.add(new EventHit(jsonObject.get("id").toString(), jsonObject.get("name").toString(), new java.sql.Date(sdf.parse(dateString).getTime()), new java.sql.Time(sdf.parse(dateString).getTime()) , venueJSON.getString("name")+" ,"+venueJSON.getString("location"), "ticketevolution", baseURL+jsonObject.get("url").toString()));
				}
			}
			*/
			/*************************/

			/*if(resuBuffer.length()>=1){
			  keywords = resuBuffer.substring(1);
			  //keywords = keywords+"&page=1";
		    }else{
		    	keywords = "";
		    }*/
			/*perfomerUrl+=keywords;
			performerConent = 	hitTicketEvolution(httpClient,perfomerUrl);
			JSONObject performerJson = new JSONObject(performerConent);
			try{		
				if(!performerJson.isNull("performers")){
				JSONArray performerArray =(JSONArray)performerJson.get("performers");				
				if(performerArray!=null && performerArray.length()>0){
					
					for(int index=0;index<performerArray.length();index++){
						JSONObject jsonObject= (JSONObject)performerArray.get(index);
						//System.out.println(jsonObject.get("id")+"\t"+jsonObject.getString("name"));
						if((originalKeywords.toUpperCase().trim().equals(jsonObject.getString("name").toUpperCase().trim()))){
							performerIds.clear();
							performerIds.add(jsonObject.getString("id"));
							break;
						}
						if(isMatch(removeIgnoreWordsList(keywords),jsonObject.getString("name")) || (jsonObject.getString("name").toUpperCase().trim().contains(originalKeywords.toUpperCase().trim()))){
							if(!performerIds.contains(jsonObject.getString("id"))){
							performerIds.add(jsonObject.getString("id"));		
							}
						}
						
					}
				}
				}
		     	
			}catch (Exception e) {
					e.printStackTrace();
			}
			//performances%5Bperformer_id%5D=15533
			try{
			String searchUrl = "https://api.ticketevolution.com/events?";//uMJlA4oZxQWvPeBxNQiLX/xHl6U5o2veejd1b1mSt5c=
			if(performerIds!=null && !performerIds.isEmpty() && performerIds.size()!=0){
				for(String perString:performerIds){
				//searchUrl+=	"page=1&performances%5Bperformer_id%5D="+perString+"";
					DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
					String occursAt="occurs_at" + df.format(fromDate) + "&";
					if(toDate != null){
						finalContent = hitTicketEvolution(httpClient,searchUrl+"performances%5Bperformer_id%5D="+perString);
					}else{
						finalContent = hitTicketEvolution(httpClient,searchUrl+occursAt.replaceAll(":", "%3A") + "performances%5Bperformer_id%5D="+perString);
					}
					//System.out.println(finalContent);
					JSONObject json = new JSONObject(finalContent);
					int totalEvent = json.getInt("total_entries")/100 + 1;
					tempEventHits = getEventsFromJSON(json,eventHits, sdf, baseURL, finalContent,fromDate,toDate,location,eventList);
					if(tempEventHits.size()>0){
						eventHits.addAll(tempEventHits);								
					}			
					if(totalEvent>0 && !(tempEventHits.size()>0)){
						for(int i=2;i<=totalEvent+1;i++){
							tempEventHits.removeAll(tempEventHits);
							//searchUrl = searchUrl.replace("page="+Integer.toString(i), "page="+Integer.toString(i+1));
							finalContent = hitTicketEvolution(httpClient,searchUrl+"page="+Integer.toString(i)+"&performances%5Bperformer_id%5D="+perString+"");
							tempEventHits = getEventsFromJSON(json,eventHits, sdf, baseURL, finalContent,fromDate,toDate,location,eventList);
							if(tempEventHits.size()>0){
								eventHits.addAll(tempEventHits);
								tempEventHits.removeAll(tempEventHits);
								break;
							}
						}
					}
				}
			}
			}catch (Exception e) {
				e.printStackTrace();
			}
			
			
			*/
		}
		return eventHitsLocal;
//		return eventHits;
	}




	private boolean isMatch(String keywords,String toMatch) {
		int count=0;
		for(String keyword:keywords.replace('+', ' ').split("\\s")){
			if(toMatch.toUpperCase().contains(keyword.toUpperCase())){
			count++;	
			}
		}
		int delta = keywords.split("\\+").length>=toMatch.split("\\s+").length?keywords.split("\\+").length/toMatch.split("\\s+").length:toMatch.split("\\s+").length/keywords.split("\\+").length; 
		if(count >= delta){
			return true;
		}else{		
		return false;
		}
	}




	public Collection<EventHit> getEventsFromJSON(JSONObject json,
			SimpleDateFormat sdf, String baseURL,String location,List<Event> eventList) {
		Collection<EventHit> eventHitsLocal = new ArrayList<EventHit>();
		try{		
			if(!json.isNull("events")){
			JSONArray eventArray =(JSONArray)json.get("events");
			String dateString="";
			if(eventArray!=null && eventArray.length()>0){
				DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
				Map<String,Boolean> dateMap = new HashMap<String, Boolean>();
				Map<String,Boolean> locationMap = new HashMap<String, Boolean>();
				for(Event event:eventList){
					if(event.getDate()==null){
						dateMap.put("TBD", true);
						locationMap.put(event.getVenue().getBuilding().replaceAll("\\s+", " ") + ":-:" + event.getVenue().getCity().replaceAll("\\s+", " ") + ":-:" + event.getVenue().getState().replaceAll("\\s+", " ") , true);
						continue;
					}
					dateMap.put(dateFormat.format(event.getDate()), true);
				}
						
				for(int index=0;index<eventArray.length();index++){
					JSONObject jsonObject= (JSONObject)eventArray.get(index);
					System.out.println(jsonObject.get("id")+"\t"+jsonObject.get("occurs_at")+"\t"+jsonObject.get("name")+"\t"+jsonObject.get("url"));
					JSONObject venueJSON =jsonObject.getJSONObject("venue");
					dateString = jsonObject.getString("occurs_at").replace('T', ' ').replace('Z', ' ').trim();				
//					if(sdf.parse(dateString).getDate()==fromDate.getDate() && sdf.parse(dateString).getMonth() == fromDate.getMonth())
//						eventHitsLocal.add(new EventHit(jsonObject.get("id").toString(), jsonObject.get("name").toString(), new java.sql.Date(sdf.parse(dateString).getTime()), new java.sql.Time(sdf.parse(dateString).getTime()) , venueJSON.getString("name")+" ,"+venueJSON.getString("location"), "ticketevolution", baseURL+jsonObject.get("url").toString())); 
//					}
					String locationName="";
					try{
						locationName=venueJSON.getString("name") ;
					}catch (Exception e) {
						locationName="";
					}
					
					String street="";
					try{
						street=venueJSON.getString("street_address") ;
					}catch (Exception e) {
						street="";
					}
					String cityState="";
					try{
						cityState=venueJSON.getString("location") ;
					}catch (Exception e) {
						cityState="";
					}
					String country="";
					try{
						country=venueJSON.getString("country_code") ;
					}catch (Exception e) {
						country="";
					}
					;
					String completeLocation  = (locationName==""?"":locationName+",")+(street==""?"":street+",")+(cityState==""?"":cityState+",")+(country);
					if(location!=null && !location.isEmpty() && !completeLocation.contains(location)){
						continue;
					}
						
					/*if(sdf.parse(dateString).after(fromDate) && sdf.parse(dateString).before(toDate))
						eventHitsLocal.add(new EventHit(jsonObject.get("id").toString(), jsonObject.get("name").toString(), new java.sql.Date(sdf.parse(dateString).getTime()), new java.sql.Time(sdf.parse(dateString).getTime()) , venueJSON.getString("name")+" ,"+venueJSON.getString("location"), "ticketevolution", baseURL+jsonObject.get("url").toString()));
						*/
					boolean flag=false;
					if(!dateMap.containsKey(dateFormat.format(sdf.parse(dateString)))){
						for(String key:locationMap.keySet()){
							String tokens[] = key.split(":-:");
							String building = tokens[0].trim();
							String city = tokens[1].trim();
							String state = tokens[2].trim();
							
							if(cityState.contains(city.trim())&& cityState.contains(state.trim()) && (TextUtil.isSimilarVenue(building, locationName, 1))){
								flag=true;
								break;
							}
            			}
//						if(!locationMap.containsKey(street.toUpperCase().replaceAll("\\s+", " "))){
//							continue;
//						}
					}else{
						flag=true;
					}
//					if(dateMap.containsKey(dateFormat.format(sdf.parse(dateString)))){
					if(flag){
						eventHitsLocal.add(new EventHit(jsonObject.get("id").toString(), jsonObject.get("name").toString(), new java.sql.Date(sdf.parse(dateString).getTime()), new java.sql.Time(sdf.parse(dateString).getTime()) , venueJSON.getString("name")+", "+venueJSON.getString("location"), "ticketevolution", baseURL+jsonObject.get("url").toString()));
					}
				}
					
			}
		}
		     	
		}catch (Exception e) {
				e.printStackTrace();
		}
		return eventHitsLocal;
	}




	public String hitTicketEvolution(HttpClient httpClient, String searchUrl)
			throws GeneralSecurityException, UnsupportedEncodingException {
		String content = "";
		String toSignIn = coumputeSignInURL(searchUrl);
		String signature = computeSignature(toSignIn,secrete);
		HttpGet httpGet = new HttpGet(searchUrl);
		httpGet.addHeader("Host", "api.ticketevolution.com");
		httpGet.addHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
//		httpGet.addHeader("Accept", "application/vnd.ticketevolution.api+json; version=9");				
		httpGet.addHeader("X-Signature", signature);
		httpGet.addHeader("Accept-Encoding", "gzip,deflate");
		httpGet.addHeader("X-Token", token);
		httpGet.addHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
		httpGet.addHeader("Referer", "api.ticketevolution.com");
		try{				
			HttpResponse response = httpClient.execute(httpGet);
			HttpEntity ticeketevolutionEntity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
			content = EntityUtils.toString(ticeketevolutionEntity);
			//System.out.println(content);
		}catch (Exception e) {
				// TODO: handle exception
			}
		return content;
	}
		
	
	

	public  String computeSignature(String baseString, String keyString) throws GeneralSecurityException, UnsupportedEncodingException {

		SecretKey signingKey = new SecretKeySpec(keyString.getBytes(), "HMACSHA256");  
        Mac mac = Mac.getInstance("HMACSHA256");  
        mac.init(signingKey);  
        byte[] digest = mac.doFinal(baseString.getBytes("UTF-8"));     //output of sha256  
        
        //1  
        String encoded64 = new String(Base64.encodeBase64(digest));   // using it as an input  
        //System.out.println("direct base64 : "+encoded64);
        return encoded64;
	}
	
	public String coumputeSignInURL(String url){
		return url.replace("https://", "GET ");		
	}

	
	public static void main(String[] args) throws Exception {
		HttpClient httpClient = new DefaultHttpClient(); //HttpClientStore.createHttpClient(Site.TICKET_EVOLUTION);
		String venueUrl="https://api.ticketevolution.com/v9/venues?";//?q="+keywords;
		TicketEvolutionEventListingFetcher t = new TicketEvolutionEventListingFetcher();
		String venues = 	t.hitTicketEvolution(httpClient,venueUrl);
		System.out.println(venues);
	}

}
