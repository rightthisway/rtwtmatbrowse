package com.admitone.tmat.eventfetcher;

import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import com.admitone.tmat.data.Site;
import com.admitone.tmat.utils.QueryUtil;
import com.admitone.tmat.utils.TextUtil;
import com.admitone.tmat.utils.eimarketplace.EIMPCredential;
import com.admitone.tmat.utils.eimarketplace.EIMPRedirectHandler;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;

/**
 * EiMarketPlace Event Fetcher.
 * When doing a search on eimarketplace, we get a page with the list of artist, then we get a page
 * with the list of venue and finally a page with the list of date.
 * 
 *  However if in one page, there is only one choice, we are automatically redirect to the next page.
 *  (can be redirected up to the event ticket listing page).
 *  
 * when doing a post, set the header with the following value:
 * 			httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");
 */
public class OldEiMarketPlaceEventListingFetcher extends EventListingFetcher {
	
	private EIMPCredential credential;

	public Collection<EventHit> getEventList(String keywords, String location, Date fromDate, Date toDate) throws Exception{
		
		// does not allow to search with no keywords. Otherwise, it will fetch all the event pages.
		if (keywords == null || keywords.trim().length() == 0) {
			return null;
		}

		keywords = QueryUtil.normalizeQuery(keywords);
		System.out.println("KEYWORDS=" + keywords);
		SimpleHttpClient httpClient = null;
		try {
			httpClient = HttpClientStore.createHttpClient(Site.EVENT_INVENTORY, null,null);
			
			SearchContext searchContext = new SearchContext(keywords, location, fromDate, toDate);
			Collection<EventHit> eventHits = new ArrayList<EventHit>();
			Set<String> usedEventIds = new HashSet<String>(); 	

			Collection<EventHit> resultEventHits = parseSearchResultPage(httpClient, keywords, searchContext);
			if (resultEventHits != null) {
				for (EventHit eventHit: resultEventHits) {
					if (!usedEventIds.contains(eventHit.getEventId()) && QueryUtil.matchAnyTerms(keywords, eventHit.getName())) {
						eventHits.add(eventHit);
						usedEventIds.add(eventHit.getEventId());
					}
				}
				if (eventHits.size() > 0) {
					return eventHits;
				}
			}

			String[] tokens = keywords.split(" ");
			for(String token: tokens) {
								
				resultEventHits = parseSearchResultPage(httpClient, token, searchContext);
					
				if (resultEventHits != null) {
					for (EventHit eventHit: resultEventHits) {
						if (!usedEventIds.contains(eventHit.getEventId()) && QueryUtil.matchAnyTerms(keywords, eventHit.getName())) {
							eventHits.add(eventHit);
							usedEventIds.add(eventHit.getEventId());
						}
					}
				}
			}
			
			return eventHits;
			
		} finally {
			HttpClientStore.releaseHttpClient(httpClient);
		}
	}

	private Collection<EventHit> parseSearchResultPage(SimpleHttpClient httpClient, String searchTerms, SearchContext searchContext) throws Exception{
		String searchUrl = "https://www.eimarketplace.com/brokers/byevent.cfm?restart=bykeyword.cfm&history=bykeyword.cfm&id=-1&b=187&next=byevent.cfm&k=" + searchTerms.replace(" ", "+") + "&x=0&y=0";
		
		EIMPRedirectHandler redirectHandler = new EIMPRedirectHandler();
		httpClient.setRedirectHandler(redirectHandler);
		
		HttpGet httpGet = new HttpGet(searchUrl);
		httpGet.addHeader("Host", "www.eimarketplace.com");
		httpGet.addHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
		httpGet.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
		httpGet.addHeader("Accept-Language", "en-us,en;q=0.5");
		httpGet.addHeader("Accept-Encoding", "gzip,deflate");
		httpGet.addHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
		httpGet.addHeader("Referer", "https://www.eimarketplace.com/members/index.cfm");
		HttpResponse response = httpClient.execute(httpGet);
		
		String currentUrl = searchUrl;
		if (redirectHandler.getRedirectUrl() != null) {
			currentUrl = redirectHandler.getRedirectUrl();
		}
		
		System.out.println("RESULT=>CURRENT URL=" + currentUrl);
						
		Collection<EventHit> resultEventHits = null;
		if (currentUrl.contains("/brokers/byevent.cfm")) {
			resultEventHits = parseArtistPage(httpClient, response.getEntity(), searchUrl, searchContext);
		} else if (currentUrl.contains("/brokers/byvenue.cfm")) {
			resultEventHits = parseVenuePage(httpClient, response.getEntity(), searchUrl, searchContext);								
		} else if (currentUrl.contains("/brokers/bydate.cfm")) {
			resultEventHits = parseDatePage(httpClient, response.getEntity(), searchContext);
		} else if (currentUrl.contains("/brokers/results.cfm")) {
			EventHit resultEventHit = parseTicketListingPage(httpClient, response.getEntity(), searchContext);
			if (resultEventHit != null) {
				resultEventHits = new ArrayList<EventHit>();
				resultEventHits.add(resultEventHit);
			}
		}
		return resultEventHits;
	}

	/**
	 * Parse Artist Page (1st step).
	 */
	private static Pattern artistPageSelectPattern = Pattern.compile("<select name=\"e\".*?>(.*?<)/select>", Pattern.DOTALL);
	private static Pattern artistPageOptionPattern = Pattern.compile("option value=\"(.*?)\".*?>(.*?)<", Pattern.DOTALL);
	
	private Collection<EventHit> parseArtistPage(DefaultHttpClient httpClient, HttpEntity entity, 
			String refererUrl, SearchContext searchContext) throws Exception {
//		System.out.println("PARSE ARTIST PAGE");
		HttpEntity uncompressedEntity = HttpEntityHelper.getUncompressedEntity(entity);
		String content = EntityUtils.toString(uncompressedEntity);

		Matcher artistPageSelectMatcher = artistPageSelectPattern.matcher(content);
		
		if (!artistPageSelectMatcher.find()) {
			return null;
		}
		
		String artistPageSelect = artistPageSelectMatcher.group(1);
		
		Matcher artistPageOptionMatcher = artistPageOptionPattern.matcher(artistPageSelect);
		
		Collection<EventHit> eventHits = new ArrayList<EventHit>();

		while(artistPageOptionMatcher.find()) {
			String artistId = artistPageOptionMatcher.group(1);
			
			if (searchContext.containsArtistId(artistId)) {
				System.out.println("SKIP ARTIST=" + artistId);
				continue;
			}
			
			searchContext.addArtistId(artistId);
			String artistName = artistPageOptionMatcher.group(2);

			if (!QueryUtil.matchAnyTerms(searchContext.getKeywords(), artistName)) {
				continue;
			}

			((EIMPRedirectHandler)httpClient.getRedirectHandler()).reset();			
			String venueUrl = "https://www.eimarketplace.com/brokers/byvenue.cfm";			
			HttpPost httpPost = new HttpPost(venueUrl);
			httpPost.addHeader("Host", "www.eimarketplace.com");
			httpPost.addHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
			httpPost.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
			httpPost.addHeader("Accept-Language", "en-us,en;q=0.5");
			httpPost.addHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
			httpPost.addHeader("Referer", refererUrl);
			httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");

			// history=byevent.cfm&venue=1&datespan=0&startdate=%7Bts+%272009-05-07+15%3A44%3A35%27%7D&e=759&next=byvenue.cfm
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String dateFormatString = URLEncoder.encode("{ts '" + dateFormat.format(new Date(new Date().getTime() - 60L * 60L * 1000L)) + "'}", "ISO-8859-1").replace("%20", "+");
			String postData = "history=byevent.cfm&venue=1&datespan=0&startdate=" + dateFormatString + "&e=" + artistId + "&next=byvenue.cfm";
			
			httpPost.setEntity(new StringEntity(postData));
			
			HttpResponse response = httpClient.execute(httpPost);

			String currentUrl = venueUrl;
			if (((EIMPRedirectHandler)httpClient.getRedirectHandler()).getRedirectUrl() != null) {
				currentUrl = ((EIMPRedirectHandler)httpClient.getRedirectHandler()).getRedirectUrl();
			}
			
			if (currentUrl.contains("/brokers/byvenue.cfm")) {
				Collection<EventHit> resultEventHits = parseVenuePage(httpClient, response.getEntity(), venueUrl, searchContext);
				if (resultEventHits != null) {
					eventHits.addAll(resultEventHits);
				}
			} else if (currentUrl.contains("/brokers/bydate.cfm")) {
					Collection<EventHit> resultEventHits = parseDatePage(httpClient, response.getEntity(), searchContext);
					if (resultEventHits != null) {
						eventHits.addAll(resultEventHits);
					}
			} else if (currentUrl.contains("/brokers/results.cfm")) {
				EventHit resultEventHit = parseTicketListingPage(httpClient, response.getEntity(), searchContext);				
				if (resultEventHit != null) {
					eventHits.add(resultEventHit);
				}
			}

		}		
		
		return eventHits;
	}
	
	/**
	 * Parse Venue Page (2nd step).
	 */
	private static Pattern venuePageEventIdPattern = Pattern.compile("<INPUT.*?NAME=\"event\" VALUE=\"(.*?)\">", Pattern.DOTALL);
	private static Pattern venuePageSelectPattern = Pattern.compile("<SELECT NAME=\"v\".*?>(.*?<)/SELECT>", Pattern.DOTALL);
	private static Pattern venuePageOptionPattern = Pattern.compile("<OPTION VALUE=\"(.*?)\".*?>(.*?)</OPTION>", Pattern.DOTALL);

	private Collection<EventHit> parseVenuePage(DefaultHttpClient httpClient, HttpEntity entity, String refererUrl, SearchContext searchContext) throws Exception {
//		System.out.println("PARSE VENUE PAGE");
		HttpEntity uncompressedEntity = HttpEntityHelper.getUncompressedEntity(entity);
		String content = EntityUtils.toString(uncompressedEntity);

		Matcher venuePageEventIdMatcher = venuePageEventIdPattern.matcher(content);
		venuePageEventIdMatcher.find();
		String eventId = venuePageEventIdMatcher.group(1);

		Matcher venuePageSelectMatcher = venuePageSelectPattern.matcher(content);
		if (!venuePageSelectMatcher.find()) {
			return null;
		}

		String venuePageSelect = venuePageSelectMatcher.group(1);
		
		Collection<EventHit> eventHits = new ArrayList<EventHit>();

		Collection<VenuePageFetcherTask> venuePageFetcherTasks = new ArrayList<VenuePageFetcherTask>();
		ExecutorService threadPoolExecutor = Executors.newFixedThreadPool(4); 
		
		Matcher venuePageOptionMatcher = venuePageOptionPattern.matcher(venuePageSelect);
		
		while(venuePageOptionMatcher.find()) {
			String venueId = venuePageOptionMatcher.group(1);
			String venueName = venuePageOptionMatcher.group(2);

			if (!QueryUtil.matchAllTerms(searchContext.getLocation(), venueName)) {
				continue;
			}

			VenuePageFetcherTask venuePageFetcherTask = new VenuePageFetcherTask(eventId, venueId,
					refererUrl, searchContext);
			venuePageFetcherTask.setCredential(credential);
			venuePageFetcherTasks.add(venuePageFetcherTask);
			
			// System.out.println("BEFORE EXECUTE " + venuePageFetcherTask.getId() + "/" + eventId + "/" + venueId);
			threadPoolExecutor.execute(venuePageFetcherTask);
			// System.out.println("AFTER EXECUTE " + venuePageFetcherTask.getId());
						
		}
		
		System.out.println("*** WAIT FOR THREAD TERMINATION");
		threadPoolExecutor.shutdown();
		try {
			
		threadPoolExecutor.awaitTermination(5, TimeUnit.MINUTES);
		} catch (Exception e) {
			System.out.println(">>>>>>>>>>>>>> ERROR WHILE WAITING TERMINATION <<<<<<<<<<<<<<<<");
			e.printStackTrace();
		}

		System.out.println("******** ADDING EVENT HITS ****");
		for (VenuePageFetcherTask venuePageFetcherTask: venuePageFetcherTasks) {
			if (venuePageFetcherTask.getEventHits() != null) {
				eventHits.addAll(venuePageFetcherTask.getEventHits());
			}
		}
						
		System.out.println("******** DONE  ****");
		return eventHits;
	}

	static class VenuePageFetcherTask implements Runnable {
		private Collection<EventHit> eventHits = new ArrayList<EventHit>();
		private String venueId;
		private String eventId;
		private String refererUrl;
		private SearchContext searchContext;
		
		private EIMPCredential credential;
		
		private static int idCounter = 0;
		private int id;
		
		public VenuePageFetcherTask(String eventId, String venueId,
				String refererUrl, SearchContext searchContext) {
			this.venueId = venueId;
			this.eventId = eventId;
			this.refererUrl = refererUrl;
			this.searchContext = searchContext;
						
			this.id = idCounter++;
			
			System.out.println("*** CREATED THREAD " + id);
		}
		
		public int getId() {
			return id;
		}
		
		public void setCredential(EIMPCredential credential) {
			this.credential = credential;
		}		
		
		public void run() {
			System.out.println("*** START THREAD " + id);
			SimpleHttpClient httpClient = null;

			try {
				httpClient = HttpClientStore.createHttpClient(Site.EI_MARKETPLACE, credential.getUsername(), credential.getPassword());
				httpClient.setRedirectHandler(new EIMPRedirectHandler());
				System.out.println("EIMARKETPLACE VENUEID=" + venueId);
				
				((EIMPRedirectHandler)httpClient.getRedirectHandler()).reset();
					
				String searchByVenueUrl = "https://www.eimarketplace.com/brokers/results.cfm";			
				HttpPost httpPost = new HttpPost(searchByVenueUrl);
				httpPost.addHeader("Host", "www.eimarketplace.com");
				httpPost.addHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
				httpPost.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
				httpPost.addHeader("Accept-Language", "en-us,en;q=0.5");
				httpPost.addHeader("Accept-Encoding", "gzip,deflate");
				httpPost.addHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
				// httpGet.addHeader("Keep-Alive", "300");
				// httpGet.addHeader("Connection", "keep-alive");
				httpPost.addHeader("Referer", refererUrl);
				httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");
		
				// history=byevent.cfm&venue=1&datespan=0&startdate=%7Bts+%272009-05-07+15%3A44%3A35%27%7D&e=759&next=byvenue.cfm
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String dateFormatString = URLEncoder.encode("{ts '" + dateFormat.format(new Date(new Date().getTime() - 60L * 60L * 1000L)) + "'}", "ISO-8859-1").replace("%20", "+");
				String postData = "history=byvenue.cfm&event=" + eventId + "&datespan=0&startdate=" + dateFormatString + "&v=" + venueId + "&next=bydate.cfm";

				httpPost.setEntity(new StringEntity(postData));
				HttpResponse response = httpClient.execute(httpPost);
				
				String currentUrl = searchByVenueUrl;
				
				if (((EIMPRedirectHandler)httpClient.getRedirectHandler()).getRedirectUrl() != null) {
					currentUrl = ((EIMPRedirectHandler)httpClient.getRedirectHandler()).getRedirectUrl();
				}
				

				if (currentUrl.contains("/brokers/bydate.cfm")) {
					Collection<EventHit> resultEventHits = parseDatePage(httpClient, response.getEntity(), searchContext);
					if (resultEventHits != null) {
						eventHits.addAll(resultEventHits);
					}
				} else if (currentUrl.contains("/brokers/results.cfm")) {
					EventHit resultEventHit = parseTicketListingPage(httpClient, response.getEntity(), searchContext);				
					if (resultEventHit != null) {
						eventHits.add(resultEventHit);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (httpClient != null) {
					HttpClientStore.releaseHttpClient(httpClient);
				}
				// System.out.println("*** STOP THREAD " + id);
			}

		}		
		
		private Collection<EventHit> getEventHits() {
			return this.eventHits;
		}
	}

	
	/**
	 * Parse Venue Page (3rd step).
	 */
	
	private static Pattern datePageSelectPattern = Pattern.compile("<SELECT NAME=\"p\".*?>(.*?)</SELECT>", Pattern.DOTALL);
	private static Pattern datePageOptionPattern = Pattern.compile("<OPTION VALUE=\"(.*?)\">(.*?)</OPTION>", Pattern.DOTALL);
	
	private static Collection<EventHit> parseDatePage(DefaultHttpClient httpClient, HttpEntity entity, SearchContext searchContext) throws Exception {
// 		System.out.println("PARSE DATE PAGE");
		
		HttpEntity uncompressedEntity = HttpEntityHelper.getUncompressedEntity(entity);
		String content = EntityUtils.toString(uncompressedEntity);
		
		Matcher datePageSelectMatcher = datePageSelectPattern.matcher(content);
		if (!datePageSelectMatcher.find()) {
			return null;
		}
		
		String datePageSelect = datePageSelectMatcher.group(1);
		
		SimpleDateFormat datePageDateFormat1 = new SimpleDateFormat("MMMMMMMMM dd, yyyy (EEE) - hh:mm aa");		
		SimpleDateFormat datePageDateFormat2 = new SimpleDateFormat("MMMMMMMMM dd, yyyy (EEE)");
				
		Collection<EventHit> eventHits = new ArrayList<EventHit>();
				
		Matcher datePageOptionMatcher = datePageOptionPattern.matcher(datePageSelect);
		
		while(datePageOptionMatcher.find()) {
			String eventId = datePageOptionMatcher.group(1);
			if (searchContext.containsEventId(eventId)) {
				System.out.println("SKIP EVENT=" + eventId);
				continue;
			}

			searchContext.addEventId(eventId);
			
			Date eventDate = null;
			String eventDateString = TextUtil.removeExtraWhitespaces(datePageOptionMatcher.group(2)).trim();
			
//			System.out.println("EVENT DATE STRING=" + eventDateString);
			if (eventDateString.contains("TBA")) {
				 eventDate = datePageDateFormat2.parse(eventDateString.split("-")[0].trim());								
			} else {
				try {
				 eventDate = datePageDateFormat1.parse(eventDateString);	
				} catch( ParseException pe) {
					continue;
				}
			}

			if (eventDate.before(searchContext.getFromDate()) || eventDate.after(searchContext.getToDate())) {
				continue;
			}
//			System.out.println("EVENT DATE=" + eventDate);


			String ticketListingUrl = "https://www.eimarketplace.com/brokers/results.cfm?restart=yes&p=" + eventId;			
			HttpGet httpGet = new HttpGet(ticketListingUrl);
			httpGet.addHeader("Host", "www.eimarketplace.com");
			httpGet.addHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
			httpGet.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
			httpGet.addHeader("Accept-Language", "en-us,en;q=0.5");
			httpGet.addHeader("Accept-Encoding", "gzip,deflate");
			httpGet.addHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
			// httpGet.addHeader("Keep-Alive", "300");
			// httpGet.addHeader("Connection", "keep-alive");
			httpGet.addHeader("Referer", "http://www.eimarketplace.com/brokers/results.cfm");

			HttpResponse response = httpClient.execute(httpGet);
			
			EventHit eventHit = parseTicketListingPage(httpClient, response.getEntity(), searchContext);
			if (eventHit != null) {
				eventHits.add(eventHit);
			}
		}
		return eventHits;
	}

	/**
	 * Parse Venue Page (4th step).
	 */
	private static Pattern ticketListingPageEventIdPattern = Pattern.compile("<input.*?name=\"p\" value=\"(.*?)\">", Pattern.DOTALL);
	
//	<strong class="accentplus2">Nov 14, 2009</strong>
//	<span class="fineprint"> at 08:30 PM<br>Saturday</span>
//
//</td><td class="fieldinfo">
//	<table cellspacing="0" cellpadding="0" border="0" align="RIGHT">
//	<tr>
//		<td align="CENTER" valign="TOP">
//			<a href="/search/venuedetail.cfm?restart=yes&v=504&b=187" onmouseover="window.status='Show detailed information on Electric Factory ...'; return true" onmouseout="window.status=' '; return true"><img align="LEFT" src="/graphics/info.gif" width="15" height="15" alt="Venue Seating Chart for Electric Factory" border="0" hspace="5" vspace="2"></a>
//	
//		
//		<a href="http://www.ticketsnow.com/maps/504-1-2-main.gif" target="_blank" onmouseover="window.status='View a Venue map for GA ...'; return true" onmouseout="window.status=' '; return true"><img src="/graphics/map.gif" width="24" height="19" alt="Venue Seating Chart for Electric Factory" border="0" hspace="0" vspace="0"></a>
//	
//
//	</td>
//	</tr>
//	</table>
//
//	
//<strong class="accentplus2">Electric Factory</strong>
//
//		<br>
//		<span class="fineprint">
//	
//			Philadelphia, 
//	
//			PA
//		</span>
//
//	</td>
//</tr>
	//                                                                                                                                                                                        event name                                event desc (optional)                                                                                                                              event date                                               event time                                                           venue name            venue location
//	private static Pattern ticketListingPageVenuePattern = Pattern.compile("<tr bgcolor=\"EEEEDD\" valign=\"TOP\" align=\"CENTER\">.*?<td class=\"fieldinfo\">.*?<strong class=\"accentplus2\">(.*?)</strong>.*?<span class=\"fineprint\">.*?</span></td>.*?<td class=\"fieldinfo\">.*?<strong class=\"accentplus2\">(.*?)</strong>.*?<span class=\"fineprint\">.*?<.*?/span>.*?</td>", Pattern.DOTALL);
	private static Pattern ticketListingPageVenuePattern = Pattern.compile("<tr bgcolor=\"EEEEDD\" valign=\"TOP\" align=\"CENTER\">.*?<td class=\"fieldinfo\">.*?<strong class=\"accentplus2\">(.*?)</strong>.*?<span class=\"fineprint\">(.*?)</span>.*?</td>.*?<td class=\"fieldinfo\">.*?<strong class=\"accentplus2\">(.*?)</strong>.*?<span class=\"fineprint\">(.*?)<.*?</table>.*?<strong class=\"accentplus2\">(.*?)</strong>(.*?)</td>", Pattern.DOTALL);
	private static Pattern ticketListingPageVenueLocationPattern = Pattern.compile("<span class=\"fineprint\">(.*?)</span>", Pattern.DOTALL);

	private static EventHit parseTicketListingPage(DefaultHttpClient httpClient, HttpEntity entity, SearchContext searchContext) throws Exception {
//		System.out.println("PARSE TICKET LISTING PAGE");
		HttpEntity uncompressedEntity = HttpEntityHelper.getUncompressedEntity(entity);		
		String content = EntityUtils.toString(uncompressedEntity);
		
		Matcher ticketListingPageEventIdMatcher = ticketListingPageEventIdPattern.matcher(content);
		ticketListingPageEventIdMatcher.find();
		String eventId = ticketListingPageEventIdMatcher.group(1);

		Matcher ticketListingPageVenueMatcher = ticketListingPageVenuePattern.matcher(content);
		if (!ticketListingPageVenueMatcher.find()) {
			System.out.println(">> RETURN 1");
			return null;
		}
		String eventName = ticketListingPageVenueMatcher.group(1);
		String eventDesc = ticketListingPageVenueMatcher.group(2);
		String[] tokens = eventDesc.split("<br>");
		if (tokens.length == 2) {
			eventName += " - " + tokens[1];
		}
		//System.out.println("1 EVENT NAME=" + eventName);
		String eventDateString = ticketListingPageVenueMatcher.group(3);
		String eventTimeString = ticketListingPageVenueMatcher.group(4).substring(3);
		String venueName = ticketListingPageVenueMatcher.group(5);
		//System.out.println("VENUE NAME=" + venueName);

		
		String rawVenueCityState = ticketListingPageVenueMatcher.group(6);
		Matcher ticketListingPageVenueLocationMatcher = ticketListingPageVenueLocationPattern.matcher(rawVenueCityState);
		String venueCityState = "";
		if (ticketListingPageVenueLocationMatcher.find()) {
			venueCityState = ticketListingPageVenueLocationMatcher.group(1);
		}
		
		String venueLocation = TextUtil.removeExtraWhitespaces(venueName + " " + venueCityState);

		//System.out.println("+ EVENT NAME=" + eventName);
		//System.out.println("+ EVENT DATE=" + eventDateString);
		//System.out.println("+ EVENT TIME=" + eventTimeString);
		//System.out.println("+ VENUE LOCATION=" + venueLocation);
		
		if (!QueryUtil.matchAnyTerms(searchContext.getKeywords(), eventName)) {
			System.out.println(">> RETURN 2");
			return null;
		}

		// Mar 12, 2009
		SimpleDateFormat ticketListingPageDateFormat1 = new SimpleDateFormat("MMM dd, yyyy hh:mm aa");			
		SimpleDateFormat ticketListingPageDateFormat2 = new SimpleDateFormat("MMM dd, yyyy");
		
		
		// try to parse both date and time
		String eventDateTimeString = eventDateString + " " + eventTimeString;
		Date venueDate = null;
		try {
			venueDate = ticketListingPageDateFormat1.parse(eventDateTimeString);
		} catch (Exception e) {
		}
		
		// if it fails, only parse date
		if (venueDate == null) {
			try {
				venueDate = ticketListingPageDateFormat2.parse(eventDateString);
			} catch (Exception e) {
			}			
		}

		if (venueDate.before(searchContext.getFromDate()) || venueDate.after(searchContext.getToDate())) {
			//System.out.println(">> RETURN NOT DATE" + venueDate);
			return null;
		}
		
		if (!QueryUtil.matchAllTerms(searchContext.getLocation(), venueLocation)) {
			//System.out.println(">> RETURN 4");
			return null;
		}

		java.sql.Date eventSqlDate = new java.sql.Date(venueDate.getTime());
		java.sql.Time eventSqlTime = new java.sql.Time(venueDate.getTime());
		EventHit eventHit = new EventHit(eventId, eventName, eventSqlDate, eventSqlTime, 
				venueLocation, Site.EI_MARKETPLACE, 
				"https://www.eimarketplace.com/brokers/results.cfm?restart=yes&p=" + eventId);
		//System.out.println("Event Hit =" + eventHit);
		return eventHit;
	}
	
	public void setCredential(EIMPCredential credential) {
		this.credential = credential;
	}

	// contains some search context data
	class SearchContext {
		private String keywords;
		private String location;
		private Date fromDate;
		private Date toDate;
		
		private Set<String> searchedArtistId = new HashSet<String>();
		private Set<String> searchedEventId = new HashSet<String>();

		
		public SearchContext(String keywords, String location, Date fromDate, Date toDate) {
			this.keywords = keywords;
			this.location = location;
			this.fromDate = fromDate;
			this.toDate = toDate;
		}
		
		public synchronized void addArtistId(String artistId) {
			searchedArtistId.add(artistId);			
		}
		
		public synchronized boolean containsArtistId(String artistId) {
			return searchedArtistId.contains(artistId);			
		}

		public synchronized void addEventId(String eventId) {
			searchedEventId.add(eventId);			
		}

		public synchronized boolean containsEventId(String eventId) {
			return searchedEventId.contains(eventId);			
		}

		public String getKeywords() {
			return keywords;
		}

		public String getLocation() {
			return location;
		}

		public Date getFromDate() {
			return fromDate;
		}

		public Date getToDate() {
			return toDate;
		}

	}
}