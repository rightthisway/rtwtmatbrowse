package com.admitone.tmat.eventfetcher;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;

import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.utils.QueryUtil;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;

/**
 * SeatWave Event Fetcher.
 * When doing a search on seatwave, we get the list of tours matching the query.
 * We need to fetch the tour pages to get the event lists for each tour.
 * Then for each event, we need to fetch the event page to get the time of the event.
 * 
 */
public class SeatWaveWebEventListingFetcher extends EventListingFetcher {
	
	private static Pattern artistGridPattern = Pattern.compile("<!-- Start Result information box -->(.*?)<!-- End Search Results Grid -->",Pattern.DOTALL);
	
	
	//<td class="Performance"> <strong> Phish </strong> </td><td class="PerformanceDate"> 24 Nov 2009<BR />to 04 Dec 2009 </td><td class="PerformanceVenue">  multiple cities </td><td class="PerformancePrice"> <p> Tickets from £36.91 </p> </td><td class="PerformanceBuySell">   <a id="ctl00_cpLeft1_grvPerfs_ctl02_hypGo" class="bt-buy-10" href="http://www.seatwave.com/phish-tickets/season">Go</a> </td>
	//                                                                                                                                                                       Event name                           Date (can be DATE<BR/>to DATE | DATE) HREF
	private static Pattern artistRowPattern = Pattern.compile("<tr.*?<td.*?<strong>(.*?)</strong>.*?</td>.*?<td.*?>(.*?)</td>.*?href=\"(.*?)\".*?</tr>", Pattern.DOTALL);
	private static Pattern eventGridPattern = Pattern.compile("<table class=\"subContentArtistNameTable\".*?>(.*?)</table>", Pattern.DOTALL);
	
	//<tr onMouseOver="mouse_event(this, 'venueListActive')" onmouseout="mouse_event(this, '')">
	// <td class="Show"> <p><a id="ctl00_cpLeft1_grvPerfsNextMonth_ctl02_HypPerComments" href="http://www.seatwave.com/u2-tickets/angel-stadium-tickets/06-june-2010/perf/275681"></a> </p> <p><strong><a id="ctl00_cpLeft1_grvPerfsNextMonth_ctl02_hypSeason" title="U2 Tickets" href="http://www.seatwave.com/u2-tickets/angel-stadium-tickets/06-june-2010/perf/275681">U2</a></strong></p> <p><a id="ctl00_cpLeft1_grvPerfsNextMonth_ctl02_HypTowName" href="http://www.seatwave.com/u2-tickets/angel-stadium-tickets/06-june-2010/perf/275681">Anaheim, USA</a> </p>  </td><td class="When"> <p> Sun 06 Jun 2010</p> </td><td class="Where"> <p> Angel Stadium</p> <p> </td><td class="PerformerBuySell"> <a id="ctl00_cpLeft1_grvPerfsNextMonth_ctl02_hypSell" title="Sell U2 Tickets" class="bt-sell-10" href="http://www.seatwave.com/sell/sell00b.aspx?Performance=275681">Sell</a> <a id="ctl00_cpLeft1_grvPerfsNextMonth_ctl02_hypBuy" title="Buy U2 Tickets" class="bt-buy-10" href="http://www.seatwave.com/u2-tickets/angel-stadium-tickets/06-june-2010/perf/275681">View tickets</a>    </td>
	//</tr>
	private static Pattern eventRowPattern = Pattern.compile("<tr.*?<td.*?>.*?<a .*?</a>.*?<a.*?href=\"(.*?)\".*?>(.*?)</a>.*?<a.*?>(.*?)</a>.*?</td>.*?<td.*?>.*?<p>(.*?)</p>.*?</td>.*?<td.*?>.*?<p>(.*?)</p>.*?</td>.*?</tr>", Pattern.DOTALL);
	
	// <a id="ctl00_cpFullWidth1_PerformanceSubHeader1_hypVenue" href="http://www.seatwave.com/angel-stadium-tickets/venue">Angel Stadium</a><span style="font-weight:normal">, Sun 06 Jun 10 at 19:00</span></h1> </div>
	//private static Pattern eventDetailPattern = Pattern.compile("<a.*?id=\"ctl00_cpFullWidth1_PerformanceSubHeader1_hypVenue\".*?>(.*?)</a>.*?<span.*?>,(.*?)</span>");
	private static Pattern eventDetailPattern = Pattern.compile("<a.*?id=\"ctl00_cpFullWidth1_PerformanceSubHeader1_hypVenue\".*?>(.*?)</a><span.*?>,(.*?)</span>");
	
	// http://www.seatwave.com/u2-tickets/wembley-stadium-tickets/14-august-2009/perf/208780
	private static Pattern eventIdPattern = Pattern.compile("/([0-9]+)$");

	
	private static Pattern ticketsDatePattern = Pattern.compile("ctl00_cpLeft1_seasonTopContentSection_labDates\".*?>(.*?)</span>");
	private static Pattern ticketsLocationPattern = Pattern.compile("ctl00_cpLeft1_seasonTopContentSection_rptVenues_ctl00_labVenue\".*?>(.*?)</span>");
	private static Pattern ticketsSeasonPattern = Pattern.compile("ctl00_MasterPageHeader_lnkSell.*?Season=(.*?)\"");

	
	@Override
	public Collection<EventHit> getEventList(String keywords, String location, Date fromDate, Date toDate,List<Event> eventList,boolean isVenue) throws Exception{

		DateFormat queryDateFormat = new SimpleDateFormat("dd/MM/yy");
		String startDateString = "";
		if (fromDate != null) {
			startDateString = queryDateFormat.format(fromDate);
		}
		
		String endDateString = "";
		if (toDate != null) {
			endDateString = queryDateFormat.format(toDate);
		}

		Collection<EventHit> eventHits = new ArrayList<EventHit>();
		Collection<String> eventIds = new ArrayList<String>();
		
		keywords = keywords.replaceAll("+"," ");	
		for(String keyword : keywords.split(" ")){

		String queryUrl = "http://www.seatwave.com/search?term=" 
			+ keyword + "&type=ADVANCED&cat=0&startdate="
			+ startDateString + "&enddate=" + endDateString + "&fromprice=&toprice=";
		
		HttpGet httpGet = new HttpGet(queryUrl);
		httpGet.addHeader("Host", "www.seatwave.com");
		httpGet.addHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
		httpGet.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
		httpGet.addHeader("Accept-Language", "en-us,en;q=0.5");
		httpGet.addHeader("Accept-Encoding", "gzip,deflate");
		httpGet.addHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
		httpGet.addHeader("Referer", "http://www.seatwave.com/");
		
		SimpleHttpClient httpClient = null;
		try {
			httpClient = HttpClientStore.createHttpClient();
			
			//
			// FETCH ARTIST RESULT
			//
			
			HttpResponse response = httpClient.execute(httpGet);
			HttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());			
			String content = EntityUtils.toString(entity);
			
			Matcher artistGridMatcher = artistGridPattern.matcher(content);
			if (!artistGridMatcher.find()) {
				return eventHits;
			}						
			
			String artistGrid = artistGridMatcher.group(1);
			Matcher artistRowMatcher = artistRowPattern.matcher(artistGrid);
			while(artistRowMatcher.find()) {
				String artistName = artistRowMatcher.group(1);
				String dateString = artistRowMatcher.group(2);
				String artistHref = artistRowMatcher.group(3);
				
				httpGet = new HttpGet(artistHref);
				httpGet.addHeader("Host", "www.seatwave.com");
				httpGet.addHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
				httpGet.addHeader("Accept-Language", "en-us,en;q=0.5");
				httpGet.addHeader("Accept-Encoding", "gzip,deflate");
				httpGet.addHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
				httpGet.addHeader("Referer", queryUrl);
				
				response = httpClient.execute(httpGet);
				entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());			
				content = EntityUtils.toString(entity);

				// two layout possible:
				// - table of events
				// - table of tickets

				Matcher eventGridMatcher = eventGridPattern.matcher(content);
				if (!eventGridMatcher.find()) {
					if (content.contains("Choose your  tickets:")) {
						Matcher ticketsDateMatcher = ticketsDatePattern.matcher(content);
						ticketsDateMatcher.find();
						String ticketsDate = ticketsDateMatcher.group(1);

						Matcher ticketsLocationMatcher = ticketsLocationPattern.matcher(content);
						ticketsLocationMatcher.find();
						String ticketsLocation = ticketsLocationMatcher.group(1);
						
						Matcher ticketsSeasonMatcher = ticketsLocationPattern.matcher(content);
						ticketsSeasonMatcher.find();
						String seasonId = ticketsSeasonMatcher.group(1);
						
						DateFormat eventDateFormat = new SimpleDateFormat("EEE, dd MMMMMMMMM yyyy");
						Date eventDate = eventDateFormat.parse(ticketsDate);
												
						java.sql.Date eventSqlDate = new java.sql.Date(eventDate.getTime()); 
						java.sql.Time eventSqlTime = new java.sql.Time(eventDate.getTime());
						
						EventHit eventHit = new EventHit("S" + seasonId, artistName, eventSqlDate, eventSqlTime, ticketsLocation, Site.SEATWAVE, artistHref);
						if(!eventIds.contains(seasonId)){
							eventHits.add(eventHit);
							System.out.println("EVENT HIT=" + eventHit);
							eventIds.add(seasonId);
						}
					}
					continue;
				}
				
				String eventGrid = eventGridMatcher.group(1);
				
				Matcher eventRowMatcher = eventRowPattern.matcher(eventGrid);
				while(eventRowMatcher.find()) {
					String eventHref = eventRowMatcher.group(1);
					String eventName = eventRowMatcher.group(2);
					String eventCityCountry = eventRowMatcher.group(3);
					String eventDateString = eventRowMatcher.group(4).trim();
					String eventVenue = eventRowMatcher.group(5);
					
					if (!QueryUtil.matchAllTerms(keyword, eventName)) {
						continue;
					}
					
					// Sun 06 Jun 2010
					DateFormat eventDateFormat1 = new SimpleDateFormat("EEE dd MMM yyyy HH:mm");
					DateFormat eventDateFormat2 = new SimpleDateFormat("EEE dd MMM yyyy");
					Date eventDate = null;
					try {
						eventDate = eventDateFormat1.parse(eventDateString);
					} catch (Exception e) {}
					
					if (eventDate == null) {
						eventDate = eventDateFormat2.parse(eventDateString);
					}	
										
					if (eventDate.before(fromDate) || eventDate.after(toDate)) {
						continue;
					}
					

					String eventLocation = eventVenue + " " + eventCityCountry;
					// first filtering by location
					if (!QueryUtil.matchAllTerms(location, eventLocation)) {
						continue;
					}

					//
					// FETCH EVENT PAGE
					//
					
					httpGet = new HttpGet(eventHref);
					httpGet.addHeader("Host", "www.seatwave.com");
					httpGet.addHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
					httpGet.addHeader("Accept-Language", "en-us,en;q=0.5");
					httpGet.addHeader("Accept-Encoding", "gzip,deflate");
					httpGet.addHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
					httpGet.addHeader("Referer", queryUrl);

					response = httpClient.execute(httpGet);
					entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());			
					content = EntityUtils.toString(entity);
					
					Matcher eventDetailMatcher = eventDetailPattern.matcher(content);
					if (!eventDetailMatcher.find()) {
						continue;
					}
					
					eventDetailMatcher.group(1);
					// Sun 06 Jun 10 at 19:00
					String dateDetailString = eventDetailMatcher.group(2);
					
					Pattern hourPattern = Pattern.compile("at (\\d+):(\\d+)");
					Matcher hourMatcher = hourPattern.matcher(dateDetailString);
					hourMatcher.find();
					
					eventDate.setHours(Integer.parseInt(hourMatcher.group(1)));
					eventDate.setMinutes(Integer.parseInt(hourMatcher.group(2)));

					Matcher eventIdMatcher = eventIdPattern.matcher(eventHref);
					eventIdMatcher.find();
					String eventId = eventIdMatcher.group(1);

					java.sql.Date eventSqlDate = new java.sql.Date(eventDate.getTime()); 
					java.sql.Time eventSqlTime = new java.sql.Time(eventDate.getTime());
					
					EventHit eventHit = new EventHit("E" + eventId, eventName, eventSqlDate, eventSqlTime, eventLocation, Site.SEATWAVE, eventHref);
					if(!eventIds.contains(eventId)){
						eventHits.add(eventHit);
						System.out.println("EVENT HIT=" + eventHit);
						eventIds.add(eventId);
					}
				}
			}
			
			
		} finally {
			HttpClientStore.releaseHttpClient(httpClient);
		}
			}
			return eventHits;
	}
	
}
