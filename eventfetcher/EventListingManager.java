package com.admitone.tmat.eventfetcher;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.admitone.tmat.crawler.InterruptedTicketListingCrawlException;
import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.enums.CrawlState;
import com.admitone.tmat.enums.TourType;
import com.admitone.tmat.indexer.TicketHitIndexer;
import com.admitone.tmat.ticketfetcher.TicketHit;
import com.admitone.tmat.ticketfetcher.TicketListingFetcher;
import com.admitone.tmat.utils.TextUtil;

/**
 * Event Listing Manager.
 */
public final class EventListingManager {
	public final static long CACHE_TIMEOUT = 3600000L; // 60 minutes
	private Map<String, EventHit> eventHitMap = new HashMap<String, EventHit>();
	private Integer MAX_CACHE_SIZE = 1000000;
	private Date lastCleanup = new Date();

	private Map<String, EventListingFetcher> eventListingFetcherMap = new HashMap<String, EventListingFetcher>();
	private Map<String, TicketListingFetcher> ticketListingFetcherMap = new HashMap<String, TicketListingFetcher>();

	private synchronized void cleanCache() {
		Date now = new Date();
		if (now.getTime() - lastCleanup.getTime() < CACHE_TIMEOUT) {
			return;
		}

		Collection<String> toDel = new ArrayList<String>();
		for (EventHit eventHit : eventHitMap.values()) {
			if (now.getTime() - eventHit.getCreationDate().getTime() > CACHE_TIMEOUT) {
				toDel.add(eventHit.getId());
			}
		}

		for (String id : toDel) {
			eventHitMap.remove(id);
		}
		
		lastCleanup = now;
	}

	public EventHit getEventHit(String id) {
		return eventHitMap.get(id);
	}

	private static String getMostRelevantWord(String str) {
		str = str.toLowerCase();
		String scrubbedWords = str.replaceAll(".*?:", "").replaceAll("\\W+", " ").replaceAll("\\d+", " ");
		for (String word: scrubbedWords.split(" ")) {
			if (word.length() < 3) {
				continue;
			}
			
			if (word.equals("the")) {
				continue;
			}
			
			return word;
		}		
		
		return scrubbedWords;
	}
	
	/**
	 * Run event search on the merchant sites. TODO: if there are any errors
	 * during a search, display them in the results.
	 */
	public Collection<EventListingResult> search(String keywords,
			String[] siteIds, String location, Date fromDate, Date toDate,List<Event> eventList,boolean isVenue) {

		cleanCache();

		String originalKeywords = keywords;
		keywords = TextUtil.getSearchStringFromName(keywords);
		String eBayKeywords = TextUtil.getEbaySearchStringFromName(keywords);
		ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors
				.newCachedThreadPool();
		executor.setCorePoolSize(10);

		if (location != null && location.trim().isEmpty()) {
			location = null;
		}

		// Collection<EventHit> allEventHits = new ArrayList<EventHit>();
		List<EventListingResult> results = new ArrayList<EventListingResult>();

		for (String siteId : siteIds) {
			EventListingFetcher fetcher = eventListingFetcherMap.get(siteId);
			if (fetcher != null) {
				EventListingResult result = new EventListingResult(siteId);
				results.add(result);
				SearchTask task = null;
				if(siteId.equals("ticketsolutions")){
					System.out.println("ticketSolutions: " + originalKeywords + "===>" + getMostRelevantWord(originalKeywords));
					task = new SearchTask(result, fetcher, getMostRelevantWord(originalKeywords),
							location, fromDate, toDate,eventList,isVenue);					
				} else if(siteId.equals("ebay")){
					task = new SearchTask(result, fetcher, eBayKeywords,
							location, fromDate, toDate,eventList,isVenue);
				} else if(siteId.equals(Site.AOP)) {
					task = new SearchTask(result, fetcher, originalKeywords,
							location, fromDate, toDate,eventList,isVenue);
				}else if(siteId.equals(Site.TICKET_EVOLUTION)) {
//					DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//					DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
//					DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					task = new SearchTask(result, fetcher, originalKeywords,location, fromDate, toDate,eventList,isVenue);
					/*if(event!=null){
						String eventDateTime ="";
						if(event.getLocalDate()!=null && event.getLocalTime()!=null){
							eventDateTime = dateFormat.format(event.getLocalDate()) + " " + timeFormat.format(event.getLocalTime());
							try {
								fromDate= dateTimeFormat.parse(eventDateTime);
							} catch (ParseException e) {
								e.printStackTrace();
							}
						}
						if(!eventDateTime.isEmpty()){
							task = new SearchTask(result, fetcher, originalKeywords,location, fromDate, toDate,null);
						}else{
							task = new SearchTask(result, fetcher, originalKeywords,location, fromDate, null,null);
						}
					}
					else{
						task = new SearchTask(result, fetcher, originalKeywords,location, fromDate, null,null);
					}*/
				}else {
					task = new SearchTask(result, fetcher, keywords,
							location, fromDate, toDate,eventList,isVenue);
				}
				executor.execute(task);
			}
		}

		executor.shutdown();
		try {
			executor.awaitTermination(900, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			executor.shutdownNow();
			e.printStackTrace();
		}

		for (EventListingResult result : results) {
			Collection<EventHit> hits = result.getEventHits();
			if (hits != null) {
				for (EventHit eventHit : hits) {
					eventHitMap.put(eventHit.getId(), eventHit);
				}
			}
		}

		return results;
	}

	public Map<String, EventListingFetcher> getEventListingFetcherMap() {
		return eventListingFetcherMap;
	}

	public void setEventListingFetcherMap(Map<String, EventListingFetcher> fetcherMap) {
		this.eventListingFetcherMap = fetcherMap;
	}
	
	public Map<String, TicketListingFetcher> getTicketListingFetcherMap() {
		return ticketListingFetcherMap;
	}

	public void setTicketListingFetcherMap(Map<String, TicketListingFetcher> fetcherMap) {
		this.ticketListingFetcherMap = fetcherMap;
	}	

	private class SearchTask implements Runnable {
		private EventListingResult result;
		private EventListingFetcher fetcher;
		private String keywords;
		private String location;
		private Date fromDate;
		private Date toDate;
		private List<Event> eventList;
		private boolean isVenue;
		public SearchTask(EventListingResult result,
				EventListingFetcher fetcher, String keywords, String location,
				Date fromDate, Date toDate,List<Event> eventList,boolean isVenue) {
			this.result = result;
			this.fetcher = fetcher;
			this.keywords = keywords;
			this.location = location;
			this.fromDate = fromDate;
			this.toDate = toDate;
			this.eventList=eventList;
			this.isVenue = isVenue;
		}

		public void run() {
			try {
				Collection<EventHit> hits = fetcher.getEventList(keywords,
						location, fromDate, toDate,eventList,isVenue);
				result.setEventHits(hits);
				Map<String, Event> eventMap = new HashMap<String, Event>();
				for(Event event:eventList){
					String key =event.getFormatedDate().replaceAll("EST", "").replaceAll("EDT", "").trim() + "-" + event.getVenue().getBuilding().toLowerCase().trim()+ ", " + event.getVenue().getCity().toLowerCase().trim() + ", " + event.getVenue().getState().toLowerCase().trim();
//					Event mapEvent = eventMap.get(key);
//					if(mapEvent==null){
//						mapEvent = new Event();
//					}
					//events.add(event);
					eventMap.put(key.trim(),event);
				}
				// get crawl states of fetched events
				if(result.getEventHits()!= null){
					for(EventHit eventHit: result.getEventHits()) {
						PreviewTicketHitIndexer ticketHitIndexer = new PreviewTicketHitIndexer(2);
						TicketListingCrawl ticketListingCrawl = new TicketListingCrawl(eventHit);
						System.out.println("TICKET LISTING CRAWL=" + ticketListingCrawl);
	
						CrawlState crawlState = null;
						try {
							TicketListingFetcher ticketListingFetcher = (TicketListingFetcher)(ticketListingFetcherMap.get(ticketListingCrawl.getSiteId()).clone());
							if(eventHit.getSiteId().equals(Site.AOP)){
								Map<String, String> map = new HashMap<String, String>();
								map.put("keywords",keywords);
								map.put("date",String.valueOf(toDate.getTime()));
								ticketListingCrawl.setExtraParameterMap(map);
	//							ticketListingCrawl.setExtraParameter("keywords", keywords);
								
							}
							// bug when using a previous eimp connection, so create a new one
							/*
							if (eventHit.getSiteId().equals(Site.EI_MARKETPLACE)) {
								SimpleHttpClient httpClient = new SimpleHttpClient(eventHit.getSiteId());
								EIMPCredential credential = ((OldEIMarketPlaceTicketListingFetcher)ticketListingFetcher).getCredential();
								EIMPUtil.login(httpClient, credential.getUsername(), credential.getPassword());
								ticketListingFetcher.runFetchTicketListing(httpClient, ticketHitIndexer, ticketListingCrawl);
							} else {
							*/
							
							ticketListingFetcher.fetchTicketListing(ticketHitIndexer, ticketListingCrawl);							
							//}
							
							Collection<Ticket> ticketHits = ticketHitIndexer.getIndexedTickets();
							if (ticketHits == null || ticketHits.size() == 0) {
								crawlState = CrawlState.NO_TIX;
							}
						} catch (InterruptedTicketListingCrawlException e) {
						} catch (Exception e) {
							crawlState = CrawlState.EXCEPTION;
						}
						eventHit.setCrawlState(crawlState);	
						String key=eventHit.getDateAndTime().trim() + "-" + eventHit.getLocation().toLowerCase().trim();
						Event mapEvent = eventMap.get(key);
						if(mapEvent!=null){
	//						for(Event event:events){
	//							if(event.getName().equalsIgnoreCase(eventHit.getName())){
									eventHit.setBestMatchEventId(mapEvent.getId());
	//								break;
	//							}
	//						}
						}
					}
				}
			} catch (Exception e) {
				result.setException(e);
			}
		}

		public boolean isVenue() {
			return isVenue;
		}

		public void setVenue(boolean isVenue) {
			this.isVenue = isVenue;
		}
	}
	
	public class PreviewTicketHitIndexer extends TicketHitIndexer {
		private Collection<Ticket> indexedTickets = new ArrayList<Ticket>();
		private Integer previewLimitSize;
		
		public PreviewTicketHitIndexer(Integer previewLimitSize) {
			this.previewLimitSize = previewLimitSize;
		}

		@Override
		public int indexTicketHit(TicketHit ticketHit) {
			if (ticketHit == null) {
				return ERROR;
			}
			
			if (previewLimitSize != null && indexedTickets.size() >= previewLimitSize) {
				throw new InterruptedTicketListingCrawlException("Preview Limit has been reached.");
			}

			Ticket ticket = new Ticket(ticketHit.getSiteId(), ticketHit.getItemId());
			updateTicketFromTicketHit(ticket, ticketHit, TourType.OTHER);
			
			indexedTickets.add(ticket);
			return NEW_ITEM;
		}
		
		public Collection<Ticket> getIndexedTickets() {
			return indexedTickets;
		}
		
		public Integer getPreviewLimitSize(){
			return previewLimitSize;
		}
	}

}
