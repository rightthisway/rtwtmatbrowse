package com.admitone.tmat.eventfetcher;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.util.EntityUtils;

import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.utils.TextUtil;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;

// just a placeholder for ticketsnow - working on it now

public class TicketsNowEventListingFetcher extends EventListingFetcher {
//	private EventListingFetcher eiMarketPlaceEventListingFetcher;
	@Override	
	public Collection<EventHit> getEventList(String keywords, String location, Date fromDate, Date toDate,List<Event> eventList,boolean isVenue) throws Exception {
		Set<String> validURLs= new HashSet<String>();
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		Map<String,Boolean> dateMap = new HashMap<String, Boolean>();
		Map<String,Boolean> locationsMap = new HashMap<String, Boolean>();
		Map<String,Boolean> locationForVenuePageMap = new HashMap<String, Boolean>();
		Map<String,List<String>> eventDateMap = new HashMap<String, List<String>>();
		
		keywords = keywords.replace('+', '-');
		SimpleHttpClient httpClient = null;
//		Collection<EventHit> eimpEventHits = new ArrayList<EventHit>();
//		Collection<String> eventIds = new ArrayList<String>();
		
		String []keyword=keywords.split(" ");
		
		for(Event event:eventList){
			if(event.getDate()==null){
				locationsMap.put(event.getVenue().getBuilding().replaceAll("\\s+", " ") + ":-:" + event.getVenue().getCity().replaceAll("\\s+", " ") + ":-:" + event.getVenue().getState().replaceAll("\\s+", " ") , true);
				dateMap.put("TBD", true);
				continue;
			}
			List<String> eventNameList =  eventDateMap.get(df.format(event.getDate()));
			if(eventNameList == null){
				eventNameList = new ArrayList<String>();
			}
			eventNameList.add(event.getName());
			eventDateMap.put(df.format(event.getDate()), eventNameList);
			locationForVenuePageMap.put(event.getVenue().getBuilding().replaceAll("\\s+", " ") + ":-:" + event.getVenue().getCity().replaceAll("\\s+", " ") + ":-:" + event.getVenue().getState().replaceAll("\\s+", " ") , true);
			dateMap.put(df.format(event.getDate()), true);
		}
		if(isVenue){
			String urlKeyword=keywords.replaceAll("-", "").replaceAll(" ", "-");
			String ticketsNowUrl="http://www.ticketsnow.com/"+ urlKeyword + "-venue-tickets";
			HttpPost httpPost= new HttpPost(ticketsNowUrl);
//			System.out.println("URL -->" + ticketsNowUrl);
			httpPost.setHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
			httpPost.addHeader("Host", "www.ticketsnow.com");		
			httpPost.addHeader("Accept", "application/json, text/javascript, */*");
			httpPost.addHeader("Accept-Encoding", "gzip,deflate");
			httpClient= HttpClientStore.createHttpClient(Site.TICKETS_NOW);
			
			HttpResponse httpResponse= httpClient.execute(httpPost);
			HttpEntity entity = HttpEntityHelper.getUncompressedEntity(httpResponse.getEntity());
			String content=EntityUtils.toString(entity).replaceAll("\\s+", " ");
//			System.out.println(content);
			if(content.toLowerCase().contains("page not found")){
				ticketsNowUrl="http://www.ticketsnow.com/Search/SearchResults.aspx?STY=SS&WHAT="+ urlKeyword;
				httpPost= new HttpPost(ticketsNowUrl);
//				System.out.println("URL -->" + ticketsNowUrl);
				httpPost.setHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
				httpPost.addHeader("Host", "www.ticketsnow.com");		
				httpPost.addHeader("Accept", "application/json, text/javascript, */*");
				httpPost.addHeader("Accept-Encoding", "gzip,deflate");
//				httpClient= HttpClientStore.createHttpClient(Site.TICKETS_NOW);
				
				httpResponse= httpClient.execute(httpPost);
				entity = HttpEntityHelper.getUncompressedEntity(httpResponse.getEntity());
				content=EntityUtils.toString(entity).replaceAll("\\s+", " ");
				Pattern pattern = Pattern.compile("searchresultvenuetext'>" +  keywords +"(.*?)>venue");
				Matcher matcher = pattern.matcher(content.toLowerCase());
//				System.out.println(content);
		        if(matcher.find()){
//		        	System.out.println(matcher.group(1));
		        	pattern = Pattern.compile("href='(.*?)'");
		        	Matcher matcher1 = pattern.matcher(matcher.group(1));
		        	if(matcher1.find()){
//		        		System.out.println(matcher1.group(1).replaceAll(" ","%20"));
		        		HttpGet httpGet= new HttpGet(matcher1.group(1).replaceAll(" ","%20"));
//		    			System.out.println("URL -->" + ticketsNowUrl);
		        		httpGet.setHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
		        		httpGet.addHeader("Host", "www.ticketsnow.com");		
		        		httpGet.addHeader("Accept", "application/json, text/javascript, */*");
		        		httpGet.addHeader("Accept-Encoding", "gzip,deflate");
//		    			httpClient= HttpClientStore.createHttpClient(Site.TICKETS_NOW);
		    			
		    			httpResponse= httpClient.execute(httpGet);
		    			entity = HttpEntityHelper.getUncompressedEntity(httpResponse.getEntity());
		    			content=EntityUtils.toString(entity).replaceAll("\\s+", " ");
		        	}
		        	
//		        	System.out.println(matcher.group(3));
		        }else{
		        	return null;
		        }
			}
//			else{
				Pattern pattern = Pattern.compile("eventsVenueGridData(.*?)</tr>");
				Matcher matcher = pattern.matcher(content);
				while(matcher.find()){
					Pattern urlPattern = Pattern.compile("href='(.*?)'");
					Matcher urlMatcher = urlPattern.matcher(matcher.group());
					
					Pattern eventNamePattern = Pattern.compile("span (.*?)</tr>");
					Matcher eventNameMatcher = eventNamePattern.matcher(matcher.group());
					
					Pattern eventDatePattern = Pattern.compile("eventsVenueGridData(.*?)<");
					Matcher eventDateMatcher = eventDatePattern.matcher(matcher.group());
					
					String url="";
					String eventName="";
					String eventDate="";
					
					if(urlMatcher.find()){
						url=urlMatcher.group();
					}
					if(eventNameMatcher.find()){
						Pattern subEventNamePattern = Pattern.compile(">(.*?)<");
						Matcher subEventNameMatcher = subEventNamePattern.matcher(eventNameMatcher.group());
						if(subEventNameMatcher.find()){
							eventName = subEventNameMatcher.group();
						}
					}
					int i=0;
					while(eventDateMatcher.find()){
						if(i==0){
							i++;
							continue;
						}
//						System.out.println(eventDateMatcher.group());
						Pattern subEventDatePattern = Pattern.compile(">(.*?)<");
						Matcher subEventDateMatcher = subEventDatePattern.matcher(eventDateMatcher.group());
						if(subEventDateMatcher.find()){
							eventDate = subEventDateMatcher.group();
						}
					}
					url = url.replaceAll("href=", "").replaceAll("'", "");
					eventName=eventName.replaceAll("<", "").replaceAll(">", "");
					eventDate=eventDate.replaceAll("<", "").replaceAll(">", "");
					DateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy");
//					System.out.println(url);
//					System.out.println(eventName);
//					System.out.println(eventDate);
					
					String fromEventDateString = eventDate.split("-")[0].trim();
					String toEventDateString = eventDate.split("-")[1].trim();
					Date fromEventDate = dateFormat.parse(fromEventDateString);
					Date toEventDate = dateFormat.parse(toEventDateString);
					
					for(Date dt=fromEventDate;dt.before(toEventDate)||dt.equals(toEventDate);){
						List<String> eventNameList = eventDateMap.get(df.format(dt));
						if(eventNameList!=null && !eventNameList.isEmpty()){
							for(String dbEventName:eventNameList){
								if(TextUtil.isKeywordsMatch(dbEventName, eventName)){
									System.out.println(dbEventName + ":" + eventName);
									validURLs.add(url);
								}
							}
						}
						Calendar cal= Calendar.getInstance();
						cal.setTime(dt);
						cal.add(Calendar.DATE, 1);
						dt=cal.getTime();
					}
//					System.out.println( eventDateMap.get(df.format()));
				}
//			}
//			return null;
		}else{
			/*for(String keyword : keywords.split(" ")){
				Collection<EventHit> tempHits = eiMarketPlaceEventListingFetcher.getEventList(keyword, location, fromDate, toDate);
				for(EventHit eHit : tempHits){
					if(!eventIds.contains(eHit.getEventId())) {
						eimpEventHits.add(eHit);
						eventIds.add(eHit.getEventId());
					}
				}
			}
			*/
			
			
			String urlKeyword=keywords.replaceAll(" ", "-");
			String ticketsNowUrl="http://www.ticketsnow.com/Search/SearchResults.aspx?STY=SS&WHAT="+ urlKeyword;
			HttpPost httpPost= new HttpPost(ticketsNowUrl);
//			System.out.println("URL -->" + ticketsNowUrl);
			httpPost.setHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
			httpPost.addHeader("Host", "www.ticketsnow.com");		
			httpPost.addHeader("Accept", "application/json, text/javascript, */*");
			httpPost.addHeader("Accept-Encoding", "gzip,deflate");
			httpClient= HttpClientStore.createHttpClient(Site.TICKETS_NOW);
			
			HttpResponse httpResponse= httpClient.execute(httpPost);
			HttpEntity entity = HttpEntityHelper.getUncompressedEntity(httpResponse.getEntity());
			String content=EntityUtils.toString(entity);
//			System.out.println(content);
			Pattern patern= Pattern.compile("href='(.*?)'");
			
			Matcher matcher=patern.matcher(content);
	        boolean isValidUrl=true;
	       
	        while(matcher.find()){
	        	isValidUrl=true;
//	        	System.out.println(matcher.group(0));
	        	if(!matcher.group(0).contains("cat=Event")){
	        		continue;
	        	}
//	        	System.out.println(matcher.group(0));
	        	for(String temp:keyword){
		        	if(!matcher.group(0).toLowerCase().contains((temp.toLowerCase()))){
		        		isValidUrl=false;
		        		break;
		        	}
	        	}
	        	if(isValidUrl){
	        		validURLs.add(matcher.group(0));
	        	}
	        }
		}
        Iterator<String> it =validURLs.iterator();
        DateFormat inputFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
        DateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yy");
        DecimalFormat decimalFormat =  new DecimalFormat("00");
       
        String fromDateString= inputFormat.format(fromDate);
        String toDateString= inputFormat.format(toDate);
        HttpClient client=null;
        Collection<EventHit> eventHits = new ArrayList<EventHit>();
//        Pattern eventURLPattern=null;
//		Matcher eventURLMatcher=null;
		
        Pattern eventIdPattern=null;
		Matcher eventIdMatcher=null;
		
		Pattern eventNamePattern=null;
		Matcher eventNameMatcher=null;
		
		Pattern eventDatePattern=null;
		Matcher eventDateMatcher=null;
		
		Pattern eventLocationPattern=null;
		Matcher eventLocationMatcher=null;
		
		Pattern patternURL=null;
		Matcher matcherURL=null;
		String eventId="";
		String locationFromTicketsNow="";
		String date="";
		String name="";
		String urlFromTicketsNow="";
		Pattern patternClass1=Pattern.compile("\"url summary(.*?)</div>");
		Matcher matcherClass1=null;
		Pattern patternClass2=Pattern.compile("class=\"dtstart\"(.*?)</div>");
		Matcher matcherClass2=null;
		Pattern patternClass3=Pattern.compile("class=\"c3 location(.*?)</div>");
		Matcher matcherClass3=null;
		
        while(it.hasNext()){
        	String url=it.next();
        	Pattern urlPattern=Pattern.compile("http(.*?)&");
        	Matcher urlMatcher=urlPattern.matcher(url);
        	while(urlMatcher.find()){
        		if(!isVenue){
        			url=urlMatcher.group(0)+"fdt="+fromDateString +"&tdt="+toDateString+"&maid=0&searchpath=1";
        		}
        		url=url.replaceAll(" ", "%20");
        		try{
        		HttpPost post= new HttpPost(url);
//        		System.out.println("URL -->" + url);
        		post.setHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
        		post.addHeader("Host", "www.ticketsnow.com");
        		
        		post.addHeader("Accept", "application/json, text/javascript, */*");
        		post.addHeader("Accept-Encoding", "gzip,deflate");
        		client= HttpClientStore.createHttpClient(Site.TICKETS_NOW);
        		
        		HttpResponse response= null;//client.execute(post);
        		try{
        			response = client.execute(post);
        		}catch (ClientProtocolException ex) {
        			String error =ex.getCause().toString();
        			System.out.println(error);
        			if(error.contains("redirect")){
        				Pattern pattern = Pattern.compile("'(.*?)'");
        				Matcher match = pattern.matcher(error);
        				if(match.find()){
        					String uri=match.group();
        					post= new HttpPost(uri.replaceAll("'", ""));
//        	        		System.out.println("URL -->" + url);
        	        		post.setHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
//        	        		post.addHeader("Host", "www.ticketsnow.com");
        	        		
        	        		post.addHeader("Accept", "application/json, text/javascript, */*");
        	        		post.addHeader("Accept-Encoding", "gzip,deflate");
        	        		response = client.execute(post);
        				}
            			
        			}
        			
        		}
        		HttpEntity ticeketsnowEntity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
        		String finalContent= EntityUtils.toString(ticeketsnowEntity);
//        		System.out.println(finalContent);
        		Pattern paternBlock=Pattern.compile("class=\"url summary(.*?)View Tickets");
        		Pattern ticketMAsterPatern=Pattern.compile("class=\"url summary(.*?)Buy Tickets");
        		finalContent=finalContent.replaceAll("\\s+", " ");
//        		System.out.println(finalContent);
                /*FileWriter writer= new FileWriter("C:\\Stubhub\\content.txt");
               
                writer.write(finalContent);
                writer.flush();
                writer.close();
                */
        		
//                Matcher matcherBlock1=paternBlock.matcher(finalContent);
                Matcher ticketMasterMatcher = ticketMAsterPatern.matcher(finalContent);
                Matcher matcherBlock=paternBlock.matcher(finalContent);
              /// added by anil //////
                boolean isMatch=matcherBlock.find();
                boolean isTicketMasterMatch = ticketMasterMatcher.find();
                
        		if(!isMatch && !isTicketMasterMatch){
        			Pattern venuePagePattern=Pattern.compile("_hlName3\" class=\"None\" href=\"(.*?)\">\\d{1,2}/\\d{1,2}/\\d{2,4} - \\d{1,2}/\\d{1,2}/\\d{2,4}");
//        			System.out.println("class=\"None\" href=\"(.*?)\">\\d{1,2}/\\d{1,2}/\\d{2,4}");
        			Matcher venuePageMatcherBlock=venuePagePattern.matcher(finalContent);
        			Pattern linkPattern=Pattern.compile("href=\"(.*?)\"");
        			Pattern datePattern=Pattern.compile("\\d{1,2}/\\d{1,2}/\\d{2,4}");
        			Matcher linkMatcher = null;
        			Matcher dateMatcher = null;
        			String[] dateSplit = null;
        			while(venuePageMatcherBlock.find()){
        				Date d1 = null;
        				Date d2 = null;
//        				System.out.println("Here-------->"+venuePageMatcherBlock.group(0));
        				linkMatcher = linkPattern.matcher(venuePageMatcherBlock.group(0));
        				dateMatcher = datePattern.matcher(venuePageMatcherBlock.group(0).trim());
        				if(dateMatcher.find()){
        					dateSplit = dateMatcher.group(0).split("/");
        					d1= simpleDateFormat.parse(decimalFormat.format(Integer.parseInt(dateSplit[0]))+"/"+decimalFormat.format(Integer.parseInt(dateSplit[1]))+"/"+decimalFormat.format(Integer.parseInt(dateSplit[2])));
        					//System.out.println("Date-------->"+d1);	
        				}
        				if(dateMatcher.find()){
        					dateSplit = dateMatcher.group(0).split("/");
        					d2= simpleDateFormat.parse(decimalFormat.format(Integer.parseInt(dateSplit[0]))+"/"+decimalFormat.format(Integer.parseInt(dateSplit[1]))+"/"+decimalFormat.format(Integer.parseInt(dateSplit[2])));
        					//System.out.println("Date-------->"+d2);	
        				}
        				
        				if((d1.getTime()-fromDate.getTime()<=0) && (d2.getTime()-fromDate.getTime()>=0) && linkMatcher.find()){
        					//System.out.println("Link-------->"+linkMatcher.group(0).trim().substring(linkMatcher.group(0).trim().indexOf("http"), linkMatcher.group(0).trim().length() -1 ));
        					post= new HttpPost(linkMatcher.group(0).trim().substring(linkMatcher.group(0).trim().indexOf("http"), linkMatcher.group(0).trim().length() -1 ));
        	        		//System.out.println("URL -->" + url);
        	        		post.setHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
        	        		post.addHeader("Host", "www.ticketsnow.com");
        	        		
        	        		post.addHeader("Accept", "application/json, text/javascript, */*");
        	        		post.addHeader("Accept-Encoding", "gzip,deflate");
        	        		client= HttpClientStore.createHttpClient();
        	        		
        	        		 response= client.execute(post);
        	        		 ticeketsnowEntity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
        	        		 finalContent= EntityUtils.toString(ticeketsnowEntity);
        	        		 finalContent=finalContent.replaceAll("\\s++", " ");
//        	        		 System.out.println(finalContent);
        	        		 matcherBlock=paternBlock.matcher(finalContent);
        	                 isMatch=matcherBlock.find();
        	                 isTicketMasterMatch = ticketMasterMatcher.find();
        	                 if(isMatch||isTicketMasterMatch){
        	                	 Pattern pattern = Pattern.compile("url fn org(.*?)\\)");
        	                	 Matcher matcher = pattern.matcher(finalContent);
        	                	 String tempLocation = "";
        	                	 if(matcher.find()){
        	                		 Pattern patternLocation = Pattern.compile(">(.*?)<");
            	                	 Matcher matcherLocation = patternLocation.matcher(matcher.group());
            	                	 
            	                	 while(matcherLocation.find()){
            	                		 String temp = matcherLocation.group().replaceAll(">", "").replaceAll("<", "").replaceAll("\\(", "").replaceAll(",", "").trim();
            	                		 if(temp.isEmpty()){
            	                			 continue;
            	                		 }
            	                		 tempLocation += temp.replaceAll("\\s+", " ").trim() + ":-:";
            	                	 }
            	                	 tempLocation = tempLocation.substring(0,tempLocation.length()-3);
//            	                	 System.out.println(tempLocation);
        	                		 if(locationForVenuePageMap.get(tempLocation) != null && locationForVenuePageMap.get(tempLocation)){
        	                			 break;
        	                		 }
        	                	 }
        	                	 
        	                 }
        				}
        			}
        		
        		} 
        		/// added by anil ends /////
        		while(isMatch || isTicketMasterMatch){
        			
            		// Added by Chirag on 06/02/2012
        			String eventListContent ="";
            		if(isMatch){
            			eventListContent= matcherBlock.group(0);
            		}else{
            			eventListContent= ticketMasterMatcher.group(0);
            		}

//        			String dateToMap ="";
        			matcherClass1=patternClass1.matcher(eventListContent);
//        			System.out.println(eventListContent);
        			isMatch=matcherBlock.find();
                    isTicketMasterMatch = ticketMasterMatcher.find();
        			if(matcherClass1.find()){
//        				System.out.println(matcherClass1.group(0));
        				
        				patternURL=Pattern.compile("href='(.*?)'");
        				matcherURL=patternURL.matcher(matcherClass1.group(0));
        				
        				if(matcherURL.find()){
//        					System.out.println(matcherURL.group(0));
        					eventIdPattern=Pattern.compile("PID=(.*?)'");
        					eventIdMatcher=eventIdPattern.matcher(matcherURL.group(0));
        					
        					if(eventIdMatcher.find()){
//        						System.out.println(eventIdMatcher.group(0));
        						eventId=eventIdMatcher.group(0).substring(4, eventIdMatcher.group(0).length()-1);
        					}
        					
        					eventNamePattern=Pattern.compile("''>(.*?)<");
        					eventNameMatcher=eventNamePattern.matcher(matcherClass1.group(0));
        					
        					if(eventNameMatcher.find()){
//        						System.out.println(eventNameMatcher.group(0));
        						name=eventNameMatcher.group(0).replaceAll("Tickets","").replaceAll(">", "").replaceAll("<", "").replaceAll("'", "").trim();
        					}
        				}
        			}
        			if(eventId==""){
        				continue;
        			}
        			java.sql.Date finalDate=null;
        			java.sql.Time finalTime=null;
        			
        			DateFormat timeFormat=new SimpleDateFormat("HH:mm aa");
        			
        			matcherClass2=patternClass2.matcher(eventListContent);
        			if(matcherClass2.find()){
//        				System.out.println(matcherClass2.group(0));
        				eventDatePattern=Pattern.compile("</span>(.*?)</div>");
        				eventDateMatcher=eventDatePattern.matcher(matcherClass2.group(0));
        				if(eventDateMatcher.find()){
        					date=eventDateMatcher.group(0).substring(16, eventDateMatcher.group(0).length()-4);
        					String delimeter="";
        					if(date.contains("th")){
        						delimeter="th";
        					}else if(date.contains("st")){
        						delimeter="st";
        					}else if(date.contains("rd")){
        						delimeter="rd";
        					}else if(date.contains("nd")){
        						delimeter="nd";
        					}
        					if(date.contains("TBD") && date.contains(".")){
        						date=date.replaceAll("TBD", "00:00 AM");
        					}
        				    DateFormat dateFormat = new SimpleDateFormat("EEE, MMM. dd'"+ delimeter +"', yyyy hh:mm aa");
        					finalDate=new java.sql.Date(dateFormat.parse(date).getTime());
//        					dateToMap=df.format(finalDate);
        					finalTime=new java.sql.Time(timeFormat.parse(timeFormat.format(dateFormat.parse(date))).getTime());
        				}
        			}
//        			System.out.println(date);
        			
        			
        			matcherClass3=patternClass3.matcher(eventListContent);
        			if(matcherClass3.find()){
//        				System.out.println(matcherClass3.group(0));
        				eventLocationPattern=Pattern.compile(">(.*?)<");
        				eventLocationMatcher=eventLocationPattern.matcher(matcherClass3.group());
        				locationFromTicketsNow="";
        				while(eventLocationMatcher.find()){
        					locationFromTicketsNow+=eventLocationMatcher.group();
        				}
        				locationFromTicketsNow=locationFromTicketsNow.replaceAll(">", "").replaceAll("<", "");
        			}
        			String venue[] = locationFromTicketsNow.split("\\(");
        			String building ="";
        			String city ="" ;
        			String state ="" ;
        			if(venue.length==2){
        				building=venue[0].replace("(", "");
        				String temp[]=venue[1].replace(")","").split(",");
        				city=temp[0];
        				state=temp[1];
        				
        			}
//        			isValidUrl=true;
        			
        			if(!(eventListContent.contains("InventoryBrowse") &&  eventListContent.contains("PID="))){
//        				isValidUrl=false;
        				continue;
        			}
//        			if(isValidUrl){
        				urlFromTicketsNow=matcherURL.group(0).substring(6, matcherURL.group(0).length()-1);
        				eventIdPattern=Pattern.compile("PID=(.*?)");
                		eventIdMatcher=eventIdPattern.matcher("");
//        				System.out.println(matcherURL.group(0) + ":" + matcherURL.group(1));
                		/*if(finalDate.after(fromDate) && finalDate.before(toDate)){                		
                			eventHits.add(new EventHit(eventId, name, finalDate,
        						finalTime, locationFromTicketsNow, Site.TICKETS_NOW, urlFromTicketsNow));
                		}*/
                		String tempDate =date.replace("</", "").trim();
                		String interMediate="";
                		if(tempDate.contains("th")){
                			interMediate="'th'";
                		}else if(tempDate.contains("st")){
                			interMediate="'st'";
                		}else if(tempDate.contains("nd")){
                			interMediate="'nd'";
                		}else if(tempDate.contains("rd")){
                			interMediate="'rd'";
                		}
                		
                		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM. dd" + interMediate + ", yyyy hh:mm a");
                		boolean flag=false;
                		if(!dateMap.containsKey(df.format(dateFormat.parse(tempDate)))){
                			
                			for(String key:locationsMap.keySet()){
                				String tokens[] = key.split(":-:");
    							String buildingToken = tokens[0].trim();
    							String cityToken = tokens[1].trim();
    							String stateToken = tokens[2].trim();
    							
    							if( cityToken.equalsIgnoreCase(city.trim())&& 
    									stateToken.equalsIgnoreCase(state.trim()) && 
    									(TextUtil.isSimilarVenue(buildingToken, building, 1))){
    								flag=true;
    								break;
    							}
                			}
//    						if(!locationMap.containsKey(locationFromTicketsNow)){
//    							continue;
//    						}
    					}else{
    						flag=true;
    					}
//                		if(dateMap.containsKey(dateToMap)){
                		if(flag){
                			eventHits.add(new EventHit(eventId, name, finalDate,
            						finalTime, building.trim() + ", " + city.trim() + ", " + state.trim(), Site.TICKETS_NOW, urlFromTicketsNow));
    					}
//        			}
        			
        		}
        		}catch (Exception e) {
        			System.err.println(e.fillInStackTrace());
        		}
        	}
        }
		/*if (eimpEventHits == null) {
			return null;
		}
		*/
		

		/*for(EventHit eventHit: eimpEventHits) {			
			String url = "http://www.ticketsnow.com/InventoryBrowse/at-tickets-at-in-?PID=" + eventHit.getEventId();
			eventHits.add(new EventHit(eventHit.getEventId(), eventHit.getName(), eventHit.getDate(),
					eventHit.getTime(), eventHit.getLocation(), Site.TICKETS_NOW, url));
			
		}*/
		return eventHits;
	}
	
	/*public void setEiMarketPlaceEventListingFetcher(EventListingFetcher eiMarketPlaceEventListingFetcher) {
		this.eiMarketPlaceEventListingFetcher = eiMarketPlaceEventListingFetcher;
	}*/
}
