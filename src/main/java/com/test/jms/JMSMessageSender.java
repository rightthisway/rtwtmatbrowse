package com.test.jms;


import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

public class JMSMessageSender {

	private Destination destination1;
	private Destination destination2;
	private Destination destination3;
	private JmsTemplate jmsTemplate;
	
	public Destination getDestination1() {
		return destination1;
	}
	public void setDestination1(Destination destination) {
		this.destination1 = destination;
	}
	public Destination getDestination2() {
		return destination2;
	}
	public void setDestination2(Destination destination) {
		this.destination2 = destination;
	}
	public Destination getDestination3() {
		return destination3;
	}
	public void setDestination3(Destination destination3) {
		this.destination3 = destination3;
	}
	public JmsTemplate getJmsTemplate() {
		return jmsTemplate;
	}
	public void setJmsTemplate(JmsTemplate jmsTemplate) {
		this.jmsTemplate = jmsTemplate;
	}
	
	public void sendMessage(final String msg){
		MessageCreator creator = new TestMessageCreator(msg);  // Queue
		jmsTemplate.send(destination1, creator);
		jmsTemplate.send(destination2, creator);
		jmsTemplate.send(destination3, creator);
	}

	
	public class TestMessageCreator implements MessageCreator{
		String msg;
		
		public TestMessageCreator(String msg){
			this.msg=msg;
		}
		
		public Message createMessage(Session session) throws JMSException {
	         TextMessage message = null;
             try {
                 message = session.createTextMessage();
                 message.setStringProperty("msg", msg);
             }catch (JMSException e) {
                 e.printStackTrace();
             }
             return message;
		}
		
	}


}
