package com.test.jms;

//import javax.jms.Destination;
import java.util.Collection;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
 
import org.springframework.jms.core.JmsTemplate;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.crawler.TicketListingCrawler;
import com.admitone.tmat.utils.SpringUtil;

public class JMSMessageReceiver implements MessageListener{
	 private JmsTemplate jmsTemplate;
	 private Destination destination;
	 
	 
	    public void setJmsTemplate(JmsTemplate jmsTemplate)
	    {
	        this.jmsTemplate = jmsTemplate;
	    }
	 
//	    public void setDestination(Destination destination)
//	    {
//	        this.destination = destination;
//	    }
	 
	    public String receiveMessage(){
	    	System.out.println(jmsTemplate);
	        Message message = jmsTemplate.receive(destination);
	        TextMessage textMessage = null;
	        if (message instanceof TextMessage)
	        {
	            textMessage = (TextMessage)message;
	            try
	            {
	            	String msg = textMessage.getStringProperty("msg");
	            	TicketListingCrawler ticketListingCrawler=  SpringUtil.getTicketListingCrawler();
	                Integer eventId= Integer.parseInt(msg);
	                Collection<TicketListingCrawl> ticketListingCrawls = ticketListingCrawler.getTicketListingCrawlByEvent(eventId);
	                ticketListingCrawler.forceRecrawlWithPriority(ticketListingCrawls, TicketListingCrawl.PRIORITY_HIGH);
	                 
	                return msg;
	            }
	            catch (JMSException e)
	            {
	                e.printStackTrace();
	                return "Error";
	            }
	       }
	        return "no message";
	    }

		public Destination getDestination() {
			return destination;
		}

		public void setDestination(Destination destination) {
			this.destination = destination;
		}
		
		public void test(Message message){
			System.out.println("called");
		}
		public void onMessage(Message message) {
			 TextMessage textMessage = null;
		        if (message instanceof TextMessage)
		        {
		            textMessage = (TextMessage)message;
		            try
		            {
		                System.out.println("....... " + textMessage.getStringProperty("msg") + ".....................");
		                String msg = textMessage.getStringProperty("msg");
		            	TicketListingCrawler ticketListingCrawler=  SpringUtil.getTicketListingCrawler();
		                Integer eventId= Integer.parseInt(msg);
		                Collection<TicketListingCrawl> ticketListingCrawls = ticketListingCrawler.getTicketListingCrawlByEvent(eventId);
		                ticketListingCrawler.forceRecrawlWithPriority(ticketListingCrawls, TicketListingCrawl.PRIORITY_HIGH);
		                
		            }
		            catch (JMSException e)
		            {
		                e.printStackTrace();
		                System.out.println("....... " + "Error" + ".....................");
		            }
		       }
			
		}
}
