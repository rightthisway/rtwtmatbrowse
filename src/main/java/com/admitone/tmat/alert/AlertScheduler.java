package com.admitone.tmat.alert;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.velocity.tools.generic.NumberTool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

import com.admitone.tmat.common.MailIdProperty;
import com.admitone.tmat.crawler.CrawlerSchedulerManager;
import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.AlertTicketGroup;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Property;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.data.User;
import com.admitone.tmat.data.UserAlert;
import com.admitone.tmat.enums.AlertFor;
import com.admitone.tmat.enums.UserAlertStatus;
import com.admitone.tmat.enums.UserAlertType;
import com.admitone.tmat.ticketfetcher.TicketListingFetcher;
import com.admitone.tmat.utils.mail.MailManager;
import com.admitone.tmat.web.Constants;

public class AlertScheduler implements InitializingBean {
	private MailIdProperty mailIdProperty;

	public MailIdProperty getMailIdProperty() {
		return mailIdProperty;
	}

	public void setMailIdProperty(MailIdProperty mailIdProperty) {
		this.mailIdProperty = mailIdProperty;
	}
	
	private static final long DEFAULT_ALERT_TIMER_WAIT = 60L * 1000L;
	private String staticUrl;
	
	// this is to prevent the same alert to be executed multiple time at the same time
	private Set<Integer> processingAlertIds = new HashSet<Integer>();
	
	private MailManager mailManager;
	private CrawlerSchedulerManager crawlerSchedulerManager;
	private Logger logger = LoggerFactory.getLogger(AlertScheduler.class);
	
	private Map<String, TicketListingFetcher> ticketListingFetcherMap = new HashMap<String, TicketListingFetcher>();	

	public AlertScheduler() {
	}

	public void afterPropertiesSet() throws Exception {
		Timer timer = new Timer();
		//timer.scheduleAtFixedRate(new AlertTimerTask(mailIdProperty), new Date(), DEFAULT_ALERT_TIMER_WAIT);
		timer.scheduleAtFixedRate(new AlertTimerTask(mailIdProperty), 500, 1000);
	}

	private class AlertTimerTask extends TimerTask {
		public AlertTimerTask(MailIdProperty mailIdProperty) {
			super();
			this.mailIdProperty = mailIdProperty;
		}
		private MailIdProperty mailIdProperty;

		public MailIdProperty getMailIdProperty() {
			return mailIdProperty;
		}

		public void setMailIdProperty(MailIdProperty mailIdProperty) {
			this.mailIdProperty = mailIdProperty;
		}
				public void run() {
			try {
				
				Property alertThreadProperty = DAORegistry.getPropertyDAO().get("alert.thread.count");
				int availableThreadCount =5;
				if(null != alertThreadProperty){
					availableThreadCount = Integer.parseInt(alertThreadProperty.getValue());
				}
				
				ExecutorService executor = Executors.newFixedThreadPool(availableThreadCount);
				
				Date now = new Date();
				Collection<UserAlert> userAlerts = DAORegistry.getUserAlertDAO().getAllAlerts(UserAlertStatus.ACTIVE);
				
				for(UserAlert userAlert: userAlerts) {
					
					Event event =userAlert.getEvent();
					if(event==null){
						continue;
					}
					long crawlFrequency = crawlerSchedulerManager.getFrequency(event.getDate(),
							event.getArtist().getGrandChildTourCategory().getChildTourCategory().getTourCategory().getName());
					if (!processingAlertIds.contains(userAlert.getId()) && userAlert.getUserAlertStatus().equals(UserAlertStatus.ACTIVE) 
						&& ((userAlert.getLastCheck() == null) || (null == userAlert.getMailInterval())
							|| now.getTime() - userAlert.getLastCheck().getTime() > crawlFrequency * 1000L
							|| (userAlert.getLastEmailDate() != null
									&& (now.getTime() - userAlert.getLastEmailDate().getTime()>=24L*3600L*1000L
											&& now.getTime()-userAlert.getLastEmailDate().getTime()<36L*3600L*1000L
											&& userAlert.getLastCheck().getTime()<userAlert.getLastEmailDate().getTime()+(24L*3600L*1000L)
									||(now.getTime()-userAlert.getLastEmailDate().getTime()>=36L*3600L*1000L
											&& now.getTime()-userAlert.getLastEmailDate().getTime()<48L*3600L*1000L
											&& userAlert.getLastCheck().getTime()<userAlert.getLastEmailDate().getTime()+(36L*3600L*1000L)))
									)))
						
					 {
						processingAlertIds.add(userAlert.getId());
						executor.execute(new AlertThread(mailIdProperty,userAlert));
					} 
					
				}
				
				executor.shutdown();	
				executor.awaitTermination(30, TimeUnit.MINUTES);
				
			} catch (Exception e) {
				logger.error("ERROR IN ALERTSCHEDULER", e);
				e.printStackTrace();
			}
		}
	}
	
	
	class AlertThread implements Runnable {
		public AlertThread(MailIdProperty mailIdProperty, UserAlert userAlert) {
			super();
			this.mailIdProperty = mailIdProperty;
			this.userAlert = userAlert;
		}

		private MailIdProperty mailIdProperty;

		public MailIdProperty getMailIdProperty() {
			return mailIdProperty;
		}

		public void setMailIdProperty(MailIdProperty mailIdProperty) {
			this.mailIdProperty = mailIdProperty;
		}	

		private UserAlert userAlert;
		
		public AlertThread(UserAlert userAlert) {
			this.userAlert = userAlert;
		}

		public void run() {
			try {
				
				
				// tickets which are currently in TMAT (active tickets)
				// used when the first time the alert is processed.
				Collection<Ticket> currentTickets = null;	
				Collection<Ticket> inTickets = new ArrayList<Ticket>();
				Collection<Ticket> outTickets = new ArrayList<Ticket>();
				
				userAlert = DAORegistry.getUserAlertDAO().get(userAlert.getId());
				List<AlertTicketGroup> existingGroup = DAORegistry.getAlertTicketGroupDAO().getAlertTicketGroupsByAlertId(userAlert.getId());
				Map<Integer, AlertTicketGroup> existingGroupMap = new HashMap<Integer, AlertTicketGroup>();
				if(existingGroup!=null && !existingGroup.isEmpty()){
					for(AlertTicketGroup alertTicket:existingGroup){
						existingGroupMap.put(alertTicket.getTicket().getId(), alertTicket);
					}
				}
				Map<Integer, String> ticketUrlById = new HashMap<Integer, String>();
				Collection<AlertTicketGroup> toBeDeleted = new ArrayList<AlertTicketGroup>();
				List<AlertTicketGroup> toBeAdded = new ArrayList<AlertTicketGroup>();
				
				//   Tickets currently available in market. 
				currentTickets = DAORegistry.getTicketDAO().getAllTicketsMatchingUserAlert(userAlert);
				
				if (userAlert.getUserAlertType().equals(UserAlertType.OUT)) {
//					outTickets = new ArrayList<Ticket>();
					if(currentTickets!=null){
						for(Ticket ticket: currentTickets) {	
							TicketListingCrawl crawl = ticket.getTicketListingCrawl();
							String url = ticketListingFetcherMap.get(ticket.getSiteId()).getTicketItemUrl(crawl, ticket);
							if (url == null) {
								url = ticketListingFetcherMap.get(ticket.getSiteId()).getTicketListingUrl(crawl);
							}
							ticketUrlById.put(ticket.getId(), url);
							if(existingGroupMap.containsKey(ticket.getId())){
								existingGroupMap.remove(ticket.getId());
							}else{
//								userAlert.setLastCheck(now);
								outTickets.add(ticket);
								AlertTicketGroup  alertTicketGroup = new AlertTicketGroup();
								alertTicketGroup.setAlertId(userAlert.getId());
								alertTicketGroup.setTicket(ticket);
								toBeAdded.add(alertTicketGroup);
							}
						}						
					}
//					outTickets = currentTickets;
					currentTickets=null;
					if(inTickets.isEmpty()){
						inTickets=null;
					}
					if(outTickets.isEmpty()){
						outTickets= null;
					}
				}else if (userAlert.getUserAlertType().equals(UserAlertType.IN)) {
//					inTickets = new ArrayList<Ticket>();
//					outTickets = new ArrayList<Ticket>();
//					Date now = new Date();
					if(currentTickets!=null){
						for(Ticket ticket: currentTickets) {	
							TicketListingCrawl crawl = ticket.getTicketListingCrawl();
							String url = ticketListingFetcherMap.get(ticket.getSiteId()).getTicketItemUrl(crawl, ticket);
							if (url == null) {
								url = ticketListingFetcherMap.get(ticket.getSiteId()).getTicketListingUrl(crawl);
							}
							ticketUrlById.put(ticket.getId(), url);
							if(existingGroupMap.containsKey(ticket.getId())){
								existingGroupMap.remove(ticket.getId());
							}else{
//								userAlert.setLastCheck(now);
								inTickets.add(ticket);
								AlertTicketGroup  alertTicketGroup = new AlertTicketGroup();
								alertTicketGroup.setAlertId(userAlert.getId());
								alertTicketGroup.setTicket(ticket);
								toBeAdded.add(alertTicketGroup);
							}
						}
						currentTickets.removeAll(inTickets);
					}
					Collection<AlertTicketGroup> list = existingGroupMap.values();
					if(list!=null){
						for(AlertTicketGroup  alertTicketGroup:list){
							outTickets.add(alertTicketGroup.getTicket());	
						}
						toBeDeleted.addAll(list);
					}
					if(inTickets!=null && inTickets.isEmpty()){
						inTickets=null;
					}
					if(outTickets!=null && outTickets.isEmpty()){
						outTickets= null;
					}
					if(currentTickets!=null && currentTickets.isEmpty()){
						currentTickets=null;
					}
				}
				
				DAORegistry.getAlertTicketGroupDAO().saveOrUpdateAll(toBeAdded);
				DAORegistry.getAlertTicketGroupDAO().deleteAll(toBeDeleted);
				
				
				String fromName = "";
				String email = "";
				String subject = "";
				String resource;
				
				if(userAlert.getLastEmailDate()==null){
					
					 fromName = "";
					 email = "";
					 subject = "ALERT CREATED" + "(" + userAlert.getId() + ") for " + userAlert.getEvent().getName() + " - " + userAlert.getEvent().getFormattedEventDate();
					 resource="";
					
					if (userAlert.getAlertFor().equals(AlertFor.TMAT)) {
						User user = DAORegistry.getUserDAO().getUserByUsername(userAlert.getUsername());
						fromName = user.getFirstName().trim() + " " + user.getLastName().trim() + "(" + userAlert.getUsername().trim() + ") ";
						email = user.getEmail();
						resource =  "mail-ticket-alert-tmat.txt";						
					} else if (userAlert.getAlertFor().equals(AlertFor.CIRCLES)) {
						User user = DAORegistry.getUserDAO().getUserByUsername(userAlert.getUsername());
						fromName = user.getFirstName().trim() + " " + user.getLastName().trim() + "(" + userAlert.getUsername().trim() + ") ";
						email = user.getEmail();
						resource =  "mail-ticket-alert-circles.txt";
					} else if (userAlert.getAlertFor().equals(AlertFor.CIRCLES_PORTAL)) {
						fromName  = userAlert.getOrderedBy();
						resource =  "mail-ticket-alert-circles-portal.txt";
						email = "";
					} else { // should be defined
						logger.error("Alert " + userAlert + " has an undefined alertFor type");
						return;
					}
					String mimeType = "text/plain";
					
					DateFormat dateFormat =	new SimpleDateFormat("MM/dd/yyyy HH:mm");
	
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("userAlert", userAlert);
					map.put("now", dateFormat.format(new Date()));
					map.put("number", new NumberTool());
					map.put("currentTickets", null);
					map.put("inTickets", null);
					map.put("outTickets", null);
					map.put("ticketUrlById", ticketUrlById);
					map.put("serverName", Constants.getInstance().getHostName());
					
					if (userAlert.getLastCheck() != null) {
						map.put("lastCheck", dateFormat.format(userAlert.getLastCheck()));
					} else {
						map.put("lastCheck", "Alert Creation Intimation");					
					}
	
					Property alertMailCcPropertyDAO = DAORegistry.getPropertyDAO().get("alert.mail.cc");
					String mailCc = null;
					if (alertMailCcPropertyDAO != null) {
						mailCc = alertMailCcPropertyDAO.getValue();
					}

					Property alertMailBccPropertyDAO = DAORegistry.getPropertyDAO().get("alert.mail.bcc");
					String mailBcc = null;
					if (alertMailBccPropertyDAO != null) {
						mailBcc = alertMailBccPropertyDAO.getValue();
					}
					
					//check if UserAlert is for Circles Portal, and set the appropriate mailCC and mailBcc
					if(userAlert.getAlertFor().equals(AlertFor.CIRCLES_PORTAL)){
						Property alertPortalMailCcPropertyDAO = DAORegistry.getPropertyDAO().get("alert.portalmail.cc");
						if (alertPortalMailCcPropertyDAO != null) {
							mailCc = alertPortalMailCcPropertyDAO.getValue();
						}

						Property alertPortalMailBccPropertyDAO = DAORegistry.getPropertyDAO().get("alert.portalmail.bcc");
						if (alertPortalMailBccPropertyDAO != null) {
							mailBcc = alertPortalMailBccPropertyDAO.getValue();
						}
					}
					
					if ("1".equals(DAORegistry.getPropertyDAO().get("alert.mail.marketmaker.enabled").getValue())) {						
						String mmEmail;
						User marketMaker = DAORegistry.getMarketMakerEventTrackingDAO().getCurrentMarketMakerForEvent(userAlert.getEventId());
						if (marketMaker == null) {
							// default handler of the alert
							mmEmail = mailIdProperty.getBccList();//"araut@rightthisway.com";
						} else {
							mmEmail = marketMaker.getEmail();
							email += "," + mmEmail;
						}
						
						
					}
					
					mailManager.sendMail(fromName, email, mailCc, mailBcc, subject, resource, map, mimeType, null);
					logger.info("Successfully sending mail for alert:" + userAlert.getId() + "!");
					userAlert.setLastEmailDate(new Date());
					
				}else{
					if(null == userAlert.getMailInterval()){
						if((currentTickets != null && !currentTickets.isEmpty()) || (inTickets != null && !inTickets.isEmpty()) 
								|| (outTickets != null && !outTickets.isEmpty())){
							 fromName = "";
							 email = "";
							 subject = "FOUND ALERT" + "(" + userAlert.getId() + ") for " + userAlert.getEvent().getName() + " - " + userAlert.getEvent().getFormattedEventDate();
							 resource="";
							
							if (userAlert.getAlertFor().equals(AlertFor.TMAT)) {
								User user = DAORegistry.getUserDAO().getUserByUsername(userAlert.getUsername());
								fromName = user.getFirstName().trim() + " " + user.getLastName().trim() + "(" + userAlert.getUsername().trim() + ") ";
								email = user.getEmail();
								resource =  "mail-ticket-alert-tmat.txt";						
							} else if (userAlert.getAlertFor().equals(AlertFor.CIRCLES)) {
								User user = DAORegistry.getUserDAO().getUserByUsername(userAlert.getUsername());
								fromName = user.getFirstName().trim() + " " + user.getLastName().trim() + "(" + userAlert.getUsername().trim() + ") ";
								email = user.getEmail();
								resource =  "mail-ticket-alert-circles.txt";
							} else if (userAlert.getAlertFor().equals(AlertFor.CIRCLES_PORTAL)) {
								fromName  = userAlert.getOrderedBy();
								resource =  "mail-ticket-alert-circles-portal.txt";
								email = "";
							} else { // should be defined
								logger.error("Alert " + userAlert + " has an undefined alertFor type");
								return;
							}
							String mimeType = "text/plain";
							
							DateFormat dateFormat =	new SimpleDateFormat("MM/dd/yyyy HH:mm");
			
							Map<String, Object> map = new HashMap<String, Object>();
							map.put("userAlert", userAlert);
							map.put("now", dateFormat.format(new Date()));
							map.put("number", new NumberTool());
							map.put("currentTickets", currentTickets);
							map.put("inTickets", inTickets);
							map.put("outTickets", outTickets);
							map.put("ticketUrlById", ticketUrlById);
							map.put("serverName", Constants.getInstance().getHostName());
							
							if (userAlert.getLastCheck() != null) {
								map.put("lastCheck", dateFormat.format(userAlert.getLastCheck()));
							} else {
								map.put("lastCheck", "the beginning");					
							}
			
							Property alertMailCcPropertyDAO = DAORegistry.getPropertyDAO().get("alert.mail.cc");
							String mailCc = null;
							if (alertMailCcPropertyDAO != null) {
								mailCc = alertMailCcPropertyDAO.getValue();
							}

							Property alertMailBccPropertyDAO = DAORegistry.getPropertyDAO().get("alert.mail.bcc");
							String mailBcc = null;
							if (alertMailBccPropertyDAO != null) {
								mailBcc = alertMailBccPropertyDAO.getValue();
							}
							
							//check if UserAlert is for Circles Portal, and set the appropriate mailCC and mailBcc
							if(userAlert.getAlertFor().equals(AlertFor.CIRCLES_PORTAL)){
								Property alertPortalMailCcPropertyDAO = DAORegistry.getPropertyDAO().get("alert.portalmail.cc");
								if (alertPortalMailCcPropertyDAO != null) {
									mailCc = alertPortalMailCcPropertyDAO.getValue();
								}

								Property alertPortalMailBccPropertyDAO = DAORegistry.getPropertyDAO().get("alert.portalmail.bcc");
								if (alertPortalMailBccPropertyDAO != null) {
									mailBcc = alertPortalMailBccPropertyDAO.getValue();
								}
							}
							
							if ("1".equals(DAORegistry.getPropertyDAO().get("alert.mail.marketmaker.enabled").getValue())) {						
								String mmEmail;
								User marketMaker = DAORegistry.getMarketMakerEventTrackingDAO().getCurrentMarketMakerForEvent(userAlert.getEventId());
								if (marketMaker == null) {
									// default handler of the alert
									mmEmail =mailIdProperty.getBccList();// "araut@rightthisway.com";
								} else {
									mmEmail = marketMaker.getEmail();
									email += "," + mmEmail;
								}
								
							}
							
							mailManager.sendMail(fromName, email, mailCc, mailBcc, subject, resource, map, mimeType, null);
							logger.info("Successfully sending mail for alert:" + userAlert.getId() + "!");
							userAlert.setLastEmailDate(new Date());
							userAlert.setMailInterval(1);
						}
					}else{
						Date now = new Date();
						long difference = now.getTime()-userAlert.getLastEmailDate().getTime();
						if (difference>=72L*3600L*1000L){
							userAlert.setUserAlertStatus(UserAlertStatus.DISABLED);
						}
					}
					userAlert.setLastCheck(new Date());
				}							
				DAORegistry.getUserAlertDAO().update(userAlert);
						}catch (Exception e) {
				e.printStackTrace();
			} finally {
				processingAlertIds.remove(userAlert.getId());				
			}
		}
	}

	public void setMailManager(MailManager mailManager) {
		this.mailManager = mailManager;
	}

	public void setCrawlerSchedulerManager(
			CrawlerSchedulerManager crawlerSchedulerManager) {
		this.crawlerSchedulerManager = crawlerSchedulerManager;
	}

	public void setTicketListingFetcherMap(
			Map<String, TicketListingFetcher> ticketListingFetcherMap) {
		this.ticketListingFetcherMap = ticketListingFetcherMap;
	}
	
	public String getStaticUrl() {
		return staticUrl;
	}

	public void setStaticUrl(String staticUrl) {
		this.staticUrl = staticUrl;
	}
	
}
