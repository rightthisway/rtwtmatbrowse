package com.admitone.tmat.pojo;

import java.util.ArrayList;

import com.admitone.tmat.data.Ticket;

public class WSTicketWrapper {
	
	private Integer crawlerId;
	private Integer eventId;
	private ArrayList<Ticket> newTickets;
	private ArrayList<Ticket> updateTickets;
	
	public Integer getCrawlerId() {
		return crawlerId;
	}
	public void setCrawlerId(Integer crawlerId) {
		this.crawlerId = crawlerId;
	}
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	public ArrayList<Ticket> getNewTickets() {
		return newTickets;
	}
	public void setNewTickets(ArrayList<Ticket> newTickets) {
		this.newTickets = newTickets;
	}
	public ArrayList<Ticket> getUpdateTickets() {
		return updateTickets;
	}
	public void setUpdateTickets(ArrayList<Ticket> updateTickets) {
		this.updateTickets = updateTickets;
	}	
}