package com.admitone.tmat.pojo;

import java.util.List;

import com.admitone.tmat.data.MXPZonesTicket;

public class WSZonesWrapper {

		private Integer eventId;
		private List<MXPZonesTicket> zoneTickets;
		
		public Integer getEventId() {
			return eventId;
		}
		public void setEventId(Integer eventId) {
			this.eventId = eventId;
		}
		public List<MXPZonesTicket> getZoneTickets() {
			return zoneTickets;
		}
		public void setZoneTickets(List<MXPZonesTicket> zoneTickets) {
			this.zoneTickets = zoneTickets;
		}
		
		
	}