package com.admitone.tmat.outlier;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.velocity.tools.generic.NumberTool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Category;
import com.admitone.tmat.data.CategoryMapping;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Property;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.data.User;
import com.admitone.tmat.data.UserOutlier;
import com.admitone.tmat.data.UserOutlierCategoryDetails;
import com.admitone.tmat.enums.AlertFor;
import com.admitone.tmat.enums.EventStatus;
import com.admitone.tmat.enums.UserOutlierStatus;
import com.admitone.tmat.ticketfetcher.TicketListingFetcher;
import com.admitone.tmat.utils.TicketUtil;
import com.admitone.tmat.utils.mail.MailManager;
import com.admitone.tmat.web.Constants;

public class OutlierScheduler implements InitializingBean {
	protected static DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	protected static DateFormat timeFormat = new SimpleDateFormat("HH:mm zzz");
	protected static DateFormat dateFormat1 = new SimpleDateFormat("MM/dd/yyyy HH:mm zzz");
	private static final long DEFAULT_OUTLIER_TIMER_WAIT = 10L * 60L * 1000L;
	
	// this is to prevent the same Outlier to be executed multiple time at the same time
	private Set<Integer> processingOutlierIds = new HashSet<Integer>();
	
	private MailManager mailManager;
	private Logger logger = LoggerFactory.getLogger(OutlierScheduler.class);
	
	private Map<String, TicketListingFetcher> ticketListingFetcherMap = new HashMap<String, TicketListingFetcher>();	

	public OutlierScheduler() {
	}

	public void afterPropertiesSet() throws Exception {
		Timer timer = new Timer();
		timer.scheduleAtFixedRate(new OutlierTimerTask(), new Date(), DEFAULT_OUTLIER_TIMER_WAIT);
	}

	private class OutlierTimerTask extends TimerTask {		
		public void run() {
			try {
				
				Property alertThreadProperty = DAORegistry.getPropertyDAO().get("alert.thread.count");
				int availableThreadCount = Integer.parseInt(alertThreadProperty.getValue());
				
				ExecutorService executor = Executors.newFixedThreadPool(availableThreadCount);
				
				Date now = new Date();
				Collection<UserOutlier> userOutliers = DAORegistry.getUserOutlierDAO().getAllOutliers(UserOutlierStatus.ACTIVE);
				
				for(UserOutlier userOutlier: userOutliers) {
//					logger.info("OUTLIER SCHEDULER: Started Processing the outlier id:"+userOutlier.getId());
					Boolean isDeleteOutlier = Boolean.FALSE;
					
				   Collection<UserOutlierCategoryDetails> categoryPriceDetails = DAORegistry.getUserOutlierCategoryDetailsDAO().getAllOutlierCategoryPriceDetailsByOutlierId(userOutlier.getId());
					
				   Event event =userOutlier.getEvent();
				   if(event==null){
					continue;
				   }
				   
				   String  currentDateStr = dateFormat1.format(now);
				   String eventDateTimeStr =userOutlier.getEvent().getFormattedEventDate();
				   Date eventDate = new Date();
				   if(eventDateTimeStr.contains("TBD")){
					   eventDate = dateFormat.parse(eventDateTimeStr);
				   }else{
					   eventDate = dateFormat1.parse(eventDateTimeStr);
				   }
				   
				   Date currentDate = dateFormat1.parse(currentDateStr);
				   
				   if(eventDate.before(currentDate)){
					   isDeleteOutlier = Boolean.TRUE;
				   }
				   
				   if(!isDeleteOutlier && event.getVenueCategoryId() != null && 
						   (null != categoryPriceDetails && !categoryPriceDetails.isEmpty())){
					   
					   if(!event.getVenueCategoryId().equals(categoryPriceDetails.iterator().next().getVenueCategoryId())){
						   isDeleteOutlier = Boolean.TRUE;
					   }
				   }
				   if (!processingOutlierIds.contains(userOutlier.getId()) && 
							userOutlier.getUserOutlierStatus().equals(UserOutlierStatus.ACTIVE)){
						
						processingOutlierIds.add(userOutlier.getId());
						executor.execute(new OutlierThread(userOutlier,isDeleteOutlier,categoryPriceDetails));
					} 
//				   logger.info("OUTLIER SCHEDULER: Ended Processing the outlier id:"+userOutlier.getId());
				}
				
				executor.shutdown();	
				executor.awaitTermination(30, TimeUnit.MINUTES);
				
			} catch (Exception e) {
				logger.error("ERROR IN OUTLIERSCHEDULER", e);
				e.printStackTrace();
			}
		}
	}
	
	
	class OutlierThread implements Runnable {
		private UserOutlier userOutlier;
		private Boolean isDeletedOutlier;
		private Collection<UserOutlierCategoryDetails> categoryPriceDetails;
		
		public OutlierThread(UserOutlier userOutlier,Boolean isDeletedOutlier,Collection<UserOutlierCategoryDetails> categoryPriceDetails) {
			this.userOutlier = userOutlier;
			this.isDeletedOutlier = isDeletedOutlier;
			this.categoryPriceDetails = categoryPriceDetails;
		}
		

		public void run() {
			try {
				
				
				if(isDeletedOutlier){
					logger.info("OUTLIER SCHEDULER: Outlier is deleted..");
					DAORegistry.getUserOutlierCategoryDetailsDAO().deleteAllOutlierCatPriceDtlByOutlierID(userOutlier.getId());
					userOutlier.setUserOutlierStatus(UserOutlierStatus.DELETED);
					userOutlier.setLastCheck(new Date());	
				}else{
					Map<Integer, String> ticketUrlById = new HashMap<Integer, String>();
					Collection<Ticket> currentTickets = null;			
					String category = "";
					userOutlier = DAORegistry.getUserOutlierDAO().get(userOutlier.getId());
					
					// Tickets currently available in market. 
					try{
						logger.info("OUTLIER SCHEDULER: Outlier Logic Begins.....");
						currentTickets = fticket(userOutlier,categoryPriceDetails);
						logger.info("OUTLIER SCHEDULER: Outlier Logic Ends......");
					}catch(Exception e){
						logger.error("OUTLIER SCHEDULER: Error Occured While Getting tickets from TMAT...");
						e.printStackTrace();
					}

					if(currentTickets!=null){
						
						for(Ticket ticket: currentTickets) {	
							TicketListingCrawl crawl = ticket.getTicketListingCrawl();
							String url = ticketListingFetcherMap.get(ticket.getSiteId()).getTicketItemUrl(crawl, ticket);
							if (url == null) {
								url = ticketListingFetcherMap.get(ticket.getSiteId()).getTicketListingUrl(crawl);
							}
							ticketUrlById.put(ticket.getId(), url);
							
						}
						Event event = userOutlier.getEvent();
						Collection<Category> categories = new ArrayList<Category>();
						if(null != event.getVenueCategory()){
							 categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(event.getVenueCategory().getId());
						}else{
							categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenuCategoryIdsByArtistId(event.getArtistId(), EventStatus.ACTIVE);
						}
						int i=0;
						for (Category catObj : categories) {
							if(i==0){
								category =catObj.getSymbol(); 
							}else{
								category += ","+catObj.getSymbol();
							}
							i++;
						}
						userOutlier.setCategory(category);
					}
					
					String fromName = "";
					String email = "";
					String subject = "";
					String resource;
					
					if(userOutlier.getLastEmailDate()==null){
						
						 fromName = "";
						 email = "";
						 subject = "OUTLIER CREATED" + "(" + userOutlier.getId() + ") for " + userOutlier.getEvent().getName() + " - " + userOutlier.getEvent().getFormattedEventDate();
						 resource="";
						
						
						User user = DAORegistry.getUserDAO().getUserByUsername(userOutlier.getUsername());
						fromName = user.getFirstName().trim() + " " + user.getLastName().trim() + "(" + userOutlier.getUsername().trim() + ") ";
						email = user.getEmail();
						resource =  "mail-ticket-outlier-tmat.txt";						
						String mimeType = "text/plain";
						
						DateFormat dateFormat =	new SimpleDateFormat("MM/dd/yyyy HH:mm");
		
						Map<String, Object> map = new HashMap<String, Object>();
						map.put("userOutlier", userOutlier);
						map.put("now", dateFormat.format(new Date()));
						map.put("number", new NumberTool());
						map.put("currentTickets", null);
						map.put("ticketUrlById", ticketUrlById);
						map.put("serverName", Constants.getInstance().getHostName());
						
						if (userOutlier.getLastCheck() != null) {
							map.put("lastCheck", dateFormat.format(userOutlier.getLastCheck()));
						} else {
							map.put("lastCheck", "Outlier Created Successfully.");					
						}
		
						Property alertMailCcPropertyDAO = DAORegistry.getPropertyDAO().get("alert.mail.cc");
						String mailCc = null;
						if (alertMailCcPropertyDAO != null) {
							mailCc = alertMailCcPropertyDAO.getValue();
						}

						Property alertMailBccPropertyDAO = DAORegistry.getPropertyDAO().get("alert.mail.bcc");
						String mailBcc = null;
						if (alertMailBccPropertyDAO != null) {
							mailBcc = alertMailBccPropertyDAO.getValue();
						}
						
						//check if UserAlert is for Circles Portal, and set the appropriate mailCC and mailBcc
						if(userOutlier.getOutlierFor().equals(AlertFor.CIRCLES_PORTAL)){
							Property alertPortalMailCcPropertyDAO = DAORegistry.getPropertyDAO().get("alert.portalmail.cc");
							if (alertPortalMailCcPropertyDAO != null) {
								mailCc = alertPortalMailCcPropertyDAO.getValue();
							}

							Property alertPortalMailBccPropertyDAO = DAORegistry.getPropertyDAO().get("alert.portalmail.bcc");
							if (alertPortalMailBccPropertyDAO != null) {
								mailBcc = alertPortalMailBccPropertyDAO.getValue();
							}
						}
						
						if ("1".equals(DAORegistry.getPropertyDAO().get("alert.mail.marketmaker.enabled").getValue())) {						
							String mmEmail;
							User marketMaker = DAORegistry.getMarketMakerEventTrackingDAO().getCurrentMarketMakerForEvent(userOutlier.getEventId());
							if (marketMaker == null) {
								// default handler of the alert
								mmEmail = "";
							} else {
								mmEmail = marketMaker.getEmail();
								email += "," + mmEmail;
							}
							
						}
						
						mailManager.sendMail(fromName, email, mailCc, mailBcc, subject, resource, map, mimeType, null);
						logger.info("OUTLIER SCHEDULER: Outlier Creation confirmation mail sent successfully.." + userOutlier.getId() + "!");
						userOutlier.setLastEmailDate(new Date());
						
					}else{
						
							if((currentTickets != null && !currentTickets.isEmpty())){
								logger.info("OUTLIER SCHEDULER:"+currentTickets.size()+" Tickets found in TMAT for this outlier id : "+userOutlier.getId());
								 fromName = "";
								 email = "";
								 subject = "FOUND OUTLIER" + "(" + userOutlier.getId() + ") for " + userOutlier.getEvent().getName() + " - " + userOutlier.getEvent().getFormattedEventDate();
								 resource="";
								
								User user = DAORegistry.getUserDAO().getUserByUsername(userOutlier.getUsername());
								fromName = user.getFirstName().trim() + " " + user.getLastName().trim() + "(" + userOutlier.getUsername().trim() + ") ";
								email = user.getEmail();
								resource =  "mail-ticket-outlier-tmat.txt";						
								
								String mimeType = "text/plain";
								
								DateFormat dateFormat =	new SimpleDateFormat("MM/dd/yyyy HH:mm");
				
								Map<String, Object> map = new HashMap<String, Object>();
								map.put("userOutlier", userOutlier);
								map.put("now", dateFormat.format(new Date()));
								map.put("number", new NumberTool());
								map.put("currentTickets", currentTickets);
								map.put("ticketUrlById", ticketUrlById);
								map.put("serverName", Constants.getInstance().getHostName());
								
								if (userOutlier.getLastCheck() != null) {
									map.put("lastCheck", dateFormat.format(userOutlier.getLastCheck()));
								} else {
									map.put("lastCheck", "the beginning");					
								}
				
								Property alertMailCcPropertyDAO = DAORegistry.getPropertyDAO().get("alert.mail.cc");
								String mailCc = null;
								if (alertMailCcPropertyDAO != null) {
									mailCc = alertMailCcPropertyDAO.getValue();
								}

								Property alertMailBccPropertyDAO = DAORegistry.getPropertyDAO().get("alert.mail.bcc");
								String mailBcc = null;
								if (alertMailBccPropertyDAO != null) {
									mailBcc = alertMailBccPropertyDAO.getValue();
								}
								
								//check if UserAlert is for Circles Portal, and set the appropriate mailCC and mailBcc
								if(userOutlier.getOutlierFor().equals(AlertFor.CIRCLES_PORTAL)){
									Property alertPortalMailCcPropertyDAO = DAORegistry.getPropertyDAO().get("alert.portalmail.cc");
									if (alertPortalMailCcPropertyDAO != null) {
										mailCc = alertPortalMailCcPropertyDAO.getValue();
									}

									Property alertPortalMailBccPropertyDAO = DAORegistry.getPropertyDAO().get("alert.portalmail.bcc");
									if (alertPortalMailBccPropertyDAO != null) {
										mailBcc = alertPortalMailBccPropertyDAO.getValue();
									}
								}
								
								if ("1".equals(DAORegistry.getPropertyDAO().get("alert.mail.marketmaker.enabled").getValue())) {						
									String mmEmail;
									User marketMaker = DAORegistry.getMarketMakerEventTrackingDAO().getCurrentMarketMakerForEvent(userOutlier.getEventId());
									if (marketMaker == null) {
										// default handler of the alert
										mmEmail = "";
									} else {
										mmEmail = marketMaker.getEmail();
										email += "," + mmEmail;
									}
									
								}
								
								mailManager.sendMail(fromName, email, mailCc, mailBcc, subject, resource, map, mimeType, null);
								logger.info("OUTLIER SCHEDULER: Successfully sending mail for outlier:" + userOutlier.getId() + "!");
								userOutlier.setLastEmailDate(new Date());
								userOutlier.setMailInterval(1);
						}else{
							
							logger.info("OUTLIER SCHEDULER: There is no Tickets found in TMAT for this outlier id : "+userOutlier.getId());
						}
						userOutlier.setLastCheck(new Date());
					}
				}

				DAORegistry.getUserOutlierDAO().update(userOutlier);
						}catch (Exception e) {
				e.printStackTrace();
			} finally {
				processingOutlierIds.remove(userOutlier.getId());				
			}
		}
	}
	
	public Collection<Ticket> fticket(UserOutlier userOutlier,Collection<UserOutlierCategoryDetails> oldOutlierCatPriceDtl){
		Collection<Ticket> finalTickets = new ArrayList<Ticket>();
		try{
		Collection<Ticket> tickets=DAORegistry.getTicketDAO().getAllTicketsMatchingUserOutlier(userOutlier);
		if (tickets == null) {
			tickets = new ArrayList<Ticket>();
		}	
		Event event = DAORegistry.getEventDAO().get(userOutlier.getEventId());
		Collection<Category> categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(event.getVenueCategoryId());		
		Map<Integer, Category> categoryById = new HashMap<Integer, Category>();
		if (categories != null) {
			for(Category category:categories) {
				categoryById.put(category.getId(), category);
			}
		}
		Map<Integer,List<CategoryMapping>> catMappingMap = new HashMap<Integer, List<CategoryMapping>>();
		Collection<CategoryMapping> categoryMappingList = DAORegistry.getCategoryMappingDAO().getAllCategoryMappingsByVenueCategoryId(event.getVenueCategoryId());
		for(CategoryMapping mapping:categoryMappingList){
			List<CategoryMapping> list =catMappingMap.get(mapping.getId());
			if(list==null){
				list = new ArrayList<CategoryMapping>();
			}
			list.add(mapping);
			catMappingMap.put(mapping.getId(), list);
		}
		Map<String, Ticket> catTicketMap = new HashMap<String, Ticket>();
		
		Collection<Double> priceList = new ArrayList<Double>();
		Collection<UserOutlierCategoryDetails> updateOutlierCatList = new ArrayList<UserOutlierCategoryDetails>();
		Collection<UserOutlierCategoryDetails> saveOutlierCatList = new ArrayList<UserOutlierCategoryDetails>();
		Collection<UserOutlierCategoryDetails> equalOutlierCatList = new ArrayList<UserOutlierCategoryDetails>();
		
		UserOutlierCategoryDetails outlierCatPriceDtlObj = new UserOutlierCategoryDetails();
		Date curDate = new Date();
		Integer eventId =userOutlier.getEventId();
		Integer outlierId =userOutlier.getId();
		Integer venueId =userOutlier.getEvent().getVenueId();
		Integer venueCatId =userOutlier.getEvent().getVenueCategoryId();
		Float outlierPerc =userOutlier.getOutlierPercentage();
		
		Boolean isNullPriceFlag = Boolean.TRUE;
		if(null != oldOutlierCatPriceDtl && !oldOutlierCatPriceDtl.isEmpty()){
			isNullPriceFlag = Boolean.FALSE;
		}
		
		Collection<Ticket> categorizedTickets= new ArrayList<Ticket>();
		
		TicketUtil.preAssignCategoriesToTickets(tickets, categories); 
		
		for(Ticket ticket: tickets) {
			if(null != ticket.getCategoryMappingId() && !ticket.getCategoryMappingId().equals("")){
				categorizedTickets.add(ticket);
			}
		}
		
		for (Category categoryObj : categories) {
			priceList = new ArrayList<Double>();
			catTicketMap = new HashMap<String, Ticket>();
			
			for(Ticket ticket: categorizedTickets) {
				Category category =ticket.getCategory();
				if(null != category && categoryObj.getId().equals(category.getId())){
					priceList.add(ticket.getBuyItNowPrice());
					catTicketMap.put(categoryObj.getId()+"-"+ticket.getBuyItNowPrice(), ticket);
				}
			}
			
			if(null != priceList && priceList.size()>2){		
				Double minPrice = Collections.min(priceList);
				Ticket ticketObj = catTicketMap.get(categoryObj.getId()+"-"+minPrice);
				outlierCatPriceDtlObj = new UserOutlierCategoryDetails();
				Double [] price = priceList.toArray(new Double[priceList.size()]);
				Arrays.sort(price);
				Double first_cheapest=price[0];
				Double second_cheapest=price[1];
				Boolean isNewCatFlag=true;				
				if(!isNullPriceFlag){		
					for (UserOutlierCategoryDetails outlierCatDtlOld : oldOutlierCatPriceDtl) {						
						if(categoryObj.getId().equals(outlierCatDtlOld.getCategoryId())){							
							Double outlierAmt=first_cheapest+(first_cheapest*outlierPerc/100);
							Double first_cheapest_history=outlierCatDtlOld.getFirstcheapestamount();
							Double second_cheapest_history = outlierCatDtlOld.getSecondcheapestamount();
							double fch =first_cheapest_history.doubleValue();
							double fc =first_cheapest.doubleValue();
							double sch =second_cheapest_history.doubleValue();
							double sc =second_cheapest.doubleValue();
							if((fch!=fc) || (sch!=sc)){
								int count=0;
								outlierCatDtlOld.setCategory(categoryObj.getSymbol());
								outlierCatDtlOld.setCategoryGroupName(categoryObj.getGroupName());
								outlierCatDtlOld.setCategoryId(categoryObj.getId());
								outlierCatDtlOld.setEventId(eventId);
								outlierCatDtlOld.setOutlierId(outlierId);
								outlierCatDtlOld.setVenueCategoryId(venueCatId);
								outlierCatDtlOld.setVenueId(venueId);
								outlierCatDtlOld.setFirstcheapestamount(minPrice);
								outlierCatDtlOld.setSecondcheapestamount(second_cheapest);
								outlierCatDtlOld.setTicketId(ticketObj.getId());
								outlierCatDtlOld.setCount(count);
								DAORegistry.getUserOutlierCategoryDetailsDAO().update(outlierCatDtlOld);
							}
							int count=outlierCatDtlOld.getCount()==null?0:outlierCatDtlOld.getCount();
							if(count<=1){
								if(outlierAmt<=second_cheapest){
								count=2;
								outlierCatDtlOld.setLastUpdate(curDate);
								outlierCatDtlOld.setFirstcheapestamount(minPrice);
								outlierCatDtlOld.setTicketId(ticketObj.getId());
								outlierCatDtlOld.setCount(count);
								updateOutlierCatList.add(outlierCatDtlOld);
							}
							}
							else{								
							if((fch!=fc) || (sch!=sc)){
							if(outlierAmt<=second_cheapest){
								outlierCatDtlOld.setLastUpdate(curDate);
								outlierCatDtlOld.setFirstcheapestamount(minPrice);
								outlierCatDtlOld.setTicketId(ticketObj.getId());
								updateOutlierCatList.add(outlierCatDtlOld);
							}
						}
							}
							isNewCatFlag =false;
						}
					}					
					if(isNewCatFlag){
						outlierCatPriceDtlObj.setCategory(categoryObj.getSymbol());
						outlierCatPriceDtlObj.setCategoryGroupName(categoryObj.getGroupName());
						outlierCatPriceDtlObj.setCategoryId(categoryObj.getId());
						outlierCatPriceDtlObj.setEventId(eventId);
						outlierCatPriceDtlObj.setOutlierId(outlierId);
						outlierCatPriceDtlObj.setVenueCategoryId(venueCatId);
						outlierCatPriceDtlObj.setVenueId(venueId);
						outlierCatPriceDtlObj.setFirstcheapestamount(minPrice);
						outlierCatPriceDtlObj.setSecondcheapestamount(second_cheapest);
						outlierCatPriceDtlObj.setTicketId(ticketObj.getId());
						saveOutlierCatList.add(outlierCatPriceDtlObj);
					}
				}else{
					int count=1;
					outlierCatPriceDtlObj.setCategory(categoryObj.getSymbol());
					outlierCatPriceDtlObj.setCategoryGroupName(categoryObj.getGroupName());
					outlierCatPriceDtlObj.setCategoryId(categoryObj.getId());
					outlierCatPriceDtlObj.setEventId(eventId);
					outlierCatPriceDtlObj.setOutlierId(outlierId);
					outlierCatPriceDtlObj.setVenueCategoryId(venueCatId);
					outlierCatPriceDtlObj.setVenueId(venueId);
					outlierCatPriceDtlObj.setFirstcheapestamount(minPrice);
					outlierCatPriceDtlObj.setSecondcheapestamount(second_cheapest);
					outlierCatPriceDtlObj.setTicketId(ticketObj.getId());
					outlierCatPriceDtlObj.setCount(count);
					saveOutlierCatList.add(outlierCatPriceDtlObj);
				}
			}
		}				
		if(null != equalOutlierCatList && !equalOutlierCatList.isEmpty()){
			oldOutlierCatPriceDtl.removeAll(equalOutlierCatList);
			if(null != oldOutlierCatPriceDtl && !oldOutlierCatPriceDtl.isEmpty())
				DAORegistry.getUserOutlierCategoryDetailsDAO().deleteAll(oldOutlierCatPriceDtl);
		}	
		if(null != updateOutlierCatList && !updateOutlierCatList.isEmpty()){	
			for (UserOutlierCategoryDetails outlierCatObj : updateOutlierCatList) {
				finalTickets.add(outlierCatObj.getTicket());
			}		
			DAORegistry.getUserOutlierCategoryDetailsDAO().updateAll(updateOutlierCatList);					
		}
		if(null != saveOutlierCatList && !saveOutlierCatList.isEmpty()){
			DAORegistry.getUserOutlierCategoryDetailsDAO().saveAll(saveOutlierCatList);
		}
		}catch(Exception e){
			e.printStackTrace();
		}
		return finalTickets;
	}

	public void setMailManager(MailManager mailManager) {
		this.mailManager = mailManager;
	}

	
	public void setTicketListingFetcherMap(
			Map<String, TicketListingFetcher> ticketListingFetcherMap) {
		this.ticketListingFetcherMap = ticketListingFetcherMap;
	}
	
}
