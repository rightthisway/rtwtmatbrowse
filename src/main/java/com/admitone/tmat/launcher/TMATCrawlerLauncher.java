package com.admitone.tmat.launcher;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

public class TMATCrawlerLauncher {
	public static final int BLOCK_SIZE = 100000;

	public static void printUsage() {
		System.out.println("Usage: java com.admitone.tmat.launcher.TMATCrawlerLauncher <jar file> <server 1>:<port 1> <server 2>:<port 2> ...");
	}
	
	public static void sendFile(String filename, String hostname, Integer port) {
		Socket socket = null;
		System.out.print("Sending " + filename + " to " + hostname + ":" + port + " [");
		try {
			BufferedInputStream is = new BufferedInputStream(new FileInputStream(filename));
			socket = new Socket(hostname, port);
			OutputStream os = socket.getOutputStream();
		    byte[] buffer = new byte[BLOCK_SIZE];
		    int len = 0;
		    int i = 0;
		    do {
		    	len = is.read(buffer);
		    	if (len <= 0) {
		    		break;
		    	}
		    	
		    	os.write(buffer, 0, len);
		    	if (i++ % 100 == 0) {
		    		System.out.print(".");
		    	}
		    } while(len == BLOCK_SIZE);
			socket.close();
			os.close();
			System.out.println("] [DONE]");
		} catch (Exception e) {
			System.out.println("] [ERROR]");
			System.out.println(">> Problem while transferring " + filename + " to " + hostname + ":" + port);
			System.out.println(e.fillInStackTrace());
			if (socket != null) {
				if (!socket.isClosed()) {
					try {
						socket.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			}
		}
	}
	
	public static void main(String[] args) throws Exception {
		if (args.length < 2) {
			printUsage();
			return;
		}

		String filename = args[0];
		for (int i = 1; i < args.length; i++) {
			String[] tokens = args[i].split(":");
			String hostname = tokens[0];
			Integer port = Integer.valueOf(tokens[1]);
			sendFile(filename, hostname, port);
		}
	}
}
