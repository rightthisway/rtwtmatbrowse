package com.admitone.tmat.launcher;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Quick launcher that will take care of receiving the tmat jar and start it 
 * @author frad
 *
 */
public class TMATCrawlerLauncherServer {
	public static final int BLOCK_SIZE = 300000;
	public static final int MAX_MEMORY = 12288; // in MB
	public static boolean running;
	
	public static void printUsage() {
		System.out.println("Usage: java com.admitone.tmat.launcher.TMATCrawlerLauncherServer <port> ...");		
	}
	
	public static void consumeInputs(InputStream is, String filename) {
		FileOutputStream fout = null;
		
		try {
			fout = new FileOutputStream(filename, true);
			
			BufferedInputStream bis = new BufferedInputStream(is);
			
			byte[] buffer = new byte[BLOCK_SIZE];
			while (running) {
				try {
					int len = bis.read(buffer);
					if (len == 0) {
						Thread.sleep(1000);
					} else {
						System.out.println("The length is: " + len + " filename is: " + filename);
						fout.write(buffer, 0, len);
					}
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { 
				if (fout != null) {
					fout.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		System.out.println("end stream...");
	}
	
	public static void main(String[] args) throws Exception {
		if (args.length < 1) {
			printUsage();
			return;
		}
		
		int port = Integer.valueOf(args[0]);
		
		System.out.println("Server started. Listening on port " + port + "...");
		ServerSocket serverSocket = new ServerSocket(port);
		while (true) {
			System.out.println("Waiting for incoming connections...");
			// we don't fork as we don't have multiple connections
			Socket socket = serverSocket.accept();
			InputStream is = socket.getInputStream();
		    FileOutputStream fos = new FileOutputStream("tmat.jar");
		    byte[] buffer = new byte[BLOCK_SIZE];
		    int len = 0;
		    System.out.print("Receving tmat.jar [");
		    int i = 0;
		    do {
		    	len = is.read(buffer);
		    	if (len <= 0) {
		    		break;
		    	}

		    	if (i % 100 == 0) {
		    		System.out.print(".");
		    	}
		    	fos.write(buffer, 0, len);
		    } while(len > 0);
		    System.out.println("] [DONE]");
			Thread.sleep(1000);
			fos.close();
			is.close();
			socket.close();
			String cmd = "java -Xms512m -Xmx" + MAX_MEMORY + "m -jar tmat.jar start";
			System.out.println("Executing command: " + cmd);
			final Process process = Runtime.getRuntime().exec(cmd);
			running = true;
			Thread threadOut = new Thread(new Runnable() {
				public void run() {
					System.out.println("In run of crawler-out.log " + process.toString());
					consumeInputs(process.getInputStream(), "crawler-out.log");
				}
			});
			threadOut.start();
			
			Thread threadErr = new Thread(new Runnable() {
				public void run() {
					System.out.println("In run of crawler-err.log " + process.toString());
					consumeInputs(process.getErrorStream(), "crawler-err.log");
				}
			});
			threadErr.start();

			process.waitFor();
			running = false;
			threadOut.interrupt();
			threadErr.interrupt();
			System.out.println("Application stopped...");
			// TODO launch the app in the background
		}
	}
}
