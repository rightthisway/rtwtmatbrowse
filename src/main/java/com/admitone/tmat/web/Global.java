package com.admitone.tmat.web;

import java.util.ArrayList;
import java.util.Collection;

import com.admitone.tmat.crawler.TicketListingCrawlStat;
import com.admitone.tmat.utils.SpringUtil;

public final class Global {
	public Integer getNumNodes() {
		return SpringUtil.getTicketListingCrawler().getTicketListingCrawlDispatcher().getNumNodes();
	}
	
	public Collection<Node> getNodes() {
		Collection<Node> nodes = new ArrayList<Node>();
		for (TicketListingCrawlStat stat: SpringUtil.getTicketListingCrawler().getTicketListingCrawlDispatcher().getStatMap().values()) {
			Node node = new Node(stat.getNodeId(), stat.getHostname());
			nodes.add(node);
		}
		return nodes;
	}	
	
	public static class Node {
		private Integer id;
		private String name;
		
		public Node(Integer id, String name) {
			this.id = id;
			this.name = name;
		}

		public Integer getId() {
			return id;
		}

		public String getName() {
			return name;
		}
	}
}
