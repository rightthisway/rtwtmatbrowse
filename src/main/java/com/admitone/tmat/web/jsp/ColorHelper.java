package com.admitone.tmat.web.jsp;

public class ColorHelper {
	public static String getMarginBackgroundColor(Double margin) {
		if (margin > .25) {
			return "green";
		} else if (margin > .19) {
			return "#99ff66";
		} else if (margin > .15) {
			return "yellow";
		} else if (margin > .10) {
			return "#ffffcc";
		} else if (margin >= 0) {
			return "white";
		} else if (margin > -.11) {
			return "#ff66ff";
		} else if (margin > -.26) {
			return "#ff3366";
		} else {
			return "red";
		}
		
	}
}
