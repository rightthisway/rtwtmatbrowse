package com.admitone.tmat.web;

import java.io.IOException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.acegisecurity.ui.webapp.AuthenticationProcessingFilter;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.servlet.view.RedirectView;

import com.admitone.tmat.crawler.CrawlerSchedulerManager;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Category;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.data.Artist;
import com.admitone.tmat.data.User;
import com.admitone.tmat.data.UserOutlier;
import com.admitone.tmat.data.UserOutlierCategoryDetails;
import com.admitone.tmat.enums.AlertFor;
import com.admitone.tmat.enums.EventStatus;
import com.admitone.tmat.enums.UserOutlierStatus;
import com.admitone.tmat.outlier.OutlierScheduler;
import com.admitone.tmat.utils.PreferenceManager;
import com.admitone.tmat.utils.mail.MailManager;

public class OutlierController extends MultiActionController {
	
	private PreferenceManager preferenceManager;
	private CrawlerSchedulerManager crawlerSchedulerManager;	
//	private OutlierScheduler outlierScheduler;
	private MailManager mailManager; 
	
	
	
	
	public ModelAndView loadEditOutlierPage(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		// check if this is the page for my Outlier
		// or the page for "manage user Outliers"
		String manageOutliersUrl;
		ModelAndView mav;

		boolean modeAllUser = request.getRequestURI().contains("EditorEditOutlier");
		if (modeAllUser) {
			manageOutliersUrl = "EditorManageOutliers";
			mav = new ModelAndView("page-editor-edit-outlier");
		} else {
			manageOutliersUrl = "MyOutliers";			
			mav = new ModelAndView("page-edit-my-outlier");
		}

		String categoryGroupName = request.getParameter("alertCategoryGroupName");

		String action = request.getParameter("action");
		String username = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);
		User user = DAORegistry.getUserDAO().getUserByUsername(username);

		if ("save".equalsIgnoreCase(action)) {
			
			Collection<Integer> eventIds = new ArrayList<Integer>();
			
			Map<Integer, String> percMap = new HashMap<Integer, String>();
			
			for (Object param: request.getParameterMap().keySet()) {
				if (!param.toString().startsWith("event_")) {
					continue;
				}
				String eventIdParam = param.toString().replace("event_", "");
				
				String perc = request.getParameter("outlierPerc_"+eventIdParam);
				Integer eventId = Integer.valueOf(eventIdParam);
				
				percMap.put(eventId, perc);
				eventIds.add(eventId);
			}
			String outlierIdString = request.getParameter("outlierId");
			UserOutlier userOutlier = null;
			Integer userOutlierId = null;
			if(outlierIdString != null && !outlierIdString.isEmpty()) {
				userOutlierId = Integer.parseInt(outlierIdString);
				userOutlier = DAORegistry.getUserOutlierDAO().get(userOutlierId);			
			}

			Boolean newOutlier;
			if(outlierIdString == null || outlierIdString.isEmpty()) {
				userOutlier = null;
				newOutlier = true;
			} else {
				Integer id = Integer.parseInt(outlierIdString);
				userOutlier = DAORegistry.getUserOutlierDAO().get(id);
				if (!modeAllUser && !userOutlier.getUsername().equals(username) && !userOutlier.getMarketMakerUsername().equals(username)) {
					return new ModelAndView(new RedirectView(manageOutliersUrl + "?error=" + URLEncoder.encode("You cannot edit another user's Outlier")));
				}
				newOutlier = false;
			}
						
			

			if (newOutlier) {
				for(Integer eventId:eventIds)
				{
					userOutlier = null;
					
					try{
						userOutlier= createUserOutlierFromInput(userOutlier, request, newOutlier);
						
						String perc =percMap.get(eventId);
						
						perc = perc.replace(" ", "");
						userOutlier.setOutlierPercentage(Float.valueOf(perc));
						
						userOutlier.setEventId(eventId);
						userOutlier.setUsername(username);				
						DAORegistry.getUserOutlierDAO().save(userOutlier);
					}catch(Exception e){
						throw e;
					}
					
				}
				return new ModelAndView(new RedirectView(manageOutliersUrl + "?info=" + URLEncoder.encode("The Outlier has been created")));
			} else {
				userOutlier = createUserOutlierFromInput(userOutlier, request, newOutlier);
				DAORegistry.getUserOutlierDAO().update(userOutlier);				
				return new ModelAndView(new RedirectView(manageOutliersUrl + "?info=" + URLEncoder.encode("The Outlier has been updated")));
			}


		} else {
			String outlierIdString = request.getParameter("outlierId");
			UserOutlier userOutlier = null;
			Integer userOutlierId = null;
			if(outlierIdString != null && !outlierIdString.isEmpty()) {
				userOutlierId = Integer.parseInt(outlierIdString);
				userOutlier = DAORegistry.getUserOutlierDAO().get(userOutlierId);			
			}			

			if (!modeAllUser && userOutlier != null && !userOutlier.getUsername().equals(username)) {
				return new ModelAndView(new RedirectView(manageOutliersUrl + "?error=" + URLEncoder.encode("You cannot edit another user's outlier")));
			}
			Integer artistId = null;
			Integer eventId= null;
			if(userOutlier == null)
			{
				String artistIdString = request.getParameter("outlierArtistId");
				try {
					artistId = Integer.parseInt(artistIdString);
				} catch(Exception e) {
				};
			}else{
				eventId = userOutlier.getEventId();
			}
			Event event = null;
			Artist artist = null;
			List<String> categoryGroupNames = new ArrayList<String>();
			Collection<Category> categories = null;
			String categorySymbol = null;
			Collection<Event> eventList = null;
			if(eventId != null){
				event = DAORegistry.getEventDAO().get(eventId);	
				if(event.getVenueCategory()!=null){
					categoryGroupNames.add(event.getVenueCategory().getCategoryGroup());
				}
//				categoryGroupNames = Categorizer.getCategoryGroupsByEvent(eventId);
				if(categoryGroupNames != null && categoryGroupName == null) {
					// take the first one
					if (categoryGroupNames.size() > 0) {
						categoryGroupName = categoryGroupNames.get(0);
					}
				}
				if (categoryGroupName != null && !categoryGroupName.isEmpty()) {
					categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueIdAndCategoryGroup(event.getVenueId(), categoryGroupName);
				}
			}else if(artistId != null) {
				artist = DAORegistry.getArtistDAO().get(artistId);	
				
				//categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenuCategoryIdsByTourId(tourId, EventStatus.ACTIVE);
				
				eventList = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(artistId);
				
			}
			String[] siteIds;

			if (userOutlier != null) {
				
				siteIds = userOutlier.getSiteIds();
			} else {
				
				siteIds = Constants.getInstance().getSiteIds();
			}

			Map<String, Boolean> siteSelected = new HashMap<String, Boolean>();
			for(String siteId: siteIds) {
				siteSelected.put(siteId, true);
			}
			mav.addObject("siteSelected", siteSelected);				

			
			mav.addObject("categorySymbol", categorySymbol);
			//mav.addObject("eventId", eventId);
			mav.addObject("userOutlier", userOutlier);
			mav.addObject("artist", artist);
			mav.addObject("event", event);
			mav.addObject("categoryGroupName", categoryGroupName);
			mav.addObject("categoryGroupNames", categoryGroupNames);		
			mav.addObject("categories", categories);

			mav.addObject("user", user);
			mav.addObject("events", eventList);
			if(null != eventList && !eventList.isEmpty()){
				mav.addObject("eventSize", eventList.size());
			}
			mav.addObject("userOutlier", userOutlier);
			mav.addObject("mode", modeAllUser?"editAll":"editMine");
			
			return mav;		
		}

	}
	
	
	/**
	 * Add alerts page.
	 */
	public ModelAndView loadManageUserOutliersPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		//UserOutlier outlier = DAORegistry.getUserOutlierDAO().get(5);
		//Collection<Ticket> tickets=DAORegistry.getTicketDAO().getAllTicketsMatchingUserOutlier(outlier);
		// check if this is the page for my alert
		// or the page for "manage user alerts"
		String manageOutliersUrl;
		boolean modeAllUser = request.getRequestURI().contains("EditorManageOutliers");
		String username = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);
		
		ModelAndView mav;
		if (modeAllUser) {
			manageOutliersUrl = "EditorManageOutliers";
			mav = new ModelAndView("page-editor-manage-outliers");			
			mav.addObject("users", DAORegistry.getUserDAO().getAll());

			// the key is status + "-" + AlertFor (e.g. ACTIVE-TMAT)
			Map<String, Integer> statByKey = new HashMap<String, Integer>();

			int alertCount = 0;
			
			Collection<UserOutlier> allUserOutliers = DAORegistry.getUserOutlierDAO().getAll();
			
			for(UserOutlier ua: allUserOutliers) {
				String key = ua.getUserOutlierStatus() + "-" + AlertFor.TMAT;
				Integer count = statByKey.get(key);
				count = (count == null)?1:(count+1);
				statByKey.put(key, count);

				String totalRowKey = ua.getUserOutlierStatus() + "-TOTAL";
				Integer totalRowCount = statByKey.get(totalRowKey);
				totalRowCount = (totalRowCount == null)?1:(totalRowCount+1);
				statByKey.put(totalRowKey, totalRowCount);

				String totalColKey = "TOTAL-" + AlertFor.TMAT;
				Integer totalColCount = statByKey.get(totalColKey);
				totalColCount = (totalColCount == null)?1:(totalColCount+1);
				statByKey.put(totalColKey, totalColCount);
				
				alertCount++;
			}
			
			statByKey.put("TOTAL-TOTAL", alertCount);
			mav.addObject("statByKey", statByKey);		
		} else {
			manageOutliersUrl = "MyOutliers";			
			mav = new ModelAndView("page-manage-my-outlier");			
		}


		String action = request.getParameter("action");
				
		
		if (action == null) {
			action = "";
		} else if (action.equalsIgnoreCase("delete")) {
			Collection<UserOutlier> selectedOutliers = new ArrayList<UserOutlier>();
			for(String selectedOutlierId: request.getParameter("outlierIds").split(",")) {
				int outlierId = Integer.parseInt(selectedOutlierId);
				UserOutlier selectedOutlier = DAORegistry.getUserOutlierDAO().get(outlierId);
				if(selectedOutlier == null) {
					return new ModelAndView(new RedirectView(manageOutliersUrl + "?error=" + URLEncoder.encode("Invalid Outlier Id " + outlierId)));				
				}
				if (!modeAllUser && !selectedOutlier.getUsername().equals(username) && !selectedOutlier.getMarketMakerUsername().equals(username)) {
					return new ModelAndView(new RedirectView(manageOutliersUrl + "?error=" + URLEncoder.encode("You cannot view another user's outlier")));
				}
				selectedOutliers.add(selectedOutlier);
			}
			for(UserOutlier selectedOutlier: selectedOutliers) {
				selectedOutlier.setUserOutlierStatus(UserOutlierStatus.DELETED);
				DAORegistry.getUserOutlierDAO().update(selectedOutlier);				
			}
			return new ModelAndView(new RedirectView(manageOutliersUrl + "?info=" + URLEncoder.encode("The " + selectedOutliers.size()+ " selected outlier(s) have been deleted")));
		} else if (action.equalsIgnoreCase("check")) {
			String outlierIdString = request.getParameter("outlierId");
			UserOutlier userOutlier = null;
			Integer userOutlierId = null;
			if(outlierIdString != null && !outlierIdString.isEmpty()) {
				userOutlierId = Integer.parseInt(outlierIdString);
				userOutlier = DAORegistry.getUserOutlierDAO().get(userOutlierId);			
			}			
			if(userOutlier == null) {
				return new ModelAndView(new RedirectView(manageOutliersUrl + "?error=" + URLEncoder.encode("Invalid outlier id")));				
			}

			if (!modeAllUser && !userOutlier.getUsername().equals(username) && !userOutlier.getMarketMakerUsername().equals(username)) {
				return new ModelAndView(new RedirectView(manageOutliersUrl + "?error=" + URLEncoder.encode("You cannot edit another user's outlier")));
			}
			Collection<UserOutlierCategoryDetails> categoryPriceDetails=null;
			
			if(null != userOutlier){
				categoryPriceDetails = DAORegistry.getUserOutlierCategoryDetailsDAO().getAllOutlierCategoryPriceDetailsByOutlierId(userOutlier.getId());
			}
			
			Collection<Ticket> outlierTickets = DAORegistry.getTicketDAO().getAllTicketsMatchingUserOutlier(userOutlier);
			mav.addObject("userOutlier", userOutlier);
			mav.addObject("outlierTickets", outlierTickets);
		} else if (action.equalsIgnoreCase("disable")) {
			Collection<UserOutlier> selectedOutliers = new ArrayList<UserOutlier>();
			for(String selectedOutlierId: request.getParameter("outlierIds").split(",")) {
				int outlierId = Integer.parseInt(selectedOutlierId);
				UserOutlier selectedOutlier = DAORegistry.getUserOutlierDAO().get(outlierId);
				if(selectedOutlier == null) {
					return new ModelAndView(new RedirectView(manageOutliersUrl + "?error=" + URLEncoder.encode("Invalid alert Id " + outlierId)));				
				}
				if (!modeAllUser && !selectedOutlier.getUsername().equals(username) && !selectedOutlier.getMarketMakerUsername().equals(username)) {
					return new ModelAndView(new RedirectView(manageOutliersUrl + "?error=" + URLEncoder.encode("You cannot view another user's outlier")));
				}
				selectedOutliers.add(selectedOutlier);
			}
			for(UserOutlier selectedOutlier: selectedOutliers) {
				selectedOutlier.setUserOutlierStatus(UserOutlierStatus.DISABLED);
				DAORegistry.getUserOutlierDAO().update(selectedOutlier);				
			}
			return new ModelAndView(new RedirectView(manageOutliersUrl + "?info=" + URLEncoder.encode("The " + selectedOutliers.size()+ " selected outlier(s) have been disabled")));
		} else if (action.equalsIgnoreCase("enable")) {
			Collection<UserOutlier> selectedOutliers = new ArrayList<UserOutlier>();
			for(String selectedOutlierId: request.getParameter("outlierIds").split(",")) {
				int outlierId = Integer.parseInt(selectedOutlierId);
				UserOutlier selectedOutlier = DAORegistry.getUserOutlierDAO().get(outlierId);
				if(selectedOutlier== null) {
					return new ModelAndView(new RedirectView(manageOutliersUrl + "?error=" + URLEncoder.encode("Invalid outlier Id " + outlierId)));				
				}
				if (!modeAllUser && !selectedOutlier.getUsername().equals(username) && !selectedOutlier.getMarketMakerUsername().equals(username)) {
					return new ModelAndView(new RedirectView(manageOutliersUrl + "?error=" + URLEncoder.encode("You cannot view another user's outlier")));
				}
				selectedOutliers.add(selectedOutlier);
			}
			for(UserOutlier selectedOutlier: selectedOutliers) {
				selectedOutlier.setUserOutlierStatus(UserOutlierStatus.ACTIVE);
				DAORegistry.getUserOutlierDAO().update(selectedOutlier);				
			}
			return new ModelAndView(new RedirectView(manageOutliersUrl + "?info=" + URLEncoder.encode("The " + selectedOutliers.size()+ " selected outlier(s) have been enabled")));
		} 
		//Shorting an alert - circles alert on shorting should become wholesale alert
		else if (action.equalsIgnoreCase("short")) {
		}
		
		String filterArtistIdString = request.getParameter("filterArtistId");
		if (filterArtistIdString == null) {
			filterArtistIdString = preferenceManager.getPreferenceValue(username, "outlierArtistId", "");
		}

		Integer filterArtistId = null;
		try {
			filterArtistId = Integer.parseInt(filterArtistIdString);
		} catch (Exception e) {
			filterArtistIdString = "";
		}
		
		preferenceManager.updatePreference(username, "outlierArtistId", filterArtistIdString);


		String filterEventIdString = request.getParameter("filterEventId");
		if (filterEventIdString == null) {
			filterEventIdString = preferenceManager.getPreferenceValue(username, "outlierEventId", "");
		}

		Integer filterEventId = null;
		if (filterArtistId != null) {
			try {
				filterEventId = Integer.parseInt(filterEventIdString);
			} catch (Exception e) {
				filterEventIdString = "";
			}
		} else {
			filterEventIdString = "";			
		}
		String outlierUsername = null;
		if (modeAllUser) {
			String filterUsername = request.getParameter("filterUsername");
			if (filterUsername == null) {
				filterUsername = preferenceManager.getPreferenceValue(username, "outlierUsername", "");
			}

			preferenceManager.updatePreference(username, "outlierUsername", filterUsername);
			
			if (!filterUsername.isEmpty()) {
				outlierUsername = filterUsername;
			}
			
			mav.addObject("filterUsername", filterUsername);

		} else {
			outlierUsername = username;			
		}

		preferenceManager.updatePreference(username, "outlierEventId", filterEventIdString);
				
		//
		// VIEW: CREATED, MARKETMAKER OR BOTH
		// 
		String view = request.getParameter("view");
		if (view == null) {
			view = preferenceManager.getPreferenceValue(username, "outlierView", null);
		} else {
			preferenceManager.updatePreference(username, "outlierView", view);			
		}

		
		
		
		// INCLUDE DISABLED ALERTS
		String includeDisabledOutliersString = request.getParameter("filterIncludeDisabledOutliers");
		if (includeDisabledOutliersString == null) {
			includeDisabledOutliersString = preferenceManager.getPreferenceValue(username, "outlierIncludeDisabledOutliers", "0");
		} 
		
		Boolean includeDisabledOutliers = false;
		if (includeDisabledOutliersString != null && includeDisabledOutliersString.equals("1")) {
			includeDisabledOutliers = true;
		} 
		preferenceManager.updatePreference(username, "outlierIncludeDisabledOutliers", includeDisabledOutliers?"1":"0");
		
		UserOutlierStatus filterUserOutlierStatus = null;
		if (!includeDisabledOutliers) {
			filterUserOutlierStatus = UserOutlierStatus.ACTIVE;
		}

		Map<Integer, UserOutlier> userOutlierById = new HashMap<Integer, UserOutlier>();
		if (view == null || view.isEmpty() || view.equals("created")) {
			Collection<UserOutlier> creatorUserOutliers;
			if (filterEventId == null) {
				creatorUserOutliers = DAORegistry.getUserOutlierDAO().getOutliersByUsernameAndArtistId(outlierUsername, filterArtistId, filterUserOutlierStatus);
			} else {
				creatorUserOutliers = DAORegistry.getUserOutlierDAO().getOutliersByUsernameAndEventId(outlierUsername, filterEventId, filterUserOutlierStatus);
			}
			if (creatorUserOutliers != null) {
				for(UserOutlier userOutlier: creatorUserOutliers) {
					userOutlierById.put(userOutlier.getId(), userOutlier);
				}
			}
		}

		if (view == null || view.isEmpty() || view.equals("marketmaker")) {
			Collection<UserOutlier> marketMakerUserOutliers;
			if (filterEventId == null) {
				marketMakerUserOutliers = DAORegistry.getUserOutlierDAO().getOutliersByMarketMakerAndArtistId(outlierUsername, filterArtistId, filterUserOutlierStatus);
			} else {
				marketMakerUserOutliers = DAORegistry.getUserOutlierDAO().getOutliersByMarketMakerAndEventId(outlierUsername, filterEventId, filterUserOutlierStatus);				
			}
			if (marketMakerUserOutliers != null) {
				for(UserOutlier userOutlier: marketMakerUserOutliers) {
					userOutlierById.put(userOutlier.getId(), userOutlier);
				}
			}
		}
		Map<Integer, Integer> frequencyByOutlierId = new HashMap<Integer, Integer>();
		for(UserOutlier forOutlier: userOutlierById.values()) {
			if (forOutlier.getEvent()!=null){
			frequencyByOutlierId.put(forOutlier.getId(), crawlerSchedulerManager.getFrequency(forOutlier.getEvent().getDate(),
					forOutlier.getEvent().getArtist().getGrandChildTourCategory().getChildTourCategory().getTourCategory().getName()));
			}
		}
		
		Collection<Event> events = null;
		if (filterArtistId != null) {
			events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(filterArtistId);
		}else{
			events = DAORegistry.getEventDAO().getAllActiveEvents();
		}

		mav.addObject("includeDisabledOutliers", includeDisabledOutliers);
		mav.addObject("view", view);
		mav.addObject("OutlierFor", AlertFor.TMAT);
		 
		mav.addObject("events", events);
		mav.addObject("frequencyByOutlierId", frequencyByOutlierId);
		mav.addObject("outliers", userOutlierById.values());
		mav.addObject("filterArtistId", filterArtistId);
		mav.addObject("filterEventId", filterEventId);
		mav.addObject("artists", DAORegistry.getArtistDAO().getAllActiveArtists());
		mav.addObject("username", username);	
		mav.addObject("mode", modeAllUser?"editAll":"editMine");
		return mav;
	}
	
	public ModelAndView loadOutlierPreviewPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		UserOutlier userOutlier = createUserOutlierFromInput(null, request,false);
		Collection<UserOutlierCategoryDetails> categoryPriceDetails=null;
		
		if(null != userOutlier){
			categoryPriceDetails = DAORegistry.getUserOutlierCategoryDetailsDAO().getAllOutlierCategoryPriceDetailsByOutlierId(userOutlier.getId());
		}
//		System.out.println("USEROUTLIER=" + userOutlier);

		ModelAndView mav = new ModelAndView("page-editor-edit-outlier-preview");
		mav.addObject("userOutlier", userOutlier);
		mav.addObject("outlierTickets", DAORegistry.getTicketDAO().getAllTicketsMatchingUserOutlier(userOutlier));
		return mav;		
	}
	
	/*public void getCategoryDetails(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try{
			Integer venueCategoryId = Integer.parseInt(request.getParameter("venueCatId"));
			Collection<Category> categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(venueCategoryId);
			PrintWriter out = response.getWriter();
			JSONArray categoryDetails = new JSONArray();
			JSONObject jObj = null;
			Boolean isFirst=true;
			for(Category category : categories) {
				jObj = new JSONObject();
				if(isFirst){
					
					VenueCategory venueCategory = DAORegistry.getVenueCategoryDAO().get(category.getVenueCategoryId());
					String venueImgUrl ="VenueImage?venueId="+venueCategory.getVenue().getId()+"&catScheme="+category.getGroupName();
					jObj.put("venueImgUrl", venueImgUrl);
				}
                jObj.put("id", category.getId());
                jObj.put("symbol", category.getSymbol());
                categoryDetails.put(jObj);
                isFirst = false;
			}
	        out.println(categoryDetails.toString());
		}catch (Exception e) {
			// TODO: handle exception
		}
	}
*/
	

	private UserOutlier createUserOutlierFromInput(UserOutlier userOutlier, 
			HttpServletRequest request,boolean newOutlier) {
	//Input the user alert values into the user_alert table	
		String eventIdString = request.getParameter("outlierEventId");
	//	String categoryGroupName = request.getParameter("alertCategoryGroupName");
		
		String outlierStatusString = request.getParameter("outlierStatus");
		String outlierPerc = request.getParameter("outlierPercentage");
		String description = request.getParameter("outlierDescription");
		
		//Alert Status
		if (userOutlier == null) {
			userOutlier = new UserOutlier();
			userOutlier.setUserOutlierStatus(UserOutlierStatus.ACTIVE);
			userOutlier.setCreationDate(new Date());
		}else{
			UserOutlierStatus newOutlierStatus = UserOutlierStatus.valueOf(outlierStatusString);

			if (!newOutlierStatus.equals(UserOutlierStatus.DISABLED)){	
				userOutlier.setLastEmailDate(null);
				userOutlier.setLastCheck(null);
			}
			userOutlier.setMailInterval(null);
			userOutlier.setUserOutlierStatus(newOutlierStatus);
			if(null != outlierPerc && outlierPerc.trim().length() >0){
				userOutlier.setOutlierPercentage(Float.valueOf(outlierPerc.trim()));
			}
			
		}

		if(!newOutlier)
			userOutlier.setEventId(Integer.parseInt(eventIdString));
		
		userOutlier.setDescription(description);
		
		Collection<String> siteIds = new ArrayList<String>();
		for(String siteId: Constants.getInstance().getSiteIds()) {
			if ("on".equals(request.getParameter("site-" + siteId))) {
				siteIds.add(siteId);
			}
		}
		userOutlier.setSiteIds(siteIds.toArray(new String[siteIds.size()]));
		
		return userOutlier;
	}
	

	
	/*public void setOutlierScheduler(OutlierScheduler outlierScheduler) {
		this.outlierScheduler = outlierScheduler;
	}*/

	public PreferenceManager getPreferenceManager() {
		return preferenceManager;
	}

	public void setPreferenceManager(PreferenceManager preferenceManager) {
		this.preferenceManager = preferenceManager;
	}	
	
	public void setCrawlerSchedulerManager(
			CrawlerSchedulerManager crawlerSchedulerManager) {
		this.crawlerSchedulerManager = crawlerSchedulerManager;
	}
	
	

	public void setMailManager(MailManager mailManager) {
		this.mailManager = mailManager;
	}

	public void afterPropertiesSet() throws Exception {
		/*if (outlierScheduler == null) {
			throw new Exception("The property outlierScheduler must be set");
		}*/
	
		if (preferenceManager == null) {
			throw new RuntimeException("Property preferenceManager must be set");
		}

		if (mailManager == null) {
			throw new RuntimeException("Property mailManager must be set");
		}
}
	
}
