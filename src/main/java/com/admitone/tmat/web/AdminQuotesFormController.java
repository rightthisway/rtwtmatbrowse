package com.admitone.tmat.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.acegisecurity.context.SecurityContextHolder;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Property;
import com.admitone.tmat.data.User;

/**
 * Class which handles the AdminEditMailerProperties Form.
 */
public class AdminQuotesFormController extends SimpleFormController {
	
	@Override
	protected  Object formBackingObject(HttpServletRequest request) {
		QuotesPropertiesCommand quotesPropertiesCommand = new QuotesPropertiesCommand();

		quotesPropertiesCommand.setSmtpHost(DAORegistry.getPropertyDAO().get("quotes.smtp.host").getValue());
		quotesPropertiesCommand.setSmtpUsername(DAORegistry.getPropertyDAO().get("quotes.smtp.username").getValue());
		quotesPropertiesCommand.setSmtpPassword(DAORegistry.getPropertyDAO().get("quotes.smtp.password").getValue());
		quotesPropertiesCommand.setSmtpAuth(Boolean.parseBoolean(DAORegistry.getPropertyDAO().get("quotes.smtp.auth").getValue()));

		quotesPropertiesCommand.setSmtpSecureConnection(DAORegistry.getPropertyDAO().get("quotes.smtp.secure.connection").getValue());

		quotesPropertiesCommand.setHeader(DAORegistry.getPropertyDAO().get("quotes.header").getValue());
		quotesPropertiesCommand.setFooter(DAORegistry.getPropertyDAO().get("quotes.footer").getValue());

		int smtpPort = 25;
		try {
			smtpPort = Integer.parseInt(DAORegistry.getPropertyDAO().get("quotes.smtp.port").getValue());
		} catch (Exception e) {
			e.printStackTrace();
		}
		quotesPropertiesCommand.setSmtpPort(smtpPort);

		quotesPropertiesCommand.setSmtpFrom(DAORegistry.getPropertyDAO().get("quotes.smtp.from").getValue());

		return quotesPropertiesCommand;
	}

	@Override
	protected ModelAndView onSubmit(Object command) {
		QuotesPropertiesCommand quotesPropertiesCommand = (QuotesPropertiesCommand)command;

		Property smtpHostProperty = DAORegistry.getPropertyDAO().get("quotes.smtp.host");
		smtpHostProperty.setValue(quotesPropertiesCommand.getSmtpHost());
		DAORegistry.getPropertyDAO().update(smtpHostProperty);

		Property smtpPortProperty = DAORegistry.getPropertyDAO().get("quotes.smtp.port");
		smtpPortProperty.setValue(Integer.toString(quotesPropertiesCommand.getSmtpPort()));
		DAORegistry.getPropertyDAO().update(smtpPortProperty);

		Property smtpUsernameProperty = DAORegistry.getPropertyDAO().get("quotes.smtp.username");
		smtpUsernameProperty.setValue(quotesPropertiesCommand.getSmtpUsername());
		DAORegistry.getPropertyDAO().update(smtpUsernameProperty);

		Property smtpPasswordProperty = DAORegistry.getPropertyDAO().get("quotes.smtp.password");
		smtpPasswordProperty.setValue(quotesPropertiesCommand.getSmtpPassword());
		DAORegistry.getPropertyDAO().update(smtpPasswordProperty);

		Property smtpFromProperty = DAORegistry.getPropertyDAO().get("quotes.smtp.from");
		smtpFromProperty.setValue(quotesPropertiesCommand.getSmtpFrom());
		DAORegistry.getPropertyDAO().update(smtpFromProperty);

		Property smtpAuthProperty = DAORegistry.getPropertyDAO().get("quotes.smtp.auth");
		smtpAuthProperty.setValue(Boolean.toString(quotesPropertiesCommand.isSmtpAuth()));
		DAORegistry.getPropertyDAO().update(smtpAuthProperty);

		Property headerProperty = DAORegistry.getPropertyDAO().get("quotes.header");
		headerProperty.setValue(quotesPropertiesCommand.getHeader());
		DAORegistry.getPropertyDAO().update(headerProperty);

		Property footerProperty = DAORegistry.getPropertyDAO().get("quotes.footer");
		footerProperty.setValue(quotesPropertiesCommand.getFooter());
		DAORegistry.getPropertyDAO().update(footerProperty);

		Property smtpSecureConnectionProperty = DAORegistry.getPropertyDAO().get("quotes.smtp.secure.connection");
		smtpSecureConnectionProperty.setValue(quotesPropertiesCommand.getSmtpSecureConnection());
		DAORegistry.getPropertyDAO().update(smtpSecureConnectionProperty);

		return new ModelAndView(new RedirectView("AdminQuotes?info=The mailer properties had been saved"));
	}
	
	@Override
	protected ModelAndView showForm(HttpServletRequest request,
			HttpServletResponse response, BindException errors, Map controlModel)
			throws Exception {
		ModelAndView mav = super.showForm(request, response, errors, controlModel);
		
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		User user = DAORegistry.getUserDAO().getUserByUsername(username);

		mav.addObject("userEmail", user.getEmail());
		return mav;
	}	

}
