package com.admitone.tmat.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.admitone.tmat.jms.JMSMessageReceiver;
import com.admitone.tmat.jms.JMSMessageSender;

public class JMSStartController extends  MultiActionController {

	private JMSMessageSender sender ;
	private JMSMessageReceiver receiver ;
	
	public ModelAndView sendMessage(HttpServletRequest request,
			HttpServletResponse response){
		System.out.println("Hello");
		ModelAndView mav = new ModelAndView("page-jms");
		String name = request.getParameter("name");
		if (name==null){
			name ="default";
		}
		try {
			sender.broadCastMessage("msg",name);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		model.addAttribute("var", name);
		return mav;
		
	}

	public ModelAndView getMessage(HttpServletRequest request,
			HttpServletResponse response){
		ModelAndView mav = new ModelAndView("page-jms");
		String name = "dd";
		try {
//			name = receiver.receiveMessage();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		model.addAttribute("var", "response: " + name);
		System.out.println(name);
		return mav;
		
	}

	public JMSMessageSender getSender() {
		return sender;
	}

	public void setSender(JMSMessageSender sender) {
		this.sender = sender;
	}

	public JMSMessageReceiver getReceiver() {
		return receiver;
	}

	public void setReceiver(JMSMessageReceiver receiver) {
		this.receiver = receiver;
	}

}
