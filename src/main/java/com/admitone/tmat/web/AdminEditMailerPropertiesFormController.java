package com.admitone.tmat.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.acegisecurity.context.SecurityContextHolder;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Property;
import com.admitone.tmat.data.User;

/**
 * Class which handles the AdminEditMailerProperties Form.
 */
public class AdminEditMailerPropertiesFormController extends SimpleFormController {
	
	@Override
	protected  Object formBackingObject(HttpServletRequest request) {
		MailerPropertiesCommand mailerPropertiesCommand = new MailerPropertiesCommand();

		mailerPropertiesCommand.setSmtpHost(DAORegistry.getPropertyDAO().get("smtp.host").getValue());
		mailerPropertiesCommand.setSmtpUsername(DAORegistry.getPropertyDAO().get("smtp.username").getValue());
		mailerPropertiesCommand.setSmtpPassword(DAORegistry.getPropertyDAO().get("smtp.password").getValue());
		mailerPropertiesCommand.setSmtpAuth(Boolean.parseBoolean(DAORegistry.getPropertyDAO().get("smtp.auth").getValue()));

		mailerPropertiesCommand.setSmtpSecureConnection(DAORegistry.getPropertyDAO().get("smtp.secure.connection").getValue());
		
		int smtpPort = 25;
		try {
			smtpPort = Integer.parseInt(DAORegistry.getPropertyDAO().get("smtp.port").getValue());
		} catch (Exception e) {
			e.printStackTrace();
		}
		mailerPropertiesCommand.setSmtpPort(smtpPort);

		mailerPropertiesCommand.setSmtpFrom(DAORegistry.getPropertyDAO().get("smtp.from").getValue());

		return mailerPropertiesCommand;
	}

	@Override
	protected ModelAndView onSubmit(Object command) {
		MailerPropertiesCommand mailerPropertiesCommand = (MailerPropertiesCommand)command;

		Property smtpHostProperty = DAORegistry.getPropertyDAO().get("smtp.host");
		smtpHostProperty.setValue(mailerPropertiesCommand.getSmtpHost());
		DAORegistry.getPropertyDAO().update(smtpHostProperty);

		Property smtpPortProperty = DAORegistry.getPropertyDAO().get("smtp.port");
		smtpPortProperty.setValue(Integer.toString(mailerPropertiesCommand.getSmtpPort()));
		DAORegistry.getPropertyDAO().update(smtpPortProperty);

		Property smtpUsernameProperty = DAORegistry.getPropertyDAO().get("smtp.username");
		smtpUsernameProperty.setValue(mailerPropertiesCommand.getSmtpUsername());
		DAORegistry.getPropertyDAO().update(smtpUsernameProperty);

		Property smtpPasswordProperty = DAORegistry.getPropertyDAO().get("smtp.password");
		smtpPasswordProperty.setValue(mailerPropertiesCommand.getSmtpPassword());
		DAORegistry.getPropertyDAO().update(smtpPasswordProperty);

		Property smtpFromProperty = DAORegistry.getPropertyDAO().get("smtp.from");
		smtpFromProperty.setValue(mailerPropertiesCommand.getSmtpFrom());
		DAORegistry.getPropertyDAO().update(smtpFromProperty);

		Property smtpAuthProperty = DAORegistry.getPropertyDAO().get("smtp.auth");
		smtpAuthProperty.setValue(Boolean.toString(mailerPropertiesCommand.isSmtpAuth()));
		DAORegistry.getPropertyDAO().update(smtpAuthProperty);

		Property smtpSecureConnectionProperty = DAORegistry.getPropertyDAO().get("smtp.secure.connection");
		smtpSecureConnectionProperty.setValue(mailerPropertiesCommand.getSmtpSecureConnection());
		DAORegistry.getPropertyDAO().update(smtpSecureConnectionProperty);

		return new ModelAndView(new RedirectView("AdminEditMailerProperties?info=The mailer properties had been saved"));
	}
	
	@Override
	protected ModelAndView showForm(HttpServletRequest request,
			HttpServletResponse response, BindException errors, Map controlModel)
			throws Exception {
		ModelAndView mav = super.showForm(request, response, errors, controlModel);
		
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		User user = DAORegistry.getUserDAO().getUserByUsername(username);

		mav.addObject("userEmail", user.getEmail());
		return mav;
	}	

}
