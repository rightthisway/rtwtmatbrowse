package com.admitone.tmat.web.pojo;

public class EIMPTicketComparator {
	private String section;
	private String row;
	private String seat;
	private double price;
	private boolean EIMPmissing=false;
	private boolean EIBOXmissing=false;;
	private int quantity;
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public String getRow() {
		return row;
	}
	public void setRow(String row) {
		this.row = row;
	}
	public String getSeat() {
		return seat;
	}
	public void setSeat(String seat) {
		this.seat = seat;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public boolean isEIMPmissing() {
		return EIMPmissing;
	}
	public void setEIMPmissing(boolean EIMPmissing) {
		this.EIMPmissing = EIMPmissing;
	}
	public boolean isEIBOXmissing() {
		return EIBOXmissing;
	}
	public void setEIBOXmissing(boolean EIBOXmissing) {
		this.EIBOXmissing = EIBOXmissing;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
}
