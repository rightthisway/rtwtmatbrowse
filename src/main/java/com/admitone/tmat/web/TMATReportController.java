package com.admitone.tmat.web;

import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.json.JSONException;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.admitone.tmat.common.MailIdProperty;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.pojo.PosEvent;
import com.admitone.tmat.pojo.ShortLongTicketGroup;
import com.admitone.tmat.pojo.ShortTicketGroup;

/*
 * Web Controller to handle pages related to the autoPricing
 */
public class TMATReportController extends MultiActionController {

	private MailIdProperty mailIdProperty;

	public MailIdProperty getMailIdProperty() {
		return mailIdProperty;
	}

	public void setMailIdProperty(MailIdProperty mailIdProperty) {
		this.mailIdProperty = mailIdProperty;
	}

	
	public ModelAndView tmatReport(HttpServletRequest request,
			HttpServletResponse response, ModelMap map) throws JSONException {
		ModelAndView mav = new ModelAndView("page-tmat-report-home");
		return mav;
	}
	
	public void downloadPosUnfilledShortswithLongInventory(HttpServletRequest request,
			HttpServletResponse response, ModelMap map) throws Exception {
		
	    //response.setContentType("text/csv");
	    //ModelAndView mav = new ModelAndView("page-tmat-admin-posinventory-report");
         
        DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		ServletOutputStream out = response.getOutputStream();
		
		 response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		// response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.setHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.setHeader("Content-disposition",
				"attachment; filename=PosUnfilledShortswithPossibleLongInventory." + today
						+ ".xlsx");
		
         //response.setHeader("Expires:", "0"); // eliminates browser caching

		try {
			 List<PosEvent> posEvents = DAORegistry.getQueryManagerDAO().getPOSAllSoldShortEvents();
			 
			 /*Event Name	Event Date	Venue Name		Short Section	Short Row	Short qty	Short Price		
			 Long Section	Long Row	Long Qty	Long Retail Price	Long WholeSale Price	Long Face Price	Long Cost
			 */
			 
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet("Short_Invoice_with_Possible_Long");
			int j = 0;
			Row rowhead =   ssSheet.createRow((int)0);
		    rowhead.createCell((int) j).setCellValue("Event Name");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Event Date");
		    j++;
	    	rowhead.createCell((int) j).setCellValue("Venue Name");
	    	j++;
	    	rowhead.createCell((int) j).setCellValue("");
	 	    j++;
	    	rowhead.createCell((int) j).setCellValue("Short Section");
	    	j++;
		    rowhead.createCell((int) j).setCellValue("Short Row");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Short Qty");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Short Sold Price");
		    j++;
		    rowhead.createCell((int) j).setCellValue("");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Long Section");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Long Row");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Long Qty");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Long Retail Price");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Long Wholesale Price");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Long Face Price");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Long Cost");
		    j++;
		    
		    DateFormat xlDateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		    //DateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			 int rowvariable = 1;
			 for (PosEvent event : posEvents) {
				 
				 List<ShortTicketGroup> shortTicketGroups = new ArrayList<ShortTicketGroup>();//DAORegistry.getQueryManagerDAO().getPOSShortTicketsByEvent(event.getId());
				 List<ShortLongTicketGroup> longTicketGroups = new ArrayList<ShortLongTicketGroup>();//DAORegistry.getQueryManagerDAO().getPOSLongTicketsByEvent(event.getId());
				 
				 //System.out.println("Short Tickets =====>"+shortTicketGroups.size());
				 //System.out.println("Long Tickets =====>"+longTicketGroups.size());
				 
				 if(null == longTicketGroups || longTicketGroups.isEmpty() || longTicketGroups.size() ==0 ){
					 continue;
				 }
				 
				 Map<Integer, ShortTicketGroup> shortMap = new HashMap<Integer, ShortTicketGroup>();
				 int i=1;
				 for (ShortTicketGroup shortTicketGroup : shortTicketGroups) {
					 shortMap.put(i, shortTicketGroup);
					 i++;
				 }
				 
				 Map<Integer, ShortLongTicketGroup> longMap = new HashMap<Integer, ShortLongTicketGroup>();
				 i=1;
				 for (ShortLongTicketGroup longTicketGroup : longTicketGroups) {
					 longMap.put(i, longTicketGroup);
					 i++;
				 }
				 
				 
				 int rowSize = shortTicketGroups.size();
				 if(null != longTicketGroups && !longTicketGroups.isEmpty() &&  longTicketGroups.size() > rowSize){
					 rowSize =longTicketGroups.size();
				 }
				 
				 int eventRow = 0;
				 
				 //System.out.println("rowSize===>"+rowSize);
				 
				 
				 for (int k = 1; k <= rowSize; k++) {
					
					 ShortTicketGroup shortTicketGroup = shortMap.remove(k);
					 ShortLongTicketGroup longTicketGroup = longMap.remove(k);
					 
					 
					 String eventName ="",venueName="",eventDate="",shortBreak="",shortSection="",
					 shortRow="",shortQty="",shortSoldPrice="",longBreak="",longSection="",longRow="",longQty="",
					 longRetailsPrice="",longWholPrice="",longFacePrice="",longcost="";
					 
					 if(eventRow == 0){
						 eventName = event.getEventName();
						 venueName = event.getVenueName();
						 //eventDate = xlDateFormat.format(dbDateTimeFormat.parse(event.getEventDateStr()));
						 eventDate = xlDateFormat.format(event.getEventDate());
					 }
					 eventRow++;
					 
					 if(null != shortTicketGroup){
						 shortSection = shortTicketGroup.getSection();
						 shortRow = shortTicketGroup.getRow();
						 shortQty = String.valueOf(shortTicketGroup.getQty());
						 shortSoldPrice = String.valueOf(shortTicketGroup.getActualSoldPrice());
					 }
					 
					 if(null != longTicketGroup){
						 longSection = longTicketGroup.getSection();
						 longRow = longTicketGroup.getRow();
						 longQty = String.valueOf(longTicketGroup.getRemainingTicketCount());
						 longRetailsPrice = String.valueOf(longTicketGroup.getRetailPrice());
						 longWholPrice = String.valueOf(longTicketGroup.getWholesalePrice());
						 longFacePrice = String.valueOf(longTicketGroup.getFacePrice());
						 longcost = String.valueOf(longTicketGroup.getCost());
					 }
					 
					// System.out.println("Row====>"+rowvariable);
					 
					 
					int column = 0;
					Row row = ssSheet.createRow(rowvariable);
			        row.createCell((int) column).setCellValue(eventName.replaceAll(",", "-"));
			        column++;
			        row.createCell((int) column).setCellValue(eventDate.replaceAll(",", "-"));
			        column++;
		        	row.createCell((int) column).setCellValue(venueName.replaceAll(",", "-"));
			    	column++;
			    	row.createCell((int) column).setCellValue(shortBreak);
			    	column++;
			    	
			    	
			    	Cell shortSectionCell = row.createCell(column,Cell.CELL_TYPE_STRING);
			    	shortSectionCell.setCellValue(shortSection.replaceAll(",", "-"));
			        column++;
			        
			        Cell shortRowCell = row.createCell(column,Cell.CELL_TYPE_STRING);
			        shortRowCell.setCellValue(shortRow.replaceAll(",", "-"));
			        column++;
			        
			        row.createCell((int) column).setCellValue(shortQty.replaceAll(",", "-"));
			        column++;
			        row.createCell((int) column).setCellValue(shortSoldPrice.replaceAll(",", "-"));
			        column++;
			        row.createCell((int) column).setCellValue(longBreak);
			        column++;
			        
			        Cell rowCell = row.createCell(column,Cell.CELL_TYPE_STRING);
			        rowCell.setCellValue(longSection.replaceAll(",", "-"));
			        column++;
			        
			        Cell seatCell = row.createCell(column,Cell.CELL_TYPE_STRING);
			        seatCell.setCellValue(longRow.replaceAll(",", "-"));
			        column++;
			        
			        row.createCell((int) column).setCellValue(longQty);
			        column++;
			        
			        row.createCell((int) column).setCellValue(longRetailsPrice);
			        column++;
			        
			        row.createCell((int) column).setCellValue(longWholPrice);
			        column++;
			        
			        row.createCell((int) column).setCellValue(longFacePrice);
			        column++;
			        
			        row.createCell((int) column).setCellValue(longcost);
			        column++;
			        rowvariable++;
				}
			}
			 System.out.println("PosUnfilledShortswithLongInventory...completed........");
			 workbook.write(out);
			 //FileOutputStream fileOut = new FileOutputStream("C:\\ShortSales\\Unfilled_Shorts_with_possible_Long.xlsx");
			 //workbook.write(fileOut);
		     //fileOut.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
			 
	}

}
