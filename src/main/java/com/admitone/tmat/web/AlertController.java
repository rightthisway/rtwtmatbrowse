package com.admitone.tmat.web;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.acegisecurity.ui.webapp.AuthenticationProcessingFilter;
import org.apache.velocity.tools.generic.NumberTool;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.servlet.view.RedirectView;

import com.admitone.tmat.alert.AlertScheduler;
import com.admitone.tmat.common.MailIdProperty;
import com.admitone.tmat.crawler.CrawlerSchedulerManager;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Artist;
import com.admitone.tmat.data.Category;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Property;
import com.admitone.tmat.data.Ticket;
//import com.admitone.tmat.data.Tour;
import com.admitone.tmat.data.User;
import com.admitone.tmat.data.UserAlert;
import com.admitone.tmat.data.VenueCategory;
import com.admitone.tmat.enums.AlertFor;
import com.admitone.tmat.enums.EventStatus;
import com.admitone.tmat.enums.UserAlertStatus;
import com.admitone.tmat.enums.UserAlertType;
import com.admitone.tmat.utils.PreferenceManager;
import com.admitone.tmat.utils.mail.MailManager;

public class AlertController extends MultiActionController {
	
	private PreferenceManager preferenceManager;
	private CrawlerSchedulerManager crawlerSchedulerManager;	
//	private AlertScheduler alertScheduler;
	private MailManager mailManager; 
	private MailIdProperty mailIdProperty;

	public MailIdProperty getMailIdProperty() {
		return mailIdProperty;
	}

	public void setMailIdProperty(MailIdProperty mailIdProperty) {
		this.mailIdProperty = mailIdProperty;
	}
	/**
	 * Add alerts page.
	 */
	public ModelAndView loadManageUserAlertsPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// check if this is the page for my alert
		// or the page for "manage user alerts"
		String manageAlertsUrl;
		boolean modeAllUser = request.getRequestURI().contains("EditorManageAlerts");
		String username = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);
		
		ModelAndView mav;
		if (modeAllUser) {
			manageAlertsUrl = "EditorManageAlerts";
			mav = new ModelAndView("page-editor-manage-alerts");			
			mav.addObject("users", DAORegistry.getUserDAO().getAll());

			// the key is status + "-" + AlertFor (e.g. ACTIVE-TMAT)
			Map<String, Integer> statByKey = new HashMap<String, Integer>();

			int alertCount = 0;
			
			Collection<UserAlert> allUserAlerts = DAORegistry.getUserAlertDAO().getAll();
			for(UserAlert ua: allUserAlerts) {
				String key = ua.getUserAlertStatus() + "-" + ua.getAlertFor();
				Integer count = statByKey.get(key);
				count = (count == null)?1:(count+1);
				statByKey.put(key, count);

				String totalRowKey = ua.getUserAlertStatus() + "-TOTAL";
				Integer totalRowCount = statByKey.get(totalRowKey);
				totalRowCount = (totalRowCount == null)?1:(totalRowCount+1);
				statByKey.put(totalRowKey, totalRowCount);

				String totalColKey = "TOTAL-" + ua.getAlertFor();
				Integer totalColCount = statByKey.get(totalColKey);
				totalColCount = (totalColCount == null)?1:(totalColCount+1);
				statByKey.put(totalColKey, totalColCount);
				
				alertCount++;
			}
			
			statByKey.put("TOTAL-TOTAL", alertCount);
			mav.addObject("statByKey", statByKey);		
		} else {
			manageAlertsUrl = "MyAlerts";			
			mav = new ModelAndView("page-manage-my-alerts");			
		}


		String action = request.getParameter("action");
				
		
		if (action == null) {
			action = "";
		} else if (action.equalsIgnoreCase("delete")) {
			Collection<UserAlert> selectedAlerts = new ArrayList<UserAlert>();
			for(String selectedAlertId: request.getParameter("alertIds").split(",")) {
				int alertId = Integer.parseInt(selectedAlertId);
				UserAlert selectedAlert = DAORegistry.getUserAlertDAO().get(alertId);
				if(selectedAlert == null) {
					return new ModelAndView(new RedirectView(manageAlertsUrl + "?error=" + URLEncoder.encode("Invalid alert Id " + alertId)));				
				}
				if (!modeAllUser && !selectedAlert.getUsername().equals(username) && !selectedAlert.getMarketMakerUsername().equals(username)) {
					return new ModelAndView(new RedirectView(manageAlertsUrl + "?error=" + URLEncoder.encode("You cannot view another user's alert")));
				}
				selectedAlerts.add(selectedAlert);
			}
			for(UserAlert selectedAlert: selectedAlerts) {
				selectedAlert.setUserAlertStatus(UserAlertStatus.DELETED);
				DAORegistry.getUserAlertDAO().update(selectedAlert);				
			}
			return new ModelAndView(new RedirectView(manageAlertsUrl + "?info=" + URLEncoder.encode("The " + selectedAlerts.size()+ " selected alert(s) have been deleted")));
		} else if (action.equalsIgnoreCase("check")) {
			String alertIdString = request.getParameter("alertId");
			UserAlert userAlert = null;
			Integer userAlertId = null;
			if(alertIdString != null && !alertIdString.isEmpty()) {
				userAlertId = Integer.parseInt(alertIdString);
				userAlert = DAORegistry.getUserAlertDAO().get(userAlertId);			
			}			
			if(userAlert == null) {
				return new ModelAndView(new RedirectView(manageAlertsUrl + "?error=" + URLEncoder.encode("Invalid alert id")));				
			}

			if (!modeAllUser && !userAlert.getUsername().equals(username) && !userAlert.getMarketMakerUsername().equals(username)) {
				return new ModelAndView(new RedirectView(manageAlertsUrl + "?error=" + URLEncoder.encode("You cannot edit another user's alert")));
			}
			
			Collection<Ticket> alertTickets = DAORegistry.getTicketDAO().getAllTicketsMatchingUserAlert(userAlert);
			mav.addObject("userAlert", userAlert);
			mav.addObject("alertTickets", alertTickets);
		} else if (action.equalsIgnoreCase("disable")) {
			Collection<UserAlert> selectedAlerts = new ArrayList<UserAlert>();
			for(String selectedAlertId: request.getParameter("alertIds").split(",")) {
				int alertId = Integer.parseInt(selectedAlertId);
				UserAlert selectedAlert = DAORegistry.getUserAlertDAO().get(alertId);
				if(selectedAlert == null) {
					return new ModelAndView(new RedirectView(manageAlertsUrl + "?error=" + URLEncoder.encode("Invalid alert Id " + alertId)));				
				}
				if (!modeAllUser && !selectedAlert.getUsername().equals(username) && !selectedAlert.getMarketMakerUsername().equals(username)) {
					return new ModelAndView(new RedirectView(manageAlertsUrl + "?error=" + URLEncoder.encode("You cannot view another user's alert")));
				}
				selectedAlerts.add(selectedAlert);
			}
			for(UserAlert selectedAlert: selectedAlerts) {
				selectedAlert.setUserAlertStatus(UserAlertStatus.DISABLED);
				DAORegistry.getUserAlertDAO().update(selectedAlert);				
			}
			return new ModelAndView(new RedirectView(manageAlertsUrl + "?info=" + URLEncoder.encode("The " + selectedAlerts.size()+ " selected alert(s) have been disabled")));
		} else if (action.equalsIgnoreCase("enable")) {
			Collection<UserAlert> selectedAlerts = new ArrayList<UserAlert>();
			for(String selectedAlertId: request.getParameter("alertIds").split(",")) {
				int alertId = Integer.parseInt(selectedAlertId);
				UserAlert selectedAlert = DAORegistry.getUserAlertDAO().get(alertId);
				if(selectedAlert == null) {
					return new ModelAndView(new RedirectView(manageAlertsUrl + "?error=" + URLEncoder.encode("Invalid alert Id " + alertId)));				
				}
				if (!modeAllUser && !selectedAlert.getUsername().equals(username) && !selectedAlert.getMarketMakerUsername().equals(username)) {
					return new ModelAndView(new RedirectView(manageAlertsUrl + "?error=" + URLEncoder.encode("You cannot view another user's alert")));
				}
				selectedAlerts.add(selectedAlert);
			}
			for(UserAlert selectedAlert: selectedAlerts) {
				selectedAlert.setUserAlertStatus(UserAlertStatus.ACTIVE);
				DAORegistry.getUserAlertDAO().update(selectedAlert);				
			}
			return new ModelAndView(new RedirectView(manageAlertsUrl + "?info=" + URLEncoder.encode("The " + selectedAlerts.size()+ " selected alert(s) have been enabled")));
		} 
		//Shorting an alert - circles alert on shorting should become wholesale alert
		else if (action.equalsIgnoreCase("short")) {
			Collection<UserAlert> selectedAlerts = new ArrayList<UserAlert>();
			for(String selectedAlertId: request.getParameter("alertIds").split(",")) {
				int alertId = Integer.parseInt(selectedAlertId);
				UserAlert selectedAlert = DAORegistry.getUserAlertDAO().get(alertId);
				if(selectedAlert == null) {
					return new ModelAndView(new RedirectView(manageAlertsUrl + "?error=" + URLEncoder.encode("Invalid alert Id " + alertId)));				
				}
				if (!modeAllUser && !selectedAlert.getUsername().equals(username) && !selectedAlert.getMarketMakerUsername().equals(username)) {
					return new ModelAndView(new RedirectView(manageAlertsUrl + "?error=" + URLEncoder.encode("You cannot view another user's alert")));
				}
				selectedAlerts.add(selectedAlert);
			}
			for(UserAlert selectedAlert: selectedAlerts) {
				// send short mail
				String resource;
				if (selectedAlert.getAlertFor() != null && (selectedAlert.getAlertFor().equals(AlertFor.CIRCLES) || selectedAlert.getAlertFor().equals(AlertFor.CIRCLES_PORTAL))) {
					resource =  "mail-ticket-alert-short-circles.txt";
				} else {
					resource =  "mail-ticket-alert-short-tmat.txt";						
				}
				String mimeType = "text/plain";
				
				DateFormat dateFormat =	new SimpleDateFormat("MM/dd/yyyy HH:mm");

				Map<String, Object> map = new HashMap<String, Object>();
				map.put("userAlert", selectedAlert);
				map.put("now", dateFormat.format(new Date()));
				map.put("number", new NumberTool());
				map.put("serverName", Constants.getInstance().getHostName());
				
				Date expirationDate = new Date(new Date().getTime() + 24L * 60L * 60L * 1000L);
				map.put("expirationDate", dateFormat.format(expirationDate));
				
				int venueId = selectedAlert.getEvent().getVenueId();
				String catScheme = selectedAlert.getCategoryGroupName();
				String imageMap = "E:/TMATIMAGESFINAL/" + venueId + "_" + catScheme + ".gif";
				File imageFile = new File(imageMap);
				
			
				if (imageFile.exists()){
					map.put("mapUrl", "http://tmat/a1/VenueImage?venueId=" + selectedAlert.getEvent().getVenueId() + "&catScheme=" + selectedAlert.getCategoryGroupName());
				} 
			
				Property alertMailCcPropertyDAO = DAORegistry.getPropertyDAO().get("alert.mail.cc");
				String mailCc = null;
				if (alertMailCcPropertyDAO != null) {
					mailCc = alertMailCcPropertyDAO.getValue();
				}
				
				Property alertMailBccPropertyDAO = DAORegistry.getPropertyDAO().get("alert.mail.bcc");
				String mailBcc = null;
				if (alertMailBccPropertyDAO != null) {
					mailBcc = alertMailBccPropertyDAO.getValue();
				}
				
				//User alertUser = DAORegistry.getUserDAO().get(selectedAlert.getUsername());
				User shortUser = DAORegistry.getUserDAO().getUserByUsername(username);

				String fromName = shortUser.getFirstName().trim() + " " + shortUser.getLastName().trim() + "(" + shortUser.getUsername().trim() + ") ";
				String email = null; 
				String subject =  "CATEGORY SALE ALERT for " + selectedAlert.getEvent().getName() + " - " + selectedAlert.getEvent().getFormattedEventDate();
				
				if ("1".equals(DAORegistry.getPropertyDAO().get("alert.mail.marketmaker.enabled").getValue())) {						
					String mmEmail;
					User marketMaker = DAORegistry.getMarketMakerEventTrackingDAO().getCurrentMarketMakerForEvent(selectedAlert.getEventId());
					if (marketMaker == null) {
						// default handler of the alert
						mmEmail = mailIdProperty.getBccList();//"araut@rightthisway.com";
					} else {
						mmEmail = marketMaker.getEmail();
					}
					
					email = mmEmail;
				}

				if (email == null) {
					email = mailCc;
				} else {
					email += "," + mailCc;
				}
				
				if (email == null && mailCc == null) {
					email = mailBcc;
				} else {
					email += "," + mailCc + "," + mailBcc;
				} 
				//email += "," + shortUser.getEmail();

				mailManager.sendMail(fromName, email, null, null, subject, resource, map, mimeType, null);
				logger.info("Successfully sending mail for alert:" + selectedAlert.getId() + "!");
				selectedAlert.setLastEmailDate(new Date());
				selectedAlert.setAlertFor(AlertFor.TMAT);
				DAORegistry.getUserAlertDAO().update(selectedAlert);
			}			
			return new ModelAndView(new RedirectView(manageAlertsUrl + "?info=" + URLEncoder.encode(selectedAlerts.size()+ " short email(s) were sent for the selected alert(s) ")));
		}
		
		String filterArtistIdString = request.getParameter("filterArtistId");
		if (filterArtistIdString == null) {
			filterArtistIdString = preferenceManager.getPreferenceValue(username, "alertArtistId", "");
		}

		Integer filterArtistId = null;
		try {
			filterArtistId = Integer.parseInt(filterArtistIdString);
		} catch (Exception e) {
			filterArtistIdString = "";
		}
		
		preferenceManager.updatePreference(username, "alertArtistId", filterArtistIdString);


		String filterEventIdString = request.getParameter("filterEventId");
		if (filterEventIdString == null) {
			filterEventIdString = preferenceManager.getPreferenceValue(username, "alertEventId", "");
		}

		Integer filterEventId = null;
		if (filterArtistId != null) {
			try {
				filterEventId = Integer.parseInt(filterEventIdString);
			} catch (Exception e) {
				filterEventIdString = "";
			}
		} else {
			filterEventIdString = "";			
		}
		String alertUsername = null;
		if (modeAllUser) {
			String filterUsername = request.getParameter("filterUsername");
			if (filterUsername == null) {
				filterUsername = preferenceManager.getPreferenceValue(username, "alertUsername", "");
			}

			preferenceManager.updatePreference(username, "alertUsername", filterUsername);
			
			if (!filterUsername.isEmpty()) {
				alertUsername = filterUsername;
			}
			
			mav.addObject("filterUsername", filterUsername);

		} else {
			alertUsername = username;			
		}

		preferenceManager.updatePreference(username, "alertEventId", filterEventIdString);
				
		//
		// VIEW: CREATED, MARKETMAKER OR BOTH
		// 
		String view = request.getParameter("view");
		if (view == null) {
			view = preferenceManager.getPreferenceValue(username, "alertView", null);
		} else {
			preferenceManager.updatePreference(username, "alertView", view);			
		}

		
		//
		// ALERTFOR: TMAT, CIRCLES OR BOTH
		// 
		String alertForString = request.getParameter("alertFor");
		if (alertForString == null) {
			alertForString = preferenceManager.getPreferenceValue(username, "alertFor", null);
		} 
		
		AlertFor filterAlertFor = null;
		if (alertForString != null) {
			if (alertForString.equalsIgnoreCase(AlertFor.TMAT.toString())) {
				filterAlertFor = AlertFor.TMAT;
				alertForString = "tmat";
			} else if (alertForString.equalsIgnoreCase(AlertFor.CIRCLES_PORTAL.toString())) {
				filterAlertFor = AlertFor.CIRCLES_PORTAL;				
				alertForString = "circles";
//			} else if (alertForString.equalsIgnoreCase(AlertFor.CIRCLES_PORTAL.toString())) {
//				filterAlertFor = AlertFor.CIRCLES_PORTAL;				
//				alertForString = "circles_portal";
			} else {
				alertForString = null;
			}
		} 
		preferenceManager.updatePreference(username, "alertFor", alertForString);
		
		// INCLUDE DISABLED ALERTS
		String includeDisabledAlertsString = request.getParameter("filterIncludeDisabledAlerts");
		if (includeDisabledAlertsString == null) {
			includeDisabledAlertsString = preferenceManager.getPreferenceValue(username, "alertIncludeDisabledAlerts", "0");
		} 
		
		Boolean includeDisabledAlerts = false;
		if (includeDisabledAlertsString != null && includeDisabledAlertsString.equals("1")) {
			includeDisabledAlerts = true;
		} 
		preferenceManager.updatePreference(username, "alertIncludeDisabledAlerts", includeDisabledAlerts?"1":"0");
		
		UserAlertStatus filterUserAlertStatus = null;
		if (!includeDisabledAlerts) {
			filterUserAlertStatus = UserAlertStatus.ACTIVE;
		}

		Map<Integer, UserAlert> userAlertById = new HashMap<Integer, UserAlert>();
		if (view == null || view.isEmpty() || view.equals("created")) {
			Collection<UserAlert> creatorUserAlerts;
			if (filterEventId == null) {
				creatorUserAlerts = DAORegistry.getUserAlertDAO().getAlertsByUsernameAndTourId(alertUsername, filterArtistId, filterAlertFor, filterUserAlertStatus);
			} else {
				creatorUserAlerts = DAORegistry.getUserAlertDAO().getAlertsByUsernameAndEventId(alertUsername, filterEventId, filterAlertFor, filterUserAlertStatus);
			}
			if (creatorUserAlerts != null) {
				for(UserAlert userAlert: creatorUserAlerts) {
					userAlertById.put(userAlert.getId(), userAlert);
				}
				
			}
		}

		if (view == null || view.isEmpty() || view.equals("marketmaker")) {
			Collection<UserAlert> marketMakerUserAlerts;
			if (filterEventId == null) {
				marketMakerUserAlerts = DAORegistry.getUserAlertDAO().getAlertsByMarketMakerAndTourId(alertUsername, filterArtistId, filterAlertFor, filterUserAlertStatus);
			} else {
				marketMakerUserAlerts = DAORegistry.getUserAlertDAO().getAlertsByMarketMakerAndEventId(alertUsername, filterEventId, filterAlertFor, filterUserAlertStatus);				
			}
			if (marketMakerUserAlerts != null) {
				for(UserAlert userAlert: marketMakerUserAlerts) {
					userAlertById.put(userAlert.getId(), userAlert);
				}
				
			}
		}
		
		
		Map<Integer, Integer> frequencyByAlertId = new HashMap<Integer, Integer>();
		for(UserAlert forAlert: userAlertById.values()) {
			if (forAlert.getEvent()!=null){
			frequencyByAlertId.put(forAlert.getId(), crawlerSchedulerManager.getFrequency(forAlert.getEvent().getDate(),
					forAlert.getEvent().getArtist().getGrandChildTourCategory().getChildTourCategory().getTourCategory().getName()));
			}
		}
		
		Collection<Event> events = null;
		if (filterArtistId != null) {
			events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(filterArtistId);
		}

		mav.addObject("includeDisabledAlerts", includeDisabledAlerts);
		mav.addObject("view", view);
		mav.addObject("alertFor", filterAlertFor);
		 
		mav.addObject("events", events);
		mav.addObject("frequencyByAlertId", frequencyByAlertId);
		mav.addObject("alerts", userAlertById.values());
		mav.addObject("filterArtistId", filterArtistId);
		mav.addObject("filterEventId", filterEventId);
		mav.addObject("artists", DAORegistry.getArtistDAO().getAllActiveArtists());
		mav.addObject("username", username);	
		mav.addObject("mode", modeAllUser?"editAll":"editMine");
		return mav;
	}
	
	public ModelAndView loadAlertPreviewPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		UserAlert userAlert = createUserAlertFromInput(null, request,false);
		
//		System.out.println("USERALERT=" + userAlert);

		ModelAndView mav = new ModelAndView("page-editor-edit-alert-preview");
		mav.addObject("userAlert", userAlert);
		mav.addObject("alertTickets", DAORegistry.getTicketDAO().getAllTicketsMatchingUserAlert(userAlert));
		return mav;		
	}
	
	public void getCategoryDetails(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try{
			Integer venueCategoryId = Integer.parseInt(request.getParameter("venueCatId"));
			Collection<Category> categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(venueCategoryId);
			PrintWriter out = response.getWriter();
			JSONArray categoryDetails = new JSONArray();
			JSONObject jObj = null;
			Boolean isFirst=true;
			for(Category category : categories) {
				jObj = new JSONObject();
				if(isFirst){
					
					VenueCategory venueCategory = DAORegistry.getVenueCategoryDAO().get(category.getVenueCategoryId());
					String venueImgUrl ="VenueImage?venueId="+venueCategory.getVenue().getId()+"&catScheme="+category.getGroupName();
					jObj.put("venueImgUrl", venueImgUrl);
				}
                jObj.put("id", category.getId());
                jObj.put("symbol", category.getSymbol());
                categoryDetails.put(jObj);
                isFirst = false;
			}
	        out.println(categoryDetails.toString());
		}catch (Exception e) {
			// TODO: handle exception
		}
	}

	public ModelAndView loadEditAlertPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		// check if this is the page for my alert
		// or the page for "manage user alerts"
		String manageAlertsUrl;
		ModelAndView mav;

		boolean modeAllUser = request.getRequestURI().contains("EditorEditAlert");
		if (modeAllUser) {
			manageAlertsUrl = "EditorManageAlerts";
			mav = new ModelAndView("page-editor-edit-alert");
		} else {
			manageAlertsUrl = "MyAlerts";			
			mav = new ModelAndView("page-edit-my-alert");
		}

		String categoryGroupName = request.getParameter("alertCategoryGroupName");

		String action = request.getParameter("action");
		String username = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);
		User user = DAORegistry.getUserDAO().getUserByUsername(username);

		if ("save".equalsIgnoreCase(action)) {
			
			Collection<Integer> eventIds = new ArrayList<Integer>();
			for (Object param: request.getParameterMap().keySet()) {
				if (!param.toString().startsWith("event_")) {
					continue;
				}
				String eventIdParam = param.toString().replace("event_", "");
				Integer eventId = Integer.valueOf(eventIdParam);
				eventIds.add(eventId);
			}
			String alertIdString = request.getParameter("alertId");
			UserAlert userAlert = null;
			Integer userAlertId = null;
//			List<UserAlert> userAlerts=null;
			if(alertIdString != null && !alertIdString.isEmpty()) {
				userAlertId = Integer.parseInt(alertIdString);
				userAlert = DAORegistry.getUserAlertDAO().get(userAlertId);			
			}

			Boolean newAlert;
			if(alertIdString == null || alertIdString.isEmpty()) {
				userAlert = null;
				newAlert = true;
			} else {
				Integer id = Integer.parseInt(alertIdString);
				userAlert = DAORegistry.getUserAlertDAO().get(id);
				if (!modeAllUser && !userAlert.getUsername().equals(username) && !userAlert.getMarketMakerUsername().equals(username)) {
					return new ModelAndView(new RedirectView(manageAlertsUrl + "?error=" + URLEncoder.encode("You cannot edit another user's alert")));
				}
				newAlert = false;
			}
						
			

			if (newAlert) {
				for(Integer eventId:eventIds)
				{
					userAlert = null;
					userAlert = createUserAlertFromInput(userAlert, request,newAlert);
					userAlert.setEventId(eventId);
					userAlert.setUsername(username);				
					DAORegistry.getUserAlertDAO().save(userAlert);
				}
				return new ModelAndView(new RedirectView(manageAlertsUrl + "?info=" + URLEncoder.encode("The alert has been created")));
			} else {
				userAlert = createUserAlertFromInput(userAlert, request,newAlert);
				DAORegistry.getUserAlertDAO().update(userAlert);				
				return new ModelAndView(new RedirectView(manageAlertsUrl + "?info=" + URLEncoder.encode("The alert has been updated")));
			}


		} else {
			String alertIdString = request.getParameter("alertId");
			UserAlert userAlert = null;
			Integer userAlertId = null;
			if(alertIdString != null && !alertIdString.isEmpty()) {
				userAlertId = Integer.parseInt(alertIdString);
				userAlert = DAORegistry.getUserAlertDAO().get(userAlertId);			
			}			

			if (!modeAllUser && userAlert != null && !userAlert.getUsername().equals(username)) {
				return new ModelAndView(new RedirectView(manageAlertsUrl + "?error=" + URLEncoder.encode("You cannot edit another user's alert")));
			}
			Integer artistId = null;
			Integer eventId= null;
			if(userAlert == null)
			{
				String artistIdString = request.getParameter("alertArtistId");
				try {
					artistId = Integer.parseInt(artistIdString);
				} catch(Exception e) {
				};
			}else{
				eventId = userAlert.getEventId();
			}
			Event event = null;
			Artist artist = null;
			List<String> categoryGroupNames = new ArrayList<String>();
			Collection<Category> categories = null;
			String categorySymbol = null;
			Collection<Event> eventList = null;
//			System.out.println("*********eventId: "+eventId+"*********tourId: "+ tourId+"********userAlert: "+ userAlert);
			if(eventId != null){
				event = DAORegistry.getEventDAO().get(eventId);	
				if(event.getVenueCategory()!=null){
					categoryGroupNames.add(event.getVenueCategory().getCategoryGroup());
				}
//				categoryGroupNames = Categorizer.getCategoryGroupsByEvent(eventId);
				if(categoryGroupNames != null && categoryGroupName == null) {
					// take the first one
					if (categoryGroupNames.size() > 0) {
						categoryGroupName = categoryGroupNames.get(0);
					}
				}
				if (categoryGroupName != null && !categoryGroupName.isEmpty()) {
					categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueIdAndCategoryGroup(event.getVenueId(), categoryGroupName);
				}
			}/*else if(tourId != null) {
				tour = DAORegistry.getTourDAO().get(tourId);	
				
				
				categoryGroupNames = Categorizer.getCategoryGroupsByTour(tourId);
				if(event.getVenueCategory()!=null){
					categoryGroupName = event.getVenueCategory().getCategoryGroup();
				}
				categoryGroupNames.add(categoryGroupName);
				
				categoryGroupNames = Categorizer.getCategoryGroupsByTour(tourId);
				if(categoryGroupNames != null && categoryGroupName == null) {
					// take the first one
					if (categoryGroupNames.size() > 0) {
						categoryGroupName = categoryGroupNames.get(0);
					}
				}
				categoryGroupNames.add(categoryGroupName);
				String categorySchema  = request.getParameter("alertCategoryGroupName");
				if(categorySchema !=null){
					categoryGroupName = categorySchema;
				}
				eventList = Categorizer.getEventListbyTourandCategorySchema(tourId, categoryGroupName);
				
				
				//eventList = DAORegistry.getEventDAO().getAllEventsByTourIdAndCategoryGroup(tourId, categoryGroupName);
				
				if (categoryGroupName != null && !categoryGroupName.isEmpty()) {
					
					for (Event eventObj : eventList) {
						categories = new ArrayList<Category>();
						categories.addAll(DAORegistry.getCategoryDAO().getAllCategoriesByVenueIdAndCategoryGroup(eventObj.getVenueId(), categoryGroupName));
					}
					
					//categories = DAORegistry.getCategoryDAO().getAllCategories(tourId, categoryGroupName);
				}
				
			}else if(tourId != null) {
				tour = DAORegistry.getTourDAO().get(tourId);	
				
				categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenuCategoryIdsByTourId(tourId, EventStatus.ACTIVE);
				
				categoryGroupNames = DAORegistry.getCategoryMappingDAO().getAllCategoryGroupNamesByTourIdByVenueCategoryId(tourId, EventStatus.ACTIVE);
				
				if(categoryGroupNames != null && categoryGroupName == null) {
					// take the first one
					if (categoryGroupNames.size() > 0) {
						categoryGroupName = categoryGroupNames.get(0);
					}
				}
				
				String categorySchema  = request.getParameter("alertCategoryGroupName");
				if(categorySchema !=null){
					categoryGroupName = categorySchema;
				}
				eventList = DAORegistry.getEventDAO().getAllEventsByTourIdAndCategoryGroup(tourId, categoryGroupName);
				
			}*/
			else if(artistId != null) {
				artist = DAORegistry.getArtistDAO().get(artistId);	
				
				//categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenuCategoryIdsByTourId(tourId, EventStatus.ACTIVE);
				
				eventList = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(artistId);
				
			}
			String[] siteIds;

			if (userAlert != null) {
				if(null != userAlert.getEvent().getVenueCategory()){
					categoryGroupName = userAlert.getEvent().getVenueCategory().getCategoryGroup();
				}
				categorySymbol = userAlert.getCategory();
				siteIds = userAlert.getSiteIds();
			} else {
				if (categories != null && !categories.isEmpty()) {
					categorySymbol = categories.iterator().next().getSymbol();
				}
				siteIds = Constants.getInstance().getSiteIds();
			}

			Map<String, Boolean> siteSelected = new HashMap<String, Boolean>();
			for(String siteId: siteIds) {
				siteSelected.put(siteId, true);
			}
			mav.addObject("siteSelected", siteSelected);				

			
			mav.addObject("categorySymbol", categorySymbol);
			//mav.addObject("eventId", eventId);
			mav.addObject("userAlert", userAlert);
			mav.addObject("artist", artist);
			mav.addObject("event", event);
			mav.addObject("categoryGroupName", categoryGroupName);
			mav.addObject("categoryGroupNames", categoryGroupNames);		
			mav.addObject("categories", categories);

			mav.addObject("user", user);
			mav.addObject("events", eventList);
			//mav.addObject("eventSize", eventList.size());
			mav.addObject("userAlert", userAlert);
			mav.addObject("mode", modeAllUser?"editAll":"editMine");
			
			return mav;		
		}

	}

	private UserAlert createUserAlertFromInput(UserAlert userAlert, 
			HttpServletRequest request,boolean newAlert) {
	//Input the user alert values into the user_alert table	
		String eventIdString = request.getParameter("alertEventId");
		String categoryGroupName = request.getParameter("alertCategoryGroupName");
		String nullCategoryString = request.getParameter("alertNullCategory");
		String category = request.getParameter("alertCategory"); 
		String priceLowString = request.getParameter("alertPriceLow");
		String priceHighString = request.getParameter("alertPriceHigh"); 
		String nullSectionString = null;
		String sectionLow = request.getParameter("alertSectionLow"); 
		String sectionHigh = request.getParameter("alertSectionHigh");
		String nullRowString = null;
		String rowLow = request.getParameter("alertRowLow");
		String rowHigh = request.getParameter("alertRowHigh");
		String quantities = request.getParameter("alertQuantities"); 
		String alertStatusString = request.getParameter("alertStatus");
		String alertTypeString = request.getParameter("alertType"); 
		String alertForString = request.getParameter("alertFor"); 
		String description = request.getParameter("alertDescription");
		
		//Alert Status
		if (userAlert == null) {
			userAlert = new UserAlert();
			userAlert.setUserAlertStatus(UserAlertStatus.ACTIVE);
			userAlert.setCreationDate(new Date());
		}else{
			UserAlertStatus newAlertStatus = UserAlertStatus.valueOf(alertStatusString);
//			UserAlertStatus oldAlertStatus = userAlert.getUserAlertStatus();
//			if (oldAlertStatus.equals(UserAlertStatus.DISABLED)&&newAlertStatus.equals(UserAlertStatus.ACTIVE)){
			if (!newAlertStatus.equals(UserAlertStatus.DISABLED)){	
				userAlert.setLastEmailDate(null);
				userAlert.setLastCheck(null);
			}
			userAlert.setMailInterval(null);
			userAlert.setUserAlertStatus(newAlertStatus);
		}


		//Alert For		
		userAlert.setAlertFor(AlertFor.TMAT);
		if (alertForString != null) {
			if (alertForString.equalsIgnoreCase("CIRCLES")) {
				userAlert.setAlertFor(AlertFor.CIRCLES);
			} else if (alertForString.equalsIgnoreCase("CIRCLES_PORTAL")) {
				userAlert.setAlertFor(AlertFor.CIRCLES_PORTAL);				
			}
		}
		if(!newAlert)
			userAlert.setEventId(Integer.parseInt(eventIdString));
		
		userAlert.setCategoryGroupName(categoryGroupName);
		
		if ("on".equals(nullCategoryString)) {
			userAlert.setCategory(null);
		} else {
			if (category == null) {
				userAlert.setCategory("");
			} else {
				userAlert.setCategory(category.trim());
			}
		}

		// section
		if ("on".equals(nullSectionString)) {
			userAlert.setSectionRange(null);
		} else {
			userAlert.setSectionRange(sectionLow + ":" + sectionHigh);				
		}


		// row
		if ("on".equals(nullRowString)) {
			userAlert.setRowRange(null);
		} else {
			userAlert.setRowRange(rowLow + ":" + rowHigh);								
		}

		// quantity
		if (quantities == null) {
			quantities = "";
		}
		userAlert.setQuantities(quantities);

		// price
		if (priceLowString == null || !priceLowString.matches("^\\d+[.]??\\d*$")) {
			priceLowString = "";
		}

		if (priceHighString == null || !priceHighString.matches("^\\d+[.]??\\d*$")) {
			priceHighString = "";
		}
		userAlert.setPriceRange(priceLowString + ":" + priceHighString);
		
		//alert type
		UserAlertType alertType = UserAlertType.valueOf(alertTypeString);
		userAlert.setUserAlertType(alertType);
		
		userAlert.setDescription(description);
		
		Collection<String> siteIds = new ArrayList<String>();
		for(String siteId: Constants.getInstance().getSiteIds()) {
			if ("on".equals(request.getParameter("site-" + siteId))) {
				siteIds.add(siteId);
			}
		}
		userAlert.setSiteIds(siteIds.toArray(new String[siteIds.size()]));
		
		return userAlert;
	}
	
	/*public void setAlertScheduler(
			AlertScheduler alertScheduler) {
		this.alertScheduler = alertScheduler;
	}*/

	public PreferenceManager getPreferenceManager() {
		return preferenceManager;
	}

	public void setPreferenceManager(PreferenceManager preferenceManager) {
		this.preferenceManager = preferenceManager;
	}	
	
	public void setCrawlerSchedulerManager(
			CrawlerSchedulerManager crawlerSchedulerManager) {
		this.crawlerSchedulerManager = crawlerSchedulerManager;
	}
	
	

	public void setMailManager(MailManager mailManager) {
		this.mailManager = mailManager;
	}

	public void afterPropertiesSet() throws Exception {
		/*if (alertScheduler == null) {
			throw new Exception("The property alertScheduler must be set");
		}*/
	
		if (preferenceManager == null) {
			throw new RuntimeException("Property preferenceManager must be set");
		}

		if (mailManager == null) {
			throw new RuntimeException("Property mailManager must be set");
		}
}
	
}
