package com.admitone.tmat.web;

import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Role;
import com.admitone.tmat.data.User;

public class AdminRegisterUserFormController extends SimpleFormController {
	protected ModelAndView onSubmit(
            HttpServletRequest request,
            HttpServletResponse response, 
            Object command,
            BindException errors)
            throws Exception {
	  FormUser givenUser = (FormUser) command;
	  User user = new User();
	  user.setEmail(givenUser.getEmail());
	  user.setFirstName(givenUser.getFirstName());
	  user.setLastName(givenUser.getLastName());
	  user.setLocked(givenUser.getLocked());
	  user.setAndEncryptPassword(givenUser.getPassword());
	  user.setPhone(givenUser.getPhone());
	  user.setUsername(givenUser.getUsername());
	  user.setLocked(givenUser.getLocked());
	  user.setEmailCrawlErrorCreatorEnabled(true);
	  user.setEmailCrawlErrorLastUpdaterEnabled(true);

	  Set<Role> roles = new HashSet<Role>();
	  for (String roleStr: givenUser.getRolesStrList()) {
		  Role role = DAORegistry.getRoleDAO().getRoleByName(roleStr);
		  roles.add(role);
	  }
	  user.setRoles(roles);
	  try {
	  	DAORegistry.getUserDAO().save(user);
	  } catch(Exception e) {
    	  User duplicateUser = DAORegistry.getUserDAO().getUserByUsernameOrEmail(user.getUsername(), user.getEmail());
    	  
    	  if (user.getUsername().equals(duplicateUser.getUsername())) {
        	  errors.rejectValue("username", "error.duplicateUsername", "Username already in use");    		  
    	  }

    	  if (user.getEmail().equals(duplicateUser.getEmail())) {
    		  errors.rejectValue("email", "error.duplicateEmail", "Email already in use");
    	  }
    	
    	  return showForm(request, errors, getFormView());		  
	  }
	  return new ModelAndView(new RedirectView("AdminManageUsers"));
	}
	
	protected Object formBackingObject(HttpServletRequest request)
	throws ServletException {
		request.setAttribute("roles", DAORegistry.getRoleDAO().getAll());
		return new FormUser();		
	}
}
