package com.admitone.tmat.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.acegisecurity.ui.webapp.AuthenticationProcessingFilter;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.servlet.view.RedirectView;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.crawler.TicketListingCrawler;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Artist;
import com.admitone.tmat.data.Venue;
import com.admitone.tmat.enums.CrawlState;
import com.admitone.tmat.enums.EventStatus;
import com.admitone.tmat.utils.PreferenceManager;
import com.admitone.tmat.web.pojo.WebTicketListingCrawlRow;

public class CrawlerController extends MultiActionController {
	private TicketListingCrawler ticketListingCrawler;
	private PreferenceManager preferenceManager;
	
	// Don't Delete this code.. This is a new implementation after 02.16.2013 -- Start.
	public void editCrawlersPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		try{
			String action= request.getParameter("action");
			String str = "";
			if (action.equals("createCrawl")) {
				String crawls = request.getParameter("crawls");
				System.out.println("==Create crawl:==" + crawls);
				logger.info("==Create crawl:==" + crawls);
				if(crawls!=null && !crawls.isEmpty()){
					TicketListingCrawl ticketListingCrawl= null;
					for(String crawl:crawls.split(",")){	
						Integer crawlId= Integer.parseInt(crawl);
						ticketListingCrawl = DAORegistry.getTicketListingCrawlDAO().get(crawlId);
						if(ticketListingCrawl!=null){
							if(!ticketListingCrawler.existTicketListingCrawl(ticketListingCrawl)) {
								ticketListingCrawler.addTicketListingCrawl(ticketListingCrawl);
//								creationEventListManager.addSitesInSitesPerEventMap(ticketListingCrawl.getEventId(), ticketListingCrawl.getSiteId());
							} else {
								str = str+ticketListingCrawl.getId()+",";
							}
						}
					}
				}
				System.out.println("==Create crawl DUP : "+str+" : "+new Date());
			}else if (action.equals("enableCrawl")) {
				String crawls = request.getParameter("crawls");
				if(crawls!=null && !crawls.isEmpty()){
					for(String crawl:crawls.split(",")){	
						Integer crawlId= Integer.parseInt(crawl);
						TicketListingCrawl crawlToBeEnable = ticketListingCrawler.getTicketListingCrawlById(crawlId);
						
						if(crawlToBeEnable == null) {
							crawlToBeEnable = DAORegistry.getTicketListingCrawlDAO().get(crawlId);
							if(crawlToBeEnable != null){
								crawlToBeEnable.setEnabled(true);
								if(!ticketListingCrawler.existTicketListingCrawl(crawlToBeEnable)) {
									ticketListingCrawler.addTicketListingCrawl(crawlToBeEnable);
//									creationEventListManager.addSitesInSitesPerEventMap(ticketListingCrawl.getEventId(), ticketListingCrawl.getSiteId());
								} else {
									str = str+crawlToBeEnable.getId()+",";
								}
							}
						} else {
							crawlToBeEnable.setEnabled(true);
//							creationEventListManager.addSitesInSitesPerEventMap(crawlToBeEnable.getId(), crawlToBeEnable.getSiteId());
						}
					}
				}
				System.out.println("==ENABLE crawl DUP : "+str+" : "+new Date());
			}else if (action.equals("disableCrawl")) {
				String crawls = request.getParameter("crawls");
				if(crawls!=null && !crawls.isEmpty()){
					for (String crawl : crawls.split(",")) {
						int crawlId = Integer.parseInt(crawl);
						TicketListingCrawl crawlToBeDisable = ticketListingCrawler.getTicketListingCrawlById(crawlId);
						crawlToBeDisable.setEnabled(false);
						
						DAORegistry.getTicketDAO().disableTickets(crawlId);
						ticketListingCrawler.removeTicketListingCrawl(crawlToBeDisable);
						
//						creationEventListManager.disableSitesInSitesPerEventMap(crawlToBeDisable.getEventId(), crawlToBeDisable.getSiteId());
					}
				}
			} else if (action.equals("forceRecrawl")) {
				String[] crawlIdsString = request.getParameter("crawls").split(",");
				Collection<TicketListingCrawl> crawls = new ArrayList<TicketListingCrawl>();
				for (String crawlIdString : crawlIdsString) {
					int crawlId = Integer.parseInt(crawlIdString);
					TicketListingCrawl crawl = ticketListingCrawler.getTicketListingCrawlById(crawlId);
					crawls.add(crawl);
				}
				ticketListingCrawler.forceRecrawlWithPriority(crawls,TicketListingCrawl.PRIORITY_HIGH);
			} else if (action.equals("removeCrawl")) {
				String[] crawlIdsString = request.getParameter("crawls").split(",");
				for (String crawlIdString : crawlIdsString) {
					int crawlId = Integer.parseInt(crawlIdString);
					TicketListingCrawl crawl = ticketListingCrawler.getTicketListingCrawlById(crawlId);

					// delete all ticket listing associated to that crawl
					DAORegistry.getTicketDAO().deleteTicketsByCrawlId(crawlId);

					// delete crawl
					ticketListingCrawler.removeTicketListingCrawl(crawl);
//					creationEventListManager.deleteSitesFromSitesPerEventMap(crawl.getEventId(), crawl.getSiteId());
					try {
						DAORegistry.getTicketListingCrawlDAO().deleteById(crawlId);
					} catch (Exception e) {
						logger.error("Error while deleting crawl from DB", e);
					}
				}
			} else if (action.equals("disableExpiredCrawl")) {
				Collection<Event> events = DAORegistry.getEventDAO().getAllEventsByStatus(EventStatus.DELETEDEXPIRED);
				if(events!=null){
					for(Event event:events){
						for(TicketListingCrawl crawl: ticketListingCrawler.getTicketListingCrawlByEvent(event.getId())) {
							if(crawl.isEnabled()){
								crawl.setEnabled(false);
								crawl.setLastUpdated(new Date());
								crawl.setLastUpdater("MANUAL EXPIRED REMOVAL");
								DAORegistry.getTicketDAO().disableTickets(crawl.getId());
								DAORegistry.getTicketListingCrawlDAO().update(crawl);
								
								ticketListingCrawler.removeTicketListingCrawl(crawl);
							}
						}
						event.setEventStatus(EventStatus.EXPIRED);
					}
					DAORegistry.getEventDAO().saveOrUpdateAll(events);
				}
			}else if(action.equals("updateCrawl")){
				String crawls = request.getParameter("crawls");
				if(crawls!=null && !crawls.isEmpty()){
					for(String crawl:crawls.split(",")){	
						Integer crawlId= Integer.parseInt(crawl);
						/*TicketListingCrawl crawlToBeUpdate = ticketListingCrawler.getTicketListingCrawlById(crawlId);
						TicketListingCrawl crawlDB = DAORegistry.getTicketListingCrawlDAO().get(crawlId);
						crawlToBeUpdate.setAutomaticCrawlFrequency(crawlDB.getAutomaticCrawlFrequency());
						crawlToBeUpdate.setCrawlFrequency(crawlDB.getCrawlFrequency());*/
						
						TicketListingCrawl crawlToBeUpdate = ticketListingCrawler.getTicketListingCrawlById(crawlId);
						if(crawlToBeUpdate != null) {
							ticketListingCrawler.removeTicketListingCrawl(crawlToBeUpdate);
						}
						TicketListingCrawl crawlDB = DAORegistry.getTicketListingCrawlDAO().get(crawlId);
						if(crawlDB != null){
							if(!ticketListingCrawler.existTicketListingCrawl(crawlDB)) {
								ticketListingCrawler.addTicketListingCrawl(crawlDB);
							} else {
								str += str+crawlDB.getId()+",";
							}
						}
					}
				}
				System.out.println("==UPDATE crawl DUP : "+str+" : "+new Date());
			}
		
			response.getWriter().write("ok");	
		}catch (Exception e) {
			response.getWriter().write("Error");
		}
		
	}
	
	public ModelAndView loadManageCrawlsPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String username = (String) request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);
		String action = request.getParameter("action");
		String selectFrequency = request.getParameter("crawlBulkFrequency");
		String autoCrawlerFrequencyStatus = request.getParameter("crawlStatus");
		String checkBoxCrawlStatus = request.getParameter("checkBoxCrawlStatus");
		String checkBoxCrawlFrequency = request.getParameter("checkBoxCrawlFrequency");
		try{
		if (action!=null && action.equals("bulkfrequency") && selectFrequency!=null && autoCrawlerFrequencyStatus!=null && checkBoxCrawlStatus!=null && checkBoxCrawlFrequency!=null) {
			String[] crawlIdsString = request.getParameter("crawlIds").split(",");
			for (String crawlIdString : crawlIdsString) {
				int crawlId = Integer.parseInt(crawlIdString);
				TicketListingCrawl crawl = ticketListingCrawler.getTicketListingCrawlById(crawlId);
				if(Boolean.parseBoolean(checkBoxCrawlFrequency)){
				crawl.setCrawlFrequency(Integer.parseInt(selectFrequency));
				}
				if(Boolean.parseBoolean(checkBoxCrawlStatus)){
				crawl.setAutomaticCrawlFrequency(Boolean.parseBoolean(autoCrawlerFrequencyStatus));
				}
				// re activate tickets
				//DAORegistry.getTicketDAO().enableTickets(crawlId);			
				DAORegistry.getTicketListingCrawlDAO().update(crawl);
			}			
		}
		}catch (Exception e) {
			System.out.println(e);
		}
		//
		// status filter option
		//
		String statusTime = request.getParameter("statusTime");
//		int statusTimeInSeconds;
		if (statusTime == null) {
			// look for a value in the preferences
			try{
				statusTime = preferenceManager.getPreferenceValue(username, "editorCrawlerStatusTime");
			}catch(Exception e){
				e.printStackTrace();
			}			
		}

		if (
				!"ERROR".equals(statusTime)
				&& !"1mn".equals(statusTime)
				&& !"30mn".equals(statusTime)
				&& !"1hr".equals(statusTime)
				&& !"2hr".equals(statusTime)
				&& !"2+hr".equals(statusTime)) {
			statusTime = "ALL";
		}

		preferenceManager.updatePreference(username, "editorCrawlerStatusTime", statusTime);

		int eventId = 0;
		//
		// Artist Id filter
		//
		Collection<Event> events = null;
		String artistIdString = request.getParameter("artistId");
		if (artistIdString == null) {
			// look for a value in the preference
			artistIdString = preferenceManager.getPreferenceValue(username, "editorCrawlerTourId");
		}
		
		int artistId = 0;
		if (artistIdString != null && artistIdString.length() > 0) {
			artistId = Integer.parseInt(artistIdString);
			events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(artistId);
		}
		
		if (artistId != 0 && DAORegistry.getArtistDAO().get(artistId) == null) {
			artistId = 0;
		}
		
		preferenceManager.updatePreference(username, "editorCrawlerTourId", artistId + "");

		//
		// Event Id filter
		//
		String eventIdString = request.getParameter("eventId");
		if (eventIdString == null) {
			// look for a value in the preferences
			eventIdString = preferenceManager.getPreferenceValue(username, "editorCrawlerEventId");
		}

		if (eventIdString != null && eventIdString.length() > 0) {
			eventId = Integer.parseInt(eventIdString);
		}
		
		// check if event id belongs to artist
		Event event = DAORegistry.getEventDAO().get(eventId);
		if (event == null || event.getArtistId() != artistId) {
			eventId = 0;
		}

		preferenceManager.updatePreference(username, "editorCrawlerEventId", Integer.toString(eventId));

		//
		// Site Id filter
		//
		String siteId = request.getParameter("siteId");
		if (siteId == null) {
			// look for a value in the preferences
			siteId = preferenceManager.getPreferenceValue(username, "editorCrawlerSiteId");
			if (siteId == null) {
				siteId = "";
			}
		}
		
		preferenceManager.updatePreference(username, "editorCrawlerSiteId", siteId);

		Boolean showDisabledCrawls = false;
		String showDisabledCrawlsString = request.getParameter("showDisabledCrawls");
		if (showDisabledCrawlsString == null) {
			showDisabledCrawlsString = preferenceManager.getPreferenceValue(username, "editorCrawlerShowDisabledCrawls");
			if (showDisabledCrawlsString == null) {
				showDisabledCrawlsString = "";
			}
		}
		showDisabledCrawls = showDisabledCrawlsString.equals("on");

		preferenceManager.updatePreference(username, "editorCrawlerShowDisabledCrawls", showDisabledCrawlsString);

		String info = "";
		if (action == null) {
			ModelAndView mav = new ModelAndView("page-editor-manage-crawls");
			Collection<Artist> artists = DAORegistry.getArtistDAO().getAll();
			
			mav.addObject("artists", artists);
			
			int disabledCrawlCount = 0;
			int readyCrawlCount = 0;
									
			Map<Integer, Artist> artistById = new HashMap<Integer, Artist>();
			for (Artist artist: artists) {
				artistById.put(artist.getId(), artist);
			}
			
			Map<Integer, Event> eventById = new HashMap<Integer, Event>();
			for (Event ev: DAORegistry.getEventDAO().getAll()) {
				eventById.put(ev.getId(), ev);
			}

			Map<Integer, Venue> venueById = new HashMap<Integer, Venue>();
			for (Venue venue: DAORegistry.getVenueDAO().getAll()) {
				venueById.put(venue.getId(), venue);
			}

			Date now = new Date();
			Collection<WebTicketListingCrawlRow> ticketListingCrawls = new ArrayList<WebTicketListingCrawlRow>();
			for (TicketListingCrawl crawl : ticketListingCrawler.getTicketListingCrawls()) {
				if (!crawl.isEnabled()) {
					disabledCrawlCount++;
				} else {
					if (crawl.getCrawlState().equals(CrawlState.STOPPED)
							&& (crawl.getEndCrawl() == null
									|| (now.getTime() - crawl.getEndCrawl().getTime()) > crawl.getCrawlFrequency() * 1000L)) {
						readyCrawlCount++;
					}
				}
								
				if (/*(artistId == 0 || crawl.getArtistId() == null || crawl.getTourId() == artistId)
						&& */(eventId == 0 || crawl.getEventId() == null || crawl.getEventId() == eventId)
						&& (siteId.length() == 0 || crawl.getSiteId().equals(siteId))
						&& (showDisabledCrawls || crawl.isEnabled())
						&& (statusTime.equals("ALL")
								|| (statusTime.equals("ERROR") && crawl.getCrawlState().isError())
								|| (statusTime.equals("1mn") && crawl.getNextCrawlWaitingTime() < 60)
								|| (statusTime.equals("30mn") && crawl.getNextCrawlWaitingTime() > 60 && crawl.getNextCrawlWaitingTime() < 1800)
								|| (statusTime.equals("1hr") && crawl.getNextCrawlWaitingTime() > 1800 && crawl.getNextCrawlWaitingTime() < 3600)
								|| (statusTime.equals("2hr") && crawl.getNextCrawlWaitingTime() > 3600 && crawl.getNextCrawlWaitingTime() < 7200)
								|| (statusTime.equals("2+hr") && crawl.getNextCrawlWaitingTime() > 7200)
						)
				) {
					// ticketListingCrawls.add(crawl);

					Event ev = eventById.get(crawl.getEventId());
					ticketListingCrawls.add(
							new WebTicketListingCrawlRow(crawl.getId(), crawl.getName(),
							crawl.getSiteId(), artistById.get(ev.getArtistId()), ev, 
							(ev == null)?null:venueById.get(ev.getVenueId()),
							crawl.getCrawlFrequency(), crawl.getAutomaticCrawlFrequency(),
							crawl.getNextCrawlWaitingTime(), crawl.getCrawlState(), 
							crawl.isEnabled(), crawl.isBroken(),
							crawl.getStartCrawl(), crawl.getEndCrawl(), crawl.getErrorMessage(),crawl.getQueryUrl(), crawl.getExtraParametersString()));
				}
			}
			

			mav.addObject("statusTime", statusTime);

			mav.addObject("ticketListingCrawler", ticketListingCrawler);
			mav.addObject("ticketListingCrawls", ticketListingCrawls);
			mav.addObject("events", events);			
						
			mav.addObject("totalTicketListingCrawlCount", ticketListingCrawler.getTicketListingCrawls().size());
			mav.addObject("failedTicketListingCrawlCount", ticketListingCrawler.getFailedTicketListingCrawlsByEventId().size());
			mav.addObject("readyTicketListingCrawlCount", readyCrawlCount);
			mav.addObject("disabledTicketListingCrawlCount", disabledCrawlCount);

			mav.addObject("editorCrawlerSiteId", siteId);
			mav.addObject("editorCrawlerTourId", Integer.toString(artistId));
			mav.addObject("editorCrawlerEventId", Integer.toString(eventId));
			mav.addObject("editorCrawlerShowDisabledCrawls", showDisabledCrawlsString);
			return mav;
		} else if (action.equals("enableCrawl")) {
			String[] crawlIdsString = request.getParameter("crawlIds").split(",");
			for (String crawlIdString : crawlIdsString) {
				int crawlId = Integer.parseInt(crawlIdString);
				TicketListingCrawl crawl = ticketListingCrawler.getTicketListingCrawlById(crawlId);
				crawl.setEnabled(true);
				// re activate tickets
				DAORegistry.getTicketDAO().enableTickets(crawlId);			
				DAORegistry.getTicketListingCrawlDAO().update(crawl);
				/*creationEventListManager.addSitesInSitesPerEventMap(crawl.getEventId(), crawl.getSiteId());*/
			}
			info = "Enabled " + crawlIdsString.length + " crawls";
		} else if (action.equals("disableCrawl")) {
			String[] crawlIdsString = request.getParameter("crawlIds").split(",");
			for (String crawlIdString : crawlIdsString) {
				int crawlId = Integer.parseInt(crawlIdString);
				TicketListingCrawl crawl = ticketListingCrawler.getTicketListingCrawlById(crawlId);
				crawl.setEnabled(false);
				crawl.setLastUpdated(new Date());
				crawl.setLastUpdater("MANUAL REMOVE");
				DAORegistry.getTicketDAO().disableTickets(crawlId);
				DAORegistry.getTicketListingCrawlDAO().update(crawl);
				/*creationEventListManager.disableSitesInSitesPerEventMap(crawl.getEventId(), crawl.getSiteId());*/
			}
			info = "Disabled " + crawlIdsString.length + " crawls";
		} else if (action.equals("forceRecrawl")) {
			String[] crawlIdsString = request.getParameter("crawlIds").split(",");
			Collection<TicketListingCrawl> crawls = new ArrayList<TicketListingCrawl>();
			for (String crawlIdString : crawlIdsString) {
				int crawlId = Integer.parseInt(crawlIdString);
				TicketListingCrawl crawl = ticketListingCrawler.getTicketListingCrawlById(crawlId);
				crawls.add(crawl);
			}
			ticketListingCrawler.forceRecrawlWithPriority(crawls,TicketListingCrawl.PRIORITY_LOW);
			info = "Forced recrawl of " + crawlIdsString.length + " crawls";
		} else if (action.equals("removeCrawl")) {
			String[] crawlIdsString = request.getParameter("crawlIds").split(",");
			for (String crawlIdString : crawlIdsString) {
				int crawlId = Integer.parseInt(crawlIdString);
				TicketListingCrawl crawl = ticketListingCrawler.getTicketListingCrawlById(crawlId);

				// delete all ticket listing associated to that crawl
				DAORegistry.getTicketDAO().deleteTicketsByCrawlId(crawlId);

				// delete crawl
				ticketListingCrawler.removeTicketListingCrawl(crawl);
				/*creationEventListManager.deleteSitesFromSitesPerEventMap(crawl.getEventId(), crawl.getSiteId());*/
				try {
					DAORegistry.getTicketListingCrawlDAO().deleteById(crawlId);
				} catch (Exception e) {
					logger.error("Error while deleting crawl from DB", e);
				}
				info += "The crawl " + crawl.getName() + " has been deleted!<br />";
			}
		} else if (action.equals("removeCrawl")) {
			int crawlId = Integer.parseInt(request.getParameter("crawlId"));
			TicketListingCrawl crawl = ticketListingCrawler.getTicketListingCrawlById(crawlId);
			if (crawl != null) {
				// delete all ticket listing associated to that crawl
				DAORegistry.getTicketDAO().deleteTicketsByCrawlId(crawlId);
	
				// delete crawl
				ticketListingCrawler.removeTicketListingCrawl(crawl);
				DAORegistry.getTicketListingCrawlDAO().deleteById(crawlId);
				info = "The crawl " + crawl.getName() + " has been deleted!";
			} else {
				info = "Problem while deleting the crawl " + crawlId + ". It has not been deleted!";				
			}
		} else if (action.equals("disableExpiredCrawl")) {
			Collection<Event> deletedExpiredEvents = DAORegistry.getEventDAO().getAllEventsByStatus(EventStatus.DELETEDEXPIRED);
			if(events!=null){
				for(Event deletedExpiredEvent:deletedExpiredEvents){
					for(TicketListingCrawl crawl: ticketListingCrawler.getTicketListingCrawlByEvent(deletedExpiredEvent.getId())) {
						if(crawl.isEnabled()){
							crawl.setEnabled(false);
							crawl.setLastUpdated(new Date());
							crawl.setLastUpdater("MANULA REMOV");
							DAORegistry.getTicketDAO().disableTickets(crawl.getId());
							DAORegistry.getTicketListingCrawlDAO().update(crawl);
						}
					}
					deletedExpiredEvent.setEventStatus(EventStatus.EXPIRED);
				}
				DAORegistry.getEventDAO().saveOrUpdateAll(events);
			}
		}

		String backUrl = request.getParameter("backUrl");
		if (backUrl == null) {
			return new ModelAndView(new RedirectView("EditorManageCrawls?info=" + info, true));
		} else {
			if (backUrl.contains("?")) {
				return new ModelAndView(new RedirectView(backUrl + "&info=" + info, true));
			} else {
				return new ModelAndView(new RedirectView(backUrl + "?info=" + info, true));				
			}		}
	}
	
	// Don't Delete this code.. This is a new implementation after 02.16.2013 -- End.

	public TicketListingCrawler getTicketListingCrawler() {
		return ticketListingCrawler;
	}

	public void setTicketListingCrawler(TicketListingCrawler ticketListingCrawler) {
		this.ticketListingCrawler = ticketListingCrawler;
	}
	
	public PreferenceManager getPreferenceManager() {
		return preferenceManager;
	}

	public void setPreferenceManager(PreferenceManager preferenceManager) {
		this.preferenceManager = preferenceManager;
	}
}
