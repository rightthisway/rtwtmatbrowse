package com.admitone.tmat.web;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.math.RoundingMode;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.acegisecurity.context.SecurityContextHolder;
import org.acegisecurity.ui.webapp.AuthenticationProcessingFilter;
import org.apache.commons.io.IOUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.velocity.tools.generic.DateTool;
import org.apache.velocity.tools.generic.NumberTool;
import org.springframework.ui.ModelMap;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.servlet.view.RedirectView;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.crawler.TicketListingCrawler;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.AdmitoneEvent;
import com.admitone.tmat.data.AdmitoneEventLocal;
import com.admitone.tmat.data.AdmitoneTicketMark;
import com.admitone.tmat.data.Artist;
import com.admitone.tmat.data.ArtistPriceAdjustment;
import com.admitone.tmat.data.ArtistPriceAdjustmentPk;
import com.admitone.tmat.data.Broker;
import com.admitone.tmat.data.Category;
import com.admitone.tmat.data.ChildTourCategory;
import com.admitone.tmat.data.DefaultPurchasePrice;
import com.admitone.tmat.data.DuplicateTicketMap;
import com.admitone.tmat.data.EbayInventoryGroup;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.EventPriceAdjustment;
import com.admitone.tmat.data.EventPriceAdjustmentPk;
import com.admitone.tmat.data.GrandChildTourCategory;
import com.admitone.tmat.data.ManagePurchasePrice;
import com.admitone.tmat.data.MarketMakerEventTracking;
import com.admitone.tmat.data.OpenOrderStatus;
import com.admitone.tmat.data.Preference;
import com.admitone.tmat.data.Quote;
import com.admitone.tmat.data.QuoteCustomer;
import com.admitone.tmat.data.QuoteManualTicket;
import com.admitone.tmat.data.SoldTicketDetail;
import com.admitone.tmat.data.Stat;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.data.TicketQuote;
import com.admitone.tmat.data.User;
import com.admitone.tmat.data.Venue;
import com.admitone.tmat.data.VenueCategory;
import com.admitone.tmat.enums.AdmitoneTicketMarkType;
import com.admitone.tmat.enums.BookmarkType;
import com.admitone.tmat.enums.EventStatus;
import com.admitone.tmat.enums.ProfitLossSign;
import com.admitone.tmat.enums.RemoveDuplicatePolicy;
import com.admitone.tmat.utils.AlphanumericSorting;
import com.admitone.tmat.utils.Categorizer;
import com.admitone.tmat.utils.PreferenceManager;
import com.admitone.tmat.utils.SpringUtil;
import com.admitone.tmat.utils.StatHelper;
import com.admitone.tmat.utils.TicketQuotesUtil;
import com.admitone.tmat.utils.TicketSorter;
import com.admitone.tmat.utils.TicketUtil;
import com.admitone.tmat.utils.mail.MailAttachment;
import com.admitone.tmat.utils.mail.MailManager;
import com.admitone.tmat.web.pojo.QuotesEvent;
import com.admitone.tmat.web.pojo.WebTicketRow;


/**
 * Class that handle the views for pages that can be accessed with the USER role.
 */
public class RetailController extends  MultiActionController {
	private PreferenceManager preferenceManager;
	private TicketListingCrawler ticketListingCrawler;
	private MailManager mailManager;
	private static Map<Integer, Map<String, ManagePurchasePrice>> managePurchasePriceMap= new HashMap<Integer, Map<String,ManagePurchasePrice>>();
	private static Map<Integer, Long> managePurchasePriceTimeMap= new HashMap<Integer, Long>();
	private static int UPDATE_FREQUENCY =5*60*1000;
	
	public ModelAndView loadMarketMakerEventTrackingUpdatePage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {		
		String creator = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);
		String[] eventIds = request.getParameter("eventId").split(",");
		String marketMakerName = request.getParameter("marketMaker");
		
		for (String eventIdStr: eventIds) {
			if (eventIdStr.trim().isEmpty()) {
				continue;
			}
			Integer eventId = Integer.valueOf(eventIdStr.trim());
			MarketMakerEventTracking oldMarketMakerEventTracking = DAORegistry.getMarketMakerEventTrackingDAO().getCurrentMarketMakerEventTracking(eventId);
			
			if (oldMarketMakerEventTracking != null) {
				if (oldMarketMakerEventTracking.getMarketMakerUsername().equals(marketMakerName)) {
					continue;
				}
				oldMarketMakerEventTracking.setEnabled(false);
				oldMarketMakerEventTracking.setEndDate(new Date());
				DAORegistry.getMarketMakerEventTrackingDAO().update(oldMarketMakerEventTracking);
			}
			
			if (!marketMakerName.equals("DESK")) {
				MarketMakerEventTracking marketMakerEventTracking = new MarketMakerEventTracking(creator,
						marketMakerName, eventId);
				DAORegistry.getMarketMakerEventTrackingDAO().save(marketMakerEventTracking);
			}
		}
		
		if (!request.getHeader("Referer").isEmpty()) {
			return new ModelAndView(new RedirectView(request.getHeader("Referer")));
		}
		
		return null;
	}
	
	/**
	 * Browse tour page
	 */
	
	/*public ModelAndView loadBrowseToursPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		String username = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);
		String artistId = request.getParameter("artistId");
		String filter = request.getParameter("filter");
		
		Collection<Tour> tours;

		String view = request.getParameter("view");
		if (view != null && !view.isEmpty()) {
			if (!view.equals("quick")) {
				view = null;
			}
			
			preferenceManager.updatePreference(username, "browseToursView", view);
		} else {
			view = preferenceManager.getPreferenceValue(username, "browseToursView");
		}

		ModelAndView mav;
		
		if (view == null || view.length() == 0) {
			mav = new ModelAndView("page-browse-tours");						
			mav.addObject("view", "normal");
		} else {
			mav = new ModelAndView("page-browse-tours-quick");
			mav.addObject("view", "quick");
		}
		
		if (artistId == null || artistId.isEmpty()) {
			if (filter != null && !filter.trim().isEmpty()) {
				tours = DAORegistry.getTourDAO().filterByName(filter);				
			} else {
				tours = DAORegistry.getTourDAO().getAll();
			}
			
		} else {
			mav.addObject("artistBookmark", DAORegistry.getBookmarkDAO().getBookmark(username, BookmarkType.ARTIST, Integer.valueOf(artistId)));
			if (filter != null && !filter.trim().isEmpty()) {
				tours = DAORegistry.getTourDAO().filterToursByArtistByName(Integer.parseInt(artistId), filter);														
			} else {
				tours = DAORegistry.getTourDAO().getAllToursByArtist(Integer.parseInt(artistId));										
			}
			//mav.addObject("artist", DAORegistry.getArtistDAO().get(Integer.parseInt(artistId)));
			mav.addObject("priceAdjustments", DAORegistry.getArtistPriceAdjustmentDAO().getPriceAdjustments(Integer.parseInt(artistId)));
		}
		
		mav.addObject("artists", DAORegistry.getArtistDAO().getAllActiveArtists());
		mav.addObject("tours", tours);
		mav.addObject("siteById", DAORegistry.getSiteDAO().getSiteByIdMap());
		mav.addObject("tourBookmarks", DAORegistry.getBookmarkDAO().getAllTourBookmarks(username));
		return mav;
	}
*/
	
	/**
	 * Browse artist page.
	 * does not seems to be used anymore.
	 */
	
	public ModelAndView loadBrowseArtistsPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String username = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);
		Collection<Artist> artists;
		
		String filter = request.getParameter("filter");
//		String venueId = request.getParameter("venueId");
		
		if (filter != null && !filter.trim().isEmpty()) {
			artists = DAORegistry.getArtistDAO().filterByName(filter, EventStatus.ACTIVE);
			
		} else {
			artists = DAORegistry.getArtistDAO().getAllActiveArtists();
		}
		ModelAndView mav = new ModelAndView("page-browse-artists");
		mav.addObject("artists", artists);
		mav.addObject("artistBookmarks", DAORegistry.getBookmarkDAO().getAllArtistBookmarks(username));
		mav.addObject("user", DAORegistry.getUserDAO().getUserByUsername(username));
		return mav;
	}
	
	
	/**
	 * Browse venue page.
	 * does not seems to be used anymore.
	 */
	
	public ModelAndView loadBrowseVenuePage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String username = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);
		ModelAndView mav = new ModelAndView("page-browse-venue");
		try{
			Collection<Venue> venue;
			

			String venueId = request.getParameter("venueId");
			String filter = request.getParameter("filter");
			
			if (filter != null && !filter.trim().isEmpty()) {
				venue = DAORegistry.getVenueDAO().filterByVenue(filter);
				mav.addObject("venue", venue);	
			}else if(venueId != null && !venueId.isEmpty()){
				Collection<Venue> venu =  DAORegistry.getVenueDAO().getVenueById(Integer.parseInt(venueId));
				mav.addObject("venue", venu);
			} else {
				venue = DAORegistry.getVenueDAO().getAll();
				mav.addObject("venue", venue);
			}
			
			
			mav.addObject("allVenue",DAORegistry.getVenueDAO().getAll());
			mav.addObject("venueBookmarks", DAORegistry.getBookmarkDAO().getAllVenueBookmarks(username));
			mav.addObject("user", DAORegistry.getUserDAO().getUserByUsername(username));
			mav.addObject("venueId", venueId);
			return mav;
			
		}catch(Exception e){
			e.printStackTrace();
			return mav;
		}
		
	}
	
	
	
	private void updateLastBrowsedEvent(String username, Integer eventId) {
		Preference lastBrowsedEvents = DAORegistry.getPreferenceDAO().getPreference(username, "lastBrowsedEventIds");
		if (lastBrowsedEvents == null) {
			lastBrowsedEvents = new Preference(username, "lastBrowsedEventIds", "" + eventId);
			DAORegistry.getPreferenceDAO().save(lastBrowsedEvents);
		} else {
			int j = 1;
			String[] eventIds = lastBrowsedEvents.getValue().split(",");
			String s = "" + eventId;
			for(int i = 0 ; i < eventIds.length && j < 10; i++) {
				if (Integer.parseInt(eventIds[i]) != eventId) {
					s += "," + eventIds[i];				
					j++;
				}
			}
			lastBrowsedEvents.setValue(s);
			DAORegistry.getPreferenceDAO().update(lastBrowsedEvents);
		}
	}

	/**
	 * Browse event page.
	 */
	public ModelAndView loadBrowseEventsPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String username = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);
		String artistId = request.getParameter("artistId");
		String venueId = request.getParameter("venueId");

		ModelAndView mav = new ModelAndView("page-browse-events");
//		Tour tour = null;
		Venue venue = null;
		Collection<Event> events;
		Artist artist = null;
//		Map<Integer, Venue> eventToVenue = null;
		if (artistId != null && !artistId.isEmpty()) {
			mav.addObject("artistBookmark", DAORegistry.getBookmarkDAO().getBookmark(username, BookmarkType.ARTIST, Integer.valueOf(artistId)));
			artist = DAORegistry.getArtistDAO().get(Integer.parseInt(artistId));
			events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(Integer.parseInt(artistId));//, EventStatus.ACTIVE);						
//			artist = tour.getArtist();
			//pdhebar performance changes
//			eventToVenue = new HashMap<Integer, Venue>();
//			for(Event event : events){
//				eventToVenue.put(event.getId(), event.getVenue());
//			}
//			mav.addObject("priceAdjustments", DAORegistry.getArtistPriceAdjustmentDAO().getPriceAdjustments(tour.getId()));	
			mav.addObject("artist", artist);
//			Date start = new Date();
		} else if(venueId != null && !venueId.isEmpty()){
			mav.addObject("venueBookmark",DAORegistry.getBookmarkDAO().getBookmark(username, BookmarkType.VENUE, Integer.valueOf(venueId)));
			events = DAORegistry.getEventDAO().getAllEventsByVenueAndStatus(Integer.parseInt(venueId), EventStatus.ACTIVE);
			venue = DAORegistry.getVenueDAO().get(Integer.parseInt(venueId));
		}else{
			events = DAORegistry.getEventDAO().getAllEventsByStatus(EventStatus.ACTIVE);
		}
		/*String catString = "";
		Integer cntOfCats = 0;
		String catList = "";
		Set<String> catSet = new TreeSet<String>();
		for(Category category : tour.getCategories()){
			catSet.add(category.getGroupName().trim());
			cntOfCats = cntOfCats + 1;
			catList = catList + category.getSymbol()+ ": " + category.getDescription() + "<br/>";
		}
		for(String s : catSet){
			catString = catString + s;
		}*/
		mav.addObject("artist", artist);
		mav.addObject("venue", venue);
		mav.addObject("marketMakers", DAORegistry.getUserDAO().getAll());
		//pdhebar performance changes
		/*mav.addObject("eventToVenue", eventToVenue);*/
		mav.addObject("siteById", DAORegistry.getSiteDAO().getSiteByIdMap());
		mav.addObject("events", events);
		mav.addObject("browseEventsPage", "BrowseEvents");
		mav.addObject("eventBookmarks", DAORegistry.getBookmarkDAO().getAllEventBookmarks(username));
		/*mav.addObject("cats",catString);
		mav.addObject("cntOfCats",cntOfCats);
		mav.addObject("catList",catList);*/
		return mav;
	}

	/**
	 * Browse event page.
	 */
/*	public ModelAndView loadBrowseEventsAnalyticPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String username = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);
		String tourId = request.getParameter("tourId");
		String venueId = request.getParameter("venueId");
		
		ModelAndView mav = new ModelAndView("page-browse-events");
		Tour tour = null;
		Venue venue=null;
		Collection<Event> events;
		Artist artist = null;
		Map<Integer, Venue> eventToVenue = null;
		if (tourId != null && !tourId.isEmpty()) {
			mav.addObject("tourBookmark", DAORegistry.getBookmarkDAO().getBookmark(username, BookmarkType.TOUR, Integer.valueOf(tourId)));
			tour = DAORegistry.getTourDAO().get(Integer.parseInt(tourId));
			events = DAORegistry.getEventDAO().getAllEventsByTour(Integer.parseInt(tourId), EventStatus.ACTIVE);						
			artist = tour.getArtist();
			//pdhebar performance improvement change
//			eventToVenue = new HashMap<Integer, Venue>();
//			for(Event event : events){
//				eventToVenue.put(event.getId(), event.getVenue());
//			}
			mav.addObject("priceAdjustments", DAORegistry.getArtistPriceAdjustmentDAO().getPriceAdjustments(tour.getId()));		
			Date start = new Date();
			Map<Integer, Map<String, Integer>> eventToActions = ActionIndicatorUtil.getActionsByTour(events);
			Date end = new Date();
			System.out.println("ACTIONINDCATOR: " + eventToActions);
			System.out.println("ACTIONINDCATOR Time: " + (end.getTime() - start.getTime()) + " in millis" );
			mav.addObject("eventToActions", eventToActions);				
		}else if(venueId != null && !venueId.isEmpty()){
			mav.addObject("venueBookmark",DAORegistry.getBookmarkDAO().getBookmark(username, BookmarkType.VENUE, Integer.valueOf(venueId)));
			events = DAORegistry.getEventDAO().getAllEventsByVenueAndStatus(Integer.parseInt(venueId), EventStatus.ACTIVE);
			venue = DAORegistry.getVenueDAO().get(Integer.parseInt(venueId));
			Map<Integer, Map<String, Integer>> eventToActions = ActionIndicatorUtil.getActionsByTour(events);
			
			mav.addObject("eventToActions", eventToActions);
		}else {
			events = DAORegistry.getEventDAO().getAllEventsByStatus(EventStatus.ACTIVE);
		}
		
		mav.addObject("venue", venue);
		mav.addObject("tour", tour);
		mav.addObject("artist", artist);
		mav.addObject("eventToVenue", eventToVenue);
		mav.addObject("siteById", DAORegistry.getSiteDAO().getSiteByIdMap());
		mav.addObject("events", events);
		mav.addObject("browseEventsPage", "BrowseEventsAnalytic");
		mav.addObject("eventBookmarks", DAORegistry.getBookmarkDAO().getAllEventBookmarks(username));
		return mav;
	}	*/

	/**
	 * Browse ticket page.
	 */
	public ModelAndView loadBrowseTicketsPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		//Query param eventid
		int eventId = Integer.parseInt(request.getParameter("eventId"));
		
		String view = request.getParameter("view");
		Boolean reset = "1".equals(request.getParameter("reset"));
		String username = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);	
		
		if (view == null) {
			if(reset) {
				view = "compact";
			}else {
				//this will be invoked at the first time in Browse tickets page
				view = preferenceManager.getPreferenceValue(username, "browseTicketView", "compact");				
			}
		}
		
		//Map declaration to track the time statistics for each flow
		Map<String, Long> timeStats = new HashMap<String, Long>();
		
		Boolean noCrawl="true".equals(request.getParameter("nocrawl"));
		String reloadTemp=request.getParameter("reload");
		Long reload=0l;
		if(reloadTemp!=null && !"".equals(reloadTemp)){
			reload= Long.parseLong(reloadTemp);	
		}
		
		//Fetch the event infor for an particular event id
		Event event = DAORegistry.getEventDAO().get(eventId);
		List<Integer> artistIds = new ArrayList<Integer>();
		artistIds.add(event.getArtistId());
		
		//creating preference for the user
		preferenceManager.updatePreference(username, "browseTicketView", view);
		
		Long handlePreferencesStartTime = System.currentTimeMillis();			
		
		timeStats.put("handlePreferences", System.currentTimeMillis() - handlePreferencesStartTime);
		
		Map<String, Object> map = handlePreferences(request);
		updateLastBrowsedEvent(username, eventId);

		/*
		 * Ticket filter
		 */
		boolean multisort = (Boolean)map.get("multisort");
		String categorySymbol = (String)map.get("categorySymbol");	
		String categorySymbols = (String)map.get("categorySymbols");
	
		List<Integer> categoryIds = (List<Integer>)map.get("categoryIds");
		Map<String, Boolean> catFilters = (Map<String, Boolean>)map.get("catFilters");
		String catScheme = (String)map.get("catScheme");
		String zoneCatScheme = (String)map.get("zoneCatScheme");
		String section = (String)map.get("section");
		String row = (String)map.get("row");
		Double startPrice = (Double)map.get("startPrice");
		Double endPrice = (Double)map.get("endPrice");
		Double zoneStartPrice = (Double)map.get("zoneStartPrice");
		Double zoneEndPrice = (Double)map.get("zoneEndPrice");
		
		Map<String, Boolean> siteFilters = (Map<String, Boolean>)map.get("siteFilters");
		ArrayList<Integer> quantities = (ArrayList<Integer>)map.get("quantities");
		RemoveDuplicatePolicy removeDuplicatePolicy = (RemoveDuplicatePolicy)map.get("removeDuplicatePolicy");
		int[] multisortFields = (int[])map.get("multisortFields");
		Map<String, Boolean> qtyFilters = (Map<String, Boolean>)map.get("qtyFilters");
		Map<String, Boolean> zoneQtyFilters = (Map<String, Boolean>)map.get("zoneQtyFilters");
		String show = (String)map.get("show");
		boolean showDuplicates = (Boolean) map.get("showDuplicates");
		Integer numTicketRows = (Integer) map.get("numTicketRows");
		String priceType = (String) map.get("priceType");
		String inHandType = (String) map.get("inHandType");
		
		//Mark up the ticket as duplicate for selected ticket for an request and event id
		Long startTimeMarkDuplicate = System.currentTimeMillis();
		handleMarkDuplicates(request, eventId);
		timeStats.put("handleMarkDuplicates", System.currentTimeMillis() - startTimeMarkDuplicate);	
		
		//Load all the tickets for an event in this list
		Collection<Ticket> tickets = new ArrayList<Ticket>();
		
		//Load categories for an event
		Long loadCategoriesStartTime = System.currentTimeMillis();
		Collection<Category> categories = null;
		VenueCategory venueCategory = null;
		
		if((catScheme==null || catScheme.isEmpty()) && event.getVenueCategory()!=null){
			categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(event.getVenueCategory().getId());
			venueCategory = event.getVenueCategory();
		}else{
			categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueIdAndCategoryGroup(event.getVenueId(), catScheme);
			venueCategory = DAORegistry.getVenueCategoryDAO().getVenueCategoryByVenueAndCategoryGroup(event.getVenueId(), catScheme);
		}
		timeStats.put("loadCategories", System.currentTimeMillis() - loadCategoriesStartTime);
		
		//Load ticket info for an event
		Long loadTicketsStartTime = System.currentTimeMillis();
		if ("completed".equals(show)) {
			// show completed  ticket					
			tickets = DAORegistry.getTicketDAO().getAllCompletedTicketsByEvent(eventId);
		} else {
			// show active ticket
			tickets = DAORegistry.getTicketDAO().getAllActiveTicketsByEvent(eventId);
		}
		timeStats.put("loadTickets", System.currentTimeMillis() - loadTicketsStartTime);

		//Assign categories for an event
		Long preAssignCategoriesStartTime = System.currentTimeMillis();		
		TicketUtil.preAssignCategoriesToTickets(tickets, categories); 
		timeStats.put("preAssignCategories", System.currentTimeMillis() - preAssignCategoriesStartTime);
		
		// Chart logic
		String tabName= request.getParameter("selectedTab");
		
		List<Ticket> filteredTickets = new ArrayList<Ticket>();
				
		ModelAndView mav = new ModelAndView("page-browse-tickets");
		//Event details 
		AdmitoneEventLocal admitoneEventLocal = null;
		if(event.getAdmitoneId()!=null){
			AdmitoneEvent admitoneEvent = DAORegistry.getAdmitoneEventDAO().getEventByEventId(event.getAdmitoneId());
			if(admitoneEvent!=null){
				mav.addObject("admitoneEventName", admitoneEvent.getEventName());
				mav.addObject("admitoneEventDate", admitoneEvent.getFormatedDate());
				
			}else{
				
				admitoneEventLocal =DAORegistry.getAdmitoneEventLocalDAO().getEventByVenueCityStateDateTime(event.getVenue().getBuilding(),event.getVenue().getCity(),event.getVenue().getState(), event.getLocalDate(), event.getLocalTime()); 
				if(admitoneEventLocal != null){
					mav.addObject("admitoneEventName", admitoneEventLocal.getEventName());
					mav.addObject("admitoneEventDate", admitoneEventLocal.getFormatedDate());
				} else {
					mav.addObject("admitoneEventName", "");
					mav.addObject("admitoneEventDate", null);
				
				}
			}
		}else{
			admitoneEventLocal =DAORegistry.getAdmitoneEventLocalDAO().getEventByVenueCityStateDateTime(event.getVenue().getBuilding(),event.getVenue().getCity(),event.getVenue().getState(), event.getLocalDate(), event.getLocalTime()); 
			if(admitoneEventLocal != null){
				mav.addObject("admitoneEventName", admitoneEventLocal.getEventName());
				mav.addObject("admitoneEventDate", admitoneEventLocal.getFormatedDate());
			} else {
				mav.addObject("admitoneEventName", "");
				mav.addObject("admitoneEventDate", null);
		
			}
		}
		
		/** Filtering tickets**/
		Long filteredTicketsStartTime = System.currentTimeMillis();
		List<Ticket> origTickets = new ArrayList<Ticket>();
				
		//Default purchase price for an exchange and ticket type
		List<DefaultPurchasePrice> defaultTourPrices = DAORegistry.getDefaultPurchasePriceDAO().getAllDefaultTourPrice();
		Map<String, DefaultPurchasePrice> defaultTourPriceMap = new HashMap<String, DefaultPurchasePrice>();
		if(defaultTourPrices!=null){
			for(DefaultPurchasePrice defaultTourPrice:defaultTourPrices){
				String key = defaultTourPrice.getExchange() + "-" + (defaultTourPrice.getTicketType()==null?"REGULAR":defaultTourPrice.getTicketType());
				defaultTourPriceMap.put(key, defaultTourPrice);
			}
		}
		
		Map<String, ManagePurchasePrice> tourPriceMap = managePurchasePriceMap.get(event.getId());
		Long time = managePurchasePriceTimeMap.get(event.getId());
		if(tourPriceMap==null){
			tourPriceMap= new HashMap<String, ManagePurchasePrice>();
		}
		Long now =new Date().getTime();
		if(time==null || now-time>UPDATE_FREQUENCY){
			List<ManagePurchasePrice> list = DAORegistry.getManagePurchasePriceDAO().getAllByManagePurchasePriceByArtistId(event.getArtistId());
			for(ManagePurchasePrice tourPrice:list){
				String mapKey = tourPrice.getExchange()+ "-" + (tourPrice.getTicketType());
				tourPriceMap.put(mapKey,tourPrice);
			}
			managePurchasePriceMap.put(event.getId(),tourPriceMap);
			managePurchasePriceTimeMap.put(event.getId(),now);
		}
		
		//Computation for ticket by event based on request parameters 
		TicketUtil.getFilteredTickets(eventId, categoryIds, quantities, siteFilters, section, row,// show, 
			origTickets, filteredTickets, null,tickets, null,categories, catScheme, true, startPrice, 
			endPrice, priceType, removeDuplicatePolicy, 
			timeStats,admitoneEventLocal,venueCategory,defaultTourPriceMap,tourPriceMap,inHandType);
		
		//view/Exclude cheapest category tickets logic  (pdhebar) 	
		String viewCheapestTickets = request.getParameter("viewCheapestTickets");
		String excludeCheapestTickets = request.getParameter("excludeCheapestTickets");
		mav.addObject("viewCheapestTickets", viewCheapestTickets);
		mav.addObject("excludeCheapestTickets", excludeCheapestTickets);
		if(viewCheapestTickets != null && !viewCheapestTickets.isEmpty())		
				filteredTickets = TicketUtil.viewCaterizedCheapestTickets(filteredTickets);	
		if(excludeCheapestTickets != null && !excludeCheapestTickets.isEmpty())
				filteredTickets = TicketUtil.excludeCaterizedCheapestTickets(filteredTickets);		
		//end cheapest ticket logic
		
		timeStats.put("filteredTickets", System.currentTimeMillis() - filteredTicketsStartTime);		
		
		Integer availableSeats = getAvailableTicketSeats(eventId, categoryIds, catScheme);
			
		
		// filters
		mav.addObject("tabName",tabName);
		
		mav.addObject("siteFilters", siteFilters);
		mav.addObject("qtyFilters", qtyFilters);
		mav.addObject("zoneQtyFilters", zoneQtyFilters);
		
		mav.addObject("noCrawl",noCrawl);
		mav.addObject("reload",reload);
		
		String browseTicketFilter = preferenceManager.getPreferenceValue(username, "browseTicketFilter", "0");
		mav.addObject("browseTicketFilter", browseTicketFilter);
		
		if (!removeDuplicatePolicy.equals(RemoveDuplicatePolicy.NONE)) {
			mav.addObject("duplicateTicketMap", DAORegistry.getDuplicateTicketMapDAO().getDuplicateTicketMap(eventId));
		}

		int removedDuplicateCount = 0;
		Double tempPrice = 0.00;
		List<WebTicketRow> newFilteredWebTicketRows = TicketUtil.getWebTicketRows(origTickets, filteredTickets, showDuplicates);
		
		List<WebTicketRow> ticketList= new ArrayList<WebTicketRow>(); 
		  if(priceType != null && priceType.equalsIgnoreCase("purchase")){
		   boolean priceRange= true;
		   for(WebTicketRow wTicketRow:newFilteredWebTicketRows){
			    priceRange = TicketUtil.isInPriceRange(wTicketRow, startPrice, endPrice, priceType);
			    if(!priceRange){
			     ticketList.add(wTicketRow);
			     priceRange = true;
			    }
		   }
		   newFilteredWebTicketRows.removeAll(ticketList);
		  }
		
		
		if (!showDuplicates) {
			removedDuplicateCount = origTickets.size() - filteredTickets.size();
		} else {
			removedDuplicateCount = TicketUtil.getRemovedDuplicateCount(newFilteredWebTicketRows);
		}		
		
		int qtyOffered = 0;

		int adminOneTicketsCount = 0;
		for(WebTicketRow ticket : newFilteredWebTicketRows){
			
			if (ticket.getSeller() != null && ticket.getSeller().equals("ADMT1")) {
				adminOneTicketsCount += ticket.getRemainingQuantity();
			}
			qtyOffered += ticket.getRemainingQuantity();
		}


		mav.addObject("numTicketRows", numTicketRows);
		mav.addObject("catGroupName", catScheme);		
		
		ArrayList<String> siteNames = DAORegistry.getTicketListingCrawlDAO().getAllCrawlerSiteName();
		mav.addObject("siteNames", siteNames);
		
		mav.addObject("zoneCatGroupName", zoneCatScheme);
		
		mav.addObject("catGroups", Categorizer.getCategoryGroupsByVenueId(event.getVenueId()));
		
		mav.addObject("qtyAvailable", availableSeats);
		mav.addObject("qtyOffered", new Integer(qtyOffered));
		mav.addObject("sectionFilter", section);
		mav.addObject("rowFilter", row);
		mav.addObject("startPriceFilter", startPrice);
		mav.addObject("endPriceFilter", endPrice);
		mav.addObject("priceType", priceType);
		mav.addObject("inHandType", inHandType);
		mav.addObject("zoneStartPrice", zoneStartPrice);
		mav.addObject("zoneEndPrice", zoneEndPrice);
				
		//Do an alphanumeric sort based on the section.
		//Collections.sort(newFilteredWebTicketRows, new WebTicketRowANSorting());
		int cnt = 1;
		for(WebTicketRow wrt : newFilteredWebTicketRows){
			wrt.setNumerizedSection(cnt);
			cnt++;
		}

		if (multisort) {
			Long sortTicketsStartTime = System.currentTimeMillis();
			TicketSorter.sortTickets(newFilteredWebTicketRows, multisortFields, catScheme);
			timeStats.put("sortTickets", System.currentTimeMillis() - sortTicketsStartTime);			
		}
		
		
		mav.addObject("webTickets", newFilteredWebTicketRows);
		ArrayList<Integer> zoneQuantityList = (ArrayList<Integer>)map.get("zoneQuantities");

		
		mav.addObject("tab",request.getParameter("tab"));
//		Tour tour = event.getTour();
//		mav.addObject("tour", tour);
		Artist artist = event.getArtist();
		mav.addObject("artist", artist);
		mav.addObject("venue", event.getVenue());
		mav.addObject("ticketCount", new Integer(qtyOffered));
		mav.addObject("removeDuplicatePolicy", removeDuplicatePolicy.toString());
		mav.addObject("removedDuplicateCount", removedDuplicateCount);
		mav.addObject("showDuplicates", showDuplicates);
		mav.addObject("multisort", multisort);
		mav.addObject("multisortField0", multisortFields[0]);
		mav.addObject("multisortField1", multisortFields[1]);
		mav.addObject("multisortField2", multisortFields[2]);
		mav.addObject("multisortField3", multisortFields[3]);		
		mav.addObject("event", event);
		mav.addObject("categorySymbol", categorySymbol);
		mav.addObject("categorySymbols", categorySymbols);
				
		mav.addObject("quantities", quantities);
		mav.addObject("zoneQuantities", zoneQuantityList);
		
		mav.addObject("show", show);
		mav.addObject("view", view);
		mav.addObject("categories", DAORegistry.getCategoryDAO().getAllCategoriesByVenueIdAndCategoryGroup(event.getVenueId(), catScheme));
    	mav.addObject("catFilters", catFilters);
		
		mav.addObject("adminOneTicketsCount", adminOneTicketsCount);
		
		mav.addObject("priceAdjustments", DAORegistry.getEventPriceAdjustmentDAO().getPriceAdjustments(event.getId()));		
		mav.addObject("siteById", DAORegistry.getSiteDAO().getSiteByIdMap());
		mav.addObject("customers", DAORegistry.getQuoteCustomerDAO().getAll());

		// bookmarks
		mav.addObject("ticketBookmarks", DAORegistry.getBookmarkDAO().getAllTicketBookmarks(username));
		mav.addObject("eventBookmark", DAORegistry.getBookmarkDAO().getBookmark(username, BookmarkType.EVENT, eventId));
		
		// crawls
		mav.addObject("crawls", ticketListingCrawler.getTicketListingCrawlsByEventId(eventId));
		mav.addObject("ticketListingCrawler", ticketListingCrawler);
		mav.addObject("lastFullUpdate", ticketListingCrawler.getLeastCrawledDateForEvent(eventId));

		mav.addObject("ticketsPage", "BrowseTickets");			

		int eventIndex = 0;
		for (int i = 0 ; i < artist.getEvents().size() ; i++) {
			if (artist.getEvents().get(i).getId().equals(event.getId())) {
				eventIndex = i;
				break;
			}
		}
		
		Event previousEvent = null;
		Event nextEvent = null;

		if (eventIndex > 0) {
			previousEvent = artist.getEvents().get(eventIndex - 1);
		}
		
		if (eventIndex < artist.getEvents().size() - 1) {
			nextEvent = artist.getEvents().get(eventIndex + 1);
		}
		mav.addObject("previousEvent", previousEvent);
		mav.addObject("nextEvent", nextEvent);	

		mav.addObject("timeStats", timeStats);
		mav.addObject("marketMaker", DAORegistry.getMarketMakerEventTrackingDAO().getCurrentMarketMakerForEvent(eventId));
		mav.addObject("marketMakers", DAORegistry.getUserDAO().getAll());
		
		TicketUtil.flushAdjustmentMaps();
				
		return mav;
	}	
	
	public void sendVenueMapAsEmail(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		String venueId = request.getParameter("venueId");
		String catScheme = request.getParameter("catScheme");
		String mailTo = request.getParameter("mailTo");
		
		String username = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);
		User user = DAORegistry.getUserDAO().getUserByUsername(username);
		String cc = user.getEmail();
		String venuName = DAORegistry.getVenueDAO().get(Integer.parseInt(venueId)).getBuilding();
		
		String filename =  "C:/TMATIMAGESFINAL/" + venueId + "_" + catScheme + ".gif";
	 	
	 	File file = new File(filename);
        
        if(!file.exists()) {
        	
        	PrintWriter out = response.getWriter();
    		out.println("No Map Found For "+venuName);
        }
        
        Map<String, Object> map = new HashMap<String, Object>();
        MailAttachment[] mailAttachments = new MailAttachment[1];       
        
		map.put("venuName", venuName);
		mailAttachments[0] = new MailAttachment(IOUtils.toByteArray(new FileInputStream(file)), "image/gif", venuName+".gif");
		
		String subject = venuName + " Seating Chart";
		String resource =  "mail-venue-map.txt";		
        String mimeType = "text/html";
		String from = DAORegistry.getPropertyDAO().get("smtp.from").getValue();
		String smtpHost = DAORegistry.getPropertyDAO().get("smtp.host").getValue();
		Integer smtpPort = Integer.valueOf(DAORegistry.getPropertyDAO().get("smtp.port").getValue());
		String smtpUsername = DAORegistry.getPropertyDAO().get("smtp.username").getValue();
		String smtpPassword = DAORegistry.getPropertyDAO().get("smtp.password").getValue();
		String smtpSecureConnection = DAORegistry.getPropertyDAO().get("smtp.secure.connection").getValue();
		Boolean smtpAuth = Boolean.valueOf(DAORegistry.getPropertyDAO().get("smtp.auth").getValue());
		String transportProtocol = "";

		mailManager.sendMailNow(smtpHost, smtpPort, transportProtocol, smtpSecureConnection, smtpAuth,
				smtpUsername, smtpPassword, null, from, mailTo, cc, null, subject,resource, map, mimeType, mailAttachments);
	
		PrintWriter out = response.getWriter();
		out.println("Email sent Successfully.");
	}
	
	private void handleMarkDuplicates(HttpServletRequest request, Integer eventId) {
		String dupAction = request.getParameter("dupAction");
		
		if (dupAction == null) {
			return;
		}

		String dups = request.getParameter("dups");
		if (dups == null) {
			return;
		}

		if(dupAction.equals("admit") || dupAction.equals("unadmit")){
			handleMarkAdmitOne(dupAction, dups, eventId);
			return;
		} 
		
		Map<Integer, DuplicateTicketMap> ticketMap = new HashMap<Integer, DuplicateTicketMap>();
		for (DuplicateTicketMap duplicateTicketMap: DAORegistry.getDuplicateTicketMapDAO().getAll()) {
			ticketMap.put(duplicateTicketMap.getTicketId(), duplicateTicketMap);
		}

		//If the dupAction request is create saveOrUpdate
		//the ticket
		if (dupAction.equals("create")) {
			Long groupId = System.currentTimeMillis();
	
			for(String ticketIdStr: dups.split(",")) {
				Integer ticketId = Integer.valueOf(ticketIdStr);
				
				DuplicateTicketMap duplicateTicketMap = ticketMap.get(ticketId);
				if (duplicateTicketMap != null) {
					DAORegistry.getDuplicateTicketMapDAO().deleteByGroupId(duplicateTicketMap.getGroupId());
				}
				
				duplicateTicketMap = new DuplicateTicketMap(ticketId, eventId, groupId);
				DAORegistry.getDuplicateTicketMapDAO().saveOrUpdate(duplicateTicketMap);
			}
		} else {
			for(String ticketIdStr: dups.split(",")) {
				Integer ticketId = Integer.valueOf(ticketIdStr);
				DAORegistry.getDuplicateTicketMapDAO().deleteById(ticketId);
			}
		}
		
		// remove all duplicates which consist of of one ticket
		DAORegistry.getDuplicateTicketMapDAO().deleteGroupsWithOneEntry(eventId);
	}
	

	private Map<String, Object> handlePreferences(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<String, Object>();
		String username = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);
		Boolean reset = "1".equals(request.getParameter("reset"));
		int eventId = Integer.parseInt(request.getParameter("eventId"));
		Event event = DAORegistry.getEventDAO().get(eventId);
		map.put("event", event);
		
		String eventIdString = "" + eventId;
		String lastEventIdString = preferenceManager.getPreferenceValue(username, "browseTicketLastEventId", "");
		preferenceManager.updatePreference(username, "browseTicketLastEventId", "" + eventIdString);
		
		boolean resetFilters = !eventIdString.equals(lastEventIdString);
		if (resetFilters) {
			preferenceManager.resetFilterPreferenceForUser(username);
		}
		
		/*
		 * Show duplicates
		 */
		
		String showDuplicatesString = request.getParameter("showDuplicates");
		if (showDuplicatesString == null && !reset) {
			showDuplicatesString = preferenceManager.getPreferenceValue(username, "browseTicketShowDuplicates", "false");
		}

		Boolean showDuplicates = false;
		try {
			showDuplicates = Boolean.parseBoolean(showDuplicatesString);
		} catch(Exception e) {
		}

		preferenceManager.updatePreference(username, "browseTicketShowDuplicates", showDuplicates.toString());
		map.put("showDuplicates", showDuplicates);

		/*
		 * Duplicate scrub filter
		 */
		String removeDuplicatePolicyString = request.getParameter("removeDuplicatePolicy");
		if (removeDuplicatePolicyString == null && !reset) {
		removeDuplicatePolicyString = preferenceManager.getPreferenceValue(username, "browseTicketRemoveDuplicatePolicy", "SUPER_SMART");
		}
		
		RemoveDuplicatePolicy removeDuplicatePolicy = RemoveDuplicatePolicy.SUPER_SMART;
		try {
			removeDuplicatePolicy = RemoveDuplicatePolicy.valueOf(removeDuplicatePolicyString);
		} catch (Exception e) {};

		preferenceManager.updatePreference(username, "browseTicketRemoveDuplicatePolicy", removeDuplicatePolicy.toString());
		map.put("removeDuplicatePolicy", removeDuplicatePolicy);
		
		/*
		 * Num ticket Rows 
		 */
		String numTicketRows = request.getParameter("numTicketRows");
		if (reset) {
			numTicketRows = "24";
		} else if (numTicketRows == null) {                              
			numTicketRows = preferenceManager.getPreferenceValue(username, "browseTicketNumTicketRows", "24");
		}
		
		preferenceManager.updatePreference(username, "browseTicketNumTicketRows", numTicketRows);		
		map.put("numTicketRows", Integer.valueOf(numTicketRows));
		
		/*
		 * Category filter
		 */
		String categorySymbol = request.getParameter("category");
		if (categorySymbol == null && !reset) {
			categorySymbol = preferenceManager.getPreferenceValue(username, "browseTicketCategory");
		}
		/*
		 * Category filter
		 */
		
		
		/*
		 * Category Scheme
		 */
		String catScheme = request.getParameter("catScheme");
			
		if(catScheme == null){
			catScheme = preferenceManager.getPreferenceValue(username, "browseTicketCatGroup");
		}
		
		preferenceManager.updatePreference(username, "browseTicketCategory", categorySymbol);
		preferenceManager.updatePreference(username, "browseTicketCatGroup", catScheme);
		
		Categorizer.fillCategoryFilterMap(event, catScheme, categorySymbol, map,"tickets",username);
		
		/*catScheme = request.getParameter("historyCatScheme");
		categorySymbol = request.getParameter("historyCategory");
		
		fillCategoryFilterMap(event, catScheme, categorySymbol, map,"history",username);
		
		catScheme = request.getParameter("liquidityCatScheme");
		categorySymbol = request.getParameter("liquidityCategory");
		
		fillCategoryFilterMap(event, catScheme, categorySymbol, map,"liquidity",username);
		
		catScheme = request.getParameter("statsCatScheme");
		categorySymbol = request.getParameter("statsCategory");
		
		fillCategoryFilterMap(event, catScheme, categorySymbol, map,"stats",username);*/
		
						

		//boolean wholesale = false;
		  String priceType = request.getParameter("priceType");
		  if(priceType == null || priceType.equalsIgnoreCase("") ){
		   //wholesaleStr = preferenceManager.getPreferenceValue(username, "browseTicketWholesale", "0");
		   priceType=null;
		  }else{
		   preferenceManager.updatePreference(username, priceType, "1");
		  }
		  /*Ticket filter issue - 03/29/2016*/
		  map.put("priceType", priceType);
		  
		  String inHandType = request.getParameter("inHandType");
		  if(inHandType == null || inHandType.equalsIgnoreCase("") ){
		   //wholesaleStr = preferenceManager.getPreferenceValue(username, "browseTicketWholesale", "0");
			  inHandType=null;
		  }/*else{
			  preferenceManager.updatePreference(username, priceType, "1");
		  }*/
		  
		  /*if (wholesaleStr.equals("1")) {
		   wholesale = true;
		  }*/
		  
		  map.put("inHandType", inHandType);	

		

		/*
		 * Show filter
		 */
		String show = request.getParameter("show");
		if (show == null && !reset) {
			show = preferenceManager.getPreferenceValue(username, "browseTicketShow");
		} else {
			preferenceManager.updatePreference(username, "browseTicketShow", show);
		}

		map.put("show", show);
		
		/*
		 * Section filter
		 */
		String section = request.getParameter("section");
		
		if (section == null  && !reset) {
			section = preferenceManager.getPreferenceValue(username, "browseTicketSection");
		}
		
		if (section != null) {
			section = section.toLowerCase();
			preferenceManager.updatePreference(username, "browseTicketSection", section);
		}
		map.put("section", section);
		
		/*
		 * Row filter
		 */
		String row = request.getParameter("row");
		if (row == null && !reset) {
			row = preferenceManager.getPreferenceValue(username, "browseTicketRow");
		}
		
		if (row != null) {
			row = row.toLowerCase();
			preferenceManager.updatePreference(username, "browseTicketRow", row);
		}	
		map.put("row", row);
		
		/*
		 * Start Price filter
		 */
		String startPrice = request.getParameter("startPrice");
		
		if (startPrice == null  && !reset) {
			startPrice = preferenceManager.getPreferenceValue(username, "browseTicketStartPrice");
		}
		Double sPrice = null;
		if (startPrice != null) {
			if(!startPrice.equals("")){ 
				sPrice = Double.parseDouble(startPrice);
			}
			preferenceManager.updatePreference(username, "browseTicketStartPrice", startPrice);
		}
		map.put("startPrice", sPrice);
		
		/*
		 * End Price filter
		 */
		String endPrice = request.getParameter("endPrice");
		
		if (endPrice == null  && !reset) {
			endPrice = preferenceManager.getPreferenceValue(username, "browseTicketEndPrice");
		}
		Double ePrice = null;
		if (endPrice != null) {
			if(!endPrice.equals("")){
				ePrice = Double.parseDouble(endPrice);
			}
			preferenceManager.updatePreference(username, "browseTicketEndPrice", endPrice);
		}
		map.put("endPrice", ePrice);
		
       String zoneStartPrice = request.getParameter("zoneStartPrice");
		
		if (zoneStartPrice == null  && !reset) {
			zoneStartPrice = preferenceManager.getPreferenceValue(username, "browseZoneStartPrice");
		}
		Double zoneSPrice = null;
		if (zoneStartPrice != null) {
			if(!zoneStartPrice.equals("")){ 
				zoneSPrice = Double.parseDouble(zoneStartPrice );
			}
			preferenceManager.updatePreference(username, "browseZoneStartPrice", zoneStartPrice);
		}
		map.put("zoneStartPrice", zoneSPrice);
		
		/*
		 * End Price filter
		 */
		String zoneEndPrice = request.getParameter("zoneEndPrice");
		
		if (zoneEndPrice == null  && !reset) {
			zoneEndPrice = preferenceManager.getPreferenceValue(username, "browseZoneEndPrice");
		}
		Double zoneEPrice = null;
		if (zoneEndPrice != null) {
			if(!zoneEndPrice.equals("")){
				zoneEPrice = Double.parseDouble(zoneEndPrice);
			}
			preferenceManager.updatePreference(username, "browseZoneEndPrice", zoneEndPrice);
		}
		map.put("zoneEndPrice", zoneEPrice);
		
		/*
		 * Quantity filter
		 */
		String quantityString = request.getParameter("quantity");
		fillQuantityFilterMap(quantityString, map, username,reset,"tickets");
		
		/*quantityString = request.getParameter("historyQuantity");
		fillQuantityFilterMap(quantityString, map, username,reset,"history");
		
		quantityString = request.getParameter("liquidityQuantity");
		fillQuantityFilterMap(quantityString, map, username,reset,"liquidity");
		
		quantityString = request.getParameter("statsQuantity");
		fillQuantityFilterMap(quantityString, map, username,reset,"stats");*/
		
		Map<String, Boolean> zoneQtyFilters = new HashMap<String, Boolean>();
		zoneQtyFilters.put("ALL", new Boolean(true));
		for(int i = 1 ; i <= 11 ; i++){
			zoneQtyFilters.put(i + "", new Boolean(false));
		}
		ArrayList<Integer> zoneQuantities = new ArrayList<Integer>();
		String zoneQuantityString = request.getParameter("zoneQuantity");
		if (zoneQuantityString == null && !reset) {
			zoneQuantityString = preferenceManager.getPreferenceValue(username, "browseZoneQuantity");
		} else {
			preferenceManager.updatePreference(username, "browseZoneQuantity", zoneQuantityString);
		}
		
		boolean lookForAllZone = true;
		
		if (zoneQuantityString != null && zoneQuantityString.length() > 0) {
			StringTokenizer tokenizer = new StringTokenizer(zoneQuantityString);
			lookForAllZone = false;
			while(tokenizer.hasMoreTokens()){
				String qty = tokenizer.nextToken(",").trim();
				if(qty.equals("ALL")){
					zoneQtyFilters.put(qty, new Boolean(true));
					lookForAllZone = true;
				} else if(!qty.isEmpty()) {
					zoneQtyFilters.put(qty, new Boolean(true));
					zoneQuantities.add(Integer.parseInt(qty));
				}
			}
		}
		if(lookForAllZone){
			zoneQuantities = new ArrayList<Integer>();
		} else {
			zoneQtyFilters.put("ALL", new Boolean(false));
		}
		
		map.put("zoneQtyFilters", zoneQtyFilters);
		map.put("zoneQuantities", zoneQuantities);
		
		/*
		 * SiteId filter
		 */
		Boolean uncheckSiteFilters = "1".equals(request.getParameter("uncheckSiteFilters"));
		Map<String, Boolean> siteFilters = new HashMap<String, Boolean>();
		
		String[] preferenceSiteIds;
		if (preferenceManager.getPreferenceValue(username, "browseTicketSites") == null) {
			preferenceSiteIds = Constants.getInstance().getSiteIds();
		} else {
			preferenceSiteIds = preferenceManager.getPreferenceValue(username, "browseTicketSites").split(",");
		}
		
		String preferenceSiteString="";
		int checkSiteIdCount = 0;
		for (String siteId: Constants.getInstance().getSiteIds()) {
			String checkedString = request.getParameter(siteId + "_checkbox");
			Boolean checked;
			if (checkedString == null && !reset && !uncheckSiteFilters) {
				// check if the site is in the preference
				checkedString = "false";
				for(String preferenceSiteId: preferenceSiteIds) {
					if (preferenceSiteId.equals(siteId)) {
						checkedString = "true";
						break;
					}
				}
			}
			if (checkedString == null) {
				checked = !uncheckSiteFilters;
			} else {
				checked = Boolean.parseBoolean(checkedString);
			}			
			
			if (checked) {
				preferenceSiteString += "," + siteId;
				checkSiteIdCount++;
			}
			siteFilters.put(siteId, checked);
			
		}

		if (!preferenceSiteString.isEmpty()) {
			preferenceSiteString = preferenceSiteString.substring(1);
		}
		//preerenceSiteString is too large to be inserted into the Db
		preferenceManager.updatePreference(username, "browseTicketSites", preferenceSiteString);

		String[] siteIds = new String[checkSiteIdCount];
		int j = 0;
		for (String site: siteFilters.keySet()) {
			if (siteFilters.get(site)) {
				siteIds[j++] = site;
			}
		}
				
		map.put("siteFilters", siteFilters);
		map.put("siteIds", siteIds);
		
		//preferenceManager.updatePreference(username, "browseTicketQuantity", quantityString);
		//map.put("quantities", quantities);

		String multisortString = request.getParameter("multisort");
		if (multisortString == null) {
			multisortString = preferenceManager.getPreferenceValue(username, "browseTicketMultisort", "0");
		} else {
			preferenceManager.updatePreference(username, "browseTicketMultisort", multisortString);
		}
		boolean multisort = "1".equals(multisortString);
		map.put("multisort", multisort);
		
		// the preference browseTicketMultisortFields should contains
		// field1,field2,field3,field4
		Integer[] multisortpreferenceFields;
		if (preferenceManager.getPreferenceValue(username, "browseTicketMultisortFields") == null) {
			multisortpreferenceFields = new Integer[] {0, 0, 0, 0};
		} else {
			String[] multisortpreferenceFieldsToken = preferenceManager.getPreferenceValue(username, "browseTicketMultisortFields", "").split(",");
			if (multisortpreferenceFieldsToken.length != 4) {
				multisortpreferenceFields = new Integer[] {0, 0, 0, 0};				
			} else {
				multisortpreferenceFields = new Integer[4];
				for(int i = 0 ; i < 4 ; i++) {
					try {
						multisortpreferenceFields[i] = Integer.parseInt(multisortpreferenceFieldsToken[i]);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			
		}
		
		String multisortFieldpreferenceString = "";
		int[] multisortFields = new int[4];
		for (int i = 0 ; i < 4 ; i++) {
			String multisortFieldString = request.getParameter("multisort_field" + i);
			if (multisortFieldString == null) {
				// check if the preference
				multisortFields[i] = multisortpreferenceFields[i];				
			} else {				
				try {
					multisortFields[i] = Integer.parseInt(multisortFieldString);
				} catch (Exception e) {};
			}
			multisortFieldpreferenceString += "," + multisortFields[i]; 
		}
		
		map.put("multisortFields", multisortFields);
		
		multisortFieldpreferenceString = multisortFieldpreferenceString.substring(1);
		preferenceManager.updatePreference(username, "browseTicketMultisortFields", multisortFieldpreferenceString);
		return map;
	}

	private List<EbayInventoryGroup> handleZoneTab(HttpServletRequest request,
			Map<String, Object> map, List<EbayInventoryGroup> ticketgroups,String username, Boolean reset,
			Event event) {
		HttpSession session = request.getSession();
		//Collection<Category> cats = (Collection<Category>) session.getAttribute("zoneCategories");
		 String zoneCatScheme =  session.getAttribute("tempZoneCatScheme")!=null? (String) request.getSession().getAttribute("tempZoneCatScheme"):"";
		List<Integer> zoneCategoryIds = new ArrayList<Integer>();
		/*start*/
		List<EbayInventoryGroup> newGroup = new ArrayList<EbayInventoryGroup>();
		String zoneCategorySymbol = request.getParameter("zoneCategory");
		if (zoneCategorySymbol == null && !reset) {
			zoneCategorySymbol = preferenceManager.getPreferenceValue(username, "browseTicketCategory");
		}
		String zoneCategorySymbols = "";
		
		Collection<Category> zoneCategories =(Collection<Category>) session.getAttribute("zoneCategories");
		List<String> zoneCatSymbolList = new ArrayList<String>();
		Map<String, Boolean> zoneCatFilters = new HashMap<String, Boolean>();
		zoneCatFilters.put("ALL", new Boolean(false));
		zoneCatFilters.put("CAT", new Boolean(false));
		zoneCatFilters.put("UNCAT", new Boolean(false));
		if(zoneCategories != null){
			
			for(Category cat: zoneCategories){
				if(zoneCatSymbolList.contains(cat.getSymbol())){
					continue;
				}
				zoneCatFilters.put(cat.getSymbol(), new Boolean(false));				
				zoneCategorySymbols = zoneCategorySymbols + cat.getSymbol() + ",";
				zoneCatSymbolList.add(cat.getSymbol());
				
			}
			
			if(zoneCatSymbolList!=null && !zoneCatSymbolList.isEmpty()){
				Collections.sort(zoneCatSymbolList);
				zoneCategorySymbols = zoneCatSymbolList.toString();
				zoneCategorySymbols=zoneCategorySymbols.substring(1, zoneCategorySymbols.length()-1);
			}
			
		}
		if (zoneCategorySymbol == null) {
			zoneCategorySymbol = ""; 
		}
		if (zoneCategorySymbol.equals("")) {
			zoneCategoryIds.add(Category.ALL_ID);
			zoneCatFilters.put("ALL", new Boolean(true));
			newGroup=ticketgroups;
		} else if(zoneCategorySymbol.length() >= 6) {
			
		} else if(zoneCategorySymbol.length() >= 4) {
			if (zoneCategorySymbol.substring(0,3).equals("ALL")) {
				zoneCategoryIds.add(Category.ALL_ID);
				zoneCatFilters.put("ALL", new Boolean(true));
				newGroup=ticketgroups;
			} 
		} 
				
		
		if(!zoneCategorySymbol.equals("")) {
			StringTokenizer tokenizer = new StringTokenizer(zoneCategorySymbol);
			while(tokenizer.hasMoreTokens()){
				String cat = tokenizer.nextToken(",").trim();
				if(!cat.equals("ALL") 					
						&& !cat.equals("")){
					zoneCatFilters.put(cat, new Boolean(true));
					Category category = DAORegistry.getCategoryDAO().getCategoryByVenueCategoryIdAndCategorySymbol(event.getVenueCategory().getId(), cat);
					if(category != null){
						zoneCategoryIds.add(category.getId());
					}
				}
			}
		}
			
		if(zoneCategoryIds.isEmpty()){
			zoneCategoryIds.add(Category.ALL_ID);
		}
		if(ticketgroups!=null){
		for(EbayInventoryGroup ticketgroup:ticketgroups){
			if(zoneCategoryIds.contains(ticketgroup.getEbayInventory1().getTicket().getCategory().getId())){
				newGroup.add(ticketgroup);
			}
		}
		}
		if(newGroup!=null){		
		Collections.sort(newGroup,new java.util.Comparator<EbayInventoryGroup>() {

			public int compare(EbayInventoryGroup o1, EbayInventoryGroup o2) {
				if(o1.getEbayInventory1().getTicket().getCategory().getSymbol()==null ||o2.getEbayInventory1().getTicket().getCategory().getSymbol()==null ){
					return 0;
				}
			
				int symbolDiff =o1.getEbayInventory1().getTicket().getCategory().getSymbol().compareTo(o2.getEbayInventory1().getTicket().getCategory().getSymbol()) ;
				if(symbolDiff==0){
					return o1.getQuantity().compareTo(o2.getQuantity());
				}else{
					return symbolDiff;
				}
			}
		});
		}
		map.put("zoneCatScheme", zoneCatScheme);
		map.put("zoneCatFilters", zoneCatFilters);
		map.put("zoneCategoryIds", zoneCategoryIds);
		map.put("zoneCategorySymbol", zoneCategorySymbol);
		map.put("zoneCategorySymbols", zoneCategorySymbols);
		return newGroup;
		
		
		
	}	
	
	public void fillQuantityFilterMap(String quantityString,Map<String,Object> map,String username,Boolean reset,String tabName){
		ArrayList<Integer> quantities = new ArrayList<Integer>();
		Map<String, Boolean> qtyFilters = new HashMap<String, Boolean>();
		qtyFilters.put("ALL", new Boolean(true));
		for(int i = 1 ; i <= 11 ; i++){
			qtyFilters.put(i + "", new Boolean(false));
		}
		
			if (quantityString == null && !reset) {
				quantityString = preferenceManager.getPreferenceValue(username, "browseTicketQuantity");
			} else {
				if(tabName.equals("tickets")){
					preferenceManager.updatePreference(username, "browseTicketQuantity", quantityString);
				}
			}
		
		boolean lookForAll = true;
		
		if (quantityString != null && quantityString.length() > 0) {
			StringTokenizer tokenizer = new StringTokenizer(quantityString);
			lookForAll = false;
			while(tokenizer.hasMoreTokens()){
				String qty = tokenizer.nextToken(",").trim();
				if(qty.equals("ALL")){
					qtyFilters.put(qty, new Boolean(true));
					lookForAll = true;
				} else if(!qty.isEmpty()) {
					qtyFilters.put(qty, new Boolean(true));
					quantities.add(Integer.parseInt(qty));
				}
			}
		}
		if(lookForAll){
			quantities = new ArrayList<Integer>();
		} else {
			qtyFilters.put("ALL", new Boolean(false));
		}
		if(tabName.equals("tickets")){
			map.put("qtyFilters", qtyFilters);
			map.put("quantities", quantities);
		}
		/*if(tabName.equals("liquidity")){
			map.put("liquidityQtyFilters", qtyFilters);
			map.put("liquidityQuantities", quantities);
		}
		if(tabName.equals("history")){
			map.put("historyQtyFilters", qtyFilters);
			map.put("historyQuantities", quantities);
		}
		if(tabName.equals("stats")){
			map.put("statsQtyFilters", qtyFilters);
			map.put("statsQuantities", quantities);
		}*/
		if(tabName.equals("crawls")){
			map.put("crawlsQtyFilters", qtyFilters);
			map.put("crawlsQuantities", quantities);
		}
		if(tabName.equals("filters")){
			map.put("filtersQtyFilters", qtyFilters);
			map.put("filtersQuantities", quantities);
		}
	}
	
		
	/**
	 * Index History Page. Used by the flash chart.
	 */
	public ModelAndView loadIndexHistory(HttpServletRequest request,
			   HttpServletResponse response) throws ServletException, IOException {
		try {
		String username = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);
		String catScheme = request.getParameter("catScheme");
		int eventId = Integer.parseInt(request.getParameter("eventId"));
		/*if(catScheme == null || catScheme.isEmpty()){
			catScheme = Categorizer.getCategoryGroupsByEvent(eventId).get(0);
		}*/
		Event event = DAORegistry.getEventDAO().get(eventId);
		if(event.getVenueCategory()!=null){
			catScheme = event.getVenueCategory().getCategoryGroup();
		}
		
		String categorySymbol = preferenceManager.getPreferenceValue(username, "browseTicketCategory");
		List<Integer> categoryIds = new ArrayList<Integer>();
		if (categorySymbol == null) {
			categorySymbol = ""; 
		}
		if (categorySymbol.equals("")) {
			categoryIds.add(Category.ALL_ID);
		} else if(categorySymbol.length() >= 3) {
			if (categorySymbol.substring(0,3).equals("ALL")) {
				categoryIds.add(Category.ALL_ID);
			} else if (categorySymbol.substring(0,3).equals("CAT")) {
				categoryIds.add(Category.CATEGORIZED_ID);	
			}
		} else if(categorySymbol.length() >= 5) {
			if (categorySymbol.substring(0,5).equals("UNCAT")) {
				categoryIds.add(Category.UNCATEGORIZED_ID);		
			} 
		} else {
			String[] tokens = categorySymbol.split(",");
			for(String cat: tokens) {
				cat = cat.trim();
				if(!cat.equals("ALL") && !cat.equals("CAT") && !cat.equals("UNCAT") && !cat.equals("")){
					Category category = DAORegistry.getCategoryDAO().getCategoryByVenueCategoryIdAndCategorySymbol(event.getVenueCategory().getId(), cat);
					if(category != null){
						categoryIds.add(category.getId());
					}
				}
			}
		}
		
		ArrayList<Integer> quantities = new ArrayList<Integer>();
		// quantity filter
		String quantityString = preferenceManager.getPreferenceValue(username, "browseTicketQuantity");
		try {
			if (!quantityString.contains("ALL")) {
				for (String q: quantityString.split(",")) {
					quantities.add(Integer.valueOf(q));
				}
			}
		} catch (Exception e) {
			logger.warn("Error parsing quantities: " + quantityString + " got to: " + quantities);
		}
		
		String section = preferenceManager.getPreferenceValue(username, "browseTicketSection");
		String row = preferenceManager.getPreferenceValue(username, "browseTicketRow");
		
		// site ids
		Map<String, Boolean> siteFilters = new HashMap<String, Boolean>();
		String[] preferenceSiteIds;
		if (preferenceManager.getPreferenceValue(username, "browseTicketSites") == null) {
			preferenceSiteIds = Constants.getInstance().getSiteIds();
		} else {
			preferenceSiteIds = preferenceManager.getPreferenceValue(username, "browseTicketSites").split(",");
		}
		
		int checkSiteIdCount = 0;
		for (String siteId: Constants.getInstance().getSiteIds()) {
			String checkedString = request.getParameter(siteId + "_checkbox");
			// check if the site is in the preference
			checkedString = "false";
			for(String preferenceSiteId: preferenceSiteIds) {
				if (preferenceSiteId.equals(siteId)) {
					checkedString = "true";
					break;
				}
			}
			boolean checked = Boolean.parseBoolean(checkedString);
			
			if (checked) {
				checkSiteIdCount++;
				siteFilters.put(siteId, checked);
			}
			
		}

		String[] siteIds = new String[checkSiteIdCount];		
		siteFilters.keySet().toArray(siteIds);
		
		// duplicate filter
		String removeDuplicatePolicyString = request.getParameter("removeDuplicatePolicy");
		if (removeDuplicatePolicyString == null) {
			removeDuplicatePolicyString = preferenceManager.getPreferenceValue(username, "browseTicketRemoveDuplicatePolicy", "MANUAL");
		}

		if (removeDuplicatePolicyString == null) {
			removeDuplicatePolicyString = "MANUAL";
		}
		
		RemoveDuplicatePolicy removeDuplicatePolicy = RemoveDuplicatePolicy.valueOf(removeDuplicatePolicyString);

		Date startDate = new Date(new Date().getTime() - 30L * 24L * 3600L * 1000L); 
		
		Double startPrice = null;
		if(preferenceManager.getPreferenceValue(username, "startPrice") != null && !preferenceManager.getPreferenceValue(username, "startPrice").isEmpty()){
			startPrice = Double.parseDouble(preferenceManager.getPreferenceValue(username, "startPrice"));
		}
		Double endPrice = null;
		if(preferenceManager.getPreferenceValue(username, "endPrice") != null && !preferenceManager.getPreferenceValue(username, "endPrice").isEmpty()){
			endPrice = Double.parseDouble(preferenceManager.getPreferenceValue(username, "endPrice"));
		}
		String show = preferenceManager.getPreferenceValue(username, "show");
		String priceType = null;
		if(preferenceManager.getPreferenceValue(username, "wholesale") != null && !preferenceManager.getPreferenceValue(username, "wholesale").isEmpty()){
			boolean wholesale = Boolean.parseBoolean(preferenceManager.getPreferenceValue(username, "wholesale"));
			if(wholesale)
				priceType = "wholesale";
		}
		List<Ticket> origTickets = new ArrayList<Ticket>();
		List<Ticket> filteredTickets = new ArrayList<Ticket>();
		List<EbayInventoryGroup> filteredZones = new ArrayList<EbayInventoryGroup>();
		List<Ticket> hTickets = new ArrayList<Ticket>();
		Collection<Ticket> tickets = new ArrayList<Ticket>();
		
		Long loadCategoriesStartTime = System.currentTimeMillis();
//		Event event = DAORegistry.getEventDAO().get(eventId);
//		Collection<Category> categories = DAORegistry.getCategoryDAO().getAllCategories(event.getTourId(), catScheme);
		Collection<Category> categories = null;
		VenueCategory venueCategory = null;
		if(event.getVenueCategory()!=null){
			categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(event.getVenueCategory().getId());
			venueCategory = event.getVenueCategory();
		}else{
			categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueIdAndCategoryGroup(event.getVenueId(), catScheme);
			venueCategory = DAORegistry.getVenueCategoryDAO().getVenueCategoryByVenueAndCategoryGroup(event.getVenueId(), catScheme);
		}
		Map<String, Long>  timeStats =new HashMap<String, Long>();
		timeStats.put("loadCategories", System.currentTimeMillis() - loadCategoriesStartTime);
		
		Long loadTicketsStartTime = System.currentTimeMillis();
		if ("completed".equals(show)) {
			// show completed  ticket					
			tickets = DAORegistry.getTicketDAO().getAllCompletedTicketsByEvent(eventId);
		} else {
			// show active ticket
			tickets = DAORegistry.getTicketDAO().getAllActiveTicketsByEvent(eventId);
		}

		timeStats.put("loadTickets", System.currentTimeMillis() - loadTicketsStartTime);

		Long preAssignCategoriesStartTime = System.currentTimeMillis();		
		TicketUtil.preAssignCategoriesToTickets(tickets, categories); 
		timeStats.put("preAssignCategories", System.currentTimeMillis() - preAssignCategoriesStartTime);
		List<DefaultPurchasePrice> defaultTourPrices = DAORegistry.getDefaultPurchasePriceDAO().getAllDefaultTourPrice();
		Map<String, DefaultPurchasePrice> defaultTourPriceMap = new HashMap<String, DefaultPurchasePrice>();
		if(defaultTourPrices!=null){
			for(DefaultPurchasePrice defaultTourPrice:defaultTourPrices){
				String key = defaultTourPrice.getExchange() + "-" + (defaultTourPrice.getTicketType()==null?"REGULAR":defaultTourPrice.getTicketType());
				defaultTourPriceMap.put(key, defaultTourPrice);
			}
		}
		
		Map<String, ManagePurchasePrice> tourPriceMap = managePurchasePriceMap.get(event.getId());
		Long time = managePurchasePriceTimeMap.get(event.getId());
		if(tourPriceMap==null){
			tourPriceMap= new HashMap<String, ManagePurchasePrice>();
		}
		Long now =new Date().getTime();
		if(time==null || now-time>UPDATE_FREQUENCY){
			List<ManagePurchasePrice> list = DAORegistry.getManagePurchasePriceDAO().getAllByManagePurchasePriceByArtistId(event.getArtistId());
			for(ManagePurchasePrice tourPrice:list){
				String mapKey = tourPrice.getExchange()+ "-" + (tourPrice.getTicketType());
				tourPriceMap.put(mapKey,tourPrice);
			}
			managePurchasePriceMap.put(event.getId(),tourPriceMap);
			managePurchasePriceTimeMap.put(event.getId(),now);
		}
		TicketUtil.getFilteredTickets(new Integer(eventId), categoryIds, quantities, siteFilters, section, row, //show, 
				origTickets, filteredTickets, hTickets,tickets,hTickets,categories, catScheme, true, startPrice, endPrice, priceType, removeDuplicatePolicy, timeStats,null,venueCategory,defaultTourPriceMap,tourPriceMap,null);		
		for (Stat stat: StatHelper.getEventCategorySiteQuantityLotStats(startDate, 30, filteredTickets, catScheme)) {
			String line = String.format("%1d,%2.2f,%3.2f,%4.2f,%5.2f,%6.2f,%7d,%8d,%9d,%10d,%11d,%12.2f\n",
					stat.getDate().getTime(), 
					stat.getAverage(),
					stat.getMedian(),
					stat.getMinPrice(), 
					stat.getMaxPrice(),
					stat.getDeviation(),
					stat.getCount(),
					stat.getMinQuantity(),
					stat.getMaxQuantity(),
					stat.getInTicketCount(),
					stat.getOutTicketCount(),
					stat.getLiquidityRatio());
			response.getOutputStream().write(line.getBytes());			
		}
	} catch (Exception e) {
		System.out.println("Caught exception in Charting: " + e);
		e.printStackTrace();
	}
		return null;
	}

	private void handleMarkAdmitOne(String action, String ticketIds, Integer eventId) {

		try {
				for(String ticketIdStr: ticketIds.split(",")) {
					Ticket thisTicket = DAORegistry.getTicketDAO().get(Integer.parseInt(ticketIdStr));
					if (action.equals("admit")) {
						AdmitoneTicketMark mark = DAORegistry.getAdmitoneTicketMarkDAO().get(thisTicket.getId());
						if (mark == null) {
							mark = new AdmitoneTicketMark(eventId, thisTicket.getId(), AdmitoneTicketMarkType.MARK);
							DAORegistry.getAdmitoneTicketMarkDAO().save(mark);
						} else if (mark.getType().equals(AdmitoneTicketMarkType.UNMARK)) {
							mark.setType(AdmitoneTicketMarkType.MARK);
							DAORegistry.getAdmitoneTicketMarkDAO().update(mark);
						}
					} else {
						AdmitoneTicketMark mark = DAORegistry.getAdmitoneTicketMarkDAO().get(thisTicket.getId());
						if (mark == null) {
							mark = new AdmitoneTicketMark(eventId, thisTicket.getId(), AdmitoneTicketMarkType.UNMARK);
							DAORegistry.getAdmitoneTicketMarkDAO().save(mark);
						} else if (mark.getType().equals(AdmitoneTicketMarkType.MARK)) {
							mark.setType(AdmitoneTicketMarkType.UNMARK);
							DAORegistry.getAdmitoneTicketMarkDAO().update(mark);
						}						
					}
				}
		} catch (Exception e) {
			System.out.println("Caught Exception waiting for scheduler in ViewController.markAdmitOne");
		}

	}
	
	private Integer getAvailableTicketSeats(Integer eventId, List<Integer> categoryIds, String catScheme) {
		
		Long startTime = System.currentTimeMillis();
		
		Collection<Category> categories = null;
		Event event = DAORegistry.getEventDAO().get(eventId);
		if (categoryIds.get(0).equals(Category.ALL_ID) || categoryIds.get(0).equals(Category.CATEGORIZED_ID)) {
			 categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueIdAndCategoryGroup(event.getVenueId(), catScheme);
		} else if (categoryIds.get(0).equals(Category.UNCATEGORIZED_ID)) {
			return 0;							
		} else {
			categories = new ArrayList<Category>();
			for(Integer catId : categoryIds){
				categories.add(DAORegistry.getCategoryDAO().get(catId));		
			}
		}
		int available = 0;
		for(Category cat : categories){
			Integer catSeatQty = cat.getSeatQuantity();
			if(catSeatQty != null) {
				available += catSeatQty;
			}
		}
		
		logger.info("getAvailableTicketSeats took " + (System.currentTimeMillis() - startTime));
		
		return new Integer(available);
	}
	
	public ModelAndView loadDownloadTicketsCsvPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		String username = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);

		Map<String, Long> timeStats = new HashMap<String, Long>();
		
		Boolean reset = "1".equals(request.getParameter("reset"));
		int eventId = Integer.parseInt(request.getParameter("eventId"));	
		
		Event event = DAORegistry.getEventDAO().get(eventId);
		
		String view = "compact";
		
		Long handlePreferencesStartTime = System.currentTimeMillis();			
		Map<String, Object> map = handlePreferences(request);
		timeStats.put("handlePreferences", System.currentTimeMillis() - handlePreferencesStartTime);		
		
		updateLastBrowsedEvent(username, eventId);

		/*
		 * View filter
		 */

		boolean multisort = (Boolean)map.get("multisort");
		
		String categorySymbol = (String)map.get("categorySymbol");	
		String categorySymbols = (String)map.get("categorySymbols");		
		List<Integer> categoryIds = (List<Integer>)map.get("categoryIds");
		Map<String, Boolean> catFilters = (Map<String, Boolean>)map.get("catFilters");
		String catScheme = (String)map.get("catScheme");
		/*if(catScheme == null || catScheme.isEmpty()){
			catScheme = Categorizer.getCategoryGroupsByEvent(eventId).get(0);
		}*/
		if(catScheme != null && event.getVenueCategory()!=null){
			catScheme = event.getVenueCategory().getCategoryGroup();
		}
		String section = (String)map.get("section");
		String row = (String)map.get("row");
		Double startPrice = (Double)map.get("startPrice");
		Double endPrice = (Double)map.get("endPrice");
		Map<String, Boolean> siteFilters = (Map<String, Boolean>)map.get("siteFilters");
		ArrayList<Integer> quantities = (ArrayList<Integer>)map.get("quantities");
		RemoveDuplicatePolicy removeDuplicatePolicy = (RemoveDuplicatePolicy)map.get("removeDuplicatePolicy");
		int[] multisortFields = (int[])map.get("multisortFields");
		Map<String, Boolean> qtyFilters = (Map<String, Boolean>)map.get("qtyFilters");		
		String show = (String)map.get("show");
		boolean showDuplicates = (Boolean) map.get("showDuplicates");
		Integer numTicketRows = (Integer) map.get("numTicketRows");
		String priceType = (String) map.get("priceType");
		String inHandType = (String) map.get("inHandType");						
		Long startTimeMarkDuplicate = System.currentTimeMillis();
		handleMarkDuplicates(request, eventId);
		timeStats.put("handleMarkDuplicates", System.currentTimeMillis() - startTimeMarkDuplicate);		

		List<Ticket> filteredTickets = new ArrayList<Ticket>();
		
		Long filteredTicketsStartTime = System.currentTimeMillis();
		List<Ticket> origTickets = new ArrayList<Ticket>();
		
		Collection<Ticket> tickets = new ArrayList<Ticket>();
		
		Long loadCategoriesStartTime = System.currentTimeMillis();
//		Event event = DAORegistry.getEventDAO().get(eventId);
//		Collection<Category> categories = DAORegistry.getCategoryDAO().getAllCategories(event.getTourId(), catScheme);
		Collection<Category> categories = null;
		VenueCategory venueCategory = null;
		if(event.getVenueCategory()!=null){
			categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(event.getVenueCategory().getId());
			venueCategory = event.getVenueCategory();
		}else{
			categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueIdAndCategoryGroup(event.getVenueId(), catScheme);
			venueCategory = DAORegistry.getVenueCategoryDAO().getVenueCategoryByVenueAndCategoryGroup(event.getVenueId(), catScheme);
		}
		timeStats.put("loadCategories", System.currentTimeMillis() - loadCategoriesStartTime);
		
		Long loadTicketsStartTime = System.currentTimeMillis();
		if ("completed".equals(show)) {
			// show completed  ticket					
			tickets = DAORegistry.getTicketDAO().getAllCompletedTicketsByEvent(eventId);
		} else {
			// show active ticket
			tickets = DAORegistry.getTicketDAO().getAllActiveTicketsByEvent(eventId);
		}

		timeStats.put("loadTickets", System.currentTimeMillis() - loadTicketsStartTime);

		Long preAssignCategoriesStartTime = System.currentTimeMillis();		
		TicketUtil.preAssignCategoriesToTickets(tickets, categories); 
		timeStats.put("preAssignCategories", System.currentTimeMillis() - preAssignCategoriesStartTime);
		
//		Collection<Ticket> hTickets = DAORegistry.getHistoricalTicketDAO().getAllTicketsByEvent(eventId);
		Collection<Ticket> hTickets = new ArrayList<Ticket>();
		Collection<Ticket> tempHTickets = DAORegistry.getTicketDAO().getAllCompletedTicketsByEvent(eventId);
		if(tempHTickets != null) {
			hTickets.addAll(tempHTickets);
		}

		TicketUtil.preAssignCategoriesToTickets(hTickets, categories);
		List<DefaultPurchasePrice> defaultTourPrices = DAORegistry.getDefaultPurchasePriceDAO().getAllDefaultTourPrice();
		Map<String, DefaultPurchasePrice> defaultTourPriceMap = new HashMap<String, DefaultPurchasePrice>();
		if(defaultTourPrices!=null){
			for(DefaultPurchasePrice defaultTourPrice:defaultTourPrices){
				String key = defaultTourPrice.getExchange() + "-" + (defaultTourPrice.getTicketType()==null?"REGULAR":defaultTourPrice.getTicketType());
				defaultTourPriceMap.put(key, defaultTourPrice);
			}
		}
		
		Map<String, ManagePurchasePrice> tourPriceMap = managePurchasePriceMap.get(event.getId());
		Long time = managePurchasePriceTimeMap.get(event.getId());
		if(tourPriceMap==null){
			tourPriceMap= new HashMap<String, ManagePurchasePrice>();
		}
		Long now =new Date().getTime();
		if(time==null || now-time>UPDATE_FREQUENCY){
			List<ManagePurchasePrice> list = DAORegistry.getManagePurchasePriceDAO().getAllByManagePurchasePriceByArtistId(event.getArtistId());
			for(ManagePurchasePrice tourPrice:list){
				String mapKey = tourPrice.getExchange()+ "-" + (tourPrice.getTicketType());
				tourPriceMap.put(mapKey,tourPrice);
			}
			managePurchasePriceMap.put(event.getId(),tourPriceMap);
			managePurchasePriceTimeMap.put(event.getId(),now);
		}
		TicketUtil.getFilteredTickets(eventId, categoryIds, quantities, siteFilters, section, row, //show, 
				origTickets, filteredTickets, null, tickets,hTickets,categories,catScheme, true, startPrice, endPrice, priceType, removeDuplicatePolicy, timeStats,null,venueCategory,defaultTourPriceMap,tourPriceMap,inHandType);
		
		List<WebTicketRow> newFilteredWebTicketRows = TicketUtil.getWebTicketRows(origTickets, filteredTickets, showDuplicates);
		
		StringBuffer stringBuffer = new StringBuffer();
		for (WebTicketRow ticketRow: newFilteredWebTicketRows) {
			String category = ticketRow.getCategorySymbol();
			if (category == null) {
				category = "";
			}
			
			stringBuffer.append(ticketRow.getQuantity() + "," + category + "," + ticketRow.getSection()
					   + "," + ticketRow.getRow() + "," + ticketRow.getAdjustedCurrentPrice() + "," + ticketRow.getCurrentPrice()
					   + "," + dateFormat.format(ticketRow.getInsertionDate()) + "," + dateFormat.format(ticketRow.getLastUpdate()) + "\n");
		}

		// output the result
//		Date today = new Date();
// 		DateFormat format = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
		
		OutputStream os = response.getOutputStream();
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		String dateStr="TBD";
		if(event.getDate()!=null){
			dateStr=dateFormat.format(event.getDate());
		}
		response.setHeader("Content-disposition",
				"attachment; filename=tickets_" + URLEncoder.encode(username) + "_" + URLEncoder.encode(event.getName()) 
				+ dateStr + ".csv");
		os.write(stringBuffer.toString().getBytes());
		return null;
	}
	
	public void getManualQuote(ModelAndView mav,java.sql.Date quoteDate,Integer customerId,
			String userName,Map<Integer, QuotesEvent> quotesEventMap){
		
		Collection<Quote> Quotes = DAORegistry.getQuoteDAO().getQuotesByUserCustomerAndDate(userName, customerId, quoteDate);
		Collection<QuoteManualTicket> manualQuotes = new ArrayList<QuoteManualTicket>();
		if(null != quotesEventMap && quotesEventMap.values().size() >0){
			
			for(QuotesEvent quoteEvObj :quotesEventMap.values()){
			
				for (Quote quoteObj : Quotes) {
					
					Collection<QuoteManualTicket> quoManObjList = DAORegistry.getQuoteManualTicketDAO().getAllQuoteManualTicket(quoteEvObj.getEvent().getId(), quoteObj.getId());
					if(null != quoManObjList){
						manualQuotes.addAll(quoManObjList);
					}
				}
			}
		}
		if(null == manualQuotes || manualQuotes.size() == 0){
			QuoteManualTicket  quoManObj = null;
			for(int i=0;i<1;i++){
				quoManObj = new QuoteManualTicket();
				manualQuotes.add(quoManObj);
			}	
		}
		mav.addObject("manualQuotes", manualQuotes);
		mav.addObject("manualQuotesSize", manualQuotes.size());
	}

	public ModelAndView loadQuotesPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String username = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);
		String creator = (String)request.getParameter("creator");
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		ModelAndView mav = null;
		String action = request.getParameter("action");		
		mav = new ModelAndView("page-quotes");
		User user = DAORegistry.getUserDAO().getUserByUsername(username);
		
		String from = "";
		if(user==null){
		 from = DAORegistry.getPropertyDAO().get("quotes.smtp.from").getValue();
		}else{
			from = user.getFirstName() + " " + user.getLastName();
		}
		
		Collection<String> creators = DAORegistry.getQuoteDAO().getQuoteUsernames();
		if (creators == null || creators.isEmpty()) {
			getManualQuote(mav, null, null, null, null);
			return mav;
		}

		if (creator == null) {
			if (creators.contains(username)) {
				creator = username;
			} else {
				creator = creators.iterator().next();
			}
		}
		
		Collection<QuoteCustomer> customers = DAORegistry.getQuoteCustomerDAO().getCustomersByUsername(creator);

		if (customers == null || customers.isEmpty()) {
			getManualQuote(mav, null, null, null, null);
			return mav;
		}
		
		
		QuoteCustomer customer = null;
		if (request.getParameter("customerId") == null) {
			Quote quote = DAORegistry.getQuoteDAO().getMostRecentQuote(creator);
			customer = quote.getCustomer();
		} else {
			customer = DAORegistry.getQuoteCustomerDAO().get(Integer.valueOf(request.getParameter("customerId")));
			if (DAORegistry.getQuoteDAO().getQuotesByUserAndCustomer(creator, customer.getId()).isEmpty()) {
				Quote quote = DAORegistry.getQuoteDAO().getMostRecentQuote(creator);
				customer = quote.getCustomer();
			}
		}

		Collection<java.sql.Date> dates = DAORegistry.getQuoteDAO().getQuotesDatesByCustomer(customer.getId());
		java.sql.Date date = null;
		if (request.getParameter("date") == null && (action == null || !action.startsWith("delete"))) {
			date = dates.iterator().next();
		} else {
			try {
				date = new java.sql.Date(dateFormat.parse(request.getParameter("date")).getTime());
				if (!dates.contains(date)) {
					date = dates.iterator().next();
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				getManualQuote(mav, null, null, null, null);
				e.printStackTrace();
				return mav;
			} 
		}
		
		Collection<Quote> quotes = DAORegistry.getQuoteDAO().getQuotesByCustomerAndDate(customer.getId(), date);
		String quoteIdStr = request.getParameter("quoteId");
		Quote quote;
		if (quoteIdStr == null) {
			quote = quotes.iterator().next();
		} else {
			quote = DAORegistry.getQuoteDAO().get(Integer.valueOf(quoteIdStr));
			if (!quote.getCustomer().getName().equals(customer.getName())) {
				quote = quotes.iterator().next();
			}
		}
		List<Integer> ids = new ArrayList<Integer>();
		for(Quote q:quotes){
			ids.add(q.getId());
			
		}
		//Collection<TicketQuote> ticketQuotes = new ArrayList<TicketQuote>();
		Collection<TicketQuote> ticketQuotes = null;
		Collection<TicketQuote>  zoneQuotes= new ArrayList<TicketQuote>();
		if(quoteIdStr != null){
			ticketQuotes = DAORegistry.getTicketQuoteDAO().getTicketQuotesByQuoteId(quote.getId());
		}else{
			ticketQuotes = DAORegistry.getTicketQuoteDAO().getTicketQuotesByQuoteIds(ids);
		}
		if(ticketQuotes!=null && !ticketQuotes.isEmpty()){
		for(TicketQuote tq: ticketQuotes){
			if(tq.getZone()!=null && !tq.getZone().equalsIgnoreCase("")){
				zoneQuotes.add(tq);
			}
		}		
		}
		Map<Integer, QuotesEvent> quotesEventMap = new HashMap<Integer, QuotesEvent>();
		String defaultSubject = "Your Ticket Quote ";
		String lastEventName="";
		for (QuotesEvent quotesEvent: TicketQuotesUtil.getQuotesEvent(ticketQuotes)) {
			for(TicketQuote tQuote:quotesEvent.getQuotes()){
				if(tQuote.getFileName() != null){
					quotesEvent.setSavedFile(tQuote.getFileName().replaceAll("\\\\+", "\\\\\\\\"));
					break;
				}
				
			}
			quotesEventMap.put(quotesEvent.getEvent().getId(), quotesEvent);
			lastEventName = quotesEvent.getEvent().getName().trim();
			/*if(TicketQuotesUtil.getQuotesEvent(ticketQuotes).size() == 1){
			defaultSubject +=" - "+ quotesEvent.getEvent().getName() +" ("+quotesEvent.getEvent().getFormatedDate()+")";
			}*/
		}
		if(quotesEventMap.size()>1){
			defaultSubject += " - Multiple Events";
		}else{
			defaultSubject += " - " + lastEventName;
		}
		if (action != null) {
			try {
				if (action.equalsIgnoreCase("deleteQuotes")) {
//					int customerId = Integer.valueOf(request.getParameter("customerId"));
					DAORegistry.getQuoteDAO().delete(quote);
					return new ModelAndView(new RedirectView("Quotes?info=" + customer.getName() + "'s quotes have been deleted."));
				} else if (action.equalsIgnoreCase("deleteCustomer")) {
					int customerId = Integer.valueOf(request.getParameter("customerId"));
					DAORegistry.getQuoteCustomerDAO().deleteById(customerId);
					return new ModelAndView(new RedirectView("Quotes?info=" + customer.getName() + " has been deleted."));
				} else if (action.equalsIgnoreCase("updateEmail")) {
//					int customerId = Integer.valueOf(request.getParameter("customerId"));
//					java.sql.Date date = new java.sql.Date(dateFormat.parse(request.getParameter("date")).getTime());
//					String email = request.getParameter("email");
//					TicketQuoteCustomer customer = DAORegistry.getTicketQuoteCustomerDAO().get(customerId);
//					customer.setEmail(email);
//					DAORegistry.getTicketQuoteCustomerDAO().update(customer);
				} else if (action.toLowerCase().startsWith("send") || action.toLowerCase().startsWith("preview")) {
//					User user = DAORegistry.getUserDAO().get(username);
					String to = request.getParameter("to");
					String bcc = request.getParameter("bcc");
					//customer.setEmail(to);
					DAORegistry.getQuoteCustomerDAO().update(customer);
					String subject = request.getParameter("subject");
					String header = request.getParameter("header");
					String footer = request.getParameter("footer");
					String sendingZoneData = request.getParameter("sendingZoneData");
					String sendingTicketData = request.getParameter("sendingTicketData");
					if (footer == null) {
						footer = "";
					}
					
					for (TicketQuote ticketQuote: ticketQuotes) {
						String markPrice = request.getParameter("markedPrice_" + ticketQuote.getId());
						if(markPrice!=null && !markPrice.equals("")){
							markPrice = markPrice.replaceAll(",", "");
							ticketQuote.setMarkedPrice(Double.valueOf(markPrice));
						}
						
						Integer remQty = ticketQuote.getTicket() != null ? ticketQuote.getTicket().getRemainingQuantity() : 0;
						String zoneMarkPrice = request.getParameter("zoneMarkedPrice_" + ticketQuote.getId());
						if(zoneMarkPrice!=null && !zoneMarkPrice.equals("")){
							zoneMarkPrice = zoneMarkPrice.replaceAll(",", "");
							ticketQuote.setZoneMarkedPrice(Double.valueOf(zoneMarkPrice));
						}
						
											
						if(request.getParameter("qty_" + ticketQuote.getId()) != null && (remQty >= Integer.valueOf(request.getParameter("qty_" + ticketQuote.getId())))){
						 ticketQuote.setQty(Integer.valueOf(request.getParameter("qty_" + ticketQuote.getId())));
						}
					}
					DAORegistry.getTicketQuoteDAO().updateAll(ticketQuotes);
					
			        MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest)request;
			        multipartHttpServletRequest.getFileNames();
			        ArrayList<Integer> eventIdList = new ArrayList<Integer>();
			        ArrayList<MailAttachment> mailAttachmentsList = new ArrayList<MailAttachment>();	
			        Collection<TicketQuote> tQuotes = new ArrayList<TicketQuote>();
			        for (Object entryObj: multipartHttpServletRequest.getFileMap().entrySet()) {
			        	Map.Entry<String, MultipartFile> entry = (Map.Entry<String, MultipartFile>)entryObj; 
			        	Integer eventId = Integer.parseInt(entry.getKey().replaceAll("map_", ""));
			        	MultipartFile file = entry.getValue();
			        	if (!file.isEmpty()) {
			        		eventIdList.add(eventId);
				        	quotesEventMap.get(eventId).setBoundToImage(true);
				        	for(TicketQuote tQuote:quotesEventMap.get(eventId).getQuotes()){
				        		tQuote.setFileName("S:\\TMATIMAGESFINAL\\"+file.getOriginalFilename());
				        		tQuotes.add(tQuote);
				        	}
				        	String imageType = file.getOriginalFilename().replaceAll(".*\\.", "");
				        	quotesEventMap.get(eventId).setImageType(imageType);
				        	mailAttachmentsList.add(new MailAttachment(IOUtils.toByteArray(file.getInputStream()), "image/" + imageType, DAORegistry.getEventDAO().get(eventId).getName()+ "." + imageType));
			        	}
			        }
			        for(QuotesEvent q:quotesEventMap.values()){
			        	String fileName = request.getParameter("map_"+q.getEvent().getId());
			        	if(fileName != null && !"".equals(fileName)){
			        	File file = new File(fileName);
			        	if (file.exists()) { 
			        		boolean flag = true;
				        	q.setBoundToImage(true);
				        	String imageType = file.getName().replaceAll(".*\\.", "");
				        	q.setImageType(imageType);
				        	if(eventIdList.contains(q.getEvent().getId())){
				        			flag = false;				        			
				        		}
				        	
				        	if(flag){
				        	//mailAttachmentsList.add(new MailAttachment(IOUtils.toByteArray(new FileInputStream(file)), "image/" + imageType, q.getEvent().getName() + "." + imageType));
				        	q.setSavedFile(fileName);
				        	for(TicketQuote tQuote:q.getQuotes()){
				        		tQuote.setFileName(fileName);
				        		tQuotes.add(tQuote);
				        	}
				        	}
			        	}
			        	}
			        }
			        DAORegistry.getTicketQuoteDAO().saveOrUpdateAll(tQuotes);
			        MailAttachment[] mailAttachments = new MailAttachment[mailAttachmentsList.size()];
			        int i = 0;
			        for (MailAttachment mailAttachment: mailAttachmentsList) {
			        	mailAttachments[i++] = mailAttachment; 
			        }

					Map<String, Object> map = new HashMap<String, Object>();
					Collection<QuoteManualTicket> manualQuoteTickets = sendManualQuotesTickets(request, username);
					if(null != manualQuoteTickets && manualQuoteTickets.size() >0){
			        	map.put("manualQuoteTickets", manualQuoteTickets);
			        	map.put("isManualQuote", Boolean.TRUE);
			        }else{
			        	map.put("isManualQuote", Boolean.FALSE);
			        }
					map.put("customer", customer);
					map.put("user", DAORegistry.getUserDAO().getUserByUsername(username));
					map.put("header", header.replaceAll("\n", "<br/>\n").replaceAll("%CUSTOMER%", customer.getName()));
					map.put("footer", footer.replaceAll("\n", "<br/>\n"));
					map.put("quotesEvents", quotesEventMap.values());
				    map.put("date", new DateTool());
				    map.put("number", new NumberTool());
				    map.put("mapQuoteReference", quote.getReferral());
				    map.put("sendingZoneData", sendingZoneData);
				    map.put("sendingTicketData", sendingTicketData);
			       // String imageFilename = request.getSession().getServletContext().getRealPath("") + "/images/rightthisway_logo.gif";
					//mailAttachments[0] = new MailAttachment(IOUtils.toByteArray(new FileInputStream(imageFilename)), "image/gif", "rightthisway_logo.gif");
					String smtpHost = DAORegistry.getPropertyDAO().get("quotes.smtp.host").getValue();
					Integer smtpPort = Integer.valueOf(DAORegistry.getPropertyDAO().get("quotes.smtp.port").getValue());
					String smtpUsername = DAORegistry.getPropertyDAO().get("quotes.smtp.username").getValue();
					String smtpPassword = DAORegistry.getPropertyDAO().get("quotes.smtp.password").getValue();
					String smtpSecureConnection = DAORegistry.getPropertyDAO().get("quotes.smtp.secure.connection").getValue();
					Boolean smtpAuth = Boolean.valueOf(DAORegistry.getPropertyDAO().get("quotes.smtp.auth").getValue());
					String transportProtocol = "";
					String info= "Email not sent.";
					
					if(action.toLowerCase().startsWith("send") && to != null && !"".equals(to)){						
					mailManager.sendMailNow(smtpHost, smtpPort, transportProtocol, smtpSecureConnection, smtpAuth, smtpUsername, smtpPassword, from,user.getEmail(), to, null, bcc, subject, "mail-quotes.html", map, "text/html", mailAttachments);
					quote.setSentDate(new Date());
					quote.setSentTo(to);
					info= "Email sent.";
					}
					DAORegistry.getQuoteDAO().saveOrUpdate(quote);
					return new ModelAndView(new RedirectView("Quotes?creator=" + creator + "&customerId=" + customer.getId() + "&date=" + dateFormat.format(date) + "&quoteId=" + quote.getId() + "&info="+info));
				}
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		getManualQuote(mav, date, customer.getId(), username, quotesEventMap);			
		String customerWithCapFirstLetter = customer.getName().substring(0, 1).toUpperCase() + customer.getName().substring(1); 
		mav.addObject("quotesHeader", DAORegistry.getPropertyDAO().get("quotes.header").getValue().replaceAll("%CUSTOMER%", customerWithCapFirstLetter));
		//mav.addObject("quotesHeader","Thank you for choosing Admit One. The following quotes are for your review. Please let me know if you have any questions.");
		mav.addObject("quotesFooter", DAORegistry.getPropertyDAO().get("quotes.footer").getValue().replaceAll("%CUSTOMER%", customerWithCapFirstLetter));
		mav.addObject("from", from);
		mav.addObject("user", DAORegistry.getUserDAO().getUserByUsername(username));
		mav.addObject("customers", customers);
		mav.addObject("creator", creator);
		mav.addObject("creators", creators);
		mav.addObject("customer", customer);
		mav.addObject("quote", quote);
		mav.addObject("quoteDates", dates);
		mav.addObject("quotes", quotes);
		mav.addObject("zoneQuotes", zoneQuotes);		
		mav.addObject("quotesEvents", quotesEventMap.values());
		mav.addObject("subject", defaultSubject);
		return mav;
	}
	
	public Collection<QuoteManualTicket> sendManualQuotesTickets(HttpServletRequest request,String userName) throws ParseException{
		Collection<QuoteManualTicket> manualQuotes = new ArrayList<QuoteManualTicket>();
		int size =0;
		
		SimpleDateFormat ds =new SimpleDateFormat("MM/dd/yyyy");
		
		if(null != request.getParameter("manualQuoteRowCount") &&
				request.getParameter("manualQuoteRowCount").trim().length() >0){
			size = Integer.parseInt(request.getParameter("manualQuoteRowCount").trim());
			Integer eventId = null;
			Integer quoteId =null;
			Date quoteDate = null;
			QuoteManualTicket manualTicket = null;
			Event event = new Event();
			Quote quote = new Quote();
			
			for(int i=0;i<size;i++){
				
				if(null != request.getParameter("rowHiddenId_"+i) &&
						request.getParameter("rowHiddenId_"+i).trim().length() >0){
					
					if(null != request.getParameter("tQty_"+i) && request.getParameter("tQty_"+i).trim().length() >0){
						if(i==0){
							 eventId = Integer.parseInt(request.getParameter("eventId").trim());
							 quoteId =Integer.parseInt(request.getParameter("quoteId").trim());
							 quoteDate = new java.sql.Date(ds.parse(request.getParameter("date")).getTime());
							 event = DAORegistry.getEventDAO().get(eventId);
							 quote = DAORegistry.getQuoteDAO().get(quoteId);						 
						}					
						manualTicket = new QuoteManualTicket();
						if(null != request.getParameter("manualQuoteId_"+i) &&
								request.getParameter("manualQuoteId_"+i).trim().length() >0){
							manualTicket.setId(Integer.parseInt(request.getParameter("manualQuoteId_"+i).trim()));
						}			
						
						manualTicket.setCreator(userName);
						manualTicket.setCustomer(quote.getCustomer());
						manualTicket.setDate(quoteDate);
						manualTicket.setEvent(event);
						manualTicket.setQuote(quote);
						manualTicket.setMarkedPrice(Double.valueOf(request.getParameter("tMarkedPrice_"+i)));
						manualTicket.setPrice(Double.valueOf(request.getParameter("tPrice_"+i)));
						manualTicket.setQty(Integer.parseInt(request.getParameter("tQty_"+i).trim()));
						manualTicket.setRow(request.getParameter("tRow_"+i));
						manualTicket.setSection(request.getParameter("tSection_"+i));
						manualQuotes.add(manualTicket);
					}
					
					
				}
				
			}
		}
		
		if(null != manualQuotes && manualQuotes.size() >0){
			DAORegistry.getQuoteManualTicketDAO().saveOrUpdateAll(manualQuotes);
		}
		return manualQuotes;
	}
	
	public ModelAndView loadViewQuotesPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String username = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);
		String creator = (String)request.getParameter("creator");
		
		ModelAndView mav = new ModelAndView("page-view-quotes");
		try{
 		listquotes(request, username, creator, mav,true);
		}catch(Exception e){
			//e.printStackTrace();
			}
		return mav;
	}
	
	public ModelAndView loadListQuotesPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String username = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);
		String creator = (String)request.getParameter("creator");
				
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		ModelAndView mav = new ModelAndView("page-list-quotes");
		listquotes(request, username, creator, mav);
		
		return mav;
	}
	public void listquotes(HttpServletRequest request, String username,
			String creator, ModelAndView mav) {
		listquotes(request, username,
				creator,  mav,false);
		
	}
	public void listquotes(HttpServletRequest request, String username,
			String creator, ModelAndView mav,boolean both) {
		String quoteType = (String)request.getParameter("type");
		mav.addObject("type", quoteType);
		if(request.getParameter("action") != null && "delete".equals(request.getParameter("action"))){
			String ticketQuoteId = request.getParameter("ticketQuoteId");
			
			if(ticketQuoteId != null && !"".equals(ticketQuoteId)){
				TicketQuote ticketQuote = DAORegistry.getTicketQuoteDAO().get(Integer.valueOf(ticketQuoteId));
				List<Integer> ids = new ArrayList<Integer>();
				if(ticketQuote != null){
				Integer quoteId = ticketQuote.getQuote().getId();	
				Collection<TicketQuote> ticketQuotes = DAORegistry.getTicketQuoteDAO().getTicketQuotesByQuoteId(ticketQuote.getQuote().getId());
				for(TicketQuote tq:ticketQuotes){
					if(tq.getTicket().getEvent().getId().equals(ticketQuote.getTicket().getEvent().getId())){
						ids.add(tq.getId())	;
					}
				}
				DAORegistry.getTicketQuoteDAO().deleteByIds(ids);
				Quote quote = DAORegistry.getQuoteDAO().get(quoteId);					
				DAORegistry.getQuoteDAO().delete(quote);
				}
			}
		}
		Collection<String> creators = DAORegistry.getQuoteDAO().getQuoteUsernames();
		QuoteCustomer customer = null;
		if (creator == null) {
			if (creators.contains(username)) {
				creator = username;
			} else {
				creator = creators.iterator().next();
			}
		}
		Collection<QuoteCustomer> customers = null;
		
		if(creator.equals("ALL")){
			customers = DAORegistry.getQuoteCustomerDAO().getAll();
		}else{
			customers = DAORegistry.getQuoteCustomerDAO().getCustomersByUsername(creator);
		}
		mav.addObject("customers", customers);
		List<Quote> quotes = new ArrayList<Quote>();
		//List<Quote> zoneQuotes = new ArrayList<Quote>();
		String customerParam = request.getParameter("customerId");
		if(customerParam == null)
			customerParam = "ALL";
		ArrayList<String> emails = new ArrayList<String>();		
		if(customerParam != null && !"ALL".equals(customerParam)){
			customer = DAORegistry.getQuoteCustomerDAO().get(Integer.valueOf(customerParam));
			
				quotes = (List<Quote>) DAORegistry.getQuoteDAO().getQuotesByCustomer(Integer.valueOf(customerParam));
				emails.add(customer.getEmail());
			
		}else if(customerParam.equals("ALL")){			
				quotes = (List<Quote>) DAORegistry.getQuoteDAO().getQuotes(false);
			
		}
		if(quotes!=null) {
		for(Quote q:quotes){
			if(q.getSentTo()!= null){
			for(String emailString:q.getSentTo().split(",")){
				if(!emails.contains(emailString)){
					emails.add(emailString);
				}
			}
			}
		}
	 }
		
		quotes.removeAll(quotes);
				
		String email = request.getParameter("email");
		String status = request.getParameter("status");
		if(email == null){
			email = "ALL";
		}
		if(status == null){
			status = "1";
		}
		String fromDate = request.getParameter("fromDay");
		String fromMonth = request.getParameter("fromMonth");
		String fromYear = request.getParameter("fromYear");
		String toDate = request.getParameter("toDay");
		String toMonth = request.getParameter("toMonth");
		String toYear = request.getParameter("toYear");
		Date d = new Date();
		if(fromDate == null){
			fromDate = Integer.valueOf(d.getDate()).toString();
		}
		if(fromMonth == null){
			fromMonth = Integer.valueOf(d.getMonth()+1).toString();
		}
		if(fromYear == null){
			fromYear = Integer.valueOf(d.getYear()+1900).toString();
		}
		if(toDate == null){
			toDate = Integer.valueOf(d.getDate()).toString();
		}
		if(toMonth == null){
			toMonth = Integer.valueOf(d.getMonth()+1).toString();
		}
		if(toYear == null){
			toYear = Integer.valueOf(d.getYear()+1900).toString();
		}
		if((request.getParameter("fetch") != null && "fetch".equals(request.getParameter("fetch"))) || (request.getParameter("action") != null && "delete".equals(request.getParameter("action")))){
			try{
				String custId = request.getParameter("customerId");
				if (request.getParameter("customerId") == null) {
					quotes = (List<Quote>) DAORegistry.getQuoteDAO().getAll();
					customer = quotes.get(0).getCustomer();
				}
				if(custId != null && !"ALL".equals(custId)){
					customer = DAORegistry.getQuoteCustomerDAO().get(Integer.valueOf(custId));
					custId = customer.getId().toString();
				}
				if(custId != null && "ALL".equals(custId)){
					custId = "ALL";
				}	
			
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
				
			java.sql.Date fromQuoteDate = null;
			if(fromDate != null &&fromMonth != null && fromYear != null){
				fromQuoteDate = new java.sql.Date(dateFormat.parse(fromMonth+"/"+fromDate+"/"+fromYear).getTime());	
			}			
			
			java.sql.Date toQuoteDate = null;
			if(toDate  != null && toMonth != null && toYear != null){
				DateFormat toDateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss aa");
				toQuoteDate = new java.sql.Date(toDateFormat.parse(toMonth+"/"+toDate+"/"+toYear+" 23:59:59 PM").getTime());
			}
			
			quotes = DAORegistry.getQuoteDAO().getFilteredQuotes(creator,custId,fromQuoteDate,toQuoteDate,email,status);
						
			mav.addObject("append", true);
			mav.addObject("status",status);
			mav.addObject("email",email);
			
			}catch (Exception e) {
				// TODO: handle exception
			}
		}
		
		List<Integer> ids = new ArrayList<Integer>();		
		if(quotes != null && !quotes.isEmpty()){
		for(Quote qt:quotes){			
			ids.add(qt.getId());
		}	
		}
		
		List<TicketQuote> ticketQuotes = new ArrayList<TicketQuote>();
		if(quotes != null && quotes.size() != 0){
			ticketQuotes = DAORegistry.getTicketQuoteDAO().getTicketQuotesByQuoteIds(ids);	
		}
		
		if(null != ticketQuotes && ticketQuotes.size() >0 ){
			try{
			ticketQuotes = getManualQuoteTicketsByQuote(ids, ticketQuotes);
			}catch(Exception e){
				//e.printStackTrace();
				}
		}
		
		List<TicketQuote> finalTicketQuotes = new ArrayList<TicketQuote>();
		if(ticketQuotes.size() > 0){
			finalTicketQuotes.addAll(ticketQuotes);
		}
				
		if(ticketQuotes!=null && !ticketQuotes.isEmpty()){
		for(TicketQuote ticketQuote:ticketQuotes){
			boolean flag = false;
			for(TicketQuote finalTicketQuote:finalTicketQuotes){
				if(finalTicketQuote.getTicket() 
						!= null && ticketQuote.getTicket()!= null && finalTicketQuote.getTicket().getEvent().getId().equals(ticketQuote.getTicket().getEvent().getId()) && finalTicketQuote.getQuote().getId().equals(ticketQuote.getQuote().getId())){
					flag = true ;
					break;
				}
			}
			if(flag == false){				
				finalTicketQuotes.add(ticketQuote);
			}
		}	
		}		
		mav.addObject("ticketQuotes", finalTicketQuotes);		
		mav.addObject("emails",emails);
		mav.addObject("creators", creators);
		mav.addObject("creator", creator);
		mav.addObject("customer", customer);		
		mav.addObject("quotes", quotes);
		mav.addObject("fromDate",fromDate);
		mav.addObject("fromMonth",fromMonth);
		mav.addObject("fromYear",fromYear);
		mav.addObject("toDate",toDate);
		mav.addObject("toMonth",toMonth);
		mav.addObject("toYear",toYear);
		mav.addObject("users", DAORegistry.getUserDAO().getAll());
		mav.addObject("ticketIds", (String)request.getParameter("ticketIds"));
		mav.addObject("groupIds", (String)request.getParameter("groupIds"));		
		mav.addObject("customerName", (String)request.getParameter("customer"));
		mav.addObject("quoteDate", (String)request.getParameter("quoteDate"));
		mav.addObject("markupPercentAddition", (String)request.getParameter("markup"));		
		
		
		 
	}
	
	public List<TicketQuote> getManualQuoteTicketsByQuote(List<Integer> quoteIds,List<TicketQuote> ticketQuotes){

		List<TicketQuote> newTicketQuotes = new ArrayList<TicketQuote>();	
		List<QuoteManualTicket> list = DAORegistry.getQuoteManualTicketDAO().getAllQuoteManualTicketByQuoteID(quoteIds);
		TicketQuote ticketQuote = null;
		Ticket newTicket  = null;
		List<Integer> dupList = new ArrayList<Integer>();
		
		
		for (TicketQuote ticketQuoteObj : ticketQuotes) {
			
			newTicketQuotes.add(ticketQuoteObj);
			Ticket ticket  = ticketQuoteObj.getTicket();
			try{
			if(!dupList.contains(ticketQuoteObj.getTicket().getEventId())){
				for (QuoteManualTicket quoteManualTicket : list) {
				
					if(ticketQuoteObj.getTicket().getEventId().equals(quoteManualTicket.getEvent().getId())){
						
						newTicket = new Ticket();
						ticketQuote = new TicketQuote();
						ticketQuote.setQuote(quoteManualTicket.getQuote());
						ticketQuote.setQty(quoteManualTicket.getQty());
						ticketQuote.setTicketId(ticket.getId());
						newTicket = ticketQuote.getTicket();
						newTicket.setId(null);
						newTicket.setSection(quoteManualTicket.getSection());
						newTicket.setRow(quoteManualTicket.getRow());
						ticketQuote.setTicket(newTicket);
						ticketQuote.setIsManQuoteTic('Y');
						ticketQuote.setId(quoteManualTicket.getId());					
						newTicketQuotes.add(ticketQuote);
					}
				}
			}
			}catch(Exception e){
				//e.printStackTrace();
				}
			dupList.add(ticketQuoteObj.getTicket().getEventId());
			
		}
		
		return newTicketQuotes;
	}
	
	public void deleteTicketQuote(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException {
		String ticketOuoteId = request.getParameter("ticketOuoteId");
		String message = "TicketQuote not deleted";
		if(ticketOuoteId != null){
			DAORegistry.getTicketQuoteDAO().deleteById(Integer.valueOf(ticketOuoteId));
			message ="TicketQuote deleted successfully";
		}
		IOUtils.write(message.getBytes(), response.getOutputStream());
	}
	
	public void updateTicketQuote(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException {
		String ticketQuoteId = request.getParameter("ticketQuoteId");
		String ticketQty = request.getParameter("ticketQty");
		String zone = request.getParameter("zone");
		String message = "TicketQuote is not updated";
		if(ticketQuoteId != null){
			TicketQuote quote= DAORegistry.getTicketQuoteDAO().get(Integer.valueOf(ticketQuoteId));
			if(quote!=null){
				try{
					int qty = Integer.valueOf(ticketQty);
					if(zone.equals("0")){
						quote.setQty(qty);
					}else{
						quote.setZoneQty(qty);
					}
					DAORegistry.getTicketQuoteDAO().update(quote);
					message ="TicketQuote updated successfully";
				}catch (Exception ex) {
					message ="Quantity must be whole no.";
				}
			}else{
				message ="TicketQuote is not found";
			}
		}
		IOUtils.write(message.getBytes(), response.getOutputStream());
	}
	
	public ModelAndView loadEditorManageTCAP(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
//		String username = (String) request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);
//		String action = request.getParameter("action");
		Collection<Event> events = null;
		String artistIdString = request.getParameter("artistId");
		String venueIdString = request.getParameter("venueId");
		EventPriceAdjustment eventPriceAdjustment = null;
		Integer eventId;
		ModelAndView mav = new ModelAndView("page-editor-manage-tcap");
		List<Artist> artists = (List<Artist>) DAORegistry.getArtistDAO().getAllActiveArtists();
		List<Venue> venues = (List<Venue>)DAORegistry.getVenueDAO().getAll();
	
		Collections.sort(artists, new Comparator<Artist>(){
			public int compare(Artist t1,	Artist t2) {
				AlphanumericSorting ans = new AlphanumericSorting();
				return ans.compare(t1.getName(), t2.getName());
			}
		}
		);
		
		mav.addObject("artists", artists);
		mav.addObject("venues", venues);
		
		if (artistIdString == null && venueIdString == null) {
			// look for a value in the preference
			//tourIdString = preferenceManager.getPreferenceValue(username, "editorCrawlerTourId");
			return mav;
		}
		
		int artistId = 0;
		if (artistIdString != null && artistIdString.length() > 0) {
			try{	
				artistId = Integer.parseInt(artistIdString);
			}catch(Exception e){
				e.printStackTrace();
			}
			if (artistId != 0 && artistId != -1 && DAORegistry.getArtistDAO().get(artistId) == null) {
				artistId = 0;
			}else{
				if(artistId == -1){
					events = DAORegistry.getEventDAO().getAllActiveEvents();
				}else{
					events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(artistId);
				}
			}
		}
		
		int venueId = 0;
		if (venueIdString!=null && venueIdString.length()>0){
			try{
			venueId = Integer.parseInt(venueIdString);
			}catch(Exception e){
				e.printStackTrace();
			}
			if (venueId != 0 && DAORegistry.getVenueDAO().get(venueId) == null) {
				venueId = 0;
			}else{
			events = DAORegistry.getEventDAO().getAllEventsByVenueAndStatus(venueId,EventStatus.ACTIVE);
		}
		}
		mav.addObject("events", events);
		String siteId = "stubhub";
		ArtistPriceAdjustment artistPriceAdjustment = DAORegistry.getArtistPriceAdjustmentDAO().get(new ArtistPriceAdjustmentPk(artistId, siteId));
		Double percentAdjustment = 0.0;
	
		if (events!=null){
			for (Event event:events){
				eventId = event.getId();
				
				eventPriceAdjustment = DAORegistry.getEventPriceAdjustmentDAO().get(new EventPriceAdjustmentPk(eventId, siteId));
				
				if (eventPriceAdjustment!=null){
					percentAdjustment = eventPriceAdjustment.getPercentAdjustment();
				}
				else {
					if (artistPriceAdjustment != null){
						percentAdjustment = artistPriceAdjustment.getPercentAdjustment();
					}
				}
				mav.addObject("shPercentAdjustment",percentAdjustment);
			}
		}
		
//		if(artistPriceAdjustment != null){
//			mav.addObject("shtourPriceAdjustment",artistPriceAdjustment.getPercentAdjustment());
//		}
		
		mav.addObject("editorTCAPArtistId", Integer.toString(artistId));
		//mav.addObject("editorTCAPEventId", Integer.toString(eventId));	
		mav.addObject("editorTCAPVenueId", Integer.toString(venueId));
		return mav;				
	}
	
	// Mehul : Open orders for rtw added on - 01/06/2015
	public ModelAndView loadOpenOrderPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		ModelAndView mav = new ModelAndView("page-open-order");
		String brokerIdStr = request.getParameter("brokerId");
		String focusRowStr = request.getParameter("focusRow");
		String profitLossSignStr = request.getParameter("profitLossSign");
		
		String userName = SecurityContextHolder.getContext().getAuthentication().getName();
		User user = DAORegistry.getUserDAO().getUserByUsername(userName);
		Integer brokerId= null;
		Integer eventId = 0;
		Integer artistId = 0;
		Integer venueId = 0;
		//Broker broker = user.getBroker();
		//System.out.println("broker.."+broker.getId());
		/*if(broker.getId()==1){
			List<Broker> brokers = DAORegistry.getBrokerDAO().getAllActiveAutopricingBrokers();
			mav.addObject("brokers",brokers);
		}*/
		//brokerId = broker.getId();
		
		if(brokerIdStr!=null && !brokerIdStr.isEmpty()){
			brokerId = Integer.parseInt(brokerIdStr);
		}
		List<Broker> brokers = DAORegistry.getBrokerDAO().getAllOpenOrderEligibleBrokers();
		mav.addObject("brokers",brokers);
		//mav.addObject("broker", broker);
		mav.addObject("selectedBrokerId", brokerIdStr);
		String artistIdStr = request.getParameter("artistId");
		String venueIdStr = request.getParameter("venueId");
		
		String eventIdStr = request.getParameter("eventId");
		String selectedValue = request.getParameter("selectedVal");
		
		mav.addObject("artistId", artistIdStr);	
		mav.addObject("venueId", venueIdStr);
//		mav.addObject("selectedValue", selectedVal);
		
		String startDateStr = request.getParameter("startDate");
		String endDateStr = request.getParameter("endDate");
		Date startDate = null;
		Date endDate = null;
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
		if(startDateStr!=null && !startDateStr.isEmpty()){
			try {
				startDate = df.parse(startDateStr + " 00:00:00");
				mav.addObject("startDate", startDateStr);	
			} catch (Exception e) {
				startDate = null;
			}
		}
		
		if(endDateStr!=null && !endDateStr.isEmpty()){
			try {
				endDate = df.parse(endDateStr + " 23:59:59");
				mav.addObject("endDate", endDateStr);
			} catch (Exception e) {
				endDate = null;
			}
		}
		
		String selectedOption="";
//		String selectedValue="";
		Collection<OpenOrderStatus> ticketDetail = null;
		
		if(null ==  profitLossSignStr || profitLossSignStr.isEmpty()){
			profitLossSignStr = String.valueOf(ProfitLossSign.ALL);
		}
		
		mav.addObject("profitLossSigns",ProfitLossSign.values());
		mav.addObject("selectedSign",ProfitLossSign.valueOf(profitLossSignStr.trim()));
		
		if(artistIdStr!=null && !artistIdStr.isEmpty()){
			artistId = Integer.parseInt(artistIdStr);
			Collection<Event> events = DAORegistry.getEventDAO().getAllOpenOrderEventsByArtistandVenueId(artistId, null);
			mav.addObject("events", events);
			selectedOption="Artist";
		}
		if(venueIdStr!=null && !venueIdStr.isEmpty()){
			venueId = Integer.parseInt(venueIdStr);
			Collection<Event> events = DAORegistry.getEventDAO().getAllOpenOrderEventsByArtistandVenueId(null, venueId);
			mav.addObject("events", events);
			selectedOption="Venue";
		}
		if(eventIdStr!=null && !eventIdStr.isEmpty()){
				Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventIdStr));
				eventId = event.getId();
				mav.addObject("eventId", eventIdStr);
		}
		
		String action = request.getParameter("action");
		if(action !=null && action.equals("update")){
			Map<String, String[]> requestParams = request.getParameterMap();
			Integer id = 0;
			Timestamp date = new Timestamp(new java.util.Date().getTime());
			
			for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
				String key = entry.getKey();
				if(key.contains("tic-")){
					 id=Integer.parseInt(key.replace("tic-",""));
					 OpenOrderStatus openOrderStatusId = DAORegistry.getOpenOrderStatusDAO().get(id);
					 if (openOrderStatusId != null) {
						openOrderStatusId.setPrice(Double.parseDouble(request.getParameter("price_" + id)));
						openOrderStatusId.setDiscountCouponPrice(Double.parseDouble(request.getParameter("discountCouponPrice_"
											+ id)));
						openOrderStatusId.setUrl(request.getParameter("url_"+ id));
						Double price = Double.parseDouble(request.getParameter("price_" + id));
						/*openOrderStatusId.setPriceUpdated(true);*/
						//Double discountCouponPrice = Double.parseDouble(request.getParameter("discountCouponPrice_"+ id));
						if(price != null){
							openOrderStatusId.setLastUpdatedPriceDate(date);
						}else{
							openOrderStatusId.setLastUpdatedPriceDate(null);
						}
							
						DAORegistry.getOpenOrderStatusDAO().update(openOrderStatusId);
					}
						//System.out.println("======" +id);
				}
			}
		}
		
		mav.addObject("selectedOption",selectedOption);
		mav.addObject("selectedValue",selectedValue);
		//ticketDetail = DAORegistry.getSoldTicketDetailDAO().getSoldTicketDetailsByBrokerIdAndArtistIdAndEventIdAndBySalesDateRange(brokerId,artistId,eventId,startDate,endDate);
		// Mehul : broker id is 5 hard coded here as we need only rtw open orders
		//ticketDetail = DAORegistry.getQueryManagerDAO().getSoldTicketDetails(5,artistId,venueId,eventId,startDate,endDate);
		Integer focusRowId = (null!=focusRowStr && !focusRowStr.isEmpty())?Integer.parseInt(focusRowStr.trim()):0;
		ticketDetail = DAORegistry.getOpenOrderStatusDAO().getSoldTicketDetailsByBrokerIdAndArtistIdAndEventIdAndBySalesDateRange(brokerId,artistId,eventId,startDate,endDate,ProfitLossSign.valueOf(profitLossSignStr));
		
		DecimalFormat decimalFmt = new DecimalFormat("#.##");
		Integer noOfOrders =0,sectionConsideredOrders=0,sectionNotConsideredOrders=0,totalSectionQty=0,
		zoneConsideredOrders=0,zoneNotConsideredOrders=0,totalZoneQty=0;
		Double sectionTotalNetSoldPrice=0.0,sectionTotalMarketPrice=0.0,sectionTotalActualSoldPrice=0.0,sectionTotalProfitLoss=0.0,
		zoneTotalNetSoldPrice=0.0,zoneTotalPrice=0.0,zoneTotalActualSoldPrice=0.0,zoneTotalProfitLoss=0.0;
		
		for (OpenOrderStatus openOrderStatus : ticketDetail) {
			
			if(openOrderStatus.getId().equals(focusRowId)){
				openOrderStatus.setIsFocusRow(true);
			}
			noOfOrders++;			
			if(openOrderStatus.getSectionTixQty() > 0) {
				totalSectionQty +=openOrderStatus.getSoldQty();
				sectionTotalNetSoldPrice += openOrderStatus.getNetTotalSoldPrice();
				sectionTotalActualSoldPrice += openOrderStatus.getTotalActualSoldPrice();
				sectionTotalMarketPrice += openOrderStatus.getTotalMarketPrice();
				sectionTotalProfitLoss += openOrderStatus.getProfitAndLoss();
				sectionConsideredOrders++;
			} else {
				sectionNotConsideredOrders++;
			}
			
			if(openOrderStatus.getZoneTixQty() > 0) {
				totalZoneQty +=openOrderStatus.getSoldQty();
				zoneTotalNetSoldPrice += openOrderStatus.getNetTotalSoldPrice();
				zoneTotalActualSoldPrice += openOrderStatus.getTotalActualSoldPrice();
				zoneTotalPrice += openOrderStatus.getZoneTotalPrice();
				zoneTotalProfitLoss += openOrderStatus.getZoneProfitAndLoss();
				zoneConsideredOrders++;
			} else {
				zoneNotConsideredOrders++;
			}
		}
		
		mav.addObject("noOfOrders",noOfOrders);
		mav.addObject("totalSectionQty",totalSectionQty);	
		mav.addObject("sectionTotalNetSoldPrice",decimalFmt.format(sectionTotalNetSoldPrice));
		mav.addObject("sectionTotalActualSoldPrice",decimalFmt.format(sectionTotalActualSoldPrice));
		mav.addObject("sectionTotalMarketPrice",decimalFmt.format(sectionTotalMarketPrice));
		mav.addObject("sectionTotalProfitLoss",decimalFmt.format(sectionTotalProfitLoss));
		mav.addObject("sectionConsideredOrders",sectionConsideredOrders);
		mav.addObject("sectionNotConsideredOrders",sectionNotConsideredOrders);
		mav.addObject("totalZoneQty",totalZoneQty);
		mav.addObject("zoneTotalNetSoldPrice",decimalFmt.format(zoneTotalNetSoldPrice));
		mav.addObject("zoneTotalActualSoldPrice",decimalFmt.format(zoneTotalActualSoldPrice));
		mav.addObject("zoneTotalPrice",decimalFmt.format(zoneTotalPrice));
		mav.addObject("zoneTotalProfitLoss",decimalFmt.format(zoneTotalProfitLoss));
		mav.addObject("zoneConsideredOrders",zoneConsideredOrders);
		mav.addObject("zoneNotConsideredOrders",zoneNotConsideredOrders);
		mav.addObject("ticketDetails",ticketDetail);
		mav.addObject("focusRowId",focusRowId);
		return mav;
	}
	
	public void downloadOpenOrderReport(HttpServletRequest request,
			HttpServletResponse response, ModelMap map) throws Exception {
		
		String profitLossSignStr = request.getParameter("profitLossSign");
		Integer brokerId= null;
		Integer eventId = 0;
		Integer artistId = 0;
		Integer venueId = 0;
		
		String artistIdStr = request.getParameter("artistId");
		String venueIdStr = request.getParameter("venueId");
		String eventIdStr = request.getParameter("eventId");
		String brokerIdStr = request.getParameter("brokerId");
		
		if(brokerIdStr != null && !brokerIdStr.equals("")) {
			brokerId = Integer.parseInt(brokerIdStr);
		}
		String startDateStr = request.getParameter("startDate");
		String endDateStr = request.getParameter("endDate");
		Date startDate = null;
		Date endDate = null;
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
		if(startDateStr!=null && !startDateStr.isEmpty()){
			try {
				startDate = df.parse(startDateStr + " 00:00:00");
			} catch (Exception e) {
				startDate = null;
			}
		}
		
		if(endDateStr!=null && !endDateStr.isEmpty()){
			try {
				endDate = df.parse(endDateStr + " 23:59:59");
			} catch (Exception e) {
				endDate = null;
			}
		}
		if(null == profitLossSignStr || profitLossSignStr.isEmpty()){
			profitLossSignStr = "ALL";
		}
		
		Collection<OpenOrderStatus> ticketDetail = null;
		if(artistIdStr!=null && !artistIdStr.isEmpty()){
			artistId = Integer.parseInt(artistIdStr);
		}
		if(venueIdStr!=null && !venueIdStr.isEmpty()){
			venueId = Integer.parseInt(venueIdStr);
		}
		if(eventIdStr!=null && !eventIdStr.isEmpty()){
				Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventIdStr));
				eventId = event.getId();
		}
		//brokerId = 5;
		
		
        DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		ServletOutputStream out = response.getOutputStream();
		
		 response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.setHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.setHeader("Content-disposition",
				"attachment; filename=RTWOpenOrders." + today
						+ ".xlsx");
		try {
			 
			ticketDetail = DAORegistry.getOpenOrderStatusDAO().getSoldTicketDetailsByBrokerIdAndArtistIdAndEventIdAndBySalesDateRange(brokerId,artistId,eventId,startDate,endDate,ProfitLossSign.valueOf(profitLossSignStr));
			
			 /*Event Name	Event Date	Venue Name		Short Section	Short Row	Short qty	Short Price		
			 Long Section	Long Row	Long Qty	Long Retail Price	Long WholeSale Price	Long Face Price	Long Cost
			 */
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet("RTW_Open_Orders");
			int j = 0;
			
			Row rowhead =   ssSheet.createRow((int)0);
			rowhead.createCell((int) j).setCellValue("Broker");
			j++;
			rowhead.createCell((int) j).setCellValue("Invoice No");
			j++;
			rowhead.createCell((int) j).setCellValue("Event Name");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Event Date");
		    j++;
	    	rowhead.createCell((int) j).setCellValue("Venue Name");
	    	j++;
	    	rowhead.createCell((int) j).setCellValue("Zone");
	 	    j++;
	 	    rowhead.createCell((int) j).setCellValue("section");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Row");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Quantity");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Section Tix Count");
		    j++;
	 	   	rowhead.createCell((int) j).setCellValue("Zone Tix Count");
	 	    j++;
	 	    rowhead.createCell((int) j).setCellValue("Event Tix Count");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Net Sold Price");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Market Price");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Net Total Sold Price");
		    j++;
		    rowhead.createCell((int) j).setCellValue("P/L Value");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Section Margin(%)");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Zone Cheapest Price");
	 	    j++;
		    rowhead.createCell((int) j).setCellValue("Zone Total Price");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Zone P/L Value");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Zone Margin(%)");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Actual Sold Price");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Total Market price");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Total Actual Sold Price");
		    j++;
	 	    rowhead.createCell((int) j).setCellValue("Available Zone Tix Group Count");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Invoice Date");
		    j++;
	    	rowhead.createCell((int) j).setCellValue("Internal Notes");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Last Updated Price");
		    j++;
		   
		    
		    CellStyle cellamountStyle = workbook.createCellStyle();
		    cellamountStyle.setDataFormat(
		    		workbook.getCreationHelper().createDataFormat().getFormat("#0.00"));
		    
		    /*DateFormat xlDateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
		    DateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");*/
		    int rowvariable = 1;
		    for (OpenOrderStatus obj : ticketDetail) {
		    	if(null == obj ){
		    		continue;
		    	}
		    	int column = 0;
				Row row = ssSheet.createRow(rowvariable);
				row.createCell((int) column).setCellValue(obj.getBrokerName().toString().replaceAll(",", "-"));//Broker Name
		        column++;
				row.createCell((int) column).setCellValue(obj.getInvoiceNo().toString().replaceAll(",", "-"));//Invoice No
		        column++;
		        row.createCell((int) column).setCellValue(obj.getEventName().toString().replaceAll(",", "-"));//Event Name
		        column++;
		        row.createCell((int) column).setCellValue(obj.getEventDateStr().toString());//Event Date
		        column++;
		    	row.createCell((int) column).setCellValue(obj.getVenueName().toString().replaceAll(",", "-"));//Venue
		    	column++;
		    	Cell shortZoneCell = row.createCell(column,Cell.CELL_TYPE_STRING);
		    	shortZoneCell.setCellValue(obj.getZone().toString().replaceAll(",", "-"));//Zone
		        column++;
		    	Cell shortSectionCell = row.createCell(column,Cell.CELL_TYPE_STRING);
		    	shortSectionCell.setCellValue(obj.getSection().toString().replaceAll(",", "-"));//Section
		        column++;
		        Cell shortRowCell = row.createCell(column,Cell.CELL_TYPE_STRING);
		        shortRowCell.setCellValue(obj.getRow().toString().replaceAll(",", "-"));//row
		        column++;
		    	row.createCell((int) column).setCellValue(obj.getSoldQty().toString().replaceAll(",", "-"));//Quantity
		    	column++;
		    	row.createCell((int) column).setCellValue(obj.getSectionTixQty().toString());//Section Tix Count
		        column++;
		        row.createCell((int) column).setCellValue(obj.getZoneTixQty().toString());//Availabe Zone Tix Count
		        column++;
		        row.createCell((int) column).setCellValue(obj.getEventTixQty().toString());//Event Tix Count
		        column++;
		        Cell amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
		        amountCell.setCellValue(Double.valueOf(obj.getNetSoldPrice().toString()));//Net Sold Price
		        amountCell.setCellStyle(cellamountStyle);
		        column++;
		        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
		        amountCell.setCellValue(Double.valueOf(obj.getMarketPrice().toString()));//Market Price
		        amountCell.setCellStyle(cellamountStyle);
		        column++;
		        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
		        amountCell.setCellValue(Double.valueOf(obj.getNetTotalSoldPrice().toString()));//Net Total Sold Price
		        amountCell.setCellStyle(cellamountStyle);
		        column++;
		        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
		        amountCell.setCellValue(Double.valueOf(obj.getProfitAndLoss().toString()));//P/L Value
		        amountCell.setCellStyle(cellamountStyle);
		        column++;
		        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
		        amountCell.setCellValue(Double.valueOf(obj.getSectionMargin().toString()));//Section MArgin
		        amountCell.setCellStyle(cellamountStyle);
		        column++;
		        
		        Cell zoneCheapestPriceCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
		        zoneCheapestPriceCell.setCellValue(Double.valueOf(obj.getZoneCheapestPrice().toString()));//Cheapest Zone Price
		        zoneCheapestPriceCell.setCellStyle(cellamountStyle);
		        column++;
		        
		        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
		        amountCell.setCellValue(Double.valueOf(obj.getZoneTotalPrice().toString()));//Zone Total Price
		        amountCell.setCellStyle(cellamountStyle);
		        column++;
		        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
		        amountCell.setCellValue(Double.valueOf(obj.getZoneProfitAndLoss().toString()));//Zone P/L Value
		        amountCell.setCellStyle(cellamountStyle);
		        column++;
		        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
		        amountCell.setCellValue(Double.valueOf(obj.getZoneMargin().toString()));//Zone Margin
		        amountCell.setCellStyle(cellamountStyle);
		        column++;
		        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
		        amountCell.setCellValue(Double.valueOf(obj.getActualSoldPrice().toString()));//Actual Sold Price
		        amountCell.setCellStyle(cellamountStyle);
		        column++;
		        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
		        amountCell.setCellValue(Double.valueOf(obj.getTotalMarketPrice().toString()));//Total Market Price
		        amountCell.setCellStyle(cellamountStyle);
		        column++;
		        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
		        amountCell.setCellValue(Double.valueOf(obj.getTotalActualSoldPrice().toString()));//Total Sold Price
		        amountCell.setCellStyle(cellamountStyle);
		        column++;
		        row.createCell((int) column).setCellValue(obj.getZoneTixGroupCount().toString());//Availabe Zone Tix Group Count
		        column++;
		        row.createCell((int) column).setCellValue(obj.getInvoiceDateStr().toString().replaceAll(",", "-"));//Invoice Date
		        column++;
		    	row.createCell((int) column).setCellValue(obj.getInternalNotes().toString().replaceAll(",", "-"));//Internal Notes
		    	column++;
		        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
		        amountCell.setCellValue(Double.valueOf(obj.getLastUpdatedPrice().toString()));//Last Updated Price
		        amountCell.setCellStyle(cellamountStyle);
		        column++;
		       
		        rowvariable++;
			}
			 workbook.write(out);
		} catch (Exception e) {
			e.printStackTrace();
		}
			 
	}
	
	// Mehul : filled orders for rtw added on - 01/06/2015
	public ModelAndView loadFilledOrderPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		ModelAndView mav = new ModelAndView("page-filled-order");
		String brokerIdStr = request.getParameter("brokerId");
		String userName = SecurityContextHolder.getContext().getAuthentication().getName();
		User user = DAORegistry.getUserDAO().getUserByUsername(userName);
		Integer brokerId= null;
		Integer spBrokerId= 0;
		Integer eventId = 0;
		Integer artistId = 0;
		Integer venueId = 0;
		//Broker broker = user.getBroker();
		/*if(broker.getId()==1){
			List<Broker> brokers = DAORegistry.getBrokerDAO().getAllActiveAutopricingBrokers();
			mav.addObject("brokers",brokers);
		}*/
		//brokerId = broker.getId();
		
		//mav.addObject("broker", broker);
		String artistIdStr = request.getParameter("artistId");
		String venueIdStr = request.getParameter("venueId");
		
		String eventIdStr = request.getParameter("eventId");
		String selectedValue = request.getParameter("selectedVal");
		
		if(brokerIdStr!=null && !brokerIdStr.isEmpty()){
			brokerId = Integer.parseInt(brokerIdStr);
		}
		List<Broker> brokers = DAORegistry.getBrokerDAO().getAllOpenOrderEligibleBrokers();
		mav.addObject("brokers",brokers);
		mav.addObject("selectedBrokerId", brokerIdStr);
		mav.addObject("artistId", artistIdStr);	
		mav.addObject("venueId", venueIdStr);
//		mav.addObject("selectedValue", selectedVal);
		
		String startDateStr = request.getParameter("startDate");
		String endDateStr = request.getParameter("endDate");
		Date startDate = null;
		Date endDate = null;
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
		if(startDateStr!=null && !startDateStr.isEmpty()){
			try {
				startDate = df.parse(startDateStr + " 00:00:00");
				mav.addObject("startDate", startDateStr);	
			} catch (Exception e) {
				startDate = null;
			}
		}
		
		if(endDateStr!=null && !endDateStr.isEmpty()){
			try {
				endDate = df.parse(endDateStr + " 23:59:59");
				mav.addObject("endDate", endDateStr);
			} catch (Exception e) {
				endDate = null;
			}
		}
		
		String selectedOption="";
//		String selectedValue="";
		Collection<SoldTicketDetail> ticketDetail = null;
		if(artistIdStr!=null && !artistIdStr.isEmpty()){
			artistId = Integer.parseInt(artistIdStr);
			Collection<Event> events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(artistId);
			mav.addObject("events", events);
			selectedOption="Artist";
		}
		if(venueIdStr!=null && !venueIdStr.isEmpty()){
			venueId = Integer.parseInt(venueIdStr);
			Collection<Event> events = DAORegistry.getEventDAO().getAllEventsByVenue(venueId);
			mav.addObject("events", events);
			selectedOption="Venue";
		}
		if(brokerIdStr!=null && !brokerIdStr.isEmpty()){
			try{
				
				brokerId = Integer.parseInt(brokerIdStr);
				if(brokerId==1){
					brokerId=0;
				}
			}catch (Exception e) {
				brokerId = 0;
			}
		}
		if(eventIdStr!=null && !eventIdStr.isEmpty()){
				Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventIdStr));
				eventId = event.getId();
				mav.addObject("eventId", eventIdStr);
		}
		mav.addObject("selectedOption",selectedOption);
		mav.addObject("selectedValue",selectedValue);
//		ticketDetail = DAORegistry.getSoldTicketDetailDAO().getSoldTicketDetailsByBrokerIdAndArtistIdAndEventIdAndBySalesDateRange(brokerId,artistId,eventId,startDate,endDate);
		// Mehul : broker id is 5 hard coded here as we need only rtw filled orders
		ticketDetail = DAORegistry.getQueryManagerDAO().getSoldFilledTicketDetails(brokerId,artistId,venueId,eventId,startDate,endDate);
		mav.addObject("ticketDetails",ticketDetail);	
		return mav;
	}
	
	// Mehul : tmat tickets to full fill an open order
	public ModelAndView getSoldTickets(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("page-order-fulfillment-option");
		String eventIdStr = request.getParameter("eventId");
		String brokerIdStr = request.getParameter("brokerId");
		String crawlTimeStr = request.getParameter("crawlTime");
		Date crawlTime;
		Integer eventId = null;
		Event event = null;
		if(eventIdStr!=null && !eventIdStr.isEmpty()){
			eventId = Integer.parseInt(eventIdStr);
			event = DAORegistry.getEventDAO().get(eventId);;
		}
		if(event==null){
			return mav;
		}
		if(crawlTimeStr == null){
			crawlTime = new Date();
		}else{
			crawlTime = new Date(Long.parseLong(crawlTimeStr));
		}
		Collection<Ticket> tickets = new ArrayList<Ticket>();
		String noCrawl = request.getParameter("nocrawl");
		String id = request.getParameter("id");
		mav.addObject("eventId",event.getId());
		mav.addObject("id",id);
		mav.addObject("eventAdmitoneId", eventId);
		mav.addObject("crawlTime", crawlTime.getTime());
		mav.addObject("brokerId", brokerIdStr);
		if(noCrawl==null){
			noCrawl = "false";
		}
		
		OpenOrderStatus openOrderStatus = DAORegistry.getOpenOrderStatusDAO().get(Integer.parseInt(id));		
		if("true".equalsIgnoreCase(noCrawl)){
			
			try{
				tickets = DAORegistry.getTicketDAO().getAllActiveTicketsByNormalizedSectionAndEventId(event.getId(), openOrderStatus.getSection());
				if(tickets != null && tickets.size() > 0){
					List<ManagePurchasePrice> managePurchasePrices = DAORegistry.getManagePurchasePriceDAO().getAllManagePurchasePriceByEventId(event.getId());
					Map<String,ManagePurchasePrice> managePurchasePriceMap = new HashMap<String, ManagePurchasePrice>();
					if(managePurchasePrices!=null && !managePurchasePrices.isEmpty() ){
						for(ManagePurchasePrice managePurchasePrice:managePurchasePrices){
							managePurchasePriceMap.put(managePurchasePrice.getExchange()+"-" + managePurchasePrice.getTicketType(), managePurchasePrice);
						}
					}
					Collection<DefaultPurchasePrice> defaultPurchasePrices = DAORegistry.getDefaultPurchasePriceDAO().getAll();
					Map<String, DefaultPurchasePrice> defaultPurhasePriceMap = new HashMap<String, DefaultPurchasePrice>();
					for(DefaultPurchasePrice defaultPurchasePrice:defaultPurchasePrices){
						defaultPurhasePriceMap.put(defaultPurchasePrice.getExchange()+ "-" + defaultPurchasePrice.getTicketType(), defaultPurchasePrice);
					}
					
					DecimalFormat df2 = new DecimalFormat(".##");
					df2.setRoundingMode(RoundingMode.UP);
					
					for(Ticket ticket:tickets){
						String mapKey = ticket.getSiteId()+ "-" + (ticket.getTicketDeliveryType()==null?"REGULAR":ticket.getTicketDeliveryType());
						ManagePurchasePrice managePurchasePrice=managePurchasePriceMap.get(mapKey);
						double tempPrice = ticket.getCurrentPrice();
						
						if(managePurchasePrice!=null){
							double serviceFee=managePurchasePrice.getServiceFee();
							double  shippingFee=managePurchasePrice.getShipping();
							int currencyType=managePurchasePrice.getCurrencyType();
							tempPrice=Double.parseDouble(df2.format((currencyType==1?((ticket.getCurrentPrice()*(1+serviceFee/100)) + (shippingFee/ticket.getQuantity())):(ticket.getCurrentPrice() + serviceFee + (shippingFee/ticket.getQuantity())))));
						}else{
							DefaultPurchasePrice defaultPurchasePrice =  defaultPurhasePriceMap.get(mapKey);
							if(defaultPurchasePrice!=null){
								double serviceFee=defaultPurchasePrice.getServiceFees();
								double  shippingFee=defaultPurchasePrice.getShipping();
								int currencyType=defaultPurchasePrice.getCurrencyType();
								tempPrice=Double.parseDouble(df2.format((currencyType==1?((ticket.getCurrentPrice()*(1+serviceFee/100)) + (shippingFee/ticket.getQuantity())):(ticket.getCurrentPrice() + serviceFee + (shippingFee/ticket.getQuantity())))));
							}
						}
						ticket.setPurchasePrice(tempPrice);
					}
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
			noCrawl = "true";
			mav.addObject("reload", new Date().getTime());
		}else{
			noCrawl = "false";
		}
		mav.addObject("noCrawl", noCrawl);
		mav.addObject("tickets", tickets);
		mav.addObject("openOrderStatus", openOrderStatus);
		return mav;
	}
	public void getCrawlsDetailsForEvent(HttpServletRequest request,
			HttpServletResponse response)throws ServletException, IOException {
		try {
			String param= request.getParameter("eventId");
			Integer eventId = Integer.parseInt(param);
			
			String text = "";
			Collection<TicketListingCrawl> crawls = ticketListingCrawler.getTicketListingCrawlsByEventId(eventId);
			for (TicketListingCrawl crawl : crawls) {
				String temp = "bean : "+crawl+" : "+crawl.getStartCrawl()+":"+crawl.getEndCrawl()+":"+crawl.getLastSuccessfulRun()
				+":"+crawl.isCancelled()+":"+crawl.getItemIndexed()+":"+crawl.getDispatchingTime()+":"+crawl.getFetchingTime()+":"+
				crawl.getExtractionTime()+":"+crawl.getIndexationTime()+":"+crawl.getFlushingTime()+":"+crawl.getExpirationTime()+crawl.getFetchedByteCount()+" : "+new Date();
				System.out.println(temp);
				text = text + "\n \n <br />"+temp;
			}
			TicketListingCrawler ticketListingCrawler1 = SpringUtil.getTicketListingCrawler();
			crawls = ticketListingCrawler1.getTicketListingCrawlsByEventId(eventId);
			for (TicketListingCrawl crawl : crawls) {
				String temp ="spring bean : "+crawl+" : "+crawl.getStartCrawl()+":"+crawl.getEndCrawl()+":"+crawl.getLastSuccessfulRun()
				+":"+crawl.isCancelled()+":"+crawl.getItemIndexed()+":"+crawl.getDispatchingTime()+":"+crawl.getFetchingTime()+":"+
				crawl.getExtractionTime()+":"+crawl.getIndexationTime()+":"+crawl.getFlushingTime()+":"+crawl.getExpirationTime()+crawl.getFetchedByteCount()+" : "+new Date();
				System.out.println(temp);
				
				text = text + "\n \n <br />"+temp;
			}
			System.out.println("EventCrawlStatus Done For Event : "+eventId);
			PrintWriter writer =  response.getWriter();
			String res = "EventCrawlStatus Done For Event : "+eventId+" : \n \n <br />"+text;
			writer.write(res);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void getAutoCompleteGrandChildAndArtistAndVenue(HttpServletRequest request,
			HttpServletResponse response)throws ServletException, IOException {
				String param= request.getParameter("q");
//				String brokerId = request.getParameter("brokerId");
			
				Collection<Artist> artists =DAORegistry.getArtistDAO().filterByName(param);
				Collection<Venue> venues=DAORegistry.getVenueDAO().filterByVenue(param);
				Collection<GrandChildTourCategory> grandChildCategories=DAORegistry.getGrandChildTourCategoryDAO().getGrandChildCategoriesByName(param);
				Collection<ChildTourCategory> childTourCategories=DAORegistry.getChildTourCategoryDAO().getChildTourCategoriesByName(param);
				
				if(artists == null && venues == null && grandChildCategories == null ){
					return;
				}
				if(artists != null){
					for(Artist tour :artists){
						response.getOutputStream().println("ARTIST" + "|" + tour.getId() + "|"  + tour.getName());
					}
				}
				if(venues != null){
					for(Venue venue :venues){
						response.getOutputStream().println("VENUE" + "|"+ venue.getId() + "|" +  venue.getBuilding());
					}
				}
				if(grandChildCategories != null) {
					for(GrandChildTourCategory grandChildCateogry :grandChildCategories){
						response.getOutputStream().println("GRANDCHILD" + "|"+grandChildCateogry.getId()+"|"+ grandChildCateogry.getName());
					}
				}
				
				if(childTourCategories != null) {
					for(ChildTourCategory childTourCategory :childTourCategories){
						response.getOutputStream().println("CHILD" + "|"+childTourCategory.getId()+"|"+ childTourCategory.getName());
					}
				}
				
		}
	
	public void getAutoCompleteRTWOpenOrderArtistAndVenue(HttpServletRequest request,
			HttpServletResponse response)throws ServletException, IOException {
				String param= request.getParameter("q");
			
				Collection<Artist> artists =DAORegistry.getOpenOrderStatusDAO().getAllActiveOpenOrderArtist(param);
				Collection<Venue> venues=DAORegistry.getOpenOrderStatusDAO().getAllActiveOpenOrdersVenues(param);
				if(artists == null && venues == null ){
					return;
				}
				if(artists != null){
					for(Artist tour :artists){
						response.getOutputStream().println("ARTIST" + "|" + tour.getId() + "|"  + tour.getName());
					}
				}
				if(venues != null){
					for(Venue venue :venues){
						response.getOutputStream().println("VENUE" + "|"+ venue.getId() + "|" +  venue.getBuilding());
					}
				}
				
		}
	
	public void setTicketListingCrawler(TicketListingCrawler ticketListingCrawler) {
		this.ticketListingCrawler = ticketListingCrawler;
	}
	
	public PreferenceManager getPreferenceManager() {
		return preferenceManager;
	}

	public void setPreferenceManager(PreferenceManager preferenceManager) {
		this.preferenceManager = preferenceManager;
	}	

	public MailManager getMailManager() {
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		this.mailManager = mailManager;
	}
	private static int compareDates(Calendar cal1, Calendar cal2) {
		if (cal1.get(Calendar.YEAR) < cal2.get(Calendar.YEAR)) {
			return -1;
		} else if (cal1.get(Calendar.YEAR) > cal2.get(Calendar.YEAR)) {
			return 1;
		}

		if (cal1.get(Calendar.MONTH) < cal2.get(Calendar.MONTH)) {
			return -1;
		} else if (cal1.get(Calendar.MONTH) > cal2.get(Calendar.MONTH)) {
			return 1;
		}

		if (cal1.get(Calendar.DATE) < cal2.get(Calendar.DATE)) {
			return -1;
		} else if (cal1.get(Calendar.DATE) > cal2.get(Calendar.DATE)) {
			return 1;
		}
		
		return 0;
	}

}
