package com.admitone.tmat.web;

import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.admitone.tmat.common.SharedProperty;
import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Category;
import com.admitone.tmat.data.ChildTourCategory;
import com.admitone.tmat.data.DefaultPurchasePrice;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.GrandChildTourCategory;
import com.admitone.tmat.data.ManagePurchasePrice;
import com.admitone.tmat.data.OpenOrderStatus;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.data.VenueCategory;
import com.admitone.tmat.enums.EventStatus;
import com.admitone.tmat.pojo.CrawlServerResponse;
import com.admitone.tmat.util.RTWOpenOrderStatusUpdater;
import com.admitone.tmat.utils.TicketUtil;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;
import com.thoughtworks.xstream.XStream;

import edu.emory.mathcs.backport.java.util.Collections;

public class AjaxController extends MultiActionController{

	private SharedProperty sharedProperty;
	
	public SharedProperty getSharedProperty() {
		return sharedProperty;
	}

	public void setSharedProperty(SharedProperty sharedProperty) {
		this.sharedProperty = sharedProperty;
	}

	public void getChildCategory(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String tourCategoryId = request.getParameter("tourCategoryId");
		Collection<ChildTourCategory> cats = DAORegistry.getChildTourCategoryDAO().getChildTourCategoryByTourCategory(Integer.parseInt(tourCategoryId));
		OutputStream out =  response.getOutputStream();
		String res="";
		for(ChildTourCategory tourCategory:cats){
			res+=tourCategory.getId() + ":" + tourCategory.getName() + ",";
		}
		if(!res.isEmpty()){
			res= res.substring(0,res.length()-1);
			out.write(res.getBytes());
		}
	}
	
	public void getGrandChildCategory(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String childCategoryId = request.getParameter("childCategoryId");
		Collection<GrandChildTourCategory> cats = DAORegistry.getGrandChildTourCategoryDAO().getGrandChildTourCategoryByChildTourCategory(Integer.parseInt(childCategoryId));
		OutputStream out =  response.getOutputStream();
		String res="";
		for(GrandChildTourCategory tourCategory:cats){
			res+=tourCategory.getId() + ":" + tourCategory.getName() + ",";
		}
		if(!res.isEmpty()){
			res= res.substring(0,res.length()-1);
			out.write(res.getBytes());
		}
	}
	
	public void getEventsByTourId(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String tourId = request.getParameter("tourId");
		Collection<Event> events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(Integer.parseInt(tourId));
		OutputStream out =  response.getOutputStream();
		DateFormat df = new SimpleDateFormat("MMM dd yyyy");
		DateFormat tf = new SimpleDateFormat("h-mm aa");
		String res="";
		for(Event event:events){
			res+=event.getId() + ":" + event.getName() + ":" + (event.getDate()==null?"TBD":df.format(event.getDate())) + ":" + (event.getTime()==null?"TBD":tf.format(event.getTime())) + ":" + event.getVenue().getBuilding()+ ":" + event.getVenue().getCity() + ":" + event.getVenue().getState() + ",";
		}
		if(!res.isEmpty()){
			res= res.substring(0,res.length()-1);
			out.write(res.getBytes());
		}
	}
	
	public void getEventsByArtistIdAndVenueId(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String artistIdStr = request.getParameter("artistId");
		Integer artistId = null;
		String venueIdStr = request.getParameter("venueId");
		Integer venueId = null;
		Collection<Event> events = null;
		if(venueIdStr!=null && !venueIdStr.isEmpty()){
			venueId= Integer.parseInt(venueIdStr);
		}
		if(artistIdStr!=null && !artistIdStr.isEmpty()){
			artistId= Integer.parseInt(artistIdStr);
		}
		if(artistId!=null && venueId!=null){
			events = DAORegistry.getEventDAO().getAllEventsByArtistAndVenueAndStatus(artistId, venueId, EventStatus.ACTIVE);
		}else if(artistId!=null){
			events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(artistId);
		}else if(venueId!=null){
			events = DAORegistry.getEventDAO().getAllEventsByVenue(venueId);
		}
		
		OutputStream out =  response.getOutputStream();
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		DateFormat tf = new SimpleDateFormat("HH:mm aa");
		String res="";
		for(Event event:events){
			res= res + "{\"id\":\"" +event.getId() + "\",\"name\":\"" + event.getName() + "\",\"date\":\"" + (event.getDate()==null?"TBD":df.format(event.getDate())) + "\",\"time\":\"" + (event.getTime()==null?"TBD":tf.format(event.getTime())) + "\",venue:\"" + event.getVenue().getBuilding()+ ", " + event.getVenue().getCity() + " ," + event.getVenue().getState() + "\"},";
//			res+=event.getId() + ":" + event.getName() + ":" + (event.getDate()==null?"TBD":df.format(event.getDate())) + ":" + (event.getTime()==null?"TBD":tf.format(event.getTime())) + ":" + event.getVenue().getBuilding()+ ":" + event.getVenue().getCity() + ":" + event.getVenue().getState() + ",";
		}
		if(!res.isEmpty()){
//			res= res.substring(0,res.length()-1);
			res= "[" + res + "]";
			out.write(res.getBytes());
		}
	}
	
	
	public void getOpenOrderEventsByArtistIdAndVenueId(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String artistIdStr = request.getParameter("artistId");
		Integer artistId = null;
		String venueIdStr = request.getParameter("venueId");
		Integer venueId = null;
		Collection<Event> events = null;
		if(venueIdStr!=null && !venueIdStr.isEmpty()){
			venueId= Integer.parseInt(venueIdStr);
		}
		if(artistIdStr!=null && !artistIdStr.isEmpty()){
			artistId= Integer.parseInt(artistIdStr);
		}
		
		try{
			events = DAORegistry.getEventDAO().getAllOpenOrderEventsByArtistandVenueId(artistId, venueId);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		OutputStream out =  response.getOutputStream();
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		DateFormat tf = new SimpleDateFormat("HH:mm aa");
		String res="";
		if(null != events && !events.isEmpty()){
			for(Event event:events){
				res= res + "{\"id\":\"" +event.getId() + "\",\"name\":\"" + event.getName() + "\",\"date\":\"" + (event.getDate()==null?"TBD":df.format(event.getDate())) + "\",\"time\":\"" + (event.getTime()==null?"TBD":tf.format(event.getTime())) + "\",venue:\"" + event.getVenue().getBuilding()+ ", " + event.getVenue().getCity() + " ," + event.getVenue().getState() + "\"},";
			}
		}
		
		if(!res.isEmpty()){
//			res= res.substring(0,res.length()-1);
			res= "[" + res + "]";
			out.write(res.getBytes());
		}
	}
	
	public void forceCrawlerByEvent(HttpServletRequest request , HttpServletResponse response) throws Exception{
		ServletOutputStream out = response.getOutputStream();
		String eventId = request.getParameter("eventId");
		String url = sharedProperty.getBrowseUrl()+"WSForceEvents";		
//		HttpClient hc = new DefaultHttpClient();
		SimpleHttpClient hc = HttpClientStore.createHttpClient();
		HttpPost hp = new HttpPost(url);
		NameValuePair nameValuePair = new BasicNameValuePair("eventIds", eventId);
		NameValuePair nameValuePair1 = new BasicNameValuePair("postBackUrl", sharedProperty.getBrowseLiteUrl()+"ForceCrawledEventResult");
		NameValuePair nameValuePair2 = new BasicNameValuePair("priority", "high");
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(nameValuePair);
		parameters.add(nameValuePair1);
		parameters.add(nameValuePair2);
		UrlEncodedFormEntity entity = new UrlEncodedFormEntity(parameters);
		hp.setEntity(entity);
		HttpResponse res = hc.execute(hp);
		HttpEntity result = res.getEntity();
		String content = EntityUtils.toString(result);
//		sender.sendMessageToBrowse("crawl-force-event-analytics", eventId);
		XStream xstream = new XStream();
		xstream.alias("CrawlResponseWrapper", CrawlServerResponse.class);
		xstream.processAnnotations(CrawlServerResponse.class);
		CrawlServerResponse csr = new CrawlServerResponse();
		xstream.fromXML(content,csr);
		
		Date leastDate =null;
		Collection<TicketListingCrawl> crawls = DAORegistry.getTicketListingCrawlDAO().getAllActiveTicketListingCrawlByEvent(Integer.parseInt(eventId.trim()));
		for (TicketListingCrawl crawl : crawls) {
			if (crawl.getEndCrawl() == null) {
				continue;
			}
			if (leastDate == null) {
				leastDate = crawl.getEndCrawl();
			} else if (crawl.getEndCrawl().before(leastDate)) {
				leastDate = crawl.getEndCrawl();
			}
		}
		Long timeInMillis = -1L;
		if (leastDate != null) {
			timeInMillis = leastDate.getTime();
		}
		String msg = "OK|" + csr.getNumberofcrawls() +"|0"+ "|" + timeInMillis;
		out.write(msg.toString().getBytes());
	}
	
	public void getCrawlerStatus(HttpServletRequest request , HttpServletResponse response) throws IOException{
		String eventId = request.getParameter("eventId");
		String crawlTimeStr = request.getParameter("crawlTime");
		//System.out.println("Time... "+ crawlTimeStr);
		Date crawlTime = new Date(Long.parseLong(crawlTimeStr));
		Date now = new Date();
		String res = "";
		ServletOutputStream out = response.getOutputStream();
		List<TicketListingCrawl> crawls = DAORegistry.getTicketListingCrawlDAO().getAllActiveTicketListingCrawlByEvent(Integer.parseInt(eventId));
		if(now.getTime() - crawlTime.getTime() > 2*60*1000){
			res = "OK|2|2";
		}else{
			int count = 0;
			for(TicketListingCrawl craw : crawls){
				if(craw.getEndCrawl().after(crawlTime)){
					count++;
				}
			}
			
			res = "OK|" + crawls.size() + "|" + count;
		}
		System.out.println("res... "+res);
		out.write(res.toString().getBytes());
	}
	
	//Method to update the tmat updated online price to that RTW Open Order
	//Added By Ulaganathan 
	public void getRTWOrderTicketStatus(HttpServletRequest request , HttpServletResponse response) throws IOException{
		String eventIdStr = request.getParameter("eventId");
		String orderIdStr = request.getParameter("orderId");
		OpenOrderStatus opOrderStatus = DAORegistry.getOpenOrderStatusDAO().get(Integer.parseInt(orderIdStr.trim()));
		String res = "";
		ServletOutputStream out = response.getOutputStream();
		DecimalFormat df = new DecimalFormat("#.##");
		if(null != opOrderStatus){
			try{
				Integer eventTicketsQty =0, sectionTixCount=0;
				Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventIdStr.trim()));
				List<Ticket> sectionTixs = new ArrayList<Ticket>();
				
				Collection<Ticket> tickets = DAORegistry.getTicketDAO().getAllActiveTicketsByEvent(Integer.parseInt(eventIdStr.trim()));
				if(null ==event || (null == tickets || tickets.isEmpty())){
					if(opOrderStatus.getLastUpdatedPrice() > 0 ){
						opOrderStatus.setProfitAndLoss(0.0);
						opOrderStatus.setSectionTixQty(sectionTixCount);
						opOrderStatus.setEventTixQty(eventTicketsQty);
						opOrderStatus.setLastUpdatedPrice(opOrderStatus.getMarketPrice());
						opOrderStatus.setPriceUpdateCount(opOrderStatus.getPriceUpdateCount()+1);
						opOrderStatus.setMarketPrice(0.00);
						opOrderStatus.setOnlinePrice(0.00);
						opOrderStatus.setLastUpdated(new Date());
					}

				}else{
					boolean isZPTicket = false;
					
					if(opOrderStatus.getSection().contains("ZONE") || opOrderStatus.getSection().contains("zone")){
						
						Collection<Category> categories = null;
						if(event.getVenueCategory()!=null){
							categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(event.getVenueCategory().getId());
						}else{
							List<String> venueCategories = DAORegistry.getVenueCategoryDAO().getCategoryGroupByVenueId(event.getVenueId());
							categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueIdAndCategoryGroup(event.getVenueId(), venueCategories.get(0));
						}
						TicketUtil.preAssignCategoriesToTickets(tickets, categories);
						isZPTicket = true;
					}
					
					Collection<DefaultPurchasePrice> defaultPurchasePrices = DAORegistry.getDefaultPurchasePriceDAO().getAll();
					Map<String, DefaultPurchasePrice> defaultPurchasePriceMap = new HashMap<String, DefaultPurchasePrice>();
					for(DefaultPurchasePrice defaultPurchasePrice:defaultPurchasePrices){
						defaultPurchasePriceMap.put(defaultPurchasePrice.getExchange() + "-" + defaultPurchasePrice.getTicketType(), defaultPurchasePrice);
					}
					Collection<ManagePurchasePrice> managePurchasePricelist = DAORegistry.getManagePurchasePriceDAO().getAllManagePurchasePriceByEventId(event.getId());//tour.getTmatPurchaseId());
					Map<String, ManagePurchasePrice> tourPriceMap = new HashMap<String, ManagePurchasePrice>();
					for(ManagePurchasePrice tourPrice:managePurchasePricelist){
						tourPriceMap.put(tourPrice.getExchange()+"-" + tourPrice.getTicketType(),tourPrice);
					}
					
					for (Ticket ticket : tickets) {
						
						TicketUtil.getPurchasePrice(ticket, defaultPurchasePriceMap, null, tourPriceMap);
						
						eventTicketsQty += ticket.getRemainingQuantity();
						String key = "";
						if(isZPTicket){
							if(null != ticket.getCategory()){
								key = "ZONE "+ticket.getCategory().getSymbol();
							}
						}else{
							key = ticket.getNormalizedSection();
						}
						
						if(!key.toUpperCase().equalsIgnoreCase(opOrderStatus.getSection().toUpperCase())){
							continue;
						}
						sectionTixCount += ticket.getRemainingQuantity();
						sectionTixs.add(ticket);
					}
					
					Double marketPrice = 0.00,oldMarketPrice=0.00,onlinePrice=0.00;
					
					if(null != sectionTixs && !sectionTixs.isEmpty()){
						Collections.sort(sectionTixs, RTWOpenOrderStatusUpdater.sortingTicketbyPrice);
						marketPrice = sectionTixs.get(0).getPurchasePrice();
						onlinePrice= sectionTixs.get(0).getBuyItNowPrice();
					}else{
						marketPrice = 0.00;
						onlinePrice=0.00;
					}
					Double profitandLoss = 0.00;
					
					if(sectionTixCount > 0){
						profitandLoss = (opOrderStatus.getActualSoldPrice() * opOrderStatus.getSoldQty()) - (marketPrice * opOrderStatus.getSoldQty());
					}
					marketPrice = Double.valueOf(df.format(marketPrice));
					oldMarketPrice = Double.valueOf(df.format(opOrderStatus.getMarketPrice()));
					if(marketPrice.equals(oldMarketPrice)){
						opOrderStatus.setPriceUpdateCount(opOrderStatus.getPriceUpdateCount()+1);
						opOrderStatus.setLastUpdatedPrice(oldMarketPrice);
						opOrderStatus.setMarketPrice(marketPrice);
						opOrderStatus.setOnlinePrice(onlinePrice);
						
					}
					opOrderStatus.setProfitAndLoss(profitandLoss);
					opOrderStatus.setEventTixQty(eventTicketsQty);
					opOrderStatus.setSectionTixQty(sectionTixCount);
					opOrderStatus.setLastUpdated(new Date());
				}
				DAORegistry.getOpenOrderStatusDAO().saveOrUpdate(opOrderStatus);
			}catch(Exception e){
				e.printStackTrace();
			}
			res = "OK|1";
		}
		out.write(res.toString().getBytes());
	}
	
	
}
