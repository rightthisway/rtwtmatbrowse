package com.admitone.tmat.web;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.crawler.TicketListingCrawler;
import com.admitone.tmat.crawler.util.CrawlerJMSUtil;
import com.admitone.tmat.crawler.util.CrawlerWSUtil;
import com.admitone.tmat.crawler.util.LocalDestination;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.DuplicateTicketMap;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.MXPZonesTicket;
import com.admitone.tmat.enums.CrawlState;
import com.admitone.tmat.pojo.CrawlServerResponse;
import com.admitone.tmat.pojo.DuplicateTicketWrapper;
import com.admitone.tmat.pojo.WSZonesWrapper;
import com.admitone.tmat.utils.PostBackURL;
import com.admitone.tmat.utils.SpringUtil;
import com.thoughtworks.xstream.XStream;

public class WSController extends  MultiActionController {
	
	private TicketListingCrawler ticketListingCrawler;
	private PostBackURL postBackUrls ;
	private Map<String, List<String>> zonesUrl= new HashMap<String, List<String>>();
	private CrawlerWSUtil crawlerUtil;
	private CrawlerJMSUtil crawlerJMSUtil;
	
	public TicketListingCrawler getTicketListingCrawler() {
		return ticketListingCrawler;
	}

	public void setTicketListingCrawler(TicketListingCrawler ticketListingCrawler) {
		this.ticketListingCrawler = ticketListingCrawler;
	}
	
	public void forceCrawlForEventsJMS(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ServletOutputStream out = response.getOutputStream();
		String eventIds = request.getParameter("eventIds");
		String dest = request.getParameter("destination");
		Integer count =0;
		String eventIdStr = "";
		LocalDestination destination = null;
		if(dest.toUpperCase().equals("ADMIN")){
			destination = LocalDestination.ADMIN;
		}else if(dest.toUpperCase().equals("ANALYTICS")){
			destination = LocalDestination.ANALYTICS;
		}else{
			destination = LocalDestination.BROWSE;
		}
		for(String eventId:eventIds.split(",")){
			if(eventId==null || eventId.isEmpty()){
				continue;
			}
			Collection<TicketListingCrawl> eventCrawls = ticketListingCrawler.getTicketListingCrawlByEvent(Integer.parseInt(eventId));
			if(eventCrawls.size()>0){
				count += eventCrawls.size();
				eventIdStr += eventId + ",";
			}
			crawlerJMSUtil.addEventToCheckStatus(eventId, destination, eventCrawls.size());
			ticketListingCrawler.forceRecrawlWithPriority(eventCrawls, TicketListingCrawl.PRIORITY_HIGH);
		}
		CrawlServerResponse csr = new CrawlServerResponse();
		csr.setMsg("Success Invoking Crawls for :" + eventIdStr);
		csr.setNumberofcrawls(count);
		csr.setStatus("success");
		XStream xstream = new XStream();
		xstream.alias("CrawlServerResponse", CrawlServerResponse.class);
		String retString = xstream.toXML(csr);
		out.write(retString.getBytes());
		
	}
	public void forceCrawlForEvents(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, InterruptedException {
		ServletOutputStream out = response.getOutputStream();
		String eventIds = request.getParameter("eventIds");
		String postBackUrl = request.getParameter("postBackUrl");
		String priorityStr = request.getParameter("priority");
		int priority;
		if(priorityStr!=null){
			if(priorityStr.equalsIgnoreCase("low")){
				priority = TicketListingCrawl.PRIORITY_LOW;
			}else if(priorityStr.equalsIgnoreCase("high")){
				priority = TicketListingCrawl.PRIORITY_HIGH;
			}else{
				priority = TicketListingCrawl.PRIORITY_NORMAL;
			}
		}else{
			priority = TicketListingCrawl.PRIORITY_LOW;
		}
		if(postBackUrl==null){
			return;
		}
		Integer count =0;
		String eventIdStr = "";
		int i=0;
		for(String eventId:eventIds.split(",")){
			if(i%20==0){
				Thread.sleep(5000);
			}
			i++;
			if(eventId==null || eventId.isEmpty()){
				continue;
			}
			Integer crawlerCount = ticketListingCrawler.forceRecrawlForEvent(Integer.parseInt(eventId),priority);
			if(crawlerCount>0){
				count += crawlerCount;
				eventIdStr += eventId + ",";
				crawlerUtil.addEventToCheckStatus(eventId, postBackUrl, crawlerCount);
			}
		}
		CrawlServerResponse csr = new CrawlServerResponse();
		csr.setMsg("Success Invoking Crawls for :" + eventIdStr);
		csr.setNumberofcrawls(count);
		csr.setStatus("success");
		XStream xstream = new XStream();
		xstream.alias("CrawlServerResponse", CrawlServerResponse.class);
		String retString = xstream.toXML(csr);
		out.write(retString.getBytes());
	}
	public void forceCrawlsByIds(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, InterruptedException {
		String crawlsString = request.getParameter("crawlIds");
		String eventId = request.getParameter("eventId");
		String postBackUrl = request.getParameter("postBackUrl");
		String priority = request.getParameter("priority");
		ServletOutputStream out = response.getOutputStream();
		if(postBackUrl==null){
			return;
		}
		TicketListingCrawler ticketListingCrawler=  SpringUtil.getTicketListingCrawler();
		List<TicketListingCrawl>  crawls = new ArrayList<TicketListingCrawl>(); 
		for(String crawlId:crawlsString.split(",")){
			if(crawlId==null || crawlId.isEmpty()){
				continue;
			}
			TicketListingCrawl crawl = ticketListingCrawler.getTicketListingCrawlById(Integer.parseInt(crawlId));
			crawls.add(crawl);
			
		}
//		ticketListingCrawler.forceRecrawlWithPriority(crawls, TicketListingCrawl.PRIORITY_HIGH);
		crawlerUtil.addEventToCheckStatus(eventId, postBackUrl, crawls.size());
		if(priority!=null){
			if(priority.equalsIgnoreCase("low")){
				ticketListingCrawler.forceRecrawlWithPriority(crawls, TicketListingCrawl.PRIORITY_LOW);
			}else if(priority.equalsIgnoreCase("high")){
				ticketListingCrawler.forceRecrawlWithPriority(crawls, TicketListingCrawl.PRIORITY_HIGH);
			}else{
				ticketListingCrawler.forceRecrawlWithPriority(crawls, TicketListingCrawl.PRIORITY_NORMAL);
			}
		}else{
			ticketListingCrawler.forceRecrawlWithPriority(crawls, TicketListingCrawl.PRIORITY_HIGH);
		}
		int count = crawls.size();
		CrawlServerResponse csr = new CrawlServerResponse();
		csr.setMsg("Success Invoking Crawls for :" + eventId);
		csr.setNumberofcrawls(count);
		csr.setStatus("success");
		XStream xstream = new XStream();
		xstream.alias("CrawlServerResponse", CrawlServerResponse.class);
		String retString = xstream.toXML(csr);
		out.write(retString.getBytes());
	}
	public void forceCrawl(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream os = response.getOutputStream();
		String eventIdStr = request.getParameter("eventId");
		String postBackUrl = request.getParameter("postBackUrl");
//		String exposureDescription = request.getParameter("exposureDescription");
//		String exposure = "";
//		String ownerType="";
		/*boolean flag=false;
		if(exposureDescription!=null){
			String[] temp=exposureDescription.split("-");
			if(temp.length!=2){
				flag=true;
			}else{
				ownerType=temp[1];
				if(ownerType==null || ownerType.isEmpty() || !(ownerType.length()==3 || ownerType.length()==4)){
					flag=true;
				}
			}
		}else{
			flag=true;
		}
		if(flag){
			CrawlServerResponse csr = new CrawlServerResponse();
			csr.setMsg("Invalid Exposure,Exposure format should be like \"(Exposure-type) eg.1-MXP or 1-OXP. \"");
			csr.setNumberofcrawls(0);
			csr.setStatus("error");
			XStream xstream = new XStream();
			xstream.alias("CrawlServerResponse", CrawlServerResponse.class);
			String retString = xstream.toXML(csr);
			os.write(retString.getBytes());
			return;
		}*/
		if(eventIdStr == null){
			//String retString = "<xml><status>error</status><msg>Invalid event id, event id is null<msg><numberofcrawls>0</numberofcrawls></xml>";
			CrawlServerResponse csr = new CrawlServerResponse();
			csr.setMsg("Invalid event id, eventId is null");
			csr.setNumberofcrawls(0);
			csr.setStatus("error");
			XStream xstream = new XStream();
			xstream.alias("CrawlServerResponse", CrawlServerResponse.class);
			String retString = xstream.toXML(csr);
			os.write(retString.getBytes());
			return;
		}
		Integer eventId = Integer.parseInt(eventIdStr);
		Event event = DAORegistry.getEventDAO().get(eventId);
		if(event == null){
			//String retString = "<xml><status>error</status><msg>Invalid event id, Event does not exist<msg><numberofcrawls>0</numberofcrawls></xml>";
			CrawlServerResponse csr = new CrawlServerResponse();
			csr.setMsg("Invalid event id, event does not exist");
			csr.setNumberofcrawls(0);
			csr.setStatus("error");
			XStream xstream = new XStream();
			xstream.alias("CrawlServerResponse", CrawlServerResponse.class);
			String retString = xstream.toXML(csr);
			os.write(retString.getBytes());
			return;
		}
		
		if(postBackUrl == null || postBackUrl.length()<1){
			//String retString = "<xml><status>error</status><msg>Invalid postbackurl, postBackUrl is null<msg><numberofcrawls>0</numberofcrawls></xml>";
			CrawlServerResponse csr = new CrawlServerResponse();
			csr.setMsg("Invalid postbackurl");
			csr.setNumberofcrawls(0);
			csr.setStatus("error");
			XStream xstream = new XStream();
			xstream.alias("CrawlServerResponse", CrawlServerResponse.class);
			String retString = xstream.toXML(csr);
			os.write(retString.getBytes());
			return;
		}
		Collection<TicketListingCrawl> ticketListingCrawlersForEvent = ticketListingCrawler.getActiveTicketListingCrawlsByEventId(eventId);
		if(ticketListingCrawlersForEvent == null || ticketListingCrawlersForEvent.isEmpty()){
			CrawlServerResponse csr = new CrawlServerResponse();
			csr.setMsg("Error Invoking Crawls, No Active Crawls available");
			csr.setNumberofcrawls(0);
			csr.setStatus("error");
			XStream xstream = new XStream();
			xstream.alias("CrawlServerResponse", CrawlServerResponse.class);
			String retString = xstream.toXML(csr);
			os.write(retString.getBytes());
			return;
		}
		synchronized (postBackUrls) {
			Map<Integer, List<String>> urls= postBackUrls.getPostBackUrlMap();
			for(TicketListingCrawl tlc : ticketListingCrawlersForEvent){
				//tlc.setPostBackUrl(postBackUrl);
				List<String> reqs= urls.get(tlc.getId());
				if(reqs==null ){
					reqs= new ArrayList<String>(); 
				}
				if(!reqs.contains(postBackUrl)){
					reqs.add(postBackUrl);
					urls.put(tlc.getId(), reqs);
					tlc.setXmlPostBack(Boolean.TRUE);
				}
			}
		}

				
		//DAORegistry.getTicketListingCrawlDAO().saveOrUpdateAll(ticketListingCrawlersForEvent);
		while(true){
			Boolean running = Boolean.FALSE;
			for(TicketListingCrawl tlc : ticketListingCrawlersForEvent){
				if(tlc.getCrawlState().equals(CrawlState.RUNNING)){
					running = Boolean.TRUE;
				}
			}
			if(!running){
				break;
			}
		}
		ticketListingCrawler.forceRecrawlWithPriority(ticketListingCrawlersForEvent,TicketListingCrawl.PRIORITY_HIGH);
		//String retString = "<xml><status>success</status><msg>Success Invoking Crawls<msg><numberofcrawls>"+ ticketListingCrawlersForEvent.size() +"</numberofcrawls></xml>";
		CrawlServerResponse csr = new CrawlServerResponse();
		csr.setMsg("Success Invoking Crawls");
		csr.setNumberofcrawls(ticketListingCrawlersForEvent.size());
		csr.setStatus("success");
		XStream xstream = new XStream();
		xstream.alias("CrawlServerResponse", CrawlServerResponse.class);
		String retString = xstream.toXML(csr);
		os.write(retString.getBytes());
		return;
		/*if(ownerType.equals("MXP")){
			if(event.getAdmitoneId()==null){
				CrawlServerResponse csr = new CrawlServerResponse();
				csr.setMsg("Error Invoking Crawls, No Active Crawls available");
				csr.setNumberofcrawls(0);
				csr.setStatus("error");
				XStream xstream = new XStream();
				xstream.alias("CrawlServerResponse", CrawlServerResponse.class);
				String retString = xstream.toXML(csr);
				os.write(retString.getBytes());
				return;
			}else{
				synchronized (zonesUrl) {
					List<String> reqs= zonesUrl.get(event.getId()+"-1");
					if(reqs==null ){
						reqs= new ArrayList<String>(); 
					}
					if(!reqs.contains(postBackUrl)){
						reqs.add(postBackUrl);
						zonesUrl.put(event.getId()+"-1", reqs);
					}
					CrawlServerResponse csr = new CrawlServerResponse();
					csr.setMsg("Success Invoking Crawls");
					csr.setNumberofcrawls(1);
					csr.setStatus("success");
					XStream xstream = new XStream();
					xstream.alias("CrawlServerResponse", CrawlServerResponse.class);
					String retString = xstream.toXML(csr);
					os.write(retString.getBytes());
					sendZonesTickets(event.getId(),event.getAdmitoneId());
					return;
				}	
			}
		}else if(ownerType.equals("OXP")){
			Collection<TicketListingCrawl> ticketListingCrawlersForEvent = ticketListingCrawler.getActiveTicketListingCrawlsByEventId(eventId);
			if(ticketListingCrawlersForEvent == null || ticketListingCrawlersForEvent.isEmpty()){
				CrawlServerResponse csr = new CrawlServerResponse();
				csr.setMsg("Error Invoking Crawls, No Active Crawls available");
				csr.setNumberofcrawls(0);
				csr.setStatus("error");
				XStream xstream = new XStream();
				xstream.alias("CrawlServerResponse", CrawlServerResponse.class);
				String retString = xstream.toXML(csr);
				os.write(retString.getBytes());
				return;
			}
			synchronized (postBackUrls) {
				Map<Integer, List<String>> urls= postBackUrls.getPostBackUrlMap();
				for(TicketListingCrawl tlc : ticketListingCrawlersForEvent){
					//tlc.setPostBackUrl(postBackUrl);
					List<String> reqs= urls.get(tlc.getId());
					if(reqs==null ){
						reqs= new ArrayList<String>(); 
					}
					if(!reqs.contains(postBackUrl)){
						reqs.add(postBackUrl);
						urls.put(tlc.getId(), reqs);
						tlc.setXmlPostBack(Boolean.TRUE);
					}
				}
			}

					
			//DAORegistry.getTicketListingCrawlDAO().saveOrUpdateAll(ticketListingCrawlersForEvent);
			while(true){
				Boolean running = Boolean.FALSE;
				for(TicketListingCrawl tlc : ticketListingCrawlersForEvent){
					if(tlc.getCrawlState().equals(CrawlState.RUNNING)){
						running = Boolean.TRUE;
					}
				}
				if(!running){
					break;
				}
			}
			ticketListingCrawler.forceRecrawlWithPriority(ticketListingCrawlersForEvent,TicketListingCrawl.PRIORITY_HIGH);
			//String retString = "<xml><status>success</status><msg>Success Invoking Crawls<msg><numberofcrawls>"+ ticketListingCrawlersForEvent.size() +"</numberofcrawls></xml>";
			CrawlServerResponse csr = new CrawlServerResponse();
			csr.setMsg("Success Invoking Crawls");
			csr.setNumberofcrawls(ticketListingCrawlersForEvent.size());
			csr.setStatus("success");
			XStream xstream = new XStream();
			xstream.alias("CrawlServerResponse", CrawlServerResponse.class);
			String retString = xstream.toXML(csr);
			os.write(retString.getBytes());
			return;
		}else if(ownerType.equals("MOXP")){
			Collection<TicketListingCrawl> ticketListingCrawlersForEvent = ticketListingCrawler.getActiveTicketListingCrawlsByEventId(eventId);
			int count=0;
			boolean hasMXP=false;
			if(ticketListingCrawlersForEvent != null && !ticketListingCrawlersForEvent.isEmpty()){
				count = ticketListingCrawlersForEvent.size();
			}
			if(event.getAdmitoneId()!=null){
				count=count + 1;
				hasMXP=true;
			}
			if(count==0){
				CrawlServerResponse csr = new CrawlServerResponse();
				csr.setMsg("Error Invoking Crawls, No Active Crawls available");
				csr.setNumberofcrawls(0);
				csr.setStatus("error");
				XStream xstream = new XStream();
				xstream.alias("CrawlServerResponse", CrawlServerResponse.class);
				String retString = xstream.toXML(csr);
				os.write(retString.getBytes());
				return;
			}else{
				CrawlServerResponse csr = new CrawlServerResponse();
				csr.setMsg("Success Invoking Crawls");
				csr.setNumberofcrawls(count);
				csr.setStatus("success");
				XStream xstream = new XStream();
				xstream.alias("CrawlServerResponse", CrawlServerResponse.class);
				String retString = xstream.toXML(csr);
				os.write(retString.getBytes());
				synchronized (postBackUrls) {
					Map<Integer, List<String>> urls= postBackUrls.getPostBackUrlMap();
					for(TicketListingCrawl tlc : ticketListingCrawlersForEvent){
						//tlc.setPostBackUrl(postBackUrl);
						List<String> reqs= urls.get(tlc.getId());
						if(reqs==null ){
							reqs= new ArrayList<String>(); 
						}
						if(!reqs.contains(postBackUrl)){
							reqs.add(postBackUrl);
							urls.put(tlc.getId(), reqs);
							tlc.setXmlPostBack(Boolean.TRUE);
						}
					}
					// Not sure why we used this
					while(true){
						Boolean running = Boolean.FALSE;
						for(TicketListingCrawl tlc : ticketListingCrawlersForEvent){
							if(tlc.getCrawlState().equals(CrawlState.RUNNING)){
								running = Boolean.TRUE;
							}
						}
						if(!running){
							break;
						}
					}
					ticketListingCrawler.forceRecrawl(ticketListingCrawlersForEvent);
				}
				if(hasMXP){
					synchronized (zonesUrl) {
						List<String> reqs= zonesUrl.get(event.getId()+"-1");
						if(reqs==null ){
							reqs= new ArrayList<String>(); 
						}
						if(!reqs.contains(postBackUrl)){
							reqs.add(postBackUrl);
							zonesUrl.put(event.getId()+"-1", reqs);
						}
					}
				}
				
				sendZonesTickets(event.getId(),event.getAdmitoneId());
				return;
			}
		}*/
		
	}
	
	public void getDupTicketMapForEvent(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream os = response.getOutputStream();
		String eventIdStr = request.getParameter("eventId");
		if(eventIdStr == null){
			DuplicateTicketWrapper dtr = new DuplicateTicketWrapper();
			dtr.setMsg("Error, Invalid eventId. eventId is null");
			dtr.setStatus("error");
			XStream xstream = new XStream();
			xstream.alias("DuplicateTicketWrapper", DuplicateTicketWrapper.class);
			xstream.alias("DuplicateTicketMap", DuplicateTicketMap.class);
			String retString = xstream.toXML(dtr);
			os.write(retString.getBytes());
			return;
		}
		
		Integer eventId = Integer.parseInt(eventIdStr);
		Event event = DAORegistry.getEventDAO().get(eventId);
		if(event == null){
			DuplicateTicketWrapper dtr = new DuplicateTicketWrapper();
			dtr.setMsg("Error, Invalid eventId. event does not exist");
			dtr.setStatus("error");
			XStream xstream = new XStream();
			xstream.alias("DuplicateTicketWrapper", DuplicateTicketWrapper.class);
			xstream.alias("DuplicateTicketMap", DuplicateTicketMap.class);
			String retString = xstream.toXML(dtr);
			os.write(retString.getBytes());
			return;
		}
		ArrayList<DuplicateTicketMap>duplicateTicketsMap = (ArrayList<DuplicateTicketMap>)DAORegistry.getDuplicateTicketMapDAO().getDuplicateTicketMap(eventId);
		XStream xstream = new XStream();
		xstream.autodetectAnnotations(false);
		DuplicateTicketWrapper dtr = new DuplicateTicketWrapper();
		dtr.setMsg("Success, Duplicate tickets found");
		dtr.setStatus("success");
		dtr.setDuplicateTicketMap(duplicateTicketsMap);
		xstream.alias("DuplicateTicketWrapper", DuplicateTicketWrapper.class);
		xstream.alias("DuplicateTicketMap", DuplicateTicketMap.class);
		String retString = xstream.toXML(dtr);
		os.write(retString.getBytes());
	}

	public PostBackURL getPostBackUrls() {
		return postBackUrls;
	}

	public void setPostBackUrls(PostBackURL postBackUrls) {
		this.postBackUrls = postBackUrls;
	}
	
	
	
	public void sendZonesTickets(Integer eventId,Integer tnEventId){
			try{
					SendRequest sr= new SendRequest(eventId,tnEventId);
					sr.setPriority(Thread.MAX_PRIORITY);
					sr.start();
			}catch (Exception e) {
				e.fillInStackTrace();
			}
		}
	public class SendRequest extends Thread{
		Integer eventId;
		Integer tnEventId;
		public SendRequest(Integer eventId,Integer tnEventId) {
			this.eventId=eventId;
			this.tnEventId=tnEventId;
		}
		@Override
		public void run() {
			try {
				XStream xstream = new XStream();
				xstream.autodetectAnnotations(false);
				xstream.alias("WSZonesWrapper", WSZonesWrapper.class);
				xstream.alias("MXPZonesTicket", MXPZonesTicket.class);
				WSZonesWrapper wsZonesWrapper = new WSZonesWrapper();
//				Collection<MXPZonesTicket> mxpTickets = DAORegistry.getMxpZonesTicketDAO().getMXPZonesTicketByTNEventId(tnEventId);
				wsZonesWrapper.setZoneTickets((ArrayList<MXPZonesTicket>)DAORegistry.getMxpZonesTicketDAO().getMXPZonesTicketByTNEventId(tnEventId));
				wsZonesWrapper.setEventId(eventId);
				String zonesDataXML = xstream.toXML(wsZonesWrapper);
				synchronized (zonesUrl) {
					List<String> urls= zonesUrl.remove(eventId + "-1");
					/***/
					for(String url:urls){
						HttpClient hc = new DefaultHttpClient();
						HttpPost hp = new HttpPost(url);
						NameValuePair nameValuePair = new BasicNameValuePair("zonesdata", zonesDataXML);
						List<NameValuePair> parameters = new ArrayList<NameValuePair>();
						parameters.add(nameValuePair);
						UrlEncodedFormEntity entity = new UrlEncodedFormEntity(parameters);
						hp.setEntity(entity);
						hc.execute(hp);
					}
					/***/
				}
			} catch (ClientProtocolException e) {
				System.out.println(e.fillInStackTrace());
			} catch (IOException e) {
				System.out.println(e.fillInStackTrace());
			} catch (Exception e) {
				System.out.println(e.fillInStackTrace());
			}

		}
		
	}
	public CrawlerWSUtil getCrawlerUtil() {
		return crawlerUtil;
	}

	public void setCrawlerUtil(CrawlerWSUtil crawlerUtil) {
		this.crawlerUtil = crawlerUtil;
	}
}