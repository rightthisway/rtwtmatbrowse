package com.admitone.tmat.web.decorators;

import javax.servlet.jsp.PageContext;

import org.displaytag.decorator.DisplaytagColumnDecorator;
import org.displaytag.exception.DecoratorException;
import org.displaytag.properties.MediaTypeEnum;

import com.admitone.tmat.utils.TextUtil;

public class ExtraWhiteSpaceStripWrapper implements DisplaytagColumnDecorator {

	public Object decorate(Object value, PageContext pageContext, MediaTypeEnum media)
			throws DecoratorException {
		
		if (value == null) {
			return null;
		}
		
		String str = value.toString();
		
		if (!media.equals(MediaTypeEnum.HTML)) {
			str = str.replaceAll("<[^>]+>", "").trim();
		}

		return TextUtil.removeExtraWhitespaces(str).trim();
	}
}
