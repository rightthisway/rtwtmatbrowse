package com.admitone.tmat.util.crawler;

import com.admitone.tmat.utils.CrawlUtils;

import java.util.Calendar;
import java.util.Date;
import java.util.Timer;

public class CrawlUtilTask
{

    private CrawlUtils crawlUtils;
    private Timer timer;

    /*public CrawlUtilTask(CrawlUtils crawlUtils)
    {
        this.crawlUtils = crawlUtils;
        timer = new Timer();
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        Date date = cal.getTime();
        timer.scheduleAtFixedRate(crawlUtils, date, 24*60*60* 1000L);
    }*/
    
    public CrawlUtilTask(CrawlUtils crawlUtils)
    {
        this.crawlUtils = crawlUtils;
        timer = new Timer();
        timer.scheduleAtFixedRate(crawlUtils, new Date(), 1*60*60* 1000L);
    }

    public CrawlUtils getCrawlUtils() {
		return crawlUtils;
	}

	public void setCrawlUtils(CrawlUtils crawlUtils) {
		this.crawlUtils = crawlUtils;
	}
}
