package com.admitone.tmat.util.crawler;

import com.admitone.tmat.crawler.util.CrawlerWSUtil;
import java.util.Date;
import java.util.Timer;

public class CrawlerUtilTask
{

    private CrawlerWSUtil crawlerUtil;
    private Timer timer;

    public CrawlerUtilTask(CrawlerWSUtil crawlerUtil)
    {
        this.crawlerUtil = crawlerUtil;
        timer = new Timer();
        timer.scheduleAtFixedRate(crawlerUtil, new Date(), 15000L);
    }

    public CrawlerWSUtil getCrawlerUtil()
    {
        return crawlerUtil;
    }

    public void setCrawlerUtil(CrawlerWSUtil crawlerUtil)
    {
        this.crawlerUtil = crawlerUtil;
    }
}
