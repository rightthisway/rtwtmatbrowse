@ECHO OFF

SET JARFILE=tmat.jar

if "%1" == "start" goto start
if "%1" == "stop" goto stop
if "%1" == "run" goto run

:usage
echo Usage: crawler
echo commands:
echo  start	Start the crawler in the background
echo  stop	Stop the crawler which  was running in the background
echo  run	Start the crawler in the foreground
goto end

:start
javaw -Xmx512m -jar %JARFILE% start
goto end

:run
java -Xmx512m -jar %JARFILE% start
goto end

:stop
java -Xmx512m -jar %JARFILE% stop
goto end

:end
