use tmattest;

--
-- This file contains the minimal data to be inserted after all the tables are created
--

--
-- Property
--
use tmattest;
INSERT INTO property (name, value) VALUES ('admit1.note.pattern',' --$');
INSERT INTO property (name, value) VALUES ('crawler.crawlMaxDuration','900');
INSERT INTO property (name, value) VALUES ('crawler.email.creator.enabled','0');
INSERT INTO property (name, value) VALUES ('crawler.email.error.enabled','1');
INSERT INTO property (name, value) VALUES ('crawler.email.lastUpdater.enabled','0');
INSERT INTO property (name, value) VALUES ('crawler.email.startStop.enabled','0');
INSERT INTO property (name, value) VALUES ('crawler.email.to','frederic.testa1@gmail.com');
INSERT INTO property (name, value) VALUES ('crawler.error.checkFrequency','600');
INSERT INTO property (name, value) VALUES ('crawler.error.crawlDurationThreshold','000');
INSERT INTO property (name, value) VALUES ('crawler.error.noTicketsFoundError','0');
INSERT INTO property (name, value) VALUES ('crawler.error.readyCrawlCountThreshold','200');
INSERT INTO property (name, value) VALUES ('crawler.idle.enabled','false');
INSERT INTO property (name, value) VALUES ('crawler.idle.startHour','22');
INSERT INTO property (name, value) VALUES ('crawler.idle.stopHour','23');
INSERT INTO property (name, value) VALUES ('crawler.scheduler.minPeriod','900');
INSERT INTO property (name, value) VALUES ('crawler.scheduler.mode','ALWAYS_SCHEDULED');
INSERT INTO property (name, value) VALUES ('crawler.scheduler.proActiveFactor','30');
INSERT INTO property (name, value) VALUES ('crawler.scheduler.properties','0d=30m, 2d=4h, 7d=6h, 14d=12h');
INSERT INTO property (name, value) VALUES ('crawler.scheduler.sortingTimeSlice','3');
INSERT INTO property (name, value) VALUES ('crawler.scheduler.startHour','0');
INSERT INTO property (name, value) VALUES ('crawler.scheduler.stopHour','23');
INSERT INTO property (name, value) VALUES ('crawler.startup.enabled','1');
INSERT INTO property (name, value) VALUES ('crawler.thread.count','8');
INSERT INTO property (name, value) VALUES ('crawler.ticket.ttl','60');
INSERT INTO property (name, value) VALUES ('ebay.category.scheme','AO');
INSERT INTO property (name, value) VALUES ('ebay.check.period','5');
INSERT INTO property (name, value) VALUES ('ebay.eimp.fan.addition','15.0');
INSERT INTO property (name, value) VALUES ('ebay.email.recipient','francois.dangngoc@gmail.com');
INSERT INTO property (name, value) VALUES ('ebay.email.subject','Ebay Notification');
INSERT INTO property (name, value) VALUES ('ebay.filter.netProfit','35.0');
INSERT INTO property (name, value) VALUES ('ebay.lastDownload.date','10-27-2009 17:18:16');
INSERT INTO property (name, value) VALUES ('ebay.lastDownload.username','admin');
INSERT INTO property (name, value) VALUES ('ebay.listeasy.login','listeasy');
INSERT INTO property (name, value) VALUES ('ebay.listeasy.password','sports09');
INSERT INTO property (name, value) VALUES ('ebay.rptFactor','25.0');
INSERT INTO property (name, value) values ('ebay.invalid.addition', 900);
INSERT INTO property (name, value) VALUES ('eimarketplace.password','bosplit09');
INSERT INTO property (name, value) VALUES ('eimarketplace.username','admit1');
INSERT INTO property (name, value) VALUES ('eventcreation.auto.active','false');
INSERT INTO property (name, value) VALUES ('eventcreation.auto.mode','NONE');
INSERT INTO property (name, value) VALUES ('eventcreation.auto.startHour','20');
INSERT INTO property (name, value) VALUES ('eventcreation.auto.stopHour','6');
INSERT INTO property (name, value) VALUES ('feedback.email.to','araut@ticketgallery.com,francois.dangngoc@gmail.com');
INSERT INTO property (name, value) VALUES ('feedback.error.email.to','araut@ticketgallery.com,francois.dangngoc@gmail.com');
INSERT INTO property (name, value) VALUES ('feedback.jira.affectsVersions','1.0');
INSERT INTO property (name, value) VALUES ('feedback.jira.components','TMAT');
INSERT INTO property (name, value) VALUES ('feedback.jira.enabled','0');
INSERT INTO property (name, value) VALUES ('feedback.jira.password','falafel');
INSERT INTO property (name, value) VALUES ('feedback.jira.project','TMAT');
INSERT INTO property (name, value) VALUES ('feedback.jira.rpcPath','/rpc/xmlrpc');
INSERT INTO property (name, value) VALUES ('feedback.jira.uri','https://jiraa1.yoonew.com');
INSERT INTO property (name, value) VALUES ('feedback.jira.username','tmat-feedback');
INSERT INTO property (name, value) VALUES ('feedback.redmine.enabled','1');
INSERT INTO property (name, value) VALUES ('feedback.redmine.password','ravioli');
INSERT INTO property (name, value) VALUES ('feedback.redmine.project','tmat2');
INSERT INTO property (name, value) VALUES ('feedback.redmine.uri','http://192.168.0.32:9000');
INSERT INTO property (name, value) VALUES ('feedback.redmine.username','feedback');
INSERT INTO property (name, value) VALUES ('smtp.auth','true');
INSERT INTO property (name, value) VALUES ('smtp.from','frederic.testa1@gmail.com');
INSERT INTO property (name, value) VALUES ('smtp.host','smtp.gmail.com');
INSERT INTO property (name, value) VALUES ('smtp.password','currydream');
INSERT INTO property (name, value) VALUES ('smtp.port','465');
INSERT INTO property (name, value) VALUES ('smtp.secure.connection','no');
INSERT INTO property (name, value) VALUES ('smtp.username','frederic.testa1@gmail.com');

--
-- Synonym
--

INSERT INTO synonym (name, value) VALUES ('center', 'centre, ctr');
INSERT INTO synonym (name, value) VALUES ('mezzanine', 'mezz');
INSERT INTO synonym (name, value) VALUES ('orchestra', 'orch');
INSERT INTO synonym (name, value) VALUES ('rear mezzanine', 'rmezz, rmzz');

--
-- Role
--

INSERT INTO role (id, description) VALUES ('ROLE_ADMIN', 'Administrators');
INSERT INTO role (id, description) VALUES ('ROLE_EBAY', 'Ebay');
INSERT INTO role (id, description) VALUES ('ROLE_EDITOR', 'Editor');
INSERT INTO role (id, description) VALUES ('ROLE_USER', 'User');

--
-- Site
--

INSERT INTO site (id, price_rounding, max_concurrent_connections) VALUES ('ebay','NONE',1);
INSERT INTO site (id, price_rounding, max_concurrent_connections) VALUES ('eimarketplace','NONE',2);
INSERT INTO site (id, price_rounding, max_concurrent_connections) VALUES ('eventinventory','NONE',1);
INSERT INTO site (id, price_rounding, max_concurrent_connections) VALUES ('fansnap','NONE',1);
INSERT INTO site (id, price_rounding, max_concurrent_connections) VALUES ('getmein','NONE',1);
INSERT INTO site (id, price_rounding, max_concurrent_connections) VALUES ('razorgator','NONE',2);
INSERT INTO site (id, price_rounding, max_concurrent_connections) VALUES ('seatwave','NONE',1);
INSERT INTO site (id, price_rounding, max_concurrent_connections) VALUES ('stubhub','DOWN_TO_NEAREST_DOLLAR',2);
INSERT INTO site (id, price_rounding, max_concurrent_connections) VALUES ('ticketmaster','NONE',1);
INSERT INTO site (id, price_rounding, max_concurrent_connections) VALUES ('ticketnetwork','NONE',2);
INSERT INTO site (id, price_rounding, max_concurrent_connections) VALUES ('ticketnetworkdirect','NONE',1);
INSERT INTO site (id, price_rounding, max_concurrent_connections) VALUES ('ticketsnow','NONE',1);
INSERT INTO site (id, price_rounding, max_concurrent_connections) VALUES ('ticketsolutions','NONE',1);
INSERT INTO site (id, price_rounding, max_concurrent_connections) VALUES ('viagogo','NONE',1);
INSERT INTO site (id, price_rounding, max_concurrent_connections) VALUES ('wstickets','NONE',1);

--
-- Sample data
--

-- amitraut / tu59op

INSERT INTO admitone_user (username, email, first_name, last_name, locked, password,
                           phone, email_crawl_error_creator_enabled,
                           email_crawl_error_last_updater_enabled,
                           code)
VALUES ('amitraut', 'araut@ticketgallery.com', 'Amit', 'Raut', 0, 'e03f87d75b25659df5c31f2ce018121807c028b1',
        '', 0, 0, NULL);

INSERT INTO user_role (username, role_id) VALUES ('amitraut', 'ROLE_ADMIN');
INSERT INTO user_role (username, role_id) VALUES ('amitraut', 'ROLE_EBAY');
INSERT INTO user_role (username, role_id) VALUES ('amitraut', 'ROLE_EDITOR');
INSERT INTO user_role (username, role_id) VALUES ('amitraut', 'ROLE_USER');

-- francois / tu59op

INSERT INTO admitone_user (username, email, first_name, last_name, locked, password,
                           phone, email_crawl_error_creator_enabled,
                           email_crawl_error_last_updater_enabled,
                           code)
VALUES ('francois', 'araut@ticketgallery.com', 'Francois', '', 0, 'e03f87d75b25659df5c31f2ce018121807c028b1',
        '', 0, 0, 0, NULL);

INSERT INTO user_role (username, role_id) VALUES ('francois', 'ROLE_ADMIN');
INSERT INTO user_role (username, role_id) VALUES ('francois', 'ROLE_EBAY');
INSERT INTO user_role (username, role_id) VALUES ('francois', 'ROLE_EDITOR');
INSERT INTO user_role (username, role_id) VALUES ('francois', 'ROLE_USER');

