You receive this email because you are in the list of the recipient of error feedbacks. If you wish to not receive these emails
anymore, log in, go to Admin Tab > Feedback Settings > Recipients of error feedbacks and then remove your email from the list.

${headerMessage}

${submitter} has submitted the following error feedback:

- Submission date: ${submissionDate}
- Url: ${url}
- Referrer url: ${referrerUrl}
- Cookies: ${cookies}
- Preferences:  #foreach( $preference in $preferences ) ${preference.name} = '${preference.value}',  #end
- User-Agent: ${userAgent}
- Client IP Address: ${clientIpAddress}

- Priority: #if($errorFeedbackPriority == 10) BLOCKER #elseif($errorFeedbackPriority == 4) NORMAL #elseif($errorFeedbackPriority == 5) HIGH #elseif($errorFeedbackPriority == 6) URGENT #elseif($errorFeedbackPriority == 7) IMMEDIATE
#end
- Feedback Summary: ${errorFeedbackSummary}

- Error feedback Message:
${errorFeedbackMessage}

- Error stack trace:
${errorStackTrace}