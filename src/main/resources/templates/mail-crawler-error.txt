You receive this email because you are in the list of the recipient of crawler notifications. If you wish to not receive these emails
anymore, log in, go to Admin Tab > Crawler Settings > "Send crawler notifications to" and then remove your email from the list.

Server Host: ${hostName}
Server Address: ${hostAddress}

#if($readyCrawlCount)
Warning: ${readyCrawlCount} crawls are in ready mode.
#end

#if($siteCountMsgList)
Ready mode Crawls in Site Level: 
#foreach($siteMsg in $siteCountMsgList)
${siteMsg}
#end
#end

#if($slowRunningCrawls)
The following crawls are running for more than 15min:
#foreach($crawl in $slowRunningCrawls)

Crawl Id: ${crawl.id}
Crawl Name: ${crawl.name}
Crawl Status: ${crawl.crawlState}
Crawl Phase: ${crawl.crawlPhase}
Canceled: ${crawl.cancelled}
Crawl Start: ${crawl.startCrawl}
#if($crawl.queryUrl)
Crawl Query URL: ${crawl.queryUrl}
#end
#if(${crawl.extraParametersString})
Crawl Extra Parameters: ${crawl.extraParametersString}
#end
#end
#end