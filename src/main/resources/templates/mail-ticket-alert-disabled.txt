This alert has been DISABLED.

=====================================
Alert Settings:
=====================================
Alert Id: ${userAlert.id}
Creator: ${userAlert.username}
Alert For: ${userAlert.alertFor}
Alert Type: ${userAlert.userAlertType}

Event: ${userAlert.event.name}
Event Date: ${userAlert.event.formattedEventDate}
Venue: ${userAlert.event.formattedVenueDescription}

#if( ${userAlert.categoryGroupName} )
Category Scheme: ${userAlert.categoryGroupName}
#end
Category: ${userAlert.categoryDescription}
Price Range: ${userAlert.priceRangeDescription} 
Section Range: ${userAlert.sectionRangeDescription}
Row Range: ${userAlert.rowRangeDescription}
Quantity: ${userAlert.quantitiesDescription}
Exchanges: ${userAlert.sitesDescription}
Description: ${userAlert.description}

Current time: ${now}
Changes in the listing since: ${lastCheck}
This alert was sent from $serverName.

To re-enable this alert, please click on this link and select 'ENABLE ALERT' and Update
${EnableUrl}