<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<c:choose>
	<c:when test="${mode == 'editAll'}">
		<c:set var="pageUrl" value="EditorEditAlert" />
		<c:set var="manageAlertsUrl" value="EditorManageAlerts" />
		<c:set var="alertPreviewUrl" value="EditorAlertPreview" />		
	</c:when>
	<c:otherwise>
		<c:set var="pageUrl" value="EditMyAlert" />
		<c:set var="manageAlertsUrl" value="MyAlerts" />
		<c:set var="alertPreviewUrl" value="MyAlertPreview" />		
	</c:otherwise>
</c:choose>

<c:choose>
	<c:when test="${not empty userAlert}">
		<h1>Edit Alert ${userAlert.id}</h1>
	</c:when>
	<c:otherwise>
		<h2>Add Alert</h2>
	</c:otherwise>
</c:choose>

<div id="container-tabs" style="margin-top: 5px">
	<ul>
    	<li><a href="#fragment-alert-parameters"><span>Parameters</span></a></li>
		<c:if test="${not empty userAlert}">
	    	<li><a href="#fragment-alert-info"><span>Info</span></a></li>
	    </c:if>
	</ul>

	<div id="fragment-alert-parameters">	
		<b>Alert me at <font color="blue">${user.email}</font> when there are tickets for the condition:</b>
		<br /><br />
		<form id="updateAlertForm" action="${pageUrl}" method="POST">
		<input type="hidden" name="action" value="save" />
		<c:if test="${not empty userAlert}">
			<input type="hidden" name="alertId" value="${userAlert.id}" />
		<input type="hidden" name="alertFor" value="${userAlert.alertFor}" />
		</c:if>
	
		<table>
			<colgroup>
				<col width="120" />
				<col width="*" />
			</colgroup>
			<tr>
			<c:if test="${not empty userAlert}">
				<td>
					Ticket in Event:
				</td>
			</c:if>
			<c:if test="${empty userAlert}">
				<td>
					Ticket in Artist:
				</td>
			</c:if>
				<td>
					<c:choose>
						<c:when test="${not empty userAlert}">
							<a href="BrowseTickets?eventId=${userAlert.event.id}">${userAlert.event.name}&nbsp;${userAlert.event.formattedEventDate}&nbsp;${userAlert.event.formattedVenueDescription}</a>
							<input type="hidden" name="alertEventId" id="alertEventId" value="${userAlert.eventId}" />
						</c:when>
						<c:otherwise>
							<span id="eventDescription">
								<c:choose>
									<c:when test="${empty artist}">
										No Artist
										<input type="hidden" name="alertArtistId" id="alertArtistId" value="" />								
									</c:when>
									<c:otherwise>
										${artist.name}
										<input type="hidden" name="alertArtistId" id="alertArtistId" value="${artist.id}" />								
									</c:otherwise>
								</c:choose>
							</span>
							<span id="eventSearch" style="display:none">
								Search <input type="text" id="eventSearchTextField" />
							</span>
							
							<a id="selectEventLink" href="javascript:selectEvent()">Select an Artist</a>
							<a id="cancelSelectEventLink" href="javascript:cancelSelectEvent()" style="display:none">Cancel</a>
							
							<span id="eventSearchTextFieldStatus"></span>
						</c:otherwise>
					</c:choose>
					
				</td>
			</tr>
			<c:if test="${not empty events and events ne null}">
			<tr>
			<td colspan='2'><br/></td>
			</tr>
			</c:if>
		<%-- 	<tr>
				<td>
					Zone Scheme:
				</td>
				<td>
					<c:choose>
						<c:when test="${not empty userAlert}">
							<c:choose>
								<c:when test="${empty categoryGroupName}">
									None
								</c:when>
								<c:otherwise>
									${categoryGroupName}
									<a href="javascript:void(0)" onclick="openVenueWindow()">
										<img id="venue-image" width="32" height="32" src="VenueImage?venueId=${event.venueId}&catScheme=${categoryGroupName}" align="middle" style="margin-left:8px;border: 1px solid #aaa" />
									</a>
									
								</c:otherwise>
							</c:choose>
							<input type="hidden" name="alertCategoryGroupName" value="${categoryGroupName}" />
						</c:when>
						<c:otherwise>
							<c:choose>
								<c:when test="${empty tour}">
									<i>Must select a Tour before category schemes are available</i>
									<input type="hidden" name="alertCategoryGroupName" value="" />
								</c:when>
								<c:when test="${empty categoryGroupNames or categoryGroupNames[0] == ''}">
									<i>The tour has no category schemes</i>
									<input type="hidden" name="alertCategoryGroupName" value="" />
								</c:when>
								<c:otherwise>
								    <select id="categoryGroupName" name="alertCategoryGroupName" onchange="changeCategoryGroupName()">
									  	<c:forEach var="forCategoryGroupName" items="${categoryGroupNames}">
									    	<option value="${forCategoryGroupName}" <c:if test="${forCategoryGroupName == categoryGroupName}">selected</c:if>>${forCategoryGroupName}</option>
									  	</c:forEach>
									</select>
									<a href="javascript:void(0)" onclick="openVenueWindow()">
										<img id="venue-image" width="32" height="32" src="VenueImage?venueId=${event.venueId}&catScheme=${categoryGroupName}" align="middle"  style="margin-left:8px;border: 1px solid #aaa"/>
									</a>
								</c:otherwise>
							</c:choose>
						</c:otherwise>
					</c:choose>							
				</td>
			</tr> --%>
			
			
			<tr <c:if test="${not empty userAlert and userAlert.eventId ne null}">style="display: none;"</c:if> >
		<td></td>
		<td>
		<c:if test="${not empty events}">
				Select:
				<a href="#" onclick="selectAll(); return false">All</a> |
				<a href="#" onclick="unselectAll(); return false">None</a> |
				<a href="#" onclick="invertAll(); return false">Invert</a>
				
		</c:if>
		
		</td>
	</tr> 
	<tr <c:if test="${not empty userAlert and userAlert.eventId ne null}">style="display: none;"</c:if>>	
				<td>Events </td>
				<td>
	<display:table class="list" name="${events}" id="event" requestURI="BrowseEvents" >
      <display:column>
        <input type="checkbox" id="event_${event_rowNum}" name="event_${event.id}" 
        value="${event.venueId}_${event.venueCategoryId}" class="event_checkbox" onclick="callChooseEvent();"/>
       <%--  <input type="hidden" id="venueId_"  value="${event.venueId}" class="event_checkbox"/>
        <input type="hidden" id="venueCategoryId_" value="${event.venueCategoryId}" class="event_checkbox"/> --%>
      </display:column>
    <display:column style="width:20px"><div id="star_${event.id}" class="starOff" onclick="toggleBookmarkEvent(${event.id})"></div></display:column>
	    <c:choose>
	      <c:when test="${browseEventsPage == 'BrowseEventsAnalytic'}">
	        <display:column>
	          <b>
	            <a href="BrowseTicketsAnalytic?eventId=${event.id}&view=analytic">View</a>
	          </b>
	        </display:column>    
	      </c:when>
	      <c:otherwise>
	        <display:column value="View"  href="BrowseTickets" paramId="eventId" paramProperty="id" class="bold" />
	      </c:otherwise>
	    </c:choose>
    <display:column title="Date" sortable="true">
    	<b>
		    <fmt:formatDate pattern="MM/dd/yyyy" value="${event.date}" />
			<c:choose>
			  <c:when test="${not empty event.time}">
			    <fmt:formatDate pattern="HH:mm" value="${event.time}" />
			  </c:when>
			  <c:otherwise>
			    TBD
			  </c:otherwise>
			</c:choose>		
		</b>
    </display:column>
    <display:column title="Event Name" sortable="true" sortProperty="name">
      <img src="../images/ico-${fn:toLowerCase(event.eventType)}.gif" align="absbottom" title="${event.eventType}"/> ${event.name}
    </display:column>
    <display:column title="Venue" sortable="true" property="venue.building" ></display:column>
    <display:column title="State" sortable="true" property="venue.state"></display:column>
    <display:column title="Country">
    	<c:if test="${not empty event.venue.country && event.venue.country != 'null' }">
    		<img src="../images/flags/${event.venue.country}.gif" />${event.venue.country}
    	</c:if>
    </display:column>
    
    <c:choose>
      <c:when test="${browseEventsPage == 'BrowseEventsAnalytic'}">
        <display:column title="OPHigh">
        	${eventToActions[event.id]['OH']}
        </display:column>
        <display:column title="OPMed">
        	${eventToActions[event.id]['OM']}
        </display:column>
        <display:column title="OPLow">
        	${eventToActions[event.id]['OL']}
        </display:column>
        <display:column title="Correct">
        	${eventToActions[event.id]['CO']}
        </display:column>
        <display:column title="UPLow">
        	${eventToActions[event.id]['UL']}
        </display:column>
        <display:column title="UPMed">
        	${eventToActions[event.id]['UM']}
        </display:column>
        <display:column title="UPHigh">
        	${eventToActions[event.id]['UH']}
        </display:column>
      </c:when>
    </c:choose>    
    <display:column title="Managed By" sortable="true" sortProperty="marketMakerManager" class="text-align-center">
      <c:if test="${event.marketMakerManager != 'DESK'}">
      <img src="../images/ico-user.gif" align="absbottom"/>
      </c:if>
      ${event.marketMakerManager}
    </display:column>
  
  	<display:column title="CATS Count" class="text-align-center">
  		<a href="javascript: return null;" onclick="$('#displaycats${event.id}').dialog('open');">${fn:length(event.category)}</a>
  	</display:column>
  	
  	<display:column title="CATS Group" class="text-align-center">
		<a href="javascript: return null;" onclick="$('#displaycats${event.id}').dialog('open');">
			${event.uniqueCategoryGroupName}
		</a>	 			
  	</display:column>
</display:table>

	</td>
	</tr>
			<tr <c:if test="${empty categories}"> style="display: none;"</c:if> id="catRowId">
				<td>
					In zone:
				</td>
				<td>
					<c:choose>
						<c:when test="${empty artist and empty userAlert}">
							<i>Must select an Artist before zones are available</i>	
							<input type="hidden" name="alertCategory" value="" />								
						</c:when>
						<%-- <c:when test="${empty categories}">
							<i>This tour has no zones</i>
							<input type="hidden" name="alertCategory" value="" />								
						</c:when> --%>
						<c:otherwise>
						    <select  id="category" name="alertCategory" onchange="refreshCategoryDescription()">
						    	<option value="ANY" <c:if test="${categorySymbol=='ANY'}">selected</c:if>>All Tickets</option>
						    	<option value="ALL Zones" <c:if test="${categorySymbol=='ALL Zones'}">selected</c:if>>Categorized</option>
						    	<option value="NO ZONES" <c:if test="${categorySymbol=='NO ZONES'}">selected</c:if>>Uncategorized</option>
							  	 <c:forEach var="forCategory" items="${categories}">
							    	<option value="${forCategory.symbol}" <c:if test="${forCategory.symbol == categorySymbol}">selected</c:if> >${forCategory.symbol}</option>
							  	</c:forEach>
							</select>
							<a href="javascript:void(0)" onclick="openVenueWindow()" id="venueReferenceMap">
							
								<c:if test="${not empty userAlert and userAlert ne null}">
									<img id="venue-image" width="32" height="32"  title="Venue Map"
									src="VenueImage?venueId=${event.venueId}&catScheme=${categoryGroupName}" 
									align="middle" style="margin-left:8px;border: 1px solid #aaa" /> 
								</c:if>
							</a>
							<!--
							<input type="checkbox" id="nullCategory" name="alertNullCategory" onchange="changeNullCategory(this.checked)" <c:if test="${userAlert.categoryNull}">checked</c:if> ><label for="nullCategory">get only tickets with null/empty category</label>
							-->
							<br />
							<span id="categoryDescription" style="color: #448844;font-style:italic"></span>					
						</c:otherwise>
					</c:choose>
				</td>
			</tr>
			
			<tr>
				<td>
					with price
				</td>
				<td>
					<input type="text" id="priceLow" name="alertPriceLow" onchange="refreshPriceDescription()" value="${userAlert.priceLow}" style="width:60px;" />
					to
					<input type="text" id="priceHigh" name="alertPriceHigh" onchange="refreshPriceDescription()"  value="${userAlert.priceHigh}" style="width:60px;" />
					<br />
					<span id="priceDescription" style="color: #448844;font-style:italic"></span>
				</td>
			</tr>
			<tr>
				<td>
					in sections:
				</td>
				<td> 
					<input id="sectionLow" name="alertSectionLow" type="text" onchange="refreshSectionDescription()" value="${userAlert.sectionLow}" style="width:60px;"/>
					to
					<input id="sectionHigh"  name="alertSectionHigh" type="text" onchange="refreshSectionDescription()" value="${userAlert.sectionHigh}" style="width:60px;" />
					<br />
					<span id="sectionDescription" style="color: #448844;font-style:italic"></span>
				</td>
			</tr>
			<tr>
				<td>
					in rows:
				</td>
				<td> 
					<input id="rowLow" name="alertRowLow" type="text" onchange="refreshRowDescription()" value="${userAlert.rowLow}" style="width:60px;" />
					to
					<input id="rowHigh" name="alertRowHigh" type="text" onchange="refreshRowDescription()" value="${userAlert.rowHigh}" style="width:60px;"/>
					<!--
					<input type="checkbox" id="nullRow" name="alertNullRow" onchange="changeNullRow(this.checked)" <c:if test="${userAlert.rowRangeNull}">checked</c:if>><label for="nullRow">get only tickets with null/empty row</label>
					-->
					<br />
					<span id="rowDescription" style="color: #448844;font-style:italic"></span>
				</td>
			</tr>
			<tr>
				<td>
					With quantities:
				</td>
				<td> 
					<input id="quantities" name="alertQuantities" type="text" style="width:200px;" value="${userAlert.quantities}" onchange="refreshQuantitiesDescription()" style="width:60px;" /><br />
					<span id="quantitiesDescription" style="color: #448844;font-style:italic"></span>
				</td>
			</tr>
			<tr>
				<td></td>
				<td style="font-size:10px">You can use <b>4+</b> to mean 4 or more tickets, <b>odd</b> to get tickets with odd quantities(excluding 1), <b>even</b> to get tickets with even quantities and <b>2-10</b> to get tickets with quantities 2(included) to 10(included)</td>
			</tr>
			
			<tr>
				<td>
					Alert Enable/Disable:
				</td>
				<td>
					<c:set var="userAlertStatus" value="ACTIVE" />
					<c:if test="${not empty userAlert}">
						<c:set var="userAlertType" value="${userAlert.userAlertStatus}" />
					</c:if>		
					<input id="enableAlertStatus" name="alertStatus" type="radio" value="ACTIVE" <c:if test="${userAlertStatus=='ACTIVE'}">checked</c:if> /><label for="enableAlertStatus">Enable Alert</label><br />
					<input id="disableAlertStatus" name="alertStatus" type="radio" value="DISABLED" <c:if test="${userAlertStatus=='DISABLED'}">checked</c:if> /><label for="disableAlertStatus">Disable Alert</label><br />
					<br />
				</td>
			</tr>
			<tr>
				<td>
					Alert me:
				</td>
				<td>
					<c:set var="userAlertType" value="IN" />
					<c:if test="${not empty userAlert}">
						<c:set var="userAlertType" value="${userAlert.userAlertType}" />
					</c:if>
		<!--			<input id="inOutAlertType" name="alertType" type="radio" value="IN_OUT" <c:if test="${userAlertType=='IN_OUT'}">checked</c:if> /><label for="inOutAlertType">IN/OUT: Whenever tickets come into/come out TMAT</label> <br /> -->
					<input id="inAlertType" name="alertType" type="radio" value="IN" <c:if test="${userAlertType=='IN'}">checked</c:if> /><label for="inAlertType">IN: Whenever new tickets come into TMAT</label><br />
					<input id="outAlertType" name="alertType" type="radio" value="OUT" <c:if test="${userAlertType=='OUT'}">checked</c:if> /><label for="outAlertType">OUT: Whenever tickets come out TMAT</label><br />
					<br />
					<span id="quantitiesDescription" style="color: #448844;font-style:italic"></span>
				</td>
			</tr>
			<tr>
				<td>
					Description:
				</td>
				<td> 
					<textarea name="alertDescription" type="text" style="width:400px;height:60px;">${userAlert.description}</textarea>
				</td>
			</tr>	
			<tr>
				<td>in these exchanges</td>
				<td>
					<a href="javascript:void(0)" onclick="checkAllSites()" style="font-size:10px;">Select all</a> - 
					<a href="javascript:void(0)" onclick="uncheckAllSites()" style="font-size:10px;">Select none</a>
					<div style="float:left;">
						<c:forEach var="sid" items="${constants.siteIds}">
							<c:set var="safeSid" value="[${sid}]" />
							<div style="float: left; width: 160px;height: 24px;">
								<input type="checkbox" name="site-${sid}" class="site-checkbox"
									<c:if test="${siteSelected[sid] and not fn:contains(constants.disabledSiteIdsString, safeSid)}">checked</c:if>
									<c:if test="${fn:contains(constants.disabledSiteIdsString, safeSid)}">disabled</c:if>
								>
								<label for="${sid}_checkbox">
							    	<img src="../images/ico-${sid}.gif" border="0" align="absmiddle"/>						
									<c:choose>
										<c:when test="${fn:contains(constants.disabledSiteIdsString, safeSid)}">
											<span style="color: #888888; text-decoration: line-through" tooltip="${sid} is disabled">
												${sid}
											</span>
										</c:when>
										<c:otherwise>
											${sid}
										</c:otherwise>
									</c:choose>
								</label>
							</div>
						</c:forEach>
					</div>
				</td>
			</tr>
		
	
	
	</table>
	
	<c:choose>
		<c:when test="${not empty userAlert && userAlert.eventId ne null && userAlert.eventId ne ''}">
		<a href="javascript:void(0)" onclick="previewAlert()">Preview Alert</a><br /><br />
		<div id="previewAlertDiv"></div>
		<br />
		</c:when>
		</c:choose>

		<c:choose>
			<c:when test="${empty userAlert}">
				<input id="addAlert" type="button" class="medButton" onclick="submitAlertForm()"  value="Add"/>
			</c:when>
			<c:otherwise>
				<input id="addAlert" type="button" class="medButton" onclick="submitAlertForm()"  value="Update"/>
			</c:otherwise>
		</c:choose>
		<input id="addAlert" type="button" class="medButton" onclick="document.location.href='${manageAlertsUrl}'"  value="Cancel"/>
		</form>
	</div>
	
	<c:if test="${not empty userAlert}">
		<div id="fragment-alert-info">
			<table>
				<tr>
					<td>
						Creation Date:
					</td>
					<td><fmt:formatDate value="${userAlert.creationDate}" pattern="yyyy/MM/dd HH:mm:ss" /></td>
				</tr>
				<tr>
					<td>
						Market Maker:
					</td>
					<td>${userAlert.marketMakerUsername}</td>
				</tr>
				<tr>
					<td>
						Last Email Sent:
					</td>
					<td><fmt:formatDate value="${userAlert.lastEmailDate}" pattern="yyyy/MM/dd HH:mm:ss" /></td>
				</tr>
				<tr>
					<td>
						Alert for:
					</td>
					<td>${userAlert.alertFor}</td>
				</tr>
			<c:if test="${userAlert.alertFor == 'CIRCLES'}">
				<tr>
					<td>
						Account Name:
					</td>
					<td>${userAlert.accountName}</td>
				</tr>
				<tr>
					<td>
						Ordered By:
					</td>
					<td>${userAlert.orderedBy}</td>
				</tr>
				<tr>
					<td>
						Phone Number:
					</td>
					<td>${userAlert.phoneNumber}</td>
				</tr>
				<tr>
					<td>
						Alt. Phone:
					</td>
					<td>${userAlert.altPhone}</td>
				</tr>
				<tr>
					<td>
						Billing Name:
					</td>
					<td>${userAlert.billingName}</td>
				</tr>
				<tr>
					<td>
						SR NO:
					</td>
					<td>${userAlert.srNo}</td>
				</tr>
			</c:if>			
			</table>


		</div>
	</c:if>	
<br />

<script type="text/javascript">

/*
function submitAlertForm() {
	
	var alertArtistId = null;
	if(null != document.getElementById("alertArtistId")){
		alertArtistId = document.getElementById("alertArtistId").value;
	}
	alert("alertArtistId---"+alertArtistId);
	
	if(null == alertArtistId || alertArtistId == ""){
		alert("Please Choose the tour.");
		return false;
		
	}
	
	
	// validate event
	
	if($('[name=alertEventId]').val() == "") {
		alert("No events defined.");
		return;
	}

	
	// validate price
	if (!refreshPriceDescription()) {
		return false;
	}
	
	// validate quantity
	if (!refreshQuantitiesDescription()) {
		return false;
	}
	alert("FINAL---");
	$('#updateAlertForm').submit();
};
*/
function submitAlertForm() {
	
	var isNone=false;

	if($('[name=alertArtistId]').val() == ""){

		alert("Please Choose the Artist.");
		isNone=true;
		return false;

	}else if($('[name=alertEventId]').val() == "") {

		alert("No events defined.");
		isNone=true;
		return false;
	}
	if(!isNone){

		if (!refreshPriceDescription()) {
			isNone=true;
			return false;
		}	
		if (!refreshQuantitiesDescription()) {
			isNone=true;
			return false;
		}
		if(verifyAnyValidData()){
			alert("Please enter Prices or Sections or Rows or Quantities.");
			isNone=true;
			return false;
		}
		if(!chooseEventAndExchange()){
			isNone=true;
			return false;
		}

	}
	if(!isNone){
		$('#updateAlertForm').submit();
	}
};

function chooseEventAndExchange(){
	var booleanVal = false;
	$.each($('.site-checkbox'), function(index, item) {
		      if ($(item).is(':checked')) {
				booleanVal = true;
		      }
	});	
	if(!booleanVal){
			alert("Please Choose atleast one exchange to proceed.");
			return false;
	}else{
		
		if(null != $('[name=alertEventId]') && $('[name=alertEventId]').val() != "" && $('[name=alertEventId]').val() != undefined){
			booleanVal = true;
		}else{
			booleanVal = false;
			$.each($('.event_checkbox'), function(index, item) {
					  if ($(item).is(':checked')) {
						booleanVal = true;
					  }
			});
			if(!booleanVal){
				alert("Please Choose atleast one event to proceed.");
				return false;
			}
		}
	}
	return booleanVal;
}

function verifyAnyValidData(){
	var noData =true;
	if( ($('[name=alertPriceLow]').val() != "") || ($('[name=alertPriceHigh]').val() != "")){
		noData=false;
	}
	if(($('[name=alertSectionLow]').val() != "") || ($('[name=alertSectionHigh]').val() != "")){
		noData=false;
	}
	if(($('[name=alertRowLow]').val() != "") || ($('[name=alertRowHigh]').val() != "")){
		noData=false;
	}
	if($('[name=alertQuantities]').val() != ""){
		noData=false;
	}
	return noData;
}

	function changeCategoryGroupName() {
		var artistId = $('INPUT[name=alertArtistId]').val();
		var categoryGroupName = $('#categoryGroupName').val();
		document.location.href = "${pageUrl}?alertArtistId=" + artistId + "&alertCategoryGroupName=" + categoryGroupName + "<c:if test="${not empty userAlert}">&alertId=${userAlert.id}</c:if>#editAlert";
	};

	function changeNullCategory(value) {	
		if(value) {
			$('#category').attr('disabled', true);
		} else {
			$('#category').removeAttr('disabled');
		}
		refreshCategoryDescription();
	};

	function refreshCategoryDescription() {
		if($('#nullCategory').attr('checked')) {
			$('#categoryDescription').text('Match null/empty category');
		} else {
			var category = $('#category').val();
			var categoryGroupName = $('[name=alertCategoryGroupName]').val();
			
			if(category == "") {
				$('#categoryDescription').html('Match <b>any</b> zones');
			} else {
				
				$('#categoryDescription').html('Match category <b>' + category + '</b> ');
				//$('#categoryDescription').html('Match category <b>' + category + '</b> of <b>' + categoryGroupName + '</b>');
			}	
		}
	};

	
	function refreshSectionDescription() {
		var sectionLow = $.trim($('#sectionLow').val()).toUpperCase();
		var sectionHigh = $.trim($('#sectionHigh').val()).toUpperCase();
		$('#sectionLow').val(sectionLow);
		$('#sectionHigh').val(sectionHigh);
		
		if(sectionLow == "") {
			if(sectionHigh == "") {
				$('#sectionDescription').html('Match <b>any</b> sections');
			} else {
				$('#sectionDescription').html('Match sections lower or equals than <b>' + sectionHigh + '</b>');
			}	
		} else {
			if(sectionHigh == "") {
				$('#sectionDescription').html('Match sections higher or equals than <b>' + sectionLow + '</b>');
			} else {
				if (sectionLow == sectionHigh) {
					$('#sectionDescription').html('Match sections equals to <b>' + sectionLow + '</b>');
				} else {
					$('#sectionDescription').html('Match sections higher or equals than <b>' + sectionLow + '</b>' + ' and lower or equals than <b>' + sectionHigh + '</b>');
				}
			}
		}
	};

	function refreshRowDescription() {
		var rowLow = $.trim($('#rowLow').val()).toUpperCase();
		var rowHigh = $.trim($('#rowHigh').val()).toUpperCase();
		$('#rowLow').val(rowLow);
		$('#rowHigh').val(rowHigh);
					
		$('#rowDescription').css('color', '#448844');			
		
		if(rowLow == "") {
			if(rowHigh == "") {
				$('#rowDescription').html('Match <b>any</b> rows');
			} else {
				$('#rowDescription').html('Match rows lower or equals than <b>' + rowHigh + '</b>');
			}	
		} else {
			if(rowHigh == "") {
				$('#rowDescription').html('Match rows higher or equals than <b>' + rowLow + '</b>');
			} else {
				if (rowLow == rowHigh) {
					$('#rowDescription').html('Match rows equals or equals to <b>' + rowLow + '</b>');
				} else {
					$('#rowDescription').html('Match rows higher or equals than <b>' + rowLow + '</b>' + ' and lower or equals than <b>' + rowHigh + '</b>');
				}
			}
		}
	};

	function refreshPriceDescription() {
		var priceLow = $.trim($('#priceLow').val());
		if (isNaN(priceLow)) {
			$('#priceDescription').html('Invalid lower price');
			$('#priceDescription').css('color', 'red');
			return false;
		}

		var priceHigh = $.trim($('#priceHigh').val());
		if (isNaN(priceHigh)) {
			$('#priceDescription').html('Invalid higher price');
			$('#priceDescription').css('color', 'red');
			return false;
		}
				
		if (priceLow != "" && priceHigh != "" && priceLow*1 > priceHigh*1) {
			$('#priceDescription').html('Lower price cannot be greater than the upper price');
			$('#priceDescription').css('color', 'red');
			return false;
		}

		$('#priceDescription').css('color', '#448844');

		if(priceLow == "") {
			if(priceHigh == "") {
				$('#priceDescription').html('Match <b>any</b> prices');
			} else {
				$('#priceDescription').html('Match price lower or equals than <b>$' + priceHigh + '</b>');
			}	
		} else {
			if(priceHigh == "") {
				$('#priceDescription').html('Match price higher or equals than <b>$' + priceLow + '</b>');
			} else {
				if (priceLow == priceHigh) {
					$('#priceDescription').html('Match price equals to <b>$' + priceLow + '</b>');
				} else { 
					$('#priceDescription').html('Match price higher or equals than <b>$' + priceLow + '</b>' + ' and lower or equals than <b>$' + priceHigh + '</b>');
				}
			}
		}
		return true;
	};

	function refreshQuantitiesDescription() {
		var quantities = $.trim($('#quantities').val());
		
		var quantityList = [];
		
		if (quantities == "") {
			$('#quantitiesDescription').html('Match any quantities');
			$('#quantitiesDescription').css('color', '#448844');
			return true;
		}
		
		
		var quantityMap = {};
		
		var quantityTokens = quantities.split(",");
		for(var i = 0 ; i < quantityTokens.length ; i++) {
			var quantityToken = $.trim(quantityTokens[i]);
			if (quantityToken != 'odd' && quantityToken != 'even'
					&& !quantityToken.match("^[0-9]+[+]$")
					&& !quantityToken.match("^[0-9]+$")
					&& !quantityToken.match("^[0-9]+-[0-9]+$")
					) {
				$('#quantityDescription').css('color', 'red');
				$('#quantitiesDescription').html('Invalid quantities');
				return false;
			}
						
			if (quantityMap[quantityToken] != 1) {
				quantityMap[quantityToken] = 1;
				quantityList.push(quantityToken);
			}
		}

		var quantityDescription = "Match quantities ";
		var quantityVal = "";
		
		quantityList.sort(function(a,b) {
			a = a.match("[0-9]*")[0]*1;
			b = b.match("[0-9]*")[0]*1;
			
			if (a < b) {return -1;}
			else if (a > b) {return 1;}
			else return 0;
		});

		for(var i = 0 ; i < quantityList.length ; i++) {
			if (i > 0) {
				if (i == quantityList.length - 1) {
					quantityDescription += " or ";
				} else {
					quantityDescription += ", ";
				}
				quantityVal += ",";
			}
			quantityDescription += quantityList[i];
			quantityVal += quantityList[i];
		}
					
		$('#quantities').val(quantityVal);
		
		$('#quantityDescription').css('color', '#448844');
		$('#quantitiesDescription').html(quantityDescription);		
		return true;
	};
	
	
	
	
	function previewAlert() {
		if($('[name=alertEventId]').val() == "") {
			alert("No events defined.");
			return;
		}
		$('#previewAlertDiv').html('<img src="../images/process-running.gif" align="absbottom"/><font color="green" />RUNNING</font>');
		$.post("${alertPreviewUrl}", {
			
				alertEventId: $('[name=alertEventId]').val(),
				alertNullCategory : $([name='alertnullCategory']).attr('checked')?"on":"",
				alertCategoryGroupName : $('[name=alertCategoryGroupName]').val(),
				alertCategory : $('[name=alertCategory]').val(),
				alertPriceLow : $('#priceLow').val(),
				alertPriceHigh : $('#priceHigh').val(),
				alertSectionLow : $('#sectionLow').val(),
				alertSectionHigh : $('#sectionHigh').val(),
				alertQuantities : $('#quantities').val(),
				alertRowLow : $('#rowLow').val(),
				alertRowHigh : $('#rowHigh').val(),
				alertType : $('INPUT[name=alertType]:checked').val(),
				alertDescription : $('TEXTAREA[name=alertDescription]').val(),
				<c:if test="${not empty userAlert}">alertFor : '${userAlert.alertFor}',</c:if>
				<c:forEach var="sid" items="${constants.siteIds}">
					'site-${sid}': $('[name=site-${sid}]').attr('checked')?"on":"",				
				</c:forEach>
				timestamp: new Date().getTime()
			}, function(data) {
				$('#previewAlertDiv').html(data);
			}
		);	
	};
	
	$('#eventSearchTextField').autocomplete("AutoCompleteSearch", {
		width: 550,
		max: 1000,
		minChars: 2,		
		extraParams: {t:'artist'},
		formatItem: function(row, i, max) {
			/*if(null != row[3] && undefined != row[3] && row[3] != ""){
				tourDate += "- <font color='#943400'> "+ formatDate(new Date(row[3]*1));
			}
			if(null != row[4] && undefined != row[4] && row[4] != ""){
				tourDate += " "+ formatShortDate(new Date(row[4]*1));
			}*/
			
			return "<div  class='searchEventTag'>ARTIST</div>" + row[1]  + "</font>";
		},
		formatResult: function(row) {
			return row[1];
		},
		startSearch: function() {
			$('#eventSearchTextFieldStatus').empty();
			$('#eventSearchTextFieldStatus').append($.IMG({src: '../images/process-running.gif', align: 'absbottom'}));
		},
		stopSearch: function(rows) {
			$('#goToTextFieldStatus').empty();
			if (rows.length == 0) {
				$('#eventSearchTextFieldStatus').text("No results");
			} else {
				$('#eventSearchTextFieldStatus').text("Found " + rows.length + " results");
			}
		}
	});
	
	$('#eventSearchTextField').result(function(event, row, formatted) {
		cancelSelectEvent();  	
		var tourDate ="";
		//if(null != row[3] && undefined != row[3] && row[3] != ""){
		//	tourDate +=" - <font color='#943400'>"+ formatShortDate(new Date(row[3]*1));
		//}
  		$('#eventDescription').html(row[1] + tourDate + "</font>");
		document.location.href = "${pageUrl}?alertArtistId=" + row[2] + "<c:if test="${not empty userAlert}">&alertId=${userAlert.id}</c:if>#editAlert";
  	});	
	
	function selectEvent() {
		$('#eventSearch').show();
		$('#eventDescription').hide();	
		$('#selectEventLink').hide();
		$('#cancelSelectEventLink').show();
		$('#eventSearchTextField').show();
		$('#eventSearchTextFieldStatus').show();
		$('#eventSearchTextFieldStatus').html("");
		$('#eventSearchTextField').focus();
	};

	function cancelSelectEvent() {
		$('#eventSearch').hide();
		$('#eventDescription').show();	
		$('#selectEventLink').show();
		$('#eventSearchTextField').hide();
		$('#cancelSelectEventLink').hide();
		$('#eventSearchTextFieldStatus').hide();
	};
	
	function checkAllSites() {
		$('.site-checkbox').attr('checked', true);	
	};

	function uncheckAllSites() {
		$('.site-checkbox').attr('checked', false);	
	};
	 function selectAll() {
		    $.each($('.event_checkbox'), function(index, item) {
		      $(item).attr('checked', true);
		    });
		    callChooseEvent();
		  }
		  
		  function unselectAll() {
		    $.each($('.event_checkbox'), function(index, item) {
		      $(item).removeAttr('checked');
		    });
		    callChooseEvent();
		  }

		  function invertAll() {
		    $.each($('.event_checkbox'), function(index, item) {
		      if ($(item).is(':checked')) {
		        $(item).removeAttr('checked');
		      } else {
		        $(item).attr('checked', true);
		      }
		    });
		    callChooseEvent();
		  }
	function openVenueWindow() {

		var src = $('#venue-image').attr('src');
		window.open(src, "venueImage", "scrollbars=no,menubar=no,fullscreen=0,height=400,width=550,resizable=no,toolbar=no,location=no,status=no,zoominherit=0");
   };
	
	$('#container-tabs').tabs();
	
		
	changeNullCategory($('#nullCategory').attr('checked'));	
	refreshCategoryDescription();
	refreshSectionDescription();
	refreshRowDescription();
	refreshPriceDescription();
	refreshQuantitiesDescription();
	
	var eventSize = '${eventSize}';
	var defaultVenueCatId= "";
	
	function contains(obj) {
		var isVenueCatTrue=true;
		if(null != defaultVenueCatId && defaultVenueCatId != ""){
	        if (defaultVenueCatId == obj) {
	        }else{
			   isVenueCatTrue = false;
			}
		}else{
			defaultVenueCatId = obj;
		}
		return isVenueCatTrue;
	}
	
	function callChooseEvent(){
		var finalflag=true;
		var venueAndCatId ="";
		defaultVenueCatId= "";
		$.each($('.event_checkbox'), function(index, item) {
			  index = index+1;
			  venueAndCatId = document.getElementById('event_'+index).value;
			  if ($(item).is(':checked')) {				  
				  if(contains(venueAndCatId)){
				  }else{
					  finalflag = false;
				  }
			  }
	    });
		if(finalflag){
		  var splitTag =defaultVenueCatId.split("_");
		  if(null != splitTag[1] && splitTag[1] != ""){
			  $("#catRowId").show();
			  removeOptionsWithoutSelect();
			  getCategoryDetails(splitTag[1]);
		  }else{
			  removeOptions();
			  $("#catRowId").hide();
		  }
		}else{
			 removeOptions();
			 $("#catRowId").hide();
		}
	}
	
	
	function removeOptions(){
		$('#category')
	    .find('option')
	    .remove()
	    .end()
	    .append('<option value="" >Select</option><option value="ANY" >All Tickets</option><option value="ALL Zones" >Categorized</option><option value="NO ZONES" >Uncategorized</option>');
	}
	function removeOptionsWithoutSelect(){
		$('#category')
	    .find('option')
	    .remove()
	    .end()
	    .append('<option value="ANY" >All Tickets</option><option value="ALL Zones" >Categorized</option><option value="NO ZONES" >Uncategorized</option>');
	}
	function getCategoryDetails(venueCatId){
		var venueCatIds ={venueCatId: venueCatId};
		var isFirst=true;
		$.ajax({
			type: "POST",
			url: "GetCategoryDetails",
			dataType:"json",
			data: venueCatIds,
			success: function(data){
		        $.each(data, function(index, element) {
		        	if(isFirst){
		        		$('#venue-image').remove();
		        		var img = $('<img id="venue-image">'); 
                        img.attr('src', element.venueImgUrl);
                        img.attr('title','Venue Map');
						img.attr('width','32');
						img.attr('height','32');
						img.attr('align','middle');
						img.attr('style','margin-left:8px;border: 1px solid #aaa');
						img.appendTo('#venueReferenceMap');	
		        	}
		        	isFirst = false;
		        	$('<option>').val(element.symbol).text(element.symbol).appendTo('#category');
		        });
			}
	    });
	}
	
</script>