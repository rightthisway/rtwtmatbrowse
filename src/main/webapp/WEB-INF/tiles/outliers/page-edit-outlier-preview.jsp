<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<h2>Preview</h2>
<display:table class="list" name="${outlierTickets}" id="ticket" requestURI="MyOutliers">
    <display:column title="Rem. Qty" sortable="true" style="text-align:right">
    	<span tooltip="Lot Size: ${ticket.lotSize}">${ticket.remainingQuantity}</span>
    </display:column>
	<display:column title="Zone" sortable="true" property="category.symbol" />
    <display:column title="Section" property="section" sortable="true" />
    <display:column title="Row" sortable="true" property="row" />
    <display:column title="Wholesale Price" sortable="true" property="adjustedCurrentPrice" format="{0,number,currency}" style="text-align:right" />
   
	<display:column title="Online Price" sortable="true" style="text-align:right">
        <c:if test="${ticket.buyItNowPrice > 0}">
          <fmt:formatNumber value="${ticket.buyItNowPrice}" type="currency" minFractionDigits="2"/>
        </c:if>
        <c:set var="timeLeft">
			<c:choose>
				<c:when test="${empty ticket.numSecondsBeforeEndDate}">
				</c:when>
				<c:when test="${ticket.numSecondsBeforeEndDate < 0}">
					(expired)
				</c:when>
				<c:when test="${ticket.numSecondsBeforeEndDate > 24 * 3600 * 1000}">
					Ending in <fmt:formatNumber type="number" maxFractionDigits="0" value="${(ticket.numSecondsBeforeEndDate / (24 * 3600 * 1000))}"/> days
				</c:when>
				<c:when test="${ticket.numSecondsBeforeEndDate > 3600 * 1000}">
					Ending in <fmt:formatNumber type="number" maxFractionDigits="0" value="${((ticket.numSecondsBeforeEndDate % (24 * 3600 * 1000)) / (3600 * 1000))}"/> hours
				</c:when>
				<c:otherwise>
					Ending in <fmt:formatNumber type="number" maxFractionDigits="0" value="${((ticket.numSecondsBeforeEndDate % (3600 * 1000)) / (60 * 1000))}"/> minutes
				</c:otherwise>
			</c:choose>	          
        </c:set>
		<c:choose>
			<c:when test="${ticket.ticketType == 'AUCTION'}">
				<span tooltip="${timeLeft}">
					<c:choose>
						<c:when test="${ticket.numSecondsBeforeEndDate > 3600 * 24}">
							<img src="../images/ico-auction.gif" align="absbottom"/>
						</c:when>
						<c:otherwise>
							<img src="../images/ico-auction-ending.gif" align="absbottom"/>
						</c:otherwise>
					</c:choose>
				</span>
			</c:when>
		</c:choose>
	</display:column>
	
	<display:column media="html" title="Seller" sortProperty="seller" sortable="true" >
		<span style="white-space:nowrap">
			<a href="RedirectToItemPage?id=${ticket.id}" tooltip="${ticket.siteId} - ${ticket.itemId}"><img src="../images/ico-${ticket.siteId}.gif" border="0" align="absbottom"/>
				(Buy Ticket)
		    </a>
			<c:if test="${ticket.ticketType != 'AUCTION' and not empty ticket.numSecondsBeforeEndDate}">
				<span tooltip="${timeLeft}">
					<c:choose>
						<c:when test="${ticket.numSecondsBeforeEndDate > 3600 * 24}">
							<img src="../images/ico-clock.gif" align="absbottom"/>
						</c:when>
						<c:otherwise>
							<img src="../images/ico-clock-ending.gif" align="absbottom"/>
						</c:otherwise>
					</c:choose>
				</span>
			</c:if>
	
			<c:choose>
				<c:when test="${ticket.ticketDeliveryType == 'EDELIVERY'}">
					<img src="../images/ico-edelivery.gif" title="eDelivery" align="absbottom" />
				</c:when>
				<c:when test="${ticket.ticketDeliveryType == 'INSTANT'}">
					<img src="../images/ico-instant-edelivery.gif" title="Instant eDelivery" align="absbottom" />
				</c:when>
			</c:choose>			    	
			${ticket.seller}
		</span>
	</display:column>
</display:table>