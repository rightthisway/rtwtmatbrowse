<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<tiles:useAttribute id="selectedSubMenu" name="selectedSubMenu" scope="request" />

<div id="userSubMenu">
	<c:choose>
		<c:when  test="${selectedSubMenu == 'Manage Outliers'}">
			<b>Manage Outliers</b>
		</c:when>
		<c:otherwise>
			<a href="./">Manage Outliers</a>
		</c:otherwise>
	</c:choose>
	
</div>

<tiles:insertAttribute name="subBody" />