<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<c:choose>
	<c:when test="${mode == 'editAll'}">
		<c:set var="pageUrl" value="EditorEditOutlier" />
		<c:set var="manageOutliersUrl" value="EditorManageOutliers" />
		<c:set var="outlierPreviewUrl" value="EditorOutlierPreview" />
	</c:when>
	<c:otherwise>
		<c:set var="pageUrl" value="EditMyOutlier" />
		<c:set var="manageOutliersUrl" value="MyOutliers" />
		<c:set var="outlierPreviewUrl" value="MyOutlierPreview" />
	</c:otherwise>
</c:choose>

<c:choose>
	<c:when test="${not empty userOutlier}">
		<h1>Edit Outlier ${userOutlier.id}</h1>
	</c:when>
	<c:otherwise>
		<h2>Add Outlier</h2>
	</c:otherwise>
</c:choose>

<div id="container-tabs" style="margin-top: 5px">
	<ul>
		<li><a href="#fragment-outlier-parameters"><span>Parameters</span></a></li>
		<c:if test="${not empty userOutlier}">
			<li><a href="#fragment-outlier-info"><span>Info</span></a></li>
		</c:if>
	</ul>

	<div id="fragment-outlier-parameters">
		<b>Outlier me at <font color="blue">${user.email}</font> when
			there are tickets for the condition:
		</b> <br />
		<br />
		<form id="updateOutlierForm" action="${pageUrl}" method="POST">
			<input type="hidden" name="action" value="save" />
			<c:if test="${not empty userOutlier}">
				<input type="hidden" name="outlierId" value="${userOutlier.id}" />
				<input type="hidden" name="outlierFor"
					value="${userOutlier.outlierFor}" />
			</c:if>

			<table>
				<colgroup>
					<col width="120" />
					<col width="*" />
				</colgroup>
				<tr>
					<c:if test="${not empty userOutlier}">
						<td>Ticket in Event:</td>
					</c:if>
					<c:if test="${empty userOutlier}">
						<td>Ticket in Artist:</td>
					</c:if>
					<td><c:choose>
							<c:when test="${not empty userOutlier}">
								<a href="BrowseTickets?eventId=${userOutlier.event.id}">${userOutlier.event.name}&nbsp;${userOutlier.event.formattedEventDate}&nbsp;${userOutlier.event.formattedVenueDescription}</a>
								<input type="hidden" name="outlierEventId" id="outlierEventId"
									value="${userOutlier.eventId}" />
							</c:when>
							<c:otherwise>
								<span id="eventDescription"> <c:choose>
										<c:when test="${empty artist}">
										No artist
										<input type="hidden" name="outlierArtistId" id="outlierArtistId"
												value="" />
										</c:when>
										<c:otherwise>
										${artist.name}
										<input type="hidden" name="outlierArtistId" id="outlierArtistId"
												value="${artist.id}" />
										</c:otherwise>
									</c:choose>
								</span>
								<span id="eventSearch" style="display: none"> Search <input
									type="text" id="eventSearchTextField" />
								</span>

								<a id="selectEventLink" href="javascript:selectEvent()">Select
									an artist</a>
								<a id="cancelSelectEventLink"
									href="javascript:cancelSelectEvent()" style="display: none">Cancel</a>

								<span id="eventSearchTextFieldStatus"></span>
							</c:otherwise>
						</c:choose></td>
				</tr>

				<c:if test="${not empty userOutlier and null ne userOutlier}">
					<tr>
						<td>Percentage(%) :</td>
						<td><input type="text" id="outlierPercentage"
							name="outlierPercentage" value="${userOutlier.outlierPercentage}" />
						</td>
					</tr>
				</c:if>


				<c:if test="${not empty events and events ne null}">
					<tr>
						<td colspan='2'><br /></td>
					</tr>
				</c:if>




				<tr
					<c:if test="${not empty userOutlier and userOutlier.eventId ne null}">style="display: none;"</c:if>>
					<td></td>
					<td><c:if test="${not empty events}">
				Select:
				<a href="#" onclick="selectAll(); return false">All</a> |
				<a href="#" onclick="unselectAll(); return false">None</a> |
				<a href="#" onclick="invertAll(); return false">Invert</a>

						</c:if></td>
				</tr>
				<tr
					<c:if test="${not empty userOutlier and userOutlier.eventId ne null}">style="display: none;"</c:if>>
					<td>Events</td>
					<td><input type="hidden" id="eventSize" name="eventSize"
						value="${eventSize}"> <display:table class="list"
							name="${events}" id="event" requestURI="BrowseEvents">
							<display:column>
								<input type="checkbox" id="event_${event_rowNum}"
									name="event_${event.id}"
									value="${event.venueId}_${event.venueCategoryId}"
									class="event_checkbox" onclick="callChooseEvent();" />
							</display:column>
							<display:column style="width:20px">
								<div id="star_${event.id}" class="starOff"
									onclick="toggleBookmarkEvent(${event.id})"></div>
							</display:column>
							<c:choose>
								<c:when test="${browseEventsPage == 'BrowseEventsAnalytic'}">
									<display:column>
										<b> <a
											href="BrowseTicketsAnalytic?eventId=${event.id}&view=analytic">View</a>
										</b>
									</display:column>
								</c:when>
								<c:otherwise>
									<display:column value="View" href="BrowseTickets"
										paramId="eventId" paramProperty="id" class="bold" />
								</c:otherwise>
							</c:choose>
							<display:column title="Date" sortable="true">
								<b> <fmt:formatDate pattern="MM/dd/yyyy"
										value="${event.date}" /> <c:choose>
										<c:when test="${not empty event.time}">
											<fmt:formatDate pattern="HH:mm" value="${event.time}" />
										</c:when>
										<c:otherwise>
			    TBD
			  </c:otherwise>
									</c:choose>
								</b>
							</display:column>
							<display:column title="Event Name" sortable="true"
								sortProperty="name">
								<img src="../images/ico-${fn:toLowerCase(event.eventType)}.gif"
									align="absbottom" title="${event.eventType}" /> ${event.name}
    </display:column>
							<display:column title="Venue" sortable="true"
								property="venue.building"></display:column>
							<display:column title="State" sortable="true"
								property="venue.state"></display:column>
							<display:column title="Country">
								<c:if
									test="${not empty event.venue.country && event.venue.country != 'null' }">
									<img src="../images/flags/${event.venue.country}.gif" />${event.venue.country}
    	</c:if>
							</display:column>

							<c:choose>
								<c:when test="${browseEventsPage == 'BrowseEventsAnalytic'}">
									<display:column title="OPHigh">
        	${eventToActions[event.id]['OH']}
        </display:column>
									<display:column title="OPMed">
        	${eventToActions[event.id]['OM']}
        </display:column>
									<display:column title="OPLow">
        	${eventToActions[event.id]['OL']}
        </display:column>
									<display:column title="Correct">
        	${eventToActions[event.id]['CO']}
        </display:column>
									<display:column title="UPLow">
        	${eventToActions[event.id]['UL']}
        </display:column>
									<display:column title="UPMed">
        	${eventToActions[event.id]['UM']}
        </display:column>
									<display:column title="UPHigh">
        	${eventToActions[event.id]['UH']}
        </display:column>
								</c:when>
							</c:choose>
							<display:column title="Managed By" sortable="true"
								sortProperty="marketMakerManager" class="text-align-center">
								<c:if test="${event.marketMakerManager != 'DESK'}">
									<img src="../images/ico-user.gif" align="absbottom" />
								</c:if>
      ${event.marketMakerManager}
    </display:column>

							<display:column title="CATS Count" class="text-align-center">
								<a href="javascript: return null;"
									onclick="$('#displaycats${event.id}').dialog('open');">${fn:length(event.category)}</a>
							</display:column>

							<display:column title="CATS Group" class="text-align-center">
								<a href="javascript: return null;"
									onclick="$('#displaycats${event.id}').dialog('open');">
									${event.uniqueCategoryGroupName} </a>
							</display:column>
							<display:column title="<input type='checkbox' id='copyAll' name='copyAll' onclick='copy()'><br/>%">
							
								<!-- % -->
								<input type="text" id="outlierPerc_${event_rowNum}"	name="outlierPerc_${event.id}" />
								
							</display:column>
						</display:table></td>
				</tr>


				<tr>
					<td>Outlier Enable/Disable:</td>
					<td><c:set var="userOutlierStatus" value="ACTIVE" /> <c:if
							test="${not empty userOutlier}">
							<c:set var="userOutlierType"
								value="${userOutlier.userOutlierStatus}" />
						</c:if> <input id="enableOutlierStatus" name="outlierStatus" type="radio"
						value="ACTIVE"
						<c:if test="${userOutlierStatus=='ACTIVE'}">checked</c:if> /><label
						for="enableOutlierStatus">Enable Outlier</label><br /> <input
						id="disableOutlierStatus" name="outlierStatus" type="radio"
						value="DISABLED"
						<c:if test="${userOutlierStatus=='DISABLED'}">checked</c:if> /><label
						for="disableOutlierStatus">Disable Outlier</label><br /> <br />
					</td>
				</tr>

				<tr>
					<td>Description:</td>
					<td><textarea name="outlierDescription" type="text"
							style="width: 400px; height: 60px;">${userOutlier.description}</textarea>
					</td>
				</tr>
				<tr>
					<td>in these exchanges</td>
					<td><a href="javascript:void(0)" onclick="checkAllSites()"
						style="font-size: 10px;">Select all</a> - <a
						href="javascript:void(0)" onclick="uncheckAllSites()"
						style="font-size: 10px;">Select none</a>
						<div style="float: left;">
							<c:forEach var="sid" items="${constants.siteIds}">
								<c:set var="safeSid" value="[${sid}]" />
								<div style="float: left; width: 160px; height: 24px;">
									<input type="checkbox" name="site-${sid}" class="site-checkbox"
										<c:if test="${siteSelected[sid] and not fn:contains(constants.disabledSiteIdsString, safeSid)}">checked</c:if>
										<c:if test="${fn:contains(constants.disabledSiteIdsString, safeSid)}">disabled</c:if>>
									<label for="${sid}_checkbox"> <img
										src="../images/ico-${sid}.gif" border="0" align="absmiddle" />
										<c:choose>
											<c:when
												test="${fn:contains(constants.disabledSiteIdsString, safeSid)}">
												<span style="color: #888888; text-decoration: line-through"
													tooltip="${sid} is disabled"> ${sid} </span>
											</c:when>
											<c:otherwise>
											${sid}
										</c:otherwise>
										</c:choose>
									</label>
								</div>
							</c:forEach>
						</div></td>
				</tr>



			</table>

			<c:choose>
				<c:when
					test="${not empty userOutlier && userOutlier.eventId ne null && userOutlier.eventId ne ''}">
					<a href="javascript:void(0)" onclick="previewOutlier()">Preview
						Outlier</a>
					<br />
					<br />
					<div id="previewOutlierDiv"></div>
					<br />
				</c:when>
			</c:choose>

			<c:choose>
				<c:when test="${empty userOutlier}">
					<input id="addOutlier" type="button" class="medButton"
						onclick="submitOutlierForm()" value="Add" />
				</c:when>
				<c:otherwise>
					<input id="addOutlier" type="button" class="medButton"
						onclick="submitOutlierForm()" value="Update" />
				</c:otherwise>
			</c:choose>
			<input id="addOutlier" type="button" class="medButton"
				onclick="document.location.href='${manageOutliersUrl}'"
				value="Cancel" />
		</form>
	</div>

	<c:if test="${not empty userOutlier}">
		<div id="fragment-outlier-info">
			<table>
				<tr>
					<td>Creation Date:</td>
					<td><fmt:formatDate value="${userOutlier.creationDate}"
							pattern="yyyy/MM/dd HH:mm:ss" /></td>
				</tr>
				<tr>
					<td>Market Maker:</td>
					<td>${userOutlier.marketMakerUsername}</td>
				</tr>
				<tr>
					<td>Last Email Sent:</td>
					<td><fmt:formatDate value="${userOutlier.lastEmailDate}"
							pattern="yyyy/MM/dd HH:mm:ss" /></td>
				</tr>

			</table>


		</div>
	</c:if>
	<br />

	<script type="text/javascript">



function submitOutlierForm() {
	
	var isNone=false;

	if($('[name=outlierArtistId]').val() == ""){

		alert("Please Choose the artist.");
		isNone=true;
		return false;

	}else if($('[name=outlierEventId]').val() == "") {

		alert("No events defined.");
		isNone=true;
		return false;
	}
	
	
	
	if(!isNone){
		if(!chooseEventAndExchange()){
			isNone=true;
			return false;
		}

	}
	if($('[name=outlierId]').val() !=null && $('[name=outlierId]').val() != ""){
		
		if($('[name=outlierPercentage]').val() ==null || $('[name=outlierPercentage]').val() == ""){
			
			isNone=true;
			alert("Please enter percentage(%).");
			return false;
		}else{
			
			var outLierPer =$('[name=outlierPercentage]').val();
			if(isNaN(outLierPer)){
				isNone=true;
				alert("Please enter valid percentage(%).");
				return false;
			}
		}
		
	}
	
	if(!isNone){
		
		$.each($('.event_checkbox'), function(index, item) {
			index = index+1;
			  if ($(item).is(':checked')) {
				 var outPerc = document.getElementById('outlierPerc_'+index).value;
				 if(null == outPerc || outPerc ==""){
					 isNone=true;
				 }
			  }
	    });
		if(isNone){
			alert("Please enter percentage(%) for selected events.");
			return false;
		}else{
			$.each($('.event_checkbox'), function(index, item) {
				index = index+1;
				  if ($(item).is(':checked')) {
					 var outPerc = document.getElementById('outlierPerc_'+index).value;
					 if(isNaN(outPerc)){
						 isNone=true;
					 }
				  }
		    });
			if(isNone){
				alert("Please enter valid percentage(%) for selected events.");
				return false;
			}
		}
 	}
	if(!isNone){
		$('#updateOutlierForm').submit();
	}
};

function getOutlierPerc(){
	
} 

function chooseEventAndExchange(){
	var booleanVal = false;
	$.each($('.site-checkbox'), function(index, item) {
		      if ($(item).is(':checked')) {
				booleanVal = true;
		      }
	});	
	if(!booleanVal){
			alert("Please Choose atleast one exchange to proceed.");
			return false;
	}else{
		
		if(null != $('[name=outlierEventId]') && $('[name=outlierEventId]').val() != "" && $('[name=outlierEventId]').val() != undefined){
			booleanVal = true;
		}else{
			booleanVal = false;
			$.each($('.event_checkbox'), function(index, item) {
					  if ($(item).is(':checked')) {
						booleanVal = true;
					  }
			});
			if(!booleanVal){
				alert("Please Choose atleast one event to proceed.");
				return false;
			}
		}
	}
	return booleanVal;
}



	
	function previewOutlier() {
		if($('[name=outlierEventId]').val() == "") {
			alert("No events defined.");
			return;
		}
		$('#previewOutlierDiv').html('<img src="../images/process-running.gif" align="absbottom"/><font color="green" />RUNNING</font>');
		$.post("${outlierPreviewUrl}", {
			
				outlierEventId: $('[name=outlierEventId]').val(),
				outlierPercentage : $('#outlierPercentage').val(),
				outlierDescription : $('TEXTAREA[name=outlierDescription]').val(),
				<c:if test="${not empty userOutlier}">outlierFor : '${userOutlier.outlierFor}',</c:if>
				<c:forEach var="sid" items="${constants.siteIds}">
					'site-${sid}': $('[name=site-${sid}]').attr('checked')?"on":"",				
				</c:forEach>
				timestamp: new Date().getTime()
			}, function(data) {
				$('#previewOutlierDiv').html(data);
			}
		);	
	};
	
	$('#eventSearchTextField').autocomplete("AutoCompleteSearch", {
		width: 550,
		max: 1000,
		minChars: 2,		
		extraParams: {t:'artist'},
		formatItem: function(row, i, max) {
			
			/* var tourDate ="";
			if(null != row[3] && undefined != row[3] && row[3] != ""){
				tourDate += "- <font color='#943400'> "+ formatDate(new Date(row[3]*1));
			}
			if(null != row[4] && undefined != row[4] && row[4] != ""){
				tourDate += " "+ formatShortDate(new Date(row[4]*1));
			} */
			
			return "<div  class='searchEventTag'>ARTIST</div>" + row[1] + "</font>";
		},
		formatResult: function(row) {
			return row[1];
		},
		startSearch: function() {
			$('#eventSearchTextFieldStatus').empty();
			$('#eventSearchTextFieldStatus').append($.IMG({src: '../images/process-running.gif', align: 'absbottom'}));
		},
		stopSearch: function(rows) {
			$('#goToTextFieldStatus').empty();
			if (rows.length == 0) {
				$('#eventSearchTextFieldStatus').text("No results");
			} else {
				$('#eventSearchTextFieldStatus').text("Found " + rows.length + " results");
			}
		}
	});
	
	$('#eventSearchTextField').result(function(event, row, formatted) {
		cancelSelectEvent();  	
		/* var tourDate ="";
		if(null != row[3] && undefined != row[3] && row[3] != ""){
			tourDate +=" - <font color='#943400'>"+ formatShortDate(new Date(row[3]*1));
		} */
  		$('#eventDescription').html(row[1] + "</font>");
  		document.location.href = "${pageUrl}?outlierArtistId=" + row[2] + "<c:if test="${not empty userOutlier}">&outlierId=${userOutlier.id}</c:if>#editOutlier";
  	});	
	
	function selectEvent() {
		$('#eventSearch').show();
		$('#eventDescription').hide();	
		$('#selectEventLink').hide();
		$('#cancelSelectEventLink').show();
		$('#eventSearchTextField').show();
		$('#eventSearchTextFieldStatus').show();
		$('#eventSearchTextFieldStatus').html("");
		$('#eventSearchTextField').focus();
	};

	function cancelSelectEvent() {
		$('#eventSearch').hide();
		$('#eventDescription').show();	
		$('#selectEventLink').show();
		$('#eventSearchTextField').hide();
		$('#cancelSelectEventLink').hide();
		$('#eventSearchTextFieldStatus').hide();
	};
	
	function checkAllSites() {
		$('.site-checkbox').attr('checked', true);	
	};

	function uncheckAllSites() {
		$('.site-checkbox').attr('checked', false);	
	};
	 function selectAll() {
		    $.each($('.event_checkbox'), function(index, item) {
		      $(item).attr('checked', true);
		    });
		    callChooseEvent();
		  }
		  
		  function unselectAll() {
		    $.each($('.event_checkbox'), function(index, item) {
		      $(item).removeAttr('checked');
		    });
		    callChooseEvent();
		  }

		  function invertAll() {
		    $.each($('.event_checkbox'), function(index, item) {
		      if ($(item).is(':checked')) {
		        $(item).removeAttr('checked');
		      } else {
		        $(item).attr('checked', true);
		      }
		    });
		    callChooseEvent();
		  }
	function openVenueWindow() {

		var src = $('#venue-image').attr('src');
		window.open(src, "venueImage", "scrollbars=no,menubar=no,fullscreen=0,height=400,width=550,resizable=no,toolbar=no,location=no,status=no,zoominherit=0");
   };
	
	$('#container-tabs').tabs();

	var eventSize = '${eventSize}';
	
	function callChooseEvent(){
		
		$.each($('.event_checkbox'), function(index, item) {
			index = index+1;
			   document.getElementById('outlierPerc_'+index).readOnly=true;
			  if ($(item).is(':checked')) {	
				  document.getElementById('outlierPerc_'+index).readOnly=false;
			   }else{
                  document.getElementById('outlierPerc_'+index).value="";
			   }
	    });
	}
	//TMAT-316
	function copy()
	{
		if(copyAll.checked==true && event_1.checked==false)	
		{		
			alert("Please Enter Outlier Percentage");
			$(copyAll).removeAttr('checked' , true);
			$(event_1).attr('checked', true);
			callChooseEvent();
		}
		if(event_1.checked==true && copyAll.checked==true)
			{
			for(var i=2;i<=eventSize;i++)
			    $('#outlierPerc_'+i).val($('#outlierPerc_1').val());
			selectAll();
			}
		if(copyAll.checked==false)
		{
			for(var i=2;i<=eventSize;i++)
			$('#event_'+i).removeAttr('checked', true);
		    callChooseEvent();
		}
	}
	callChooseEvent();
</script>