<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<c:choose>
	<c:when test="${mode == 'editAll'}">
		<div id="breadCrumbPath" class="breadCrumbPathUser">
		  <a href="..">Home</a> 
		  &gt; Manage Outliers 
		</div>
		
		<c:if test="${not empty param.info}">
			<div class="info"><div class="infoText">${param.info}</div></div>
		</c:if>
		
		<c:if test="${not empty param.error}">
			<div class="error"><div class="errorText">${param.error}</div></div>
		</c:if>

		
		<h2>Manage User Outliers</h2>

		<c:set var="outlierForList" value="TMAT,CIRCLES,TOTAL" />
		<c:set var="outlierStatusList" value="ACTIVE,DISABLED,EXPIRED,DELETED,TOTAL" />

		<img id="show-stats-image" src="../images/ico-expand.gif">
			<a id="show-stats-link" href="javascript:void(0)" onclick="toggleOutlierStatsTable()">Show stats</a>
		<br />
		
		<table id="outlier-stats-table" class="list" style="display:none">
			<thead>
				<tr>
					<th></th>
					<c:forTokens var="outlierStatus" items="${outlierStatusList}" delims=",">
						<th width="100">${outlierStatus}</th>
					</c:forTokens>
				</tr>
			</thead>
			<tbody>
				<c:forTokens var="OutlierFor" items="${outlierForList}" delims=",">
					<tr>
						<th>
							<c:choose>
								<c:when test="${OutlierFor == 'TMAT'}">Wholesale</c:when>
								<c:when test="${OutlierFor == 'CIRCLES_PORTAL'}">Retail</c:when>
								<c:otherwise>${OutlierFor}</c:otherwise>
							</c:choose>
						</th>
						<c:forTokens var="outlierStatus" items="${outlierStatusList}" delims=",">
							<c:set var="statKey" value="${outlierStatus}-${OutlierFor}" />
							<td align="right" width="100">	
								<c:choose>
									<c:when test="${empty statByKey[statKey]}">-</c:when>
									<c:otherwise>${statByKey[statKey]}</c:otherwise>
								</c:choose>
							</td>
							
						</c:forTokens>
					</tr>
				</c:forTokens>
			</tbody>
		</table>		
		<br />
		
		<c:set var="pageUrl" value="EditorManageOutliers" />
		<c:set var="editUrl" value="EditorEditOutlier" />
		
	</c:when>
	<c:otherwise>
		<div id="breadCrumbPath" class="breadCrumbPathUser">
		  <a href="..">Home</a> 
		  &gt; Manage Outliers 
		</div>
		
		<c:if test="${not empty param.info}">
			<div class="info"><div class="infoText">${param.info}</div></div>
		</c:if>
		
		<c:if test="${not empty param.error}">
			<div class="error"><div class="errorText">${param.error}</div></div>
		</c:if>

		

		<h2>Manage My Outliers</h2>

		<c:set var="pageUrl" value="MyOutliers" />
		<c:set var="editUrl" value="EditMyOutlier" />

	</c:otherwise>
</c:choose>



Filter by Artist :
<select onchange="selectFilterArtist(this.value)">
	<option value="">All</option>
	<c:forEach var="artist" items="${artists}">
		<option value="${artist.id}" <c:if test="${artist.id == filterArtistId}">selected</c:if> >${artist.name}</option>
	</c:forEach>
</select>
<br/>
Filter by Event:
<select onchange="selectFilterEvent(this.value)">
	<option value="">All</option>
	<c:forEach var="event" items="${events}">
		<option value="${event.id}" <c:if test="${event.id == filterEventId}">selected</c:if> >${event.name} - ${event.formattedEventDate}</option>
	</c:forEach>
</select>

<c:if test="${mode == 'editAll'}">
	by username
	<select onchange="selectFilterUsername(this.value)">
		<option value="">All</option>
		<c:forEach var="user" items="${users}">
			<option value="${user.username}" <c:if test="${user.username == filterUsername}">selected</c:if> >${user.username}</option>
		</c:forEach>
	</select>
</c:if>


<br /><br />
<b>View: </b>
<c:choose>
	<c:when test="${view == 'created'}"><b>Created outliers</b></c:when>
	<c:otherwise><a href="${pageUrl}?view=created">Created outliers</a></c:otherwise>
</c:choose>
-
<c:choose>
	<c:when test="${view == 'marketmaker'}"><b>Market maker assigned outliers</b></c:when>
	<c:otherwise><a href="${pageUrl}?view=marketmaker">Market maker assigned outliers</a></c:otherwise>
</c:choose>
-
<c:choose>
	<c:when test="${empty view}"><b>Both</b></c:when>
	<c:otherwise><a href="${pageUrl}?view=">Both</a></c:otherwise>
</c:choose>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

<b>Show only outliers For: </b>
<c:choose>
	<c:when test="${OutlierFor == 'TMAT'}"><b>Wholesale</b></c:when>
	<c:otherwise><a href="${pageUrl}?OutlierFor=TMAT">Wholesale</a></c:otherwise>
</c:choose>
<%-- 
|
<c:choose>
	<c:when test="${OutlierFor == 'CIRCLES_PORTAL'}"><b>Retail</b></c:when>
	<c:otherwise><a href="${pageUrl}?OutlierFor=CIRCLES_PORTAL">Retail</a></c:otherwise>
</c:choose>
|
<c:choose>
	<c:when test="${empty OutlierFor}"><b>Both</b></c:when>
	<c:otherwise><a href="${pageUrl}?OutlierFor=">Both</a></c:otherwise>
</c:choose>
 --%>
<br />
<input id="includeDisabledOutliers" type="checkbox" <c:if test="${includeDisabledOutliers}">checked</c:if> onchange="selectFilterIncludeDisabledOutliers(this.checked)" />
<label for="includeDisabledOutliers">Include disabled outliers</label>


<div style="margin-top:16px;margin-bottom:8px;">
	<div style="float: left"><b>Legends:</b></div>

	<div style="margin-left:20px;width:10px;height:10px;background-color: #EB9E6F;border: 2px solid #EB9E6F;margin-right: 6px;float: left"></div>  
	<div style="float: left">Disabled outlier</div>
	<div style="margin-left:20px;width:10px;height:10px;background-color: #c5d973;border: 2px solid #c5d973;margin-right: 6px;float: left"></div>  
	<div style="float: left">Wholesale outlier</div>
	<!-- 
	<div style="margin-left:20px;width:10px;height:10px;background-color: #bbbbff;border: 2px solid #bbbbff;margin-right: 6px;float: left"></div>  
	<div style="float: left">Retail outlier</div> -->
	<div style="margin-left:20px;font-weight: bold;margin-right:6px;float: left">[bold]</div>  
	<div style="float: left">Fulfilled outlier</div>
</div><br /><br />

<b>Select:</b>
<a href="javascript:void(0)" onclick="selectAllOutliers()">All</a> -
<a href="javascript:void(0)" onclick="unselectAllOutliers()">None</a>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

<b>Actions:</b>
<img src="../images/ico-enable.gif" align="absbottom" /> <a href="javascript:void(0)" onclick="enableSelectedOutliers()">Enable</a> -
<img src="../images/ico-disable.gif" align="absbottom" /> <a href="javascript:void(0)" onclick="disableSelectedOutliers()">Disable</a> -
<img src="../images/ico-delete.gif" align="absbottom" /> <a href="javascript:void(0)" onclick="deleteSelectedOutliers()">Delete</a><!--  -
<img src="../images/ico-download.gif" align="absbottom" /> <a href="javascript:void(0)" onclick="shortSelectedOutliers()">Short</a> --> -
<img src='../images/ico-add.gif'/><a href='${editUrl}'>Add a new outlier</a>
<br /><br />

<display:table class="list" name="${outliers}" id="tableOutlier" requestURI="${pageUrl}" decorator="userOutlierTableRowDecorator" defaultsort="3">
	<display:column>
		<input type="checkbox" id="checkbox-outlier-${tableOutlier.id}" class="checkbox-outlier" />
	</display:column>
	<display:column title="Check" sortable="false" >
    	<a href="${pageUrl}?action=check&outlierId=${tableOutlier.id}#tickets"><img src="../images/ico-browse.gif" align="absmiddle" /></a>
	</display:column>
    <display:column title="ID" sortable="true" property="id" />
    <display:column title="User" sortable="true">
	  <c:choose>
         <c:when test="${not empty tableOutlier.username}">
		${tableOutlier.username}
	    </c:when>
	    <c:otherwise>
		${tableOutlier.orderedBy}
	    </c:otherwise>
       </c:choose>
    </display:column>
    <display:column title="Market Maker" sortable="true" property="marketMakerUsername" />
    <display:column title="Event Name" sortable="true"  >
		<a href="BrowseTickets?eventId=${tableOutlier.eventId}" tooltip="${tableOutlier.event.venue.location}">
			${tableOutlier.event.name} -
			${tableOutlier.event.formattedEventDate}
		</a>
    </display:column>
    
    <display:column title="%" sortable="true" property="outlierPercentage"  format="{0,number}"/>
    <display:column title="Freq" sortable="true">
        <fmt:formatNumber value="${frequencyByOutlierId[tableOutlier.id] / 60}" pattern="#" />mn
    </display:column>        
    <display:column title="Description" sortable="false" >
    	<c:choose>
    		<c:when test="${empty tableOutlier.description || fn:length(tableOutlier.description) < 20}">
    			${tableOutlier.description}
    		</c:when>
    		<c:otherwise>
    			<span tooltip="${tableOutlier.description}">${fn:substring(tableOutlier.description, 0, 20)}...</span>
    		</c:otherwise>
    	</c:choose>
    </display:column>	
    <display:column title="Created From" sortable="false" property="outlierTransType" />
    <display:column title="" sortable="false" style="width:70px">	
		<img src="../images/ico-edit.gif" align="absmiddle" />
		<a href="${editUrl}?outlierId=${tableOutlier.id}#editOutlier" >Edit</a>
	</display:column>
</display:table>
<br />
<%-- 
<c:if test="${not empty outliers}">

	<b>Select:</b>
	<a href="javascript:void(0)" onclick="selectAllOutliers()">All</a> -
	<a href="javascript:void(0)" onclick="unselectAllOutliers()">None</a>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

	<b>Actions:</b>
	<img src="../images/ico-enable.gif" align="absbottom" /> <a href="javascript:void(0)" onclick="enableSelectedOutliers()">Enable</a> -
	<img src="../images/ico-disable.gif" align="absbottom" /> <a href="javascript:void(0)" onclick="disableSelectedOutliers()">Disable</a> -
	<img src="../images/ico-delete.gif" align="absbottom" /> <a href="javascript:void(0)" onclick="deleteSelectedOutliers()">Delete</a>
	<img src="../images/ico-download.gif" align="absbottom" /> <a href="javascript:void(0)" onclick="shortSelectedOutliers()">Short</a> -
	<img src='../images/ico-add.gif'/><a href='EditMyOutlier'>Add a new outlier</a>
</c:if>
 --%>
<c:if test="${param.action == 'check'}">
	<a name="tickets"><h3>Tickets for Outlier ID ${param.outlierId}:</h3></a>
	<b>Event: </b><a href="BrowseTickets?eventId=${userOutlier.eventId}" >${userOutlier.event.name}</a>
	<br /><br />
	<display:table class="list" name="${outlierTickets}" id="ticket"  pagesize="20" requestURI="${pageUrl}">
	    <display:column title="Rem. Qty" sortable="true" style="text-align:right">
	    	<span tooltip="Lot Size: ${ticket.lotSize}">${ticket.remainingQuantity}</span>
	    </display:column>
		<display:column title="Zone" sortable="true" property="category.symbol" />
	    <display:column title="Section" property="section" sortable="true" />
	    <display:column title="Row" sortable="true" property="row" />
	    <display:column title="Wholesale Price" sortable="true" property="adjustedCurrentPrice" format="{0,number,currency}" style="text-align:right" />
	   
		<display:column title="Online Price" sortable="true" style="text-align:right">
	        <c:if test="${ticket.buyItNowPrice > 0}">
	          <fmt:formatNumber value="${ticket.buyItNowPrice}"
	               type="currency" minFractionDigits="2"/>
	        </c:if>
	        <c:set var="timeLeft">
				<c:choose>
					<c:when test="${empty ticket.numSecondsBeforeEndDate}">
					</c:when>
					<c:when test="${ticket.numSecondsBeforeEndDate < 0}">
						(expired)
					</c:when>
					<c:when test="${ticket.numSecondsBeforeEndDate > 24 * 3600 * 1000}">
						Ending in <fmt:formatNumber type="number" maxFractionDigits="0" value="${(ticket.numSecondsBeforeEndDate / (24 * 3600 * 1000))}"/> days
					</c:when>
					<c:when test="${ticket.numSecondsBeforeEndDate > 3600 * 1000}">
						Ending in <fmt:formatNumber type="number" maxFractionDigits="0" value="${((ticket.numSecondsBeforeEndDate % (24 * 3600 * 1000)) / (3600 * 1000))}"/> hours
					</c:when>
					<c:otherwise>
						Ending in <fmt:formatNumber type="number" maxFractionDigits="0" value="${((ticket.numSecondsBeforeEndDate % (3600 * 1000)) / (60 * 1000))}"/> minutes
					</c:otherwise>
				</c:choose>	          
	        </c:set>
			<c:choose>
				<c:when test="${ticket.ticketType == 'AUCTION'}">
					<span tooltip="${timeLeft}">
						<c:choose>
							<c:when test="${ticket.numSecondsBeforeEndDate > 3600 * 24}">
								<img src="../images/ico-auction.gif" align="absbottom"/>
							</c:when>
							<c:otherwise>
								<img src="../images/ico-auction-ending.gif" align="absbottom"/>
							</c:otherwise>
						</c:choose>
					</span>
				</c:when>
			</c:choose>
		</display:column>
		
		<display:column media="html" title="Seller" sortProperty="seller" sortable="true" >
			<span style="white-space:nowrap">
				<a href="RedirectToItemPage?id=${ticket.id}" tooltip="${ticket.siteId} - ${ticket.itemId}"><img src="../images/ico-${ticket.siteId}.gif" border="0" align="absbottom"/>
					(Buy Ticket)
			    </a>
				<c:if test="${ticket.ticketType != 'AUCTION' and not empty ticket.numSecondsBeforeEndDate}">
					<span tooltip="${timeLeft}">
						<c:choose>
							<c:when test="${ticket.numSecondsBeforeEndDate > 3600 * 24}">
								<img src="../images/ico-clock.gif" align="absbottom"/>
							</c:when>
							<c:otherwise>
								<img src="../images/ico-clock-ending.gif" align="absbottom"/>
							</c:otherwise>
						</c:choose>
					</span>
				</c:if>
		
				<c:choose>
					<c:when test="${ticket.ticketDeliveryType == 'EDELIVERY'}">
						<img src="../images/ico-edelivery.gif" title="eDelivery" align="absbottom" />
					</c:when>
					<c:when test="${ticket.ticketDeliveryType == 'INSTANT'}">
						<img src="../images/ico-instant-edelivery.gif" title="Instant eDelivery" align="absbottom" />
					</c:when>
				</c:choose>			    	
				${ticket.seller}
			</span>
		</display:column>
	</display:table>

</c:if>

<script type="text/javascript">
	function selectAllOutliers() {
		$('.checkbox-outlier').attr('checked', true);
	};

	function unselectAllOutliers() {
		$('.checkbox-outlier').attr('checked', false);
	};
	
	function selectFilterArtist(artistId) {
		document.location.href="${pageUrl}?filterArtistId=" + artistId;
	};
	
	function selectFilterEvent(eventId) {
		document.location.href="${pageUrl}?filterEventId=" + eventId;
	};

	function selectFilterUsername(username) {
		document.location.href="${pageUrl}?filterUsername=" + username;
	};

	function selectFilterIncludeDisabledOutliers(includeDisabledOutliers) {
		document.location.href="${pageUrl}?filterIncludeDisabledOutliers=" + (includeDisabledOutliers?"1":"0");
	};

	function __getSelectedOutlierIds() {
		var ids = "";
		$.each($('.checkbox-outlier:checked'), function(i, elt) {
			var id = $(elt).attr('id').split("-")[2]; 
			ids += id + ",";
		});
		
		if (ids == "") {
			return null;
		}
				
		// remove the final comma
		ids = ids.substring(0, ids.length - 1)
		return ids;		
	};
	
	function enableSelectedOutliers() {
		var outlierIds = __getSelectedOutlierIds();
		if (outlierIds == null) {
			alert("No outlier selected");
			return;
		}		
		document.location.href = "${pageUrl}?action=enable&outlierIds=" + outlierIds;
	};
	
	function disableSelectedOutliers() {
		var outlierIds = __getSelectedOutlierIds();
		if (outlierIds == null) {
			alert("No outlier selected");
			return;
		}		
		document.location.href = "${pageUrl}?action=disable&outlierIds=" + outlierIds;
	};

	function deleteSelectedOutliers() {	
		var outlierIds = __getSelectedOutlierIds();
		if (outlierIds == null) {
			alert("No outlier selected");
			return;
		}		
		
		var r = confirm("Do you really want to delete the " + outlierIds.split(",").length + " selected outliers ?");
		if (r) {
			document.location.href = "${pageUrl}?action=delete&outlierIds=" + outlierIds;
		}		
	};

	function shortSelectedOutliers() {	
		var outlierIds = __getSelectedOutlierIds();
		if (outlierIds == null) {
			alert("No outlier selected");
			return;
		}		
		
		var r = confirm("Do you really want to short the " + outlierIds.split(",").length + " selected outliers ?");
		if (r) {
			document.location.href = "${pageUrl}?action=short&outlierIds=" + outlierIds;
		}		
	};
	
	function toggleOutlierStatsTable() {
		if ($('#outlier-stats-table').css('display') != 'none') {
			$('#outlier-stats-table').hide();
			$('#show-stats-link').text('Show stats');	
			$('#show-stats-image').attr('src', '../images/ico-expand.gif');
		} else {
			$('#outlier-stats-table').show();	
			$('#show-stats-link').text('Hide stats');	
			$('#show-stats-image').attr('src', '../images/ico-collapse.gif');
		}
	};
	

</script>



