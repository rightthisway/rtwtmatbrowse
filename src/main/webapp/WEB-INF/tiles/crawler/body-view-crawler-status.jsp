<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<script type="text/javascript" src="../js/ext/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="../js/ext/ext-all.js"></script>
<script type="text/javascript" src="../js/ext/examples/ux/BufferView.js"></script>
<script type="text/javascript" src="../js/livegrid/livegrid-all.js"></script>
<script type="text/javascript" src="../js/livegrid/supplement/ArrayReader.js"></script>
<link rel="stylesheet" type="text/css" href="../js/ext/resources/css/ext-all.css" />
<link rel="stylesheet" type="text/css" href="../js/livegrid/resources/css/ext-ux-livegrid.css" />

<jsp:directive.page import="com.admitone.tmat.utils.TextUtil" />
<jsp:directive.page import="com.admitone.tmat.crawler.TicketListingCrawl" />
<jsp:directive.page import="org.apache.commons.lang.StringEscapeUtils" />
<div align="center">
	<form action="ViewCrawlerStatus" method="post">
		<table>
			<tr >
				<td align="right">
					<b> Artist : </b>
				</td>
				<td>
					<select id ="artistId" name ="artistId" onchange="getEvents()" style="widht:40px">
						<option value = "0">--- ALL Artist ---</option>
						<c:forEach var="artist" items="${artists}">
							<option value="${artist.id}" <c:if test="${artist.id eq artistId }"> selected </c:if>>${artist.name}</option>
						</c:forEach>
					</select>
				</td>
			</tr>
			<tr>
				<td align="right">
					<b> Event : </b>
				</td>
				<td>
					<select id ="eventId" name ="eventId" onchange=getCrawls();>
						<option value ="0">--- ALL EVENTS ---</option>
						<c:forEach var="event" items="${events}">
						<c:choose>
							<c:when test ="${ not empty event.localDate}">
								<c:set var ="localDate" value="eventDate"/>
								<fmt:formatDate value="${event.localDate}" pattern="MMM dd yyyy" var="localDate"/>
							</c:when>
							<c:otherwise>
								<c:set var ="localDate" value="TBD"/>
							</c:otherwise>
						</c:choose>
							<option value="${event.id}" <c:if test="${event.id eq eventId }"> selected </c:if>>${event.name}-${localDate} , ${event.venue.building},${event.venue.city},${event.venue.state}</option>
						</c:forEach>
					</select>
				</td>
			</tr>
			<tr>
				<td align="right">
					<b>Crawler about to run in :</b>
				</td>
				<td >
					<input type="radio" id="timeLimit" name="timeLimit" value="1" <c:if test = '${timeLimit eq 1}'>checked="checked" </c:if> > <1MN</input>
					<input type="radio" id="timeLimit" name="timeLimit" value="30" <c:if test = '${timeLimit eq 30}'>checked="checked" </c:if> >1MN -30MN</input>
					<input type="radio" id="timeLimit" name="timeLimit" value="60" <c:if test = '${timeLimit eq 60}'>checked="checked" </c:if> >30MN - 1HR</input>
					<input type="radio" id="timeLimit" name="timeLimit" value="120" <c:if test = '${timeLimit eq 120}'>checked="checked" </c:if> >1HR - 2HR</input>
					<input type="radio" id="timeLimit" name="timeLimit" value="121" <c:if test = '${timeLimit eq 121}'>checked="checked" </c:if> > >2HR</input>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<input type="submit" value="Submit" class="medButton"/>
				</td>
			</tr>
		</table>
	</form>
</div>
<div id="ticket-grid"  align="center"></div>
<br/><br/>
	
<script type="text/javascript">

var store =null;

var crawls=[
	<c:forEach var="crawl" items="${crawls}">
	<%
	TicketListingCrawl crawl = (TicketListingCrawl)pageContext.getAttribute("crawl");
	
	String name = crawl.getName();
	if (name != null) {
		name = StringEscapeUtils.escapeJavaScript(TextUtil.removeExtraWhitespaces(name).trim());
	}
	pageContext.setAttribute("crawlName", name);
	%>
		[${crawl.id},'${crawlName}','${crawl.siteId}','','${crawl.endCrawl}',${crawl.nextCrawlWaitingTime},
		${crawl.crawlFrequency},${crawl.automaticCrawlFrequency}],
	</c:forEach>
	
];

	Ext.onReady(function(){
		store = new Ext.data.Store({
			bufferSize: 2000,
			bufferRange : 2000,
			reader: new Ext.data.ArrayReader({}, [
				{name: 'cid', type: 'int'},
				{name: 'name', type: 'string'},
				{name: 'exchange', type: 'string'},
				{name: 'status', type: 'string'},
				{name: 'lastCrawl', type: 'date'},
				{name: 'nextCrawlWaitingTime', type: 'int'},
				{name: 'crawlFrequency', type: 'int'},
				{name: 'autoCrawlFrequency', type: 'bool'}
			])
		});
	
		Ext.state.Manager.setProvider(new Ext.state.CookieProvider());
		var grid = new Ext.ux.grid.livegrid.GridPanel({
			stateful: true,
			stateId: 'objectGrid',
			stateEvents: ['columnresize', 'show', 'hide', 'columnmove'],
			enableDragDrop : false,
			width:900,
			height:${24 + 10 * 26},
			frame: false,
			store: store,
			disableSelection: true,
			cm : new Ext.grid.ColumnModel([
				{
					id: 'tBox',
					header: "Id",
					width: 40,
					align: 'left',
					dataIndex: 'cid',
					sortable:true
				},{
					id: 'name',
					header: "Name",
					width: 200,
					align: 'left',
					dataIndex: 'name',
					sortable:true
				},{
					id: 'status',
					header: "Status",
					width: 100,
					align: 'left',
					dataIndex: 'nextCrawlWaitingTime',
					renderer:renderStatus,
					sortable:true
				},{
					id: 'lastCrawl',
					header: "LastCrawl",
					width: 100,
					align: 'left',
					dataIndex: 'lastCrawl',
					sortable:true,
					renderer: Ext.util.Format.dateRenderer('M d 20y h:i A')
				},{
					id: 'exchange',
					header: "Exchange",
					width: 100,
					align: 'left',
					dataIndex: 'exchange',
					sortable:true
				},{
					id: 'crawlFrequency',
					header: "CrawlFrequency",
					width: 100,
					align: 'left',
					dataIndex: 'crawlFrequency',
					renderer:renderCrawlFrequency,
					sortable:true
				}
				]),
				loadMask: {
				msg : 'Loading...'
			}
		});
		store.loadData(crawls);
		grid.render('ticket-grid');
		
		function renderStatus(val, cell, record){
			var nextCrawlWaitingTime = record.data.nextCrawlWaitingTime;
			var waitingTime = "";
			if (nextCrawlWaitingTime == -1) {
				waitingTime ="STOPPED"; 
			} else if (nextCrawlWaitingTime == 0) {
				waitingTime ="Running"; 
			}else if (nextCrawlWaitingTime < 60) {
				waitingTime = "READY";
			} else {
				waitingTime = "IN " + parseInt(nextCrawlWaitingTime / 60) + "mn";
			}
			return "<span title='" + record.data.nextCrawlWaitingTime + "'>" + waitingTime + "</span>";
		}
		
		function renderCrawlFrequency(val, cell, record){
			var crawlFrequency = record.data.crawlFrequency;
			var res='';
			if(crawlFrequency==1576800000){
				res= 'IDLE';
			}else{
				res = crawlFrequency /(1000*60) + 'mn';
			}
			return "<span title='" + record.data.crawlFrequency + "'>" + res + "</span>";
		}
});	

function getEvents(){
	if($('#artistId').val()==0){
		var option='<option value="0">--- ALL EVENTS ---</option>';
		$('select#eventId').html(option);
	}else{
		$.ajax({
			url:"GetEventsByArtistId?artistId="+$('#artistId').val(),
			success:function(data){
				var option='<option value="0">--- ALL EVENTS ---</option>';
				$.each(data.split(","),function(i,event){
					var temp = event.split(":");
					option += '<option value="' + temp[0] +'">' + temp[1] +  " - " + temp[2] + " " + temp[3].replace('-',':') +  " ," + temp[4] + "," + temp[5] + "," + temp[6] + '</option>';
				});
				$('select#eventId').html(option);
			}
		});
	}
}

</script>
