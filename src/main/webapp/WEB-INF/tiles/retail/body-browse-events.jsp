<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath"  class="breadCrumbPathUser">
	<a href="BrowseArtists">Browse</a> 
	
	<c:if test="${not empty tour}">
		 &gt; <a href="BrowseTours?artistId=${artist.id}">${artist.name}</a>
		 &gt; ${tour.name}
	</c:if>
	<c:if test="${not empty venue}">
		 &gt; ${venue.building} 
	</c:if>
</div>

<c:if test="${not empty param.info}">
	<div id="info" class="info">
	  <div class="infoText">${param.info}</div>
	</div>
</c:if>

<div id="quickLinks">
</div>
<div style="clear:both"></div>

<h1>Browse Events</h1>


<c:if test="${not empty tour}">
	<div style="float: left">
		<div style="float:left">
			<div style="float:left"><b>Tour:</b></div>
		    <div id="star_tour_${param.tourId}" class="starOff" style="margin-top: -4px;" onclick="toggleBookmarkTour(${param.tourId})"></div>
			<div style="float:left"><img src="../images/ico-${fn:toLowerCase(tour.tourType)}.gif" align="absbottom" title="${tour.tourType}"/>${tour.name}</div>
		</div>
		<div style="float:left; clear: left;">
			<b>Type:</b> ${tour.tourType}<br />
			<b>${tour.artistLabel}:</b> <a href="BrowseTours?artistId=${artist.id}">${artist.name}</a><br />
			<b>Location:</b> ${tour.location}<br /><br />
			<b>Start Date:</b> <fmt:formatDate pattern="MM/dd/yyyy" value="${tour.startDate}"/><br />
			<b>End Date:</b> <fmt:formatDate pattern="MM/dd/yyyy" value="${tour.endDate}"/><br /><br />
			<b>Event Count:</b> ${fn:length(events)}<br />			
		</div>
	</div>
</c:if>
<c:if test="${not empty venue}">
	<div style="float: left">
		<div style="float:left">
			<div style="float:left"><b>Venue:</b></div>
		    <div id="star_venue_${venue.id}" class="starOff" style="margin-top: -4px;" onclick="toggleBookmarkVenue(${venue.id})"></div>
			<b>${venue.building}</b><br/>
		</div>
		<div style="float:left; clear: left;">
		<br/>
			<b>City :</b> ${venue.city}<br/>
			<b>State : </b>${venue.city}<br/>
			<b>Country :</b> ${venue.country}<br/>
			<b>Type:</b> ${venue.venueType}<br />
			<b>Event Count:</b> ${fn:length(events)}<br />			
		</div>
	</div>
</c:if>
<div style="clear:both"></div>


<h2>Events</h2>

<%
java.util.Date startDate = new java.util.Date();
%>
<form id="event_form" method="POST" action="EbayAddInventory">

<c:if test="${not empty events}">
Select:
<a href="#" onclick="selectAll(); return false">All</a> |
<a href="#" onclick="unselectAll(); return false">None</a> |
<a href="#" onclick="invertAll(); return false">Invert</a>
<br/><br/>
</c:if>

<display:table class="list" name="${events}" id="event" requestURI="BrowseEvents">
      <display:column>
        <input type="checkbox" id="event_${event.id}" name="event_${event.id}" class="event_checkbox"/>
      </display:column>
    <display:column style="width:20px"><div id="star_${event.id}" class="starOff" onclick="toggleBookmarkEvent(${event.id})"></div></display:column>
	    <c:choose>
	      <c:when test="${browseEventsPage == 'BrowseEventsAnalytic'}">
	        <display:column>
	          <b>
	            <a href="BrowseTicketsAnalytic?eventId=${event.id}&view=analytic">View</a>
	          </b>
	        </display:column>    
	      </c:when>
	      <c:otherwise>
	        <display:column value="View"  href="BrowseTickets" paramId="eventId" paramProperty="id" class="bold" />
	      </c:otherwise>
	    </c:choose>
    <display:column title="Date" sortable="true">
    	<b>
		    <fmt:formatDate pattern="MM/dd/yyyy" value="${event.date}" />
			<c:choose>
			  <c:when test="${not empty event.time}">
			    <fmt:formatDate pattern="HH:mm" value="${event.time}" />
			  </c:when>
			  <c:otherwise>
			    TBD
			  </c:otherwise>
			</c:choose>		
		</b>
    </display:column>
    <display:column title="Event Name" sortable="true" sortProperty="name">
      <img src="../images/ico-${fn:toLowerCase(event.eventType)}.gif" align="absbottom" title="${event.eventType}"/> ${event.name}
    </display:column>
    <display:column title="Venue" sortable="true" >${event.venue.building}</display:column>
    <display:column title="State" sortable="true" >${event.venue.state}</display:column>
    <display:column title="Country">
    	<c:if test="${not empty event.venue.country && event.venue.country != 'null' }">
    		<img src="../images/flags/${event.venue.country}.gif" />${event.venue.country}
    	</c:if>
    </display:column>
    
    <c:choose>
      <c:when test="${browseEventsPage == 'BrowseEventsAnalytic'}">
        <display:column title="OPHigh">
        	${eventToActions[event.id]['OH']}
        </display:column>
        <display:column title="OPMed">
        	${eventToActions[event.id]['OM']}
        </display:column>
        <display:column title="OPLow">
        	${eventToActions[event.id]['OL']}
        </display:column>
        <display:column title="Correct">
        	${eventToActions[event.id]['CO']}
        </display:column>
        <display:column title="UPLow">
        	${eventToActions[event.id]['UL']}
        </display:column>
        <display:column title="UPMed">
        	${eventToActions[event.id]['UM']}
        </display:column>
        <display:column title="UPHigh">
        	${eventToActions[event.id]['UH']}
        </display:column>
      </c:when>
    </c:choose>    
    <display:column title="Managed By" sortable="true" sortProperty="marketMakerManager" class="text-align-center">
      <c:if test="${event.marketMakerManager != 'DESK'}">
      <img src="../images/ico-user.gif" align="absbottom"/>
      </c:if>
      ${event.marketMakerManager}
    </display:column>
  
  	<display:column title="CATS Count" class="text-align-center">
  		<a href="javascript: return null;" onclick="$('#displaycats${event.id}').dialog('open');">${fn:length(event.category)}</a>
  	</display:column>
  	
  	<display:column title="CATS Group" class="text-align-center">
		<a href="javascript: return null;" onclick="$('#displaycats${event.id}').dialog('open');">
			${event.uniqueCategoryGroupName}
		</a>	 			
  	</display:column>
  	
      
</display:table>

<br/>
 	<c:forEach var="event" items="${events}">
		<div id="displaycats${event.id}" title="CATS" style="display:none">
		<c:choose>
		<c:when test="${event.category ne null && event.category ne ''}">
			<c:forEach var="categories" items="${event.category}">
				${categories.symbol} - ${categories.description}<br/>
			</c:forEach>
		</c:when>
		<c:otherwise>
			There is no category.
		</c:otherwise>
		
		</c:choose>
			
		</div>
	</c:forEach>	
  
    <br/>
    <authz:authorize ifAnyGranted="PRIV_BROWSE">
    <c:if test="${not empty events}">
      Assign selected events to: <img src="../images/ico-user.gif" align="absbottom" />
      <select id="marketMaker" name="marketMaker">
        <c:forEach var="curMarketMaker" items="${marketMakers}">
          <option value="${curMarketMaker.username}">${curMarketMaker.username}</option>
        </c:forEach>
        <option value="DESK" selected>DESK</option>
      </select>
      <input type="button" class="medButton" onclick="assignEventsToMarketMaker()" value="Assign"/>
    </c:if>
  <%-- </authz:authorize> --%>     
  </authz:authorize> 
</form>

<%
	java.util.Date endDate = new java.util.Date();
	out.println("<!-- Took " + ((endDate.getTime() - startDate.getTime()) /  1000) + "s -->");
%>

<script type="text/javascript">

	 <c:forEach var="event" items="${events}">
	$("#displaycats${event.id}").dialog({
		autoOpen: false,
		bgiframe: true,
		resizable: false,
		width: 300,
		height:200,
		minHeight:160,
		maxHeight:160,
		modal: true,
		overlay: {
			backgroundColor: '#000',
			opacity: 0.5
		},
		buttons: {}
	});
	</c:forEach> 
  <c:if test="${not empty tourBookmark.objectId}">
    $('#star_tour_' + ${tourBookmark.objectId}).attr('class', 'starOn');
  </c:if>
  <c:if test="${not empty venueBookmark.objectId}">
  $('#star_venue_' + ${venueBookmark.objectId}).attr('class', 'starOn');
</c:if>

  <c:forEach var="bookmark" items="${eventBookmarks}">
    $('#star_' + ${bookmark.objectId}).attr('class', 'starOn');
    $('#star_${bookmark.objectId}').parent().parent().css('background-color', '#eeee44');
  </c:forEach>

  function toggleBookmarkTour(tourId) {
    	BookmarkDwr.toggleBookmarkTour(tourId, 
		   function(response) {
			 $('#star_tour_' + tourId).attr('class', 'star' + response);
		   }
		);
  }

  function toggleBookmarkVenue(venueId) {
	  	BookmarkDwr.toggleBookmarkVenue(venueId, 
			   function(response) {
				 $('#star_venue_' + venueId).attr('class', 'star' + response);
			   }
			);
	}
  
  function toggleBookmarkEvent(eventId) {
    	BookmarkDwr.toggleBookmarkEvent(eventId, 
		   function(response) {
			 $('#star_' + eventId).attr('class', 'star' + response);  							       
			 if (response == "On") {
			 	$('#star_' + eventId).parent().parent().css('background-color', '#eeee44');
			 } else {
			 	$('#star_' + eventId).parent().parent().css('background-color', null);
			 }				   							      
		   });
  };
  
  function downloadTickets() {
    if ($('.event_checkbox:checked').length == 0) {
      alert('Please select at least an event');
      return false;
    }
    
    $('#event_form').submit()
  }
  
  function selectAll() {
    $.each($('.event_checkbox'), function(index, item) {
      $(item).attr('checked', true);
    });
  }
  
  function unselectAll() {
    $.each($('.event_checkbox'), function(index, item) {
      $(item).removeAttr('checked');
    });
  }

  function invertAll() {
    $.each($('.event_checkbox'), function(index, item) {
      if ($(item).is(':checked')) {
        $(item).removeAttr('checked');
      } else {
        $(item).attr('checked', true);
      }
    });
  }
  
  function assignEventsToMarketMaker() {
    if ($('.event_checkbox:checked').length == 0) {
      alert('Please select at least an event');
      return false;
    }
    
    var eventIdsStr = "";
    $.each($('.event_checkbox:checked'), function(index, elt) {
    	eventIdsStr += elt.id.replace(/event_/, '') + ",";
    });
    location.href='MarketMakerEventTrackingUpdate?marketMaker=' + $('#marketMaker').val() + "&eventId=" + eventIdsStr;
  }
  
</script>