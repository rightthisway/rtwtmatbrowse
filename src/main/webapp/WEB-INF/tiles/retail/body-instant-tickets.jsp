<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathUser">
	<a href="..">Home</a> 
	&gt; <a href="BrowseArtists">Browse</a> 
	&gt; InstantTickets
</div>



<h1> SH Instant Tickets </h1>
<div class="info">
	<div class=infoText">
		The CSV file is in the following format: <br/>
		Event,Venue,EventDate,EventTime,Quantity,Section,Row,SeatFrom,SeatThru,Cost,TicketId
	</div>	
</div>
		
<a href="DownloadInstantTicketXlsFile"><img src="../images/ico-download.gif" align="absbottom" /> Download Instant Ticket XLS</a>
<a href="DownloadInstantTicketCsvFile"><img src="../images/ico-download.gif" align="absbottom" /> Download Instant Ticket CSV</a>
		