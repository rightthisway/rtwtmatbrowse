<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<script type="text/javascript" src="../js/ext/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="../js/ext/ext-all.js"></script>
<script type="text/javascript" src="../js/ext/examples/ux/BufferView.js"></script>
<script type="text/javascript" src="../js/livegrid/livegrid-all.js"></script>
<script type="text/javascript" src="../js/livegrid/supplement/ArrayReader.js"></script>
<link rel="stylesheet" type="text/css" href="../js/ext/resources/css/ext-all.css" />
<link rel="stylesheet" type="text/css" href="../js/livegrid/resources/css/ext-ux-livegrid.css" />


<div id="breadCrumbPath" class="breadCrumbPathUser">
	<a href="..">Home</a> 
	&gt; Manage Maps
</div>


<h1>Manage Maps</h1>

<c:if test="${not empty param.info}">
	<div class="info"><div class="infoText">${param.info}</div></div>
</c:if>

<b>Filter:</b> <br/>
By Artist
<select onchange="changeFilterTour(this.value);">
	<option value="">Select</option>
	<option value="-1" <c:if test="${editorTCAPArtistId == -1}">selected</c:if>>View All</option>
	<c:forEach var="artist" items="${artists}">
		<option value="${artist.id}" style="height:20px;" <c:if test="${artist.id == editorTCAPArtistId}">selected</c:if>>${artist.name}</option>
	</c:forEach>
</select>

<br />
  OR
<br /> 
By Venue 
<select onchange="changeFilterVenue(this.value);">
	<option value="">Select</option>
	<c:forEach var="venue" items="${venues}">
		<option value="${venue.id}" style="height:20px;" <c:if test="${venue.id == editorTCAPVenueId}">selected</c:if>>${venue.building}</option>
	</c:forEach>
</select>
<c:if test="${editorTCAPVenueId != null && editorTCAPVenueId != 0}">
	<c:set var="link" value="&venueId=${editorTCAPVenueId}"></c:set>
</c:if>
<c:if test="${editorTCAPArtistId != null && editorTCAPArtistId != 0}">
	<c:set var="link" value="&artistId=${editorTCAPArtistId}"></c:set>
	<c:set var="artistLink" value="&artistsId=${editorTCAPArtistId}"></c:set>
</c:if>
<br /><br />

<c:choose> 
<c:when test="${not empty events}">
Export to :<c:if test="${editorTCAPVenueId !=0}"><a href="EditorManageTCAP?6578706f7274=1&amp;d-148316-e=1&amp;venueId=${editorTCAPVenueId}">CSV </a></c:if>
<c:if test="${editorTCAPArtistId !=0}"><a href="EditorManageTCAP?6578706f7274=1&amp;d-148316-e=1&amp;artistId=${editorTCAPArtistId}">CSV </a></c:if> | <c:if test="${editorTCAPVenueId !=0}"><a href="EditorManageTCAP?6578706f7274=1&amp;d-148316-e=2&amp;venueId=${editorTCAPVenueId}">Excel </a></c:if>
<c:if test="${editorTCAPArtistId !=0}"><a href="EditorManageTCAP?6578706f7274=1&amp;d-148316-e=2&amp;artistId=${editorTCAPArtistId}">Excel </a></c:if> | <c:if test="${editorTCAPVenueId !=0}"><a href="EditorManageTCAP?6578706f7274=1&amp;d-148316-e=3&amp;venueId=${editorTCAPVenueId}">Xml </a></c:if>
<c:if test="${editorTCAPArtistId !=0}"><a href="EditorManageTCAP?6578706f7274=1&amp;d-148316-e=3&amp;artistId=${editorTCAPArtistId}">Xml </a></c:if>
<display:table class="list" name="${events}" id="event"  requestURI="EditorManageTCAP" pagesize="50" export="true">
  
	 <display:setProperty name="export.excel.filename" value="Maps.xls"/> 
	  <display:setProperty name="export.csv.filename" value="Maps.csv"/>
	<display:column style="width:150px" title="Name" sortable="true" property="name" paramId="id" paramProperty="id"/>
	 <display:column title="Date"  sortable="true"  property="formatedDate" />
	 <display:column title="VenueId"  sortable="true" property="venue.id"/>
	<display:column title="Venue"  sortable="true" property="venue.building" paramId="id" paramProperty="venue.id"/>
	<display:column title="Artist"  sortable="true" property="artist.name" paramId="id" paramProperty="artist.id"/>
	<display:column title="Parent"  sortable="true" property="artist.grandChildTourCategory.childTourCategory.tourCategory.name"/>
	<display:column title="Child"  sortable="true" property="artist.grandChildTourCategory.childTourCategory.name"/>
	<display:column title="Grand Child"  sortable="true" property="artist.grandChildTourCategory.name"/>
    <display:column title="Crawl"  sortable="true"  property="crawlCount" />
    <display:column title="Zones"  sortable="true" property="uniqueCategoryGroupName" paramId="artistId" paramProperty="artist.id"/>
    <display:column title='AutoCorrect'>
		<c:choose>
				<c:when test="${event.autoCorrect}">Yes</c:when>
				<c:otherwise>No</c:otherwise>
		</c:choose>
	</display:column>
	<display:column title='IgnoreTBDTime' >
		<c:choose>
			<c:when test="${event.ignoreTBDTime}">Yes</c:when>
			<c:otherwise>No</c:otherwise>
		</c:choose>
	</display:column>
	<display:column title="Map" media="html" sortable="false" >	
	<c:choose> 
	<c:when test="${event.venueCategory ne null and event.venueCategory.isVenuMap}}">		
	<a href="javascript:loadImage(${event.id}, '${event.venueCategory.categoryGroup}')">
	<img src="../images/000_THUMB_AO.gif" width="20" height="20" style="float:left; margin: 0px 5px 5px 5px;" />
	</a>
	</c:when>
	<c:otherwise>
    		<font color="red">No Map</font>
	</c:otherwise>
	</c:choose>
	</display:column>
	<display:column title="is Map">
	<c:choose>
	<c:when test="${event.venueCategory ne null and event.venueCategory.isVenuMap}">Yes</c:when>
	<c:otherwise>No</c:otherwise>
	</c:choose>
	</display:column>
	<display:column title="Have Link " media="csv excel xml" sortable="false" property="formatedAdmitoneId" />
    <display:column title="Link" media="html" sortable="false" >
    	<c:if test="${empty event.admitoneId}">
    		<font color="red">False</font>
    	</c:if>
    	<c:if test="${not empty event.admitoneId}">
    		<font color="green">True</font>
    	</c:if>
    </display:column>
    <display:column title="Price Adjustment"  sortable="false">
    	${shPercentAdjustment}
    </display:column>
<display:setProperty name="export.banner.placement" value="top" />
</display:table>
</c:when >
<c:otherwise>
There is no event for selected option.
</c:otherwise>
</c:choose>
<script type="text/javascript">
	
function changeFilterTour(artistId) {
	document.location.href = "EditorManageTCAP?artistId=" + artistId;
	
};

function changeFilterVenue(venueId) {
document.location.href = "EditorManageTCAP?venueId=" + venueId;
};

function loadImage(eventId, catScheme) {
	window.open('EventImage?eventId=' + eventId + '&catScheme=' + catScheme,'','scrollbars=no,menubar=no,height=600,width=800,resizable=yes,toolbar=no,location=no,status=no');
};
</script>
