<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathUser">
  <a href="BrowseArtists">Browse</a> 
  &gt; Browse Venue
</div>


<div style="clear:both"></div>

<h1>Browse Venue</h1>

<div style="float:left"><b>Venue</b></div>
<%-- <c:if test="${venueId != null}">
  <div id="star_venue_${venueId}" class="starOff" onclick="toggleBookmarkVenue(${venueId})"></div>
</c:if> --%>
<div style="float: left">
<select name="venueId" onchange="if ($(this).val() != '') location.href='BrowseVenue?venueId=' + $(this).val(); else location.href='BrowseVenue'">
  <option value="">ALL</option>
  <c:forEach var="venue" items="${allVenue}">
    <option value="${venue.id}" <c:if test="${venue.id == venueId}">selected</c:if>>${venue.location}</option>
  </c:forEach>
</select>
</div>
<div style="clear:both"></div>


<br/>
<form method="GET" action="BrowseVenue">
<b>Filter Venue:</b> <input id="filter" name="filter" type="text" value="${filter}" />
  <c:if test="${not empty venueId}">
    <input type="hidden" name="venueId" value="${venueId}" />
  </c:if>
  <input class="medButton" type="submit" value="Filter" onclick="return ValidateFilter()"/>
  <c:if test="${not empty filter}"> <a href="BrowseVenue<c:if test="${not empty venueId}">?venueId=${venueId}</c:if>">Clear Filter</a></c:if>
</form>
<br />
<display:table class="list" name="${venue}" id="venue" requestURI="BrowseVenue" pagesize="100" defaultsort="2" >

    <display:column style="width:20px"><div id="star_${venue.id}" class="starOff" onclick="toggleBookmarkVenue(${venue.id})"></div></display:column>
    <display:column title="Building"  sortable="true" property="building" class="content-left-align" headerClass="header-left-align"/>
     <display:column title="City"  sortable="true" property="city" class="content-left-align" headerClass="header-left-align"/>
      <display:column title="State"  sortable="true" property="state" class="content-left-align" headerClass="header-left-align"/>
       <display:column title="Country"  sortable="true" property="country" class="content-left-align" headerClass="header-left-align"/>
         <display:column title="Venue Type"  sortable="true" property="venueType" class="content-left-align" headerClass="header-left-align"/>
</display:table>

<script type="text/javascript">
  <c:forEach var="bookmark" items="${venueBookmarks}">
    $('#star_' + ${bookmark.objectId}).attr('class', 'starOn');
    $('#star_${bookmark.venue.id}').parent().parent().css('background-color', '#eeee44');    
  </c:forEach>

 /*  <c:if test="${venueId != null}">
    $('#star_venue_' + ${venueId}).attr('class', 'starOn');
  </c:if> */
  
  function toggleBookmarkVenue(venueId) {
    	BookmarkDwr.toggleBookmarkVenue(venueId, 
		   function(response) {
			 $('#star_venue_' + venueId).attr('class', 'star' + response);
		   });
  }

  function toggleBookmarkVenue(venueId) {
	BookmarkDwr.toggleBookmarkVenue(venueId, 
		   function(response) {
			 $('#star_' + venueId).attr('class', 'star' + response);
			if (response == "On") {
			 	$('#star_' + venueId).parent().parent().css('background-color', '#eeee44');
			 } else {
			 	$('#star_' + venueId).parent().parent().css('background-color', null);
			 }				   							      
			   							       
		   });
  }
function ValidateFilter()
{


if(document.getElementById('filter').value=="")
{

alert("Please enter the name of Tours/Season in order to filter");
return false;
}

}
</script>