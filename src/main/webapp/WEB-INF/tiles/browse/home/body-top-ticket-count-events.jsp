<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathUser">
	Home &gt; Top Ticket Count Events
</div>

<c:if test="${not empty param.info}">
	<div id="info" class="info">
		<div class="infoText">${param.info}</div>
	</div>
</c:if>


<h1>Top 100 Events With Most Tickets</h1>

<div class="info">
	<div class="infoText">
		The ticket counts for the events are updated every 15 mn and includes duplicate tickets.
	</div>
</div>

<display:table class="list" name="${topTicketCountEvents}" id="event" requestURI="." defaultsort="8" defaultorder="descending">
    <display:column style="width:20px"><div id="star_${event.id}" class="starOff" onclick="toggleBookmarkEvent(${event.id})"></div></display:column>
    <display:column value="View" href="BrowseTickets" paramId="eventId" paramProperty="id" class="bold"  />
    <display:column title="Date (Local Time)">
    	<b>${event.formattedEventDate}</b>
    </display:column>
    <display:column title="Event Name" property="name" class="content-left-align" headerClass="header-left-align"/>
    <display:column title="Venue" property="venue.building" class="content-left-align" headerClass="header-left-align"/>
    <display:column title="State" property="venue.state" />
    <display:column title="Country">
    	<c:if test="${not empty venue.country && venue.country != 'null' }">
    		<img src="../images/flags/${event.venue.country}.gif" />${event.venue.country}
    	</c:if>
    </display:column>
    <display:column title="Ticket Count" property="eventTicketStat.ticketCount" />
</display:table>

<script type="text/javascript">
  <c:forEach var="bookmark" items="${eventBookmarks}">
    $('#star_' + ${bookmark.objectId}).attr('class', 'starOn');
    $('#star_${bookmark.objectId}').parent().parent().css('background-color', '#eeee44');
  </c:forEach>

  function toggleBookmarkEvent(eventId) {
    	BookmarkDwr.toggleBookmarkEvent(eventId, 
		   function(response) {
			 $('#star_' + eventId).attr('class', 'star' + response);  							       
			 if (response == "On") {
			 	$('#star_' + eventId).parent().parent().css('background-color', '#eeee44');
			 } else {
			 	$('#star_' + eventId).parent().parent().css('background-color', null);
			 }				   							      
		   });
  };
</script>