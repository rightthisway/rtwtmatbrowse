<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<br/>
<div>
<form action="FilledOrder" method="post">

	<table align="center">
		<%-- <c:if test ="{broker.id not eq '1'}"> --%>
		<tr>
			
			<td colspan="2"><b>Select Broker:</b>
				<select id='brokerId' name='brokerId'  style="width:480px;" >
					<option value=""> --- All--- </option>
					<c:forEach items="${brokers}" var="tempBroker">
						<option value="${tempBroker.id}"<c:if test="${selectedBrokerId ne null and tempBroker.id eq selectedBrokerId}">selected</c:if>> ${tempBroker.name}</option>
					</c:forEach>
				</select>
			</td>
			<td colspan="2">
				
			</td>
			<td colspan="2"></td>
		</tr>
		<%-- </c:if> --%>
		<tr>
			<td colspan="2"><b>Invoice Start Date:</b>
				<input type="text" id="startDate" name="startDate" value="${startDate}">
				<b>Invoice End Date:</b>
				<input type="text" id="endDate" name="endDate"  value="${endDate}">
			</td>
			<td colspan="2">
				
			</td>
			<td colspan="2"></td>
		</tr>
		<tr>
			<td colspan="2"><b>Select Artist or Venue:</b>
				<input type="text" id="autoArtistVenue" name="autoArtistVenue">
			</td>
			
			<td colspan="4">
				<input type="hidden" id="artistId" name="artistId" value ="${artistId}">
				<input type="hidden" id="venueId" name="venueId" value ="${venueId}">
				<input type="hidden" id="selectedVal" name="selectedVal" value ="${selectedValue}">
				<%-- <input type="hidden" id="brokerId" name="brokerId" value ="${brokerId}"> --%>
			</td>
				
		</tr>
		<tr id="selectedTR" <c:if test ="${empty selectedValue}"> style="display:none"</c:if>>
			<td colspan="2">
				<b><span id="selectedOption" >${selectedOption}</span></b>:<b><span id="selectedValue" >${selectedValue}</span></b> <a id="removeSeletion" href="javascript:removeSelectedValue();" >Remove</a>
			</td>
			<td colspan="4"></td>
		</tr>
		<tr>
			<td ><b>Events:</b>
			</td>
			<td>
				<select id='eventId' name="eventId"  style="width:480px;" >
					<option value=""> --- SELECT--- </option>
					<c:forEach items="${events}" var="event">
						<option 
							<c:if test="${event.id==eventId}"> Selected </c:if>
							value="${event.id}"> ${event.name} <fmt:formatDate pattern="MM/dd/yyyy" value="${event.localDate}" />				
								<c:choose>
									<c:when test="${event.localTime == null}">
										 TBD
									</c:when>
									<c:otherwise>
										<fmt:formatDate pattern="hh:mm aa" value="${event.localTime}" />
									</c:otherwise>
								</c:choose>
								${event.venue.building}
						</option>
					</c:forEach> 
				</select>
			</td>
			<td colspan="4"></td>
		</tr>
		<tr align="center">
			<td colspan="6"><input type="submit" class="medButton" value="Get Data" name="btnSumbit" id="btnSumbit" ></td>
		</tr>
	</table>
</form>
	<display:table class="list" name="${ticketDetails}" id="ticketDetail">
		<display:column  property="brokerName" title="Broker"/>
		<display:column  property="invoiceId" title="InvoiceId"/>
		<display:column  property="invoiceDateTime" title="InvoiceDateTime" format="{0,date ,MMM,dd yyyy hh:mm aa}"/>
		<display:column property="eventName" title="EventName"/>
		<display:column  property="eventDate" title="EventDate" format="{0,date ,MMM,dd yyyy hh:mm aa}"/>
		<display:column  title="Venue" >
			${ticketDetail.venueName}, ${ticketDetail.venueCity}, ${ticketDetail.venueState}, ${ticketDetail.venueCountry}
		</display:column>
		<display:column property="quantity" title="Quantity"/>
		<display:column property="section" title="Section"/>
		<display:column  property="row" title="Row"/>
		<display:column  property="actualSoldPrice" title="SoldPrice"/>
		<display:column  property="internalNotes" title="InternalNotes"/>
		<display:column  title="PurchaseOrder" property="purchaseOrderId"/>
		<display:column  title="PurchaseOrderDate" property="purchaseOrderDateTime" format="{0,date ,MMM,dd yyyy hh:mm aa}"/>
		
	</display:table>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		
		$( "#startDate" ).datepicker({});
		$( "#endDate" ).datepicker({});
		var allEvents = '${eventsCheckAll}';
		var isUpdate = 'true';
		function selectCheckBox(){
			if(allEvents=='true'){
				$('#eventsCheckAll').attr("checked","checked");
			}
			allEvents='false';
		}
		selectCheckBox();
		$('#autoArtistVenue').autocomplete("AutoCompleteGrandChildAndArtistAndVenue", {
			width: 650,
			max: 1000,
			minChars: 2,		
			formatItem: function(row, i, max) {
				if(row[0]=='ARTIST'){
					return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
				}else if(row[0]=='VENUE'){
					return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
				} else if(row[0]=='CHILD'){
					return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
				} else {
					return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
				}
			}
		}).result(function (event,row,formatted){
				$('#autoArtistVenue').val('');
				$('#selectedValue').text(row[2]);
				$('#selectedVal').val(row[2]);
				$("#selectedTR").show();
				if(row[0]=='ARTIST'){
					$('#selectedOption').text('Artist');
					$("#artistId").val(row[1]);
					$("#grandChild").val('');
					$("#venueId").val('');
					$("#child").val('');
					getEvents('artistId',row[1]);
					
				} else if(row[0]=='VENUE'){
					$('#selectedOption').text('Venue');
					$("#venueId").val(row[1]);
					$("#grandChild").val('');
					$("#artistId").val('');
					$("#child").val('');
					getEvents('venueId',row[1]);
					
				} else if(row[0]=='CHILD'){
					$('#selectedOption').text('Child');
					$("#child").val(row[1]);
					$("#grandChild").val('');
					$("#artistId").val('');
					$("#venueId").val('');
					getEvents('childId',row[1]);
					
				} else {
					$('#selectedOption').text('GrandChild');
					$("#grandChild").val(row[1]);
					$("#artistId").val('');
					$("#venueId").val('');
					$("#child").val('');
					getEvents('grandChildId',row[1]);
					
				} 
		});
	
		$("#eventId").change(function () {
			if(isUpdate!='true'){
				$('#ebayInventoryTabel').hide();
				$("#eventId option:selected").each(function () {
					$('#eventsCheckAll').removeAttr("checked");
				});
			}
			isUpdate='false'; 
		}).trigger('change');
		
		  function getEvents(isArtist,id){
	
			var url = "";
			var brokerId = $("#brokerId").val();
			var artistId ='';
			var venueId = '';
			if(isArtist == 'artistId'){
				artistId = id;
			}else if(isArtist == 'venueId'){
				venueId = id;
			}
			
			url = "GetEventsByArtistIdAndVenueId?artistId="+artistId+"&venueId="+venueId;
			
			$.ajax({
				dataType:'json',
				url:url,
				cache:false,
				success: function(res){
					var rowText ='';
					$('#eventId').children().remove();
					$('#eventId').append("<option value=''>--- SELECT --- </option>");
					for (var i = 0; i < res.length; i++) {
						var  data= res[i]; 
						rowText = "<option value='"+data.id+ "'	>"+ data.name+" "+data.date+" "+data.time+" , "+data.venue+"</option>"
						$('#eventId').append(rowText);	
					}
				}
			}); 
		}

		
			
	});
	function removeSelectedValue(){
		$('#selectedOption').text('');
		$("#selectedValue").text('');
		$("#artistId").val('');
		$("#venueId").val('');
		$('#eventId').children().remove();
		$('#eventId').append("<option value=''>--- SELECT --- </option>");
		$("#selectedTR").hide();
		$("#selectedVal").val('');
	}
	function popupWindow(url){
		window.open(url,"TicketOption","height=400,width=800,scrollbars=1");
	}
	   
</script>