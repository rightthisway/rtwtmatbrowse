<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<br/>

<div align="center">
<input id="closeButton"  class="largeButton" name ="closeButton" type="button" value="Click Here to Close & Refresh" onclick="closeChildWin();">
</div>


<c:if test="${openOrderStatus ne null}">
<div>
		<table>
				<tr>
					<td colspan="3"><b>Invoice No </b> </td>
					<td colspan="3"><b> : &nbsp;&nbsp; ${openOrderStatus.invoiceNo}</b></td>
				</tr>
				<tr>
					<td colspan="3"><b>Invoice Date </b> </td>
					<td colspan="3"><b> : &nbsp;&nbsp; ${openOrderStatus.invoiceDateStr}</b></td>
				</tr>
				<tr>
					<td colspan="3"><b>Event Name  </b> </td>
					<td colspan="3"><b> : &nbsp;&nbsp; ${openOrderStatus.eventName}</b></td>
				</tr>
				<tr>
					<td colspan="3"><b>Event Date  </b> </td>
					<td colspan="3"><b> : &nbsp;&nbsp; ${openOrderStatus.eventDateStr}</b></td>
				</tr>
				<tr>
					<td colspan="3"><b>Venue  </b> </td>
					<td colspan="3"><b> : &nbsp;&nbsp; ${openOrderStatus.venueName}, ${openOrderStatus.venueCity}, ${openOrderStatus.venueState}, ${openOrderStatus.venueCountry}</b></td>
				</tr>
				<tr>
					<td colspan="3"><b>Section  </b> </td>
					<td colspan="3"><b> : &nbsp;&nbsp; ${openOrderStatus.section}</b></td>
				</tr>
				<tr>
					<td colspan="3"><b>Row  </b> </td>
					<td colspan="3"><b> : &nbsp;&nbsp; ${openOrderStatus.row}</b></td>
				</tr>
				<tr>
					<td colspan="3"><b>Sold Quantity  </b> </td>
					<td colspan="3"><b> : &nbsp;&nbsp; ${openOrderStatus.soldQty}</b></td>
				</tr>
				<tr>
					<td colspan="3"><b>Sold Price  </b> </td>
					<td colspan="3"><b> : &nbsp;&nbsp; ${openOrderStatus.actualSoldPrice}</b></td>
				</tr>
		</table>
	</div>
	</c:if>
	
<div style="float: right">
	<input id="nocrawl" name ="nocrawl" type="hidden" value="${noCrawl}">
	<input id="reload" name ="reload" type="hidden" value="${reload}">
	<input id="focusRowId" name ="focusRowId" type="hidden" value="${openOrderStatus.id}">
	<span class="refreshLink" style="display:none">
		<img src="../images/ico-reload.gif" align="absbottom"/>
		Page updated <span id="numMinsSinceLastUpdate"></span>
	</span>	
	<span class="refreshedRecently" style="display:none">
		<img src="../images/ico-reload.gif" align="absbottom"/>
		<i>(updated less than <fmt:formatNumber pattern="0" value="${ticketListingCrawler.minCrawlPeriod / 60}"/>mn ago)</i>
	</span>
	<span class="refreshCrawlInfoSpan" style="display:none">
		<img src="../images/process-running.gif" align="absbottom"/>
		<span class="refreshCrawlInfo" style="color:green"></span>
	</span>
</div>
<br/>
<br/>
<div>
<c:choose>
	<c:when test="${noCrawl eq true}">
		<c:choose>
			<c:when test="${not empty tickets}">
				<display:table class="list" name="${tickets}" id="ticket" defaultsort="8">
					<display:column property="id" title="Id"/>
					<display:column title="EventName">
						${ticket.event.name}
					</display:column>
					<display:column title="EventDate" >
						${ticket.event.formatedDate}
					</display:column>
					<display:column title="Venue">
						${ticket.event.venue.building}, ${ticket.event.venue.city }, ${ticket.event.venue.state}
					</display:column>
					<display:column property="quantity" title="Quantity"/>
					<display:column property="section" title="Section"/>
					<display:column property="row" title="Row"/>
					<display:column property="purchasePrice" title="PurchasePrice"/>
					<display:column property="ticketDeliveryType" title="Delivery Type"/>
					<display:column title="Seller">
						<a href="RedirectToItemPage?id=${ticket.id}">${ticket.seller}</a>
					</display:column>
				</display:table>
				<input id="redirect" name ="redirect" type="hidden" value="false">
			</c:when>
			<c:otherwise>
				<input id="redirect" name ="redirect" type="hidden" value="true">
			</c:otherwise>
		</c:choose>
		<br/>
		<br/>
		<a href="BrowseTickets?eventId=${ticket.eventId}" style="float:right;font-size: large;"><b>Go to Browse Ticket Page for This Event</b></a>
	</c:when>
	<c:otherwise>
		 <span class="message" style="color: red;font: bold;font-size: large;" >Latest Tickets will be fetched shortly.</span>
	</c:otherwise>
</c:choose>

</div>
<script type="text/javascript">

var totalCrawls;
updateCrawls($('#nocrawl').val());
function updateCrawls(isNoCrawl) {
	
	if(isNoCrawl=='true'){
		var red = $("#redirect").val();
		if('true'== red){
			$(".message").html('There is not ticket available for this order.. we are redirecting you to event page to show available tickets.');
			document.location.href = "BrowseTickets?scroll=0&eventId="+ ${eventId} + "&reload=" + new Date().getTime();
			alert('There is not ticket available for this order.. we are redirecting you to event page to show available tickets.');
		}else{
			var time= parseInt($('#reload').val());
		  	lastUpdated = new Date(time);
		  	updateLatestTMATInformation();
          	showRefreshedRecently();
	      	return;
		}
	}
	
    
    $.ajax({
		url:"ForceRecrawlForEvent?eventId=" + ${eventId},
		success: function(res){
			var tokens = res.split('|');
			//alert(res);
			  if (tokens[0] != 'OK') {
				lastUpdated = new Date();
				showRefreshedRecently();
			 } else {
				totalCrawls = parseInt(tokens[1]);
				var runningCrawls = parseInt(tokens[2]);
			    if (totalCrawls == 0) {
					window.location.href = "GetSoldTickets?brokerId=${brokerId}&eventId=${eventId}&id=${id}&nocrawl=true&crawlTime=${crawlTime}";
				 //lastUpdated = new Date(parseInt(tokens[3]));
		          //showRefreshedRecently();
		          return;
		        }
		      	$('.refreshLink').hide();
		        $('.refreshedRecently').hide();
		      	$('.refreshCrawlInfoSpan').show();
		      	$('.refreshCrawlInfo').html(parseInt(100 * (runningCrawls / totalCrawls)) + "% updated");
		        setTimeout("getCrawlerStatus()", 10000);
		      }
		}
	});
  	return false;
}

function getCrawlerStatus() {
	$.ajax({
		url:"GetCrawlerStatus?eventId=" + ${eventId} + "&crawlTime=" + ${crawlTime},
		success: function(res){
			var tokens = res.split('|');
			  if (tokens[0] != 'OK') {
				lastUpdated = new Date();
		        showRefreshedRecently();
			 } else {
			    totalCrawls = parseInt(tokens[1]);
		        var runningCrawls = parseInt(tokens[2]);
			    if (totalCrawls == 0) {
				  //lastUpdated = new Date(tokens[3]);
				  window.location.href = "GetSoldTickets?brokerId=${brokerId}&eventId=${eventId}&id=${id}&nocrawl=true&crawlTime=${crawlTime}";
		          //showRefreshedRecently();
		          return;
		        }
		      	$('.refreshLink').hide();
		        $('.refreshedRecently').hide();
		      	$('.refreshCrawlInfoSpan').show();
		      	$('.refreshCrawlInfo').html(parseInt(100 * (runningCrawls / totalCrawls)) + "% updated");
				if(runningCrawls == 	totalCrawls){
					location.href = 'GetSoldTickets?brokerId=${brokerId}&eventId=${eventId}&crawlTime=${crawlTime}&id=${id}&nocrawl=true&reload=' + (new Date().getTime()) ;
				}else{
					setTimeout("getCrawlerStatus()", 10000);
				}
		      }
		}
	});
}
function checkCrawlsFinished() {
    CrawlerDwr.getCrawlsExecutedForEventCount(${eventId}, function(response) {
    	var result= response.split(":");
        var count = parseInt(result[0]) + parseInt(result[1]);
		$('.refreshCrawlInfo').html(parseInt(100 * (count / (totalCrawls))) + "% updated");
		if(count < (totalCrawls)) {
			setTimeout("checkCrawlsFinished()", 10000);
		} else {
			// force reload (avoid caching by adding a dummy parameter)
			$('.refreshCrawlInfo').html("100% updated! Reloading page...");
			location.href = 'GetSoldTickets?brokerId=${brokerId}&eventId=${eventId}&id=${id}&crawlTime=${crawlTime}&nocrawl=true&reload=' + (new Date().getTime()) + "&scroll=" + window.scrollY + anchor;
		}
    });
}
function showRefreshedRecently() {
	refreshRecentlySpanFirstTime();
/*
$('.refreshLink').hide();
$('.refreshedRecently').show();
var now = new Date();
var numMins = parseInt((now.getTime() - lastUpdated.getTime()) / 60000);
if (numMins >= ${1000 * ticketListingCrawler.minCrawlPeriod}) {
    refreshRecentlySpanFirstTime();
} else {
  setTimeout(refreshRecentlySpanFirstTime, ${1000 * ticketListingCrawler.minCrawlPeriod} - numMins);
}
*/
}

function refreshRecentlySpanFirstTime() {
	$('.refreshedRecently').hide();
	$('.refreshLink').show();
	refreshRecentlySpan();
	setInterval("refreshRecentlySpan()", 60000);
}


function refreshRecentlySpan() {
	var now = new Date();
	var numMins = parseInt((now.getTime() - lastUpdated.getTime()) / 60000);
	if (numMins == 0) {
	  $('#numMinsSinceLastUpdate').text("just now");
	} else {
	  $('#numMinsSinceLastUpdate').text(numMins + "mn ago");
	}
}

function closeChildWin() {
	var focusRowIdValue = $('#focusRowId').val();
	window.close();
	window.opener.location.href="OpenOrder?focusRow="+focusRowIdValue;
}

function updateLatestTMATInformation() {
	$.ajax({
		url:"GetRTWOrderTicketStatus?eventId=" + ${eventId} + "&orderId=" + ${id},
		success: function(res){
			var tokens = res.split('|');
			if (tokens[0] == 'OK') {
				//we can write any code
		    }
		}
	});
}

</script>
