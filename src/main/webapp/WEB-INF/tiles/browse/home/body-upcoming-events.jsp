<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathUser">
	Home &gt; Upcoming Events
</div>

<c:if test="${not empty param.info}">
	<div id="info" class="info">
		<div class="infoText">${param.info}</div>
	</div>
</c:if>


<h1>Upcoming Events</h1>

<display:table class="list" name="${events}" id="event" requestURI="UpcomingEvents" defaultsort="2" pagesize="20">

    <display:column style="width:20px"><div id="star_${event.id}" class="starOff" onclick="toggleBookmarkEvent(${event.id})"></div></display:column>
    <display:column value="View" href="BrowseTickets" paramId="eventId" paramProperty="id" class="bold"  />
    <display:column title="Date (Local Time)">
    	<b>
		    <fmt:formatDate pattern="MM/dd/yyyy" value="${event.localDate}" />
		    <span> </span>
			<c:choose>
			  <c:when test="${not empty event.time}">
			    <fmt:formatDate pattern="HH:mm" value="${event.time}" />
			  </c:when>
			  <c:otherwise>
			    TBD
			  </c:otherwise>
			</c:choose>		
		</b>
    </display:column>
    <display:column title="Event Name" property="name" class="content-left-align" headerClass="header-left-align"/>
    <display:column title="Venue" property="venue.building" class="content-left-align" headerClass="header-left-align"/>
    <display:column title="State" property="venue.state" />
    <display:column title="Country">
    	<c:if test="${not empty event.venue.country && event.venue.country != 'null' }">
    		<img src="../images/flags/${event.venue.country}.gif" />${event.venue.country}
    	</c:if>
    </display:column>
</display:table>

<script type="text/javascript">
  <c:forEach var="bookmark" items="${eventBookmarks}">
    $('#star_' + ${bookmark.objectId}).attr('class', 'starOn');
    $('#star_${bookmark.objectId}').parent().parent().css('background-color', '#eeee44');
  </c:forEach>

  function toggleBookmarkEvent(eventId) {
    	BookmarkDwr.toggleBookmarkEvent(eventId, 
		   function(response) {
			 $('#star_' + eventId).attr('class', 'star' + response);  							       
			 if (response == "On") {
			 	$('#star_' + eventId).parent().parent().css('background-color', '#eeee44');
			 } else {
			 	$('#star_' + eventId).parent().parent().css('background-color', null);
			 }				   							      
		   });
  };
</script>