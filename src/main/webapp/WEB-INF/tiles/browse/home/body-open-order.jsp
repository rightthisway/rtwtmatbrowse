<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<br/>
<div>
<form action="OpenOrder" method="post">

	
	<table align="center">
		<%-- <c:if test ="{broker.id not eq '1'}"> --%>
		<tr>
			
			<td colspan="2"><b>Broker:</b>
				<select id='brokerId' name='brokerId'  style="width:480px;" >
					<option value=""> --- All--- </option>
					<c:forEach items="${brokers}" var="tempBroker">
						<option value="${tempBroker.id}"<c:if test="${selectedBrokerId ne null and tempBroker.id eq selectedBrokerId}">selected</c:if>> ${tempBroker.name}</option>
					</c:forEach>
				</select>
			</td>
			<td colspan="2">
				
			</td>
			<td colspan="2"></td>
		</tr>
		<%-- </c:if> --%>
		<tr>
			<td colspan="2"><b>Invoice Start Date:</b>
				<input type="text" id="startDate" name="startDate" value="${startDate}">
				<b>Invoice End Date:</b>
				<input type="text" id="endDate" name="endDate"  value="${endDate}">
			</td>
			<td colspan="2">
				
			</td>
			<td colspan="2"></td>
		</tr>
		<tr>
			<td colspan="2"><b>Select Artist or Venue:</b>
				<input type="text" id="autoArtistVenue" name="autoArtistVenue">
			</td>
			
			<td colspan="4">
				<input type="hidden" id="artistId" name="artistId" value ="${artistId}">
				<input type="hidden" id="venueId" name="venueId" value ="${venueId}">
				<input type="hidden" id="selectedVal" name="selectedVal" value ="${selectedValue}">
				<%-- <input type="hidden" id="brokerId" name="brokerId" value ="${brokerId}"> --%>
			</td> 
				
		</tr>
		<tr id="selectedTR" <c:if test ="${empty selectedValue}"> style="display:none"</c:if>>
			<td colspan="2">
				<b><span id="selectedOption" >${selectedOption}</span></b>:<b><span id="selectedValue" >${selectedValue}</span></b> <a id="removeSeletion" href="javascript:removeSelectedValue();" >Remove</a>
			</td>
			<td colspan="4"></td>
		</tr>
		<tr>
			<td ><b>Events:</b>
			</td>
			<td>
				<select id='eventId' name="eventId"  style="width:480px;" >
					<option value=""> --- SELECT--- </option>
					<c:forEach items="${events}" var="event">
						<option 
							<c:if test="${event.id==eventId}"> Selected </c:if>
							value="${event.id}"> ${event.name} <fmt:formatDate pattern="MM/dd/yyyy" value="${event.localDate}" />				
								<c:choose>
									<c:when test="${event.localTime == null}">
										 TBD
									</c:when>
									<c:otherwise>
										<fmt:formatDate pattern="hh:mm aa" value="${event.localTime}" />
									</c:otherwise>
								</c:choose>
								${event.venue.building}
						</option>
					</c:forEach> 
				</select>
			</td>
			<td colspan="4"></td>
		</tr>
		<tr>
			<td ><b>Profit & Loss:</b>
			</td>
			<td>
				<select id='profitLossSign' name="profitLossSign"  style="width:480px;" >
					<c:forEach items="${profitLossSigns}" var="sign">
						<option 
							<c:if test="${sign==selectedSign}"> Selected </c:if>
							value="${sign}"> ${sign}
						</option>
					</c:forEach> 
				</select>
			</td>
			<td colspan="4"></td>
		</tr>
		<tr align="center">
			<td colspan="6"><input type="submit" class="medButton" value="Get Data" name="btnSumbit" id="btnSumbit" onclick="submitForms('search');"></td>
		</tr>
	</table>
<c:if test="${ticketDetails ne null}">

	<div>
		<table width="80%">
				<tr>
					<td colspan="3"><b>Total No of Orders </b> </td>
					<td colspan="3"><b> : &nbsp;&nbsp; ${noOfOrders}</b></td>
					<td colspan="3"></td>
					<td colspan="3"><b>Total No of Orders </b> </td>
					<td colspan="3"><b> : &nbsp;&nbsp; ${noOfOrders}</b></td>
				</tr>
				<tr>
					<td colspan="3"><b>Total Orders Considered for Section </b> </td>
					<td colspan="3"><b> : &nbsp;&nbsp; ${sectionConsideredOrders}</b></td>
					<td colspan="3"></td>
					<td colspan="3"><b>Total Orders Considered for Zone </b> </td>
					<td colspan="3"><b> : &nbsp;&nbsp; ${zoneConsideredOrders}</b></td>
				</tr>
				<tr>
					<td colspan="3"><b>Total Orders Not Considered for Section</b> </td>
					<td colspan="3"><b> : &nbsp;&nbsp; ${sectionNotConsideredOrders}</b></td>
					<td colspan="3"></td>
					<td colspan="3"><b>Total Orders Not Considered for Zone</b> </td>
					<td colspan="3"><b> : &nbsp;&nbsp; ${zoneNotConsideredOrders}</b></td>
				</tr>
				<tr>
					<td colspan="3"><b>Total Sold Qty for Section </b> </td>
					<td colspan="3"><b> : &nbsp;&nbsp; ${totalSectionQty}</b></td>
					<td colspan="3"></td>
					<td colspan="3"><b>Total Sold Qty for Zone</b> </td>
					<td colspan="3"><b> : &nbsp;&nbsp; ${totalZoneQty}</b></td>
				</tr>
				<tr>
					<td colspan="3"><b>Total Net Sold Price for Section </b> </td>
					<td colspan="3"><b> : &nbsp;&nbsp; ${sectionTotalNetSoldPrice}</b></td>
					<td colspan="3"></td>
					<td colspan="3"><b>Total Net Sold Price for Zone </b> </td>
					<td colspan="3"><b> : &nbsp;&nbsp; ${zoneTotalNetSoldPrice}</b></td>
				</tr>
				<tr>
					<td colspan="3"><b>Total Actual Sold Price for Section </b> </td>
					<td colspan="3"><b> : &nbsp;&nbsp; ${sectionTotalActualSoldPrice}</b></td>
					<td colspan="3"></td>
					<td colspan="3"><b>Total Actual Sold Price for Zone  </b> </td>
					<td colspan="3"><b> : &nbsp;&nbsp; ${zoneTotalActualSoldPrice}</b></td>
				</tr>
				<tr>
					<td colspan="3"><b>Total Market Price for Section </b> </td>
					<td colspan="3"><b> : &nbsp;&nbsp; ${sectionTotalMarketPrice}</b></td>
					<td colspan="3"></td>
					<td colspan="3"><b>Total Zone Price  </b> </td>
					<td colspan="3"><b> : &nbsp;&nbsp; ${zoneTotalPrice}</b></td>
				</tr>
				<tr>
					<td colspan="3"><b>Total Profit / Loss for Section </b> </td>
					<td colspan="3"><b> : &nbsp;&nbsp; ${sectionTotalProfitLoss}</b></td>
					<td colspan="3"></td>
					<td colspan="3"><b>Total Profit / Loss for Zone </b> </td>
					<td colspan="3"><b> : &nbsp;&nbsp; ${zoneTotalProfitLoss}</b></td>
				</tr>
		</table>
	</div>
	<br /><br />
	<div >
<b>Export to :&nbsp;&nbsp;&nbsp;&nbsp;<a href="OpenOrderReport?startDate=${startDate}&endDate=${endDate}&artistId=${artistId}&venueId=${venueId}&eventId=${eventId}&profitLossSign=${selectedSign}">XL </a>
</b>

<input type="button" style="float:right;" class="medButton" value="Update" onclick="submitForms('update');" name="btnSumbit" id="btnSumbit" >
<br/>
<br/>

</div>
	</c:if>
	<display:table class="list" name="${ticketDetails}" requestURI="OpenOrder" id="ticketDetail" decorator="openOrderDecorator" defaultsort="3"  >
		<fmt:formatDate pattern="MM/dd/yyyy" value="${ticketDetail.lastUpdatedPriceDate}" var="lastUpdated" />
		<display:column title ='<input type="checkbox" name="copyAll" id="copyAll">' >
			<%-- <c:if test="${ticketDetail.price != null && ticketDetail.price != null}"> checked="checked" </c:if> --%>
			<input type="checkbox" name="tic-${ticketDetail.id}" id="checkbox_${ticketDetail.id}" value="" class="selectCheck"/>
		</display:column>
		
		<display:column property="eventName" title="Event Name" sortable="true" />
		<display:column  property="eventDate" title="Event Date" format="{0,date ,MMM,dd yyyy hh:mm aa}" sortable="true" />
		<display:column  title="Venue" sortable="true" >
			${ticketDetail.venueName}, ${ticketDetail.venueCity}, ${ticketDetail.venueState}, ${ticketDetail.venueCountry}
		</display:column>
		<display:column property="section" title="Section" sortable="true" />
		<display:column  property="row" title="Row" sortable="true" />
		<display:column property="soldQty" title="Qty" sortable="true" />
		<display:column property="zone" title="Zone" sortable="true" />
		<display:column  property="marketPrice" title="Market Price/Ticket" sortable="true" />
		<display:column  property="totalMarketPrice" title="Total Market Price" sortable="true" style="background-color: #f7c8eb; color: #000000;"/>
		<display:column  property="netSoldPrice" title="Net Price/Ticket" sortable="true" style="background-color: #c7d4e8; color: #000000;"/>
		<display:column  property="netTotalSoldPrice" title="Net Sold Price" sortable="true" style="background-color: #c7d4e8; color: #000000;"/>
		
		<display:column  property="profitAndLoss" title="Section P/L" sortable="true" style="background-color: #c7d4e8; color: #000000;"/>
		<display:column property="sectionMargin" title="Section P/L %" sortable="true" style="background-color: #c7d4e8; color: #000000;"/>
		<display:column property="zoneCheapestPrice" title="Zone Price/Ticket" sortable="true" style="background-color: #eff29d; color: #000000;"/>
		<display:column property="zoneTotalPrice" title="Total Zone Price" sortable="true" style="background-color: #eff29d; color: #000000;"/>
		<display:column property="zoneProfitAndLoss" title="Zone P/L" sortable="true" style="background-color: #eff29d; color: #000000;"/>
		<display:column property="zoneMargin" title="Zone P/L %" sortable="true" style="background-color: #eff29d; color: #000000;"/>
		<display:column  title="View">
			<input type="hidden" name="rowRef_${ticketDetail.id}" id="rowRef_${ticketDetail.id}" value="${ticketDetail.id}"/>
			<a id="focusRow_${ticketDetail.id}" href="javascript:popupWindow('GetSoldTickets?brokerId=${ticketDetail.brokerId}&eventId=${ticketDetail.eventId}&id=${ticketDetail.id}')">View</a>
		</display:column>
		<display:column  property="sectionTixQty" title="Section Tix Count" sortable="true" />
		<display:column property="zoneTixQty" title="Zone Tix Count" sortable="true" />
		<display:column  property="eventTixQty" title="Event Tix Count" sortable="true"  />
		<display:column  property="brokerName" title="Broker"/>
		<display:column  property="invoiceNo" title="InvoiceId" sortable="true" />
		<display:column  property="actualSoldPrice" title="Actual Sold Price" sortable="true" />
		
		<display:column  property="totalActualSoldPrice" title="Total Actual Sold Price" sortable="true" />
		<display:column property="zoneTixGroupCount" title="Zone Tix Group Count" sortable="true" />
		<display:column  property="invoiceDate" title="InvoiceDateTime" format="{0,date ,MMM,dd yyyy hh:mm aa}" sortable="true" />
		<display:column  property="internalNotes" title="InternalNotes" sortable="true" />
		<display:column  property="lastUpdatedPrice" title="Last Updated Price" sortable="true" />
		
		<display:column  property="priceUpdateCount" title="Price Update Count" sortable="true" />
		<!-- Adding price,url,discount coupon price and last update columns -->
		<display:column  title="Price" sortable="true">
			<div>
				<!--<span>$</span>--><input type="number" name="price_${ticketDetail.id}" id="price_${ticketDetail.id}" style="width:90px;" value="${ticketDetail.price}" onkeyup="textKeyUp('${ticketDetail.id}');" placeholder="Price"/>
			</div>
		</display:column>
		<display:column  title="Discount Coupon Price" sortable="true">
			<div>
				<!--<span>$</span>--><input type="number" name="discountCouponPrice_${ticketDetail.id}" id="discountPrice_${ticketDetail.id}" style="width:90px;" value="${ticketDetail.discountCouponPrice}" placeholder="Discount coupon price"/>
			</div>
		</display:column>
		<display:column  title="Url" sortable="true">
			<input type="text" name="url_${ticketDetail.id}" id="url_${ticketDetail.id}" style="width:100px;" value="${ticketDetail.url}" placeholder="Url">
		</display:column>
		
		<display:column  title="Last Update" sortable="true">
				<c:out value="${lastUpdated}"></c:out>
			<!-- <c:choose>
				<c:when test="${not empty lastUpdated}">
					<c:out value="${lastUpdated}"></c:out>
				</c:when>
				<c:otherwise>
					-
				</c:otherwise>
			</c:choose> -->
		</display:column>
		
	</display:table>
	<br/>
	<br/>
	<input type="button" style="float:right;" class="medButton" value="Update" onclick="submitForms('update');" name="btnSumbit" id="btnSumbit" >
	</form>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		
		$( "#startDate" ).datepicker({});
		$( "#endDate" ).datepicker({});
		var allEvents = '${eventsCheckAll}';
		var isUpdate = 'true';
		function selectCheckBox(){
			if(allEvents=='true'){
				$('#eventsCheckAll').attr("checked","checked");
			}
			allEvents='false';
		}
		selectCheckBox();
		$('#autoArtistVenue').autocomplete("AutoCompleteOpenOrderArtistAndVenue", {
			width: 650,
			max: 1000,
			minChars: 2,		
			formatItem: function(row, i, max) {
				if(row[0]=='ARTIST'){
					return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
				}else if(row[0]=='VENUE'){
					return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
				} else if(row[0]=='CHILD'){
					return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
				} else {
					return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
				}
			}
		}).result(function (event,row,formatted){
				$('#autoArtistVenue').val('');
				$('#selectedValue').text(row[2]);
				$('#selectedVal').val(row[2]);
				$("#selectedTR").show();
				if(row[0]=='ARTIST'){
					$('#selectedOption').text('Artist');
					$("#artistId").val(row[1]);
					$("#grandChild").val('');
					$("#venueId").val('');
					$("#child").val('');
					getEvents('artistId',row[1]);
					
				} else if(row[0]=='VENUE'){
					$('#selectedOption').text('Venue');
					$("#venueId").val(row[1]);
					$("#grandChild").val('');
					$("#artistId").val('');
					$("#child").val('');
					getEvents('venueId',row[1]);
					
				} else if(row[0]=='CHILD'){
					$('#selectedOption').text('Child');
					$("#child").val(row[1]);
					$("#grandChild").val('');
					$("#artistId").val('');
					$("#venueId").val('');
					getEvents('childId',row[1]);
					
				} else {
					$('#selectedOption').text('GrandChild');
					$("#grandChild").val(row[1]);
					$("#artistId").val('');
					$("#venueId").val('');
					$("#child").val('');
					getEvents('grandChildId',row[1]);
					
				} 
		});
	
		$("#eventId").change(function () {
			if(isUpdate!='true'){
				$('#ebayInventoryTabel').hide();
				$("#eventId option:selected").each(function () {
					$('#eventsCheckAll').removeAttr("checked");
				});
			}
			isUpdate='false'; 
		}).trigger('change');
		
		  function getEvents(isArtist,id){
	
			var url = "";
			var brokerId = $("#brokerId").val();
			var artistId ='';
			var venueId = '';
			if(isArtist == 'artistId'){
				artistId = id;
			}else if(isArtist == 'venueId'){
				venueId = id;
			}
			
			url = "GetOpenOrderEventsByArtistIdAndVenueId?artistId="+artistId+"&venueId="+venueId;
			
			$.ajax({
				dataType:'json',
				url:url,
				cache:false,
				success: function(res){
					var rowText ='';
					$('#eventId').children().remove();
					$('#eventId').append("<option value=''>--- SELECT --- </option>");
					for (var i = 0; i < res.length; i++) {
						var  data= res[i]; 
						rowText = "<option value='"+data.id+ "'	>"+ data.name+" "+data.date+" "+data.time+" , "+data.venue+"</option>"
						$('#eventId').append(rowText);	
					}
				}
			}); 
		}

		
			
	});
	function removeSelectedValue(){
		$('#selectedOption').text('');
		$("#selectedValue").text('');
		$("#artistId").val('');
		$("#venueId").val('');
		$('#eventId').children().remove();
		$('#eventId').append("<option value=''>--- SELECT --- </option>");
		$("#selectedTR").hide();
		$("#selectedVal").val('');
	}
	
	var TicketOption;

	function popupWindow(url){
		TicketOption = window.open(url,"TicketOption","height=400,width=800,scrollbars=1");
	}
	
	 var focusRowId = '${focusRowId}';
	   if(focusRowId != 0){
		   $("#focusRow_"+focusRowId).focus();
	   }
	   
	   /**
		Unfilled order get & update
		**/
		function submitForms(action){
			if(action == "update"){
				//validate the fields
				if(validateForm()){
				  $("#action").val(action);
				  $("#unfilledOrderFormId").submit();
				}
			}else{
				$("#action").val(action);
				$("#unfilledOrderFormId").submit();
			}
		}
		
		/** Enable the check box when text box has a value**/
		function textKeyUp(id){
			$('#checkbox_'+id).attr('checked', true);
		}
		/**
		 Function to enable all check boxes
		**/
		$(function(){
			$('#copyAll').click(function(){
				if($('#copyAll').attr('checked')){
					$('.selectCheck').attr('checked', true);
				}else{
					$('.selectCheck').attr('checked', false);
				}
			});
		});	

		/**
		Function to validate the open order update action
		**/
	    function validateForm(){
			var flag= true;
			var isMinimamOneRecord = false;
			$('.selectCheck:checkbox:checked').each(function () {
				isMinimamOneRecord = true;
				var id,value;
				
				id = this.id.replace('checkbox','price');
				value = $.trim($("#"+id).val());
				if(value==''){
					alert('Price can not be blank .');
					$("#"+id).focus();
					flag = false;
					return ;
				}

				id = this.id.replace('checkbox','discountPrice');
				value = $.trim($("#"+id).val());
				if(value==''){
					alert('Default coupon price can not be blank .');
					$("#"+id).focus();
					flag = false;
					return ;
				}

				id = this.id.replace('checkbox','url');
				value = $.trim($("#"+id).val());
				if(value==''){
					alert('Url can not be blank .');
					$("#"+id).focus();
					flag = false;
					return ;
				}
			});
			if(!isMinimamOneRecord) {
				alert('Please Select Minimum one record for Update.');
				flag = false;
				//return ;
			}
			if(!flag){
				return flag;	
			}
			
			return true;
		}

</script>