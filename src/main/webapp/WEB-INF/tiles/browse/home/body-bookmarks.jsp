<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<script type="text/javascript">

function expandAll() {
	$('#statusTable').find('.event-expand-img').attr('src', '../images/ico-collapse.gif');
	$('#statusTable').find('.category-expand-img').attr('src', '../images/ico-collapse.gif');
	$('#statusTable').find('.status-child').show();
	$('#statusTable').find('.status-grandchild').show();
};

function collapseAll() {
	$('#statusTable').find('.event-expand-img').attr('src', '../images/ico-expand.gif');
	$('#statusTable').find('.category-expand-img').attr('src', '../images/ico-expand.gif');
	$('#statusTable').find('.status-child').hide();
	$('#statusTable').find('.status-grandchild').hide();
};

function toggleStatusTBody(eId) {
	if ($('#event-expand-img-' + eId).attr('src').indexOf('expand') >= 0) {
		$('#status-tbody-' + eId).show();
		$.each($('.status-child-tbody-' + eId), function(i, elt) {
			if ($(elt).hasClass("status-child")) {
				$(elt).show();
				$(elt).find('.category-expand-img').attr('src', '../images/ico-expand.gif');
			} else {
				$(elt).hide();
			}
			
		});
		$('#event-expand-img-' + eId).attr('src', '../images/ico-collapse.gif');
	} else {
		$('#status-tbody-' + eId).hide();
		$('.status-child-tbody-' + eId).hide();
		$('#event-expand-img-' + eId).attr('src', '../images/ico-expand.gif');
	}
};
    
function toggleStatusTDoubleBody(eId, cId) {
	if ($('#event-expand-img-' + eId + '-' + cId).attr('src').indexOf('expand') >= 0) {
		$('#status-tbody-' + eId + '-' + cId).show();
		$('#event-expand-img-' + eId + '-' + cId).attr('src', '../images/ico-collapse.gif');
	} else {
		$('#status-tbody-' + eId + '-' + cId).hide();
		$('#event-expand-img-' + eId + '-' + cId).attr('src', '../images/ico-expand.gif');
	}
};
</script>
<div id="breadCrumbPath" class="breadCrumbPathUser">
  <a href="..">Home</a> 
  &gt; Starred
</div>

<h1>Starred</h1>

<table width="100%">
  <tr>
    <td style="border-right-style:solid; border-width:1px; padding-right: 5px;" valign="top">
	 <%--  <h2>Artists/Teams</h2>
	<display:table class="list" name="${artistBookmarks}" id="artistBookmark" requestURI="Bookmarks">
	    <display:column style="width:20px"><div id="artist_star_${artistBookmark.artist.id}" class="starOn" onclick="toggleBookmarkArtist(${artistBookmark.artist.id})"></div></display:column>
	    <display:column title="Artist" sortable="true" property="artist.name" href="BrowseTours" paramId="artistId" paramProperty="artist.id" />
	</display:table>--%>
	 <h2>Venue</h2>
	<display:table class="list" name="${venueBookmarks}" id="venueBookmark" requestURI="Bookmarks">
	    <display:column style="width:20px"><div id="venue_star_${venueBookmark.venue.id}" class="starOn" onclick="toggleBookmarkVenue(${venueBookmark.venue.id})"></div></display:column>
	    <display:column title="Building" sortable="true" property="venue.building"  href="BrowseEvents" paramId="venueId" paramProperty="venue.id" />
	    <display:column title="City" sortable="true" property="venue.city" />
	    <display:column title="State" sortable="true" property="venue.state" />
	</display:table>
    </td> 
    <%-- <td style="border-right-style:solid; border-width:1px; padding-right: 5px" valign="top">
	  <h2>Tours/Seasons</h2>
	  <display:table class="list" name="${tourBookmarks}" id="tourBookmark" requestURI="Bookmarks">
        <display:column style="width:20px"><div id="tour_star_${tourBookmark.tour.id}" class="starOn" onclick="toggleBookmarkTour(${tourBookmark.tour.id})"></div></display:column>
        <display:column title="Dates" sortable="true">
          <fmt:formatDate pattern="MM/dd/yyyy" value="${tourBookmark.tour.startDate}"/>
          -
          <fmt:formatDate pattern="MM/dd/yyyy" value="${tourBookmark.tour.endDate}"/>
        </display:column>
        <display:column title="Name"  sortable="true" property="tour.name" href="BrowseEvents" paramId="tourId" paramProperty="tour.id" />
        <display:column title="Location" sortable="true" property="tour.location" />
		
	  </display:table>
    </td> --%>
    <td valign="top">
	  <h2>Events</h2>
	<display:table class="list" name="${eventBookmarks}" id="eventBookmark" requestURI="Bookmarks">
	<c:choose>
		<c:when test="${eventBookmark.outlier == 1}">
			<display:column style="width:20px" title="Outlier"><div id="event_outlier_${eventBookmark.event.id}" class="outlierOn" onclick="toggleOutlierEvent(${eventBookmark.event.id})"></div></display:column>
		</c:when>
		<c:otherwise>
			<display:column style="width:20px"><div id="event_outlier_${eventBookmark.event.id}" class="outlierOff" onclick="toggleOutlierEvent(${eventBookmark.event.id})"></div></display:column>
		</c:otherwise>
	</c:choose>
	    <display:column style="width:20px" title="Bookmark"><div id="event_star_${eventBookmark.event.id}" class="starOn" onclick=" toggleBookmarkEvent(${eventBookmark.event.id})"></div></display:column>
	    <display:column title="Name"><a href="javascript:browseEvent(${eventBookmark.event.id})">${eventBookmark.event.name}</a></display:column>
	    <display:column title="Date" sortable="true" property="event.date" format="{0,date,yyyy/MM/dd}" href="BrowseTickets" paramId="eventId" paramProperty="event.id" />
	    <display:column title="Artist/Team" sortable="true" property="event.artist.name" />
	    <display:column title="Location" sortable="true">${eventBookmark.event.venue.building}, ${eventBookmark.event.venue.city}, ${eventBookmark.event.venue.state}, ${eventBookmark.event.venue.country}</display:column>
	</display:table>

    </td>
  </tr>
   <tr>
    <td valign="top" colspan="3">
	  <h2>Outlier</h2>
<fmt:formatDate pattern="MM/dd/yyyy" value="<%=new java.util.Date()%>" var="today" />
<br /><br />
<a href="javascript:expandAll()"><img src="../images/ico-expand.gif" />Expand All</a> -
<a href="javascript:collapseAll()"><img src="../images/ico-collapse.gif" />Collapse All</a>
<br /><br />
<table id="statusTable" class="list">
	<thead>
		<tr>
			<th>Event Name</th>
			<th>Event Date</th>
			<th>Venue</th>		
			
			<th>Section</th>
			<th>Row</th>
			<th>Purchase Price</th>
			<th>Online Price</th>
			<th>Seller</th>
			<th>Next Purchase Price</th>
			<th>Difference(%)</th>
			<th>Section</th>
			<th>Row</th>
			<th>Qunatity</th>
		</tr>	
	</thead>
<c:if test="${not empty cheapestZoneList}">
	 <c:forEach items="${cheapestZoneList}" var="cheapestZone">
		<%--
			EVENT STATUS LEVEL
		--%>
		<tbody>
			<tr class="eventRow">
				<td style='text-align:left'>
					<a onclick="toggleStatusTBody(${cheapestZone.eventId})" style="cursor: pointer">
						<img id="event-expand-img-${cheapestZone.eventId}" class="event-expand-img" src="../images/ico-expand.gif" />
						${cheapestZone.eventName}
					</a>  					
				</td>
				<td align="right">${cheapestZone.eventDate}</td>
				<td align="right">${cheapestZone.venue}</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
	
				
			</tr>
		</tbody>
		<c:forEach var="ticket" items="${cheapestZone.children}">		
			<%--
				CATEGORY STATUS LEVEL
			--%>
			<tbody class="status-child status-child-tbody-${cheapestZone.eventId}" style="display:none">
				<tr class="catRow">
					<%-- event name --%>
					<td style='text-align:left'>
						<img src="../images/ico-branch.gif" />
						<a onclick="toggleStatusTDoubleBody(${cheapestZone.eventId}, ${ticket.categoryId})" style="cursor: pointer">
							<img id="event-expand-img-${cheapestZone.eventId}-${ticket.categoryId}" class="category-expand-img" src="../images/ico-expand.gif" />
							${ticket.category}							
						</a>  					
					</td>
					<%-- Empty Date --%>
					<td> &nbsp; </td>
					<td> &nbsp; </td>
					<td> &nbsp; </td>
					<%-- Qty Sold --%>
					
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				
				</tr>
			</tbody>				
			<%--
				TICKET GROUP STATUS LEVEL
			--%>
			<tbody id="status-tbody-${cheapestZone.eventId}-${ticket.categoryId}" class="status-grandchild status-child-tbody-${cheapestZone.eventId}" style="display:none" >
				<c:forEach var="ticketQunatity" items="${ticket.children}">	
					<tr style='text-align:left'>
						<%-- event name --%>
						<td>
							<img src="../images/ico-branch.gif" />
							 Quantity : ${ticketQunatity.quantity}
									
						</td>
						<%-- Empty Date --%>
						<td> &nbsp; </td>
						<td> &nbsp; </td>
						<%-- Qty --%>
						<td align="right">${ticketQunatity.section}</td>
						
						<td align="right">${ticketQunatity.row}</td>
						
						<%-- Exposure --%>
						<td align="right">
							<fmt:formatNumber value="${ticketQunatity.purchasePrice}" type="currency" />
						</td>
						<td align="right">
							<fmt:formatNumber value="${ticketQunatity.onlinePrice}" type="currency" />
						</td>
						<td align="right"> <a href="RedirectToItemPage?id=${ticketBookmark.id}">
										<img src="../images/ico-${ticketQunatity.siteId}.gif" border="0" />${ticketQunatity.seller}</a>
						</td>
						<td align="right">
							<fmt:formatNumber value="${ticketQunatity.nextPurchasePrice}" type="currency" />
						</td>
						<td align="right">
							<fmt:formatNumber value="${ticketQunatity.difference}" type="number" />
						</td>
						<td align="right">${ticketQunatity.nextSection}</td>
						<td align="right">${ticketQunatity.nextRow}</td>
						<td align="right">${ticketQunatity.nextQuantity}</td>
					</tr>
				</c:forEach>
			</tbody>
		</c:forEach>
	</c:forEach>
</c:if>
</table>

<%-- <c:forEach items="${cheapestTickets}" var="tickets">
	<c:set var="flag" value="true"></c:set>
	 <c:forEach items="${tickets.value}" var="ticket">
	<c:if test="${flag=='true'}"><b>${ticket.event.name}</b></c:if>
	<c:set var="flag" value="false"></c:set>	
	</c:forEach>
	  
<display:table class="list" name="${tickets.value}" id="ticketBookmark" requestURI="Bookmarks" pagesize="50">
    <display:column>
    	<div id="ticket_star_${ticketBookmark.id}" class="starOn" onclick="toggleBookmarkTicket('${ticketBookmark.id}')"></div>
    </display:column>
	<display:column title="Event" sortable="true" sortProperty="event.date.time" class="text-align-center">
	  <a href="BrowseTickets?eventId=${ticketBookmark.event.id}">
	    <fmt:formatDate value="${ticketBookmark.event.date}" pattern="yyyy/MM/dd" />
	    <fmt:formatDate value="${ticketBookmark.event.time}" pattern="HH:mm" />
	  </a>
	</display:column>
	<display:column title="Tour" sortable="true" property="event.tour.name" sortName="event.tour.name" class="text-align-center" href="BrowseEvents" paramId="tourId" paramProperty="event.tour.id" />
    <display:column title="Rem. Qty" sortable="true" sortName="remainingQuantity" class="bold text-align-center">
    	<span tooltip="Lot Size: ${ticketBookmark.lotSize}">${ticketBookmark.remainingQuantity}</span>
    </display:column>
	<display:column title="Zone" sortable="true" property="category.symbol" sortName="category" class="bold text-align-center" />
    <display:column title="Section" sortable="true" sortName="section" class="bold text-align-center"  decorator="com.admitone.tmat.web.decorators.ExtraWhiteSpaceStripWrapper">
      <c:choose>
        <c:when test="${tour.tourType == 'SPORT'}">
	      <c:choose>
	        <c:when test="{$ticketBookmark.section == ticketBookmark.normalizedSection}">
	          ${ticketBookmark.section}
	        </c:when>
	        <c:otherwise>
	          <span class="grayCell">
	            <c:set var="formattedSection" value="<font color='#000000'>${ticketBookmark.normalizedSection}</font>"/>
	            ${fn:replace(ticketBookmark.section, ticketBookmark.normalizedSection, formattedSection)}
	          </span>
	        </c:otherwise>
	      </c:choose>
        </c:when>

        <c:when test="${tour.tourType == 'THEATER'}">
	      <c:choose>
	        <c:when test="${ticketBookmark.section == ticketBookmark.normalizedSection}">
	          ${ticketBookmark.section}
	        </c:when>
	        <c:otherwise>
	          ${ticketBookmark.normalizedSection}
	          <span class="grayCell">
	            (${ticketBookmark.section})
	          </span>
	        </c:otherwise>
	      </c:choose>        
		</c:when>
        
        <c:otherwise>
          ${ticketBookmark.section}
		</c:otherwise>        
      </c:choose>


    </display:column>
    <display:column title="Row" sortable="true" sortName="row" class="bold text-align-center" decorator="com.admitone.tmat.web.decorators.ExtraWhiteSpaceStripWrapper">
      <c:choose>
        <c:when test="{$ticketBookmark.row == ticketBookmark.normalizedRow}">
          ${row}
        </c:when>
        <c:otherwise>
          <span class="grayCell">
            <c:set var="formattedRow" value="<font color='#000000'>${ticketBookmark.normalizedRow}</font>"/>
            ${fn:replace(ticketBookmark.row, ticketBookmark.normalizedRow, formattedRow)}
          </span>
        </c:otherwise>
      </c:choose>
    </display:column>
     <display:column title="Purchase Price" sortable="true" property="purchasePrice" format="{0,number,currency}" class="bold text-align-right" />
    <display:column title="Wholesale Price" sortable="true" property="adjustedCurrentPrice" sortName="currentPrice" format="{0,number,currency}" class="bold text-align-right" />
    <display:column title="Online Price" sortable="true" sortName="buyItNowPrice" class="text-align-right" decorator="com.admitone.tmat.web.decorators.ExtraWhiteSpaceStripWrapper">
      <c:choose>
        <c:when test="${ticketBookmark.buyItNowPrice > 0}">
          <fmt:formatNumber value="${ticketBookmark.buyItNowPrice}"
               type="currency" minFractionDigits="2"/>
        </c:when>
        <c:otherwise>
        </c:otherwise>
      </c:choose>
        <c:set var="timeLeft">
	    <c:choose>
	      <c:when test="${empty ticketBookmark.numSecondsBeforeEndDate}">
	      </c:when>
	      <c:when test="${ticketBookmark.numSecondsBeforeEndDate < 0}">
	         (expired)
	      </c:when>
	      <c:when test="${ticketBookmark.numSecondsBeforeEndDate > 24 * 3600 * 1000}">
	         Ending in <fmt:formatNumber type="number" maxFractionDigits="0" value="${(ticketBookmark.numSecondsBeforeEndDate / (24 * 3600 * 1000))}"/> days
	      </c:when>
	      <c:when test="${ticketBookmark.numSecondsBeforeEndDate > 3600 * 1000}">
	         Ending in <fmt:formatNumber type="number" maxFractionDigits="0" value="${((ticketBookmark.numSecondsBeforeEndDate % (24 * 3600 * 1000)) / (3600 * 1000))}"/> hours
	      </c:when>
	      <c:otherwise>
	         Ending in <fmt:formatNumber type="number" maxFractionDigits="0" value="${((ticketBookmark.numSecondsBeforeEndDate % (3600 * 1000)) / (60 * 1000))}"/> minutes
	      </c:otherwise>
	    </c:choose>	          
        </c:set>
	       <c:choose>
	         <c:when test="${ticketBookmark.ticketType == 'AUCTION'}">
 	           <span tooltip="${timeLeft}">
 	             <c:choose>
 	               <c:when test="${ticketBookmark.numSecondsBeforeEndDate > 3600 * 24}">
 	                 <img src="../images/ico-auction.gif" align="absbottom"/>
 	               </c:when>
 	               <c:otherwise>
 	                 <img src="../images/ico-auction-ending.gif" align="absbottom"/>
 	               </c:otherwise>
 	             </c:choose>
  	    	   </span>
	         </c:when>
	       </c:choose>
    </display:column>

	<display:column title="Eval" class="text-align-right" sortProperty="valuationFactor"><c:choose><c:when test="${not empty ticketBookmark.valuationFactor}">${ticketBookmark.valuationFactor}</c:when><c:otherwise>-</c:otherwise></c:choose></display:column>
	<display:column title="AI">n/a</display:column>		
		
    <display:column media="html" decorator="com.admitone.tmat.web.decorators.ExtraWhiteSpaceStripWrapper" title="Seller">
       <span style="white-space:nowrap">
  	    <a href="RedirectToItemPage?id=${ticketBookmark.id}" tooltip="${ticketBookmark.siteId} - ${ticketBookmark.itemId}"><img src="../images/ico-${ticketBookmark.siteId}.gif" border="0" align="absbottom"/>
	         ${ticketBookmark.seller}
	    </a>
  	    	 <c:if test="${ticketBookmark.ticketType != 'AUCTION' and not empty ticketBookmark.numSecondsBeforeEndDate}">
 	           <span tooltip="${timeLeft}">
 	             <c:choose>
 	               <c:when test="${ticketBookmark.numSecondsBeforeEndDate > 3600 * 24}">
 	                 <img src="../images/ico-clock.gif" align="absbottom"/>
 	               </c:when>
 	               <c:otherwise>
 	                 <img src="../images/ico-clock-ending.gif" align="absbottom"/>
 	               </c:otherwise>
 	             </c:choose>
    	       </span>
	         </c:if>

		    <c:choose>
		    	<c:when test="${ticketBookmark.ticketDeliveryType == 'EDELIVERY'}">
			    	<img src="../images/ico-edelivery.gif" title="eDelivery" align="absbottom" />
		    	</c:when>
		    	<c:when test="${ticketBookmark.ticketDeliveryType == 'INSTANT'}">
			    	<img src="../images/ico-instant-edelivery.gif" title="Instant eDelivery" align="absbottom" />
		    	</c:when>
		    </c:choose>			    	
	         
     </span>
    </display:column>
</display:table>
 </c:forEach>     
 --%>    </td>
  </tr>
</table>

<script type="text/javascript">

function browseEvent(eventId){
	window.open("BrowseTickets?eventId="+eventId);
}


  function toggleBookmarkArtist(artistId) {
    	BookmarkDwr.toggleBookmarkArtist(artistId, 
  							   function(response) {
								 $('#artist_star_' + artistId).attr('class', 'star' + response);  							       
  							   });
  }

  function toggleBookmarkVenue(venueId) {
  	BookmarkDwr.toggleBookmarkVenue(venueId, 
							   function(response) {
								 $('#venue_star_' + venueId).attr('class', 'star' + response);  							       
							   });
}

  
  function toggleBookmarkTour(tourId) {
    	BookmarkDwr.toggleBookmarkTour(tourId, 
  							   function(response) {
								 $('#tour_star_' + tourId).attr('class', 'star' + response);  							       
  							   });
  }

  function toggleBookmarkEvent(eventId) {
    	BookmarkDwr.toggleBookmarkEvent(eventId, 
  							   function(response) {
								 $('#event_star_' + eventId).attr('class', 'star' + response);  							       
  							   });
  }
  
  function toggleBookmarkTicket(ticketId) {
    	BookmarkDwr.toggleBookmarkTicket(ticketId, 
  							   function(response) {
								 $('#ticket_star_' + ticketId).attr('class', 'star' + response);  							       
  							   });
  }

  function toggleOutlierEvent(eventId){
	  BookmarkDwr.toggleOutlierEvent(eventId, 
				   function(response) {
			 $('#event_outlier_' + eventId).attr('class', 'outlier' + response);  							       
		   });
  }

</script>