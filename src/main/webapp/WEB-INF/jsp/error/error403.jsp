<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<%
pageContext.setAttribute("constants", com.admitone.tmat.web.Constants.getInstance());
%>

<html>
  <head>
    <title>Error 403</title>
    <style type="text/css">
		@import url(${pageContext.request.contextPath}/css/common.css);
	</style>
    
  </head>
  <body style="padding:10px">
    <a href="${pageContext.request.contextPath}/">
		<c:choose>
			<c:when test="${constants.ipAddress == '172.31.255.12'}"><img src="${pageContext.request.contextPath}/images/logo-admit-one.gif" border="0"/></c:when>
			<c:otherwise><img src="${pageContext.request.contextPath}/images/logo-admit-one-test.gif" border="0"/></c:otherwise>
		</c:choose>
    </a>
    <br/>
    <h1>Error 403 - Forbidden</h1>
    You don't have permission to access <font color="#0f7e00"><%= request.getAttribute("javax.servlet.forward.request_uri") %></font> on this server.
    <br /><br />    
    <a href="${pageContext.request.contextPath}/">Go to TMAT</a>
    <br /><br /> 
    <font color="red"><b>Note:</b></font><br />
    We are currently doing some changes in the access right management. If you think that you 
    should have access right but cannot access to the page. Please logout from TMAT by clicking on the link below and then login.<br />   
    <a href="${pageContext.request.contextPath}/j_acegi_logout">Logout from TMAT</a>   
  </body>
</html>