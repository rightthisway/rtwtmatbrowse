function showCrawlException(crawlId) {

	if ($('#crawlErrorDialog').length == 0) {
		var crawlErrorDialog = $.DIV({'id': 'crawlErrorDialog', title: 'Crawl Error'},
			$.TABLE({},
				$.TR({},
					$.TD({}, "ID"),
					$.TD({}, crawlId)
				),
				$.TR({},
					$.TD({}, "Name"),
					$.TD({id:'crawlErrorDialogCrawlName'})
				),
				$.TR({},
					$.TD({}, "Exception class"),
					$.TD({id:'crawlErrorDialogExceptionClassName'})
				),				
				$.TR({},
					$("<td colspan='2'><br />Stack Trace<br /><div id='crawlErrorDialogStackTrace' style='width: 566px;height:120px;overflow:auto;border:1px solid gray'></div></td>")[0]
				)
			)
		);
		
		$('BODY').append(crawlErrorDialog);
		
	    $("#crawlErrorDialog").dialog({
	    		autoOpen: false,
	            bgiframe: true,
	            resizable: false,
	            width: 600,
	            height: 320,
	            minHeight: 300,
	            modal: true,
	            overlay: {
	                    backgroundColor: '#000',
	                    opacity: 0.5
	            },
	            buttons: {
	                    'OK': function() {
							$(this).dialog('close');
						}
	            }
	    });
	}
    

	$('#crawlErrorDialogCrawlName').text("");
	$('#crawlErrorDialogExceptionClassName').text("");
	$('#crawlErrorDialogStackTrace').text("");

	DataDwr.getCrawlException(crawlId,function(result) {
		$('#crawlErrorDialogCrawlName').text(result.crawlName);
		$('#crawlErrorDialogExceptionClassName').text(result.exceptionClassName);
		$('#crawlErrorDialogStackTrace').text(result.exceptionStackTrace);
		$("#crawlErrorDialog").dialog("open");
	});
};

var feedbackAttachmentUpload;

function showFeedbackDialog() {
	if ($('#feedbackDialog').length == 0) { 
		var feedbackDialog = $.DIV({'id': 'feedbackDialog', title: 'Send a feedback'},
			$.TABLE({'id': 'feedbackTable'},
				$.COLGROUP({},
					$.COL({width: '60'}),
					$.COL({width: '200'})
				),
				$.TR({},
					$.TD({}, "Project"),
					$.TD({}, 
						$.SELECT({id: 'feedbackProject', name: 'feedbackProject', 'class': 'longInput'}, 
								$.OPTION({'value': 'TMATT'}, "TMAT-T"),
								$.OPTION({'value': 'CATEGORYSUPPORT'}, "Category Support"),
								$.OPTION({'value': 'TMATEVENTS'}, "TMAT Event Creation"),
								$.OPTION({'value': 'TMATT'}, "DEFAULT")
						)
					)
				),
				$.TR({},
					$.TD({}, "Type"),
					$.TD({}, 
						$.SELECT({id: 'feedbackType', name: 'feedbackType'}, 
							$.OPTION({}, "Improvement"),
							$.OPTION({}, "Bug"),
							$.OPTION({}, "Other")
						)
					)
				),
				$.TR({},
					$.TD({}, "Scope"),
					$.TD({}, 
						$.SELECT({id: 'feedbackScope', 'name': 'feedbackScope'}, 
							$.OPTION({}, "This page"),
							$.OPTION({}, "Application")
						)
					)
				),
				$.TR({},
					$.TD({}, "Priority"),
					$.TD({}, 
						$.SELECT({id: 'feedbackPriority', 'name': 'feedbackPriority'}, 
							$.OPTION({'value':3}, "Low"),
							$.OPTION({'value':4, 'selected':true}, "Normal"),
							$.OPTION({'value':5}, "High"),
							$.OPTION({'value':6}, "Urgent"),
							$.OPTION({'value':7}, "Immediate")
						)
					)
				),				
				$.TR({},
					$.TD({}, "Summary"),
					$.TD({}, 
						$.INPUT({id: 'feedbackSummary', 'name': 'feedbackSummary', 'type': 'text'})
					)
				),
				$.TR({},
					$.TD({}, "Attach a screenshot (optional)"),
					$.TABLE({},
						$.TR({},
							$.TD({}, $.SPAN({id: 'feedbackAttachmentName'})),
							$.TD({}, $.INPUT({id: 'feedbackAttachment', 'name': 'feedbackAttachment', 'type': 'button', 'value':'Upload', 'class': 'medbutton'}))
						)
					)
				),
				$.TR({},
					$("<td colspan='2'><br />Message<br /><textarea id='feedbackMessage' name='feedbackMessage' style='width: 566px;height:80px;border:1px solid gray'></textarea></td>")[0]
				)
			),
			$.DIV({'id': 'feedbackLoading'}, $.IMG({src: '../images/process-running.gif', 'align': 'absbottom'}), "Submitting feedback...")
			
		);

		
		$('BODY').append(feedbackDialog);
		$('#feedbackLoading').hide();

	    $("#feedbackDialog").dialog({
	            bgiframe: true,
	            resizable: false,
	            width: 600,
	            height: 400,
	            minHeight: 370,
	            modal: true,
	            autoOpen: false,
	            overlay: {
	                    backgroundColor: '#000',
	                    opacity: 0.5
	            },
	            buttons: {
	                    'OK': function() {				                    				
	            			var feedbackProject = $('#feedbackProject').val();
							var feedbackType = $('#feedbackType').val();
							var feedbackScope = $('#feedbackScope').val();
							var feedbackPriority = $('#feedbackPriority').val();
							var feedbackSummary = $.trim($('#feedbackSummary').val()).replace(/"/g, "'");
							var feedbackMessage = $.trim($('#feedbackMessage').val()).replace(/"/g, '&quot;');

							if (feedbackSummary.length == 0) {
								alert("Please enter a summary");
								return;
							}

							if (feedbackMessage.length == 0) {
								alert("Please enter a message");
								return;
							}
							
							feedbackAttachmentUpload.params({
					        	feedbackProject: feedbackProject,
					        	feedbackType: feedbackType,
					        	feedbackScope: feedbackScope, 
					        	feedbackPriority: feedbackPriority, 
					        	feedbackSummary: feedbackSummary, 
					        	feedbackMessage: feedbackMessage, 
					        	currentUrl: document.location.href,
					        	referrerUrl:__referrerUrl
					        });

							$('#feedbackLoading').show();
							$('#feedbackTable').hide();
	                   		$("#feedbackDialog").dialog('option', 'buttons', {});

							feedbackAttachmentUpload.submit();
						},
						
						'Cancel': function() {
							$(this).dialog('close');
						}
	            },
	            close: function() {
					$("#feedbackDialog").remove();	            	
	            }
	    });
	}
	
	$("#feedbackDialog").dialog("open");
	$('#feedbackSummary').val("");
	$('#feedbackSummary').css('width', '360px');
	$('#feedbackMessage').val("");

	$('#feedbackAttachmentName').text("");
	feedbackAttachmentUpload = $('#feedbackAttachment').upload({
	        name: 'feedbackAttachment',
	        action: 'SendFeedback',
	        method: 'post',
	        enctype: 'multipart/form-data',
	        autoSubmit: false,
	        onSubmit: function() {},
	        onComplete: function(result) {	        
	        	if (result == "OK") {
	        		alert("Your feedback has been sent. Thank you!");
	        	} else {
	        		if (result) {
		        		alert("ERROR: " + result);
	        		}
	        	}
				$("#feedbackDialog").dialog('close');							
	        },
	        onSelect: function() {
	        	$('#feedbackAttachmentName').text(feedbackAttachmentUpload.filename());		        
	        }
	});	
	
};