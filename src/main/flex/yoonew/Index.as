package yoonew
{
	import mx.controls.Alert;
	
	public class Index {
		
		public var color:uint;
		
		public var indexValues:Array = new Array();
		
		private var name:String;
		
		// this can be transactionPrice or percentChange
		public var valueType:String;
		
		public var historyLoaded:Boolean = false;
		public var historyURL:String
		
		public var removable:Boolean;
			
		public function Index(name:String, valueType:String, historyURL:String, removable: Boolean) {
			this.name = name;
			this.valueType = valueType;
			this.historyURL = historyURL;
			this.removable = removable;
		}

		public function getIndexValues():Array {
			return indexValues;
		}

		public function getName():String {
			return name;
		}

		public function getIndexPositionJustBefore(timestamp:Date):Number {
			if (indexValues.length == 0 || timestamp.time < indexValues[0].timestamp.time) {
				return -1;
			}

			if (timestamp.time >= indexValues[indexValues.length - 1].timestamp.time) {
				return indexValues.length - 1;
			}

			var startIndex:Number = 0;
			var endIndex:Number = indexValues.length;
			
			// dichotomic search
			var middleIndex:Number;
			while(true) {
				middleIndex = int((startIndex + endIndex) / 2);
				if (middleIndex == startIndex) {
					break;
				}		
			
				if (indexValues[middleIndex].timestamp.time < timestamp.time) {
					// the index should be after the middleIndex
					startIndex = middleIndex;
				} else if(indexValues[middleIndex].timestamp.time > timestamp.time) {
					// the index should be before the middleIndex
					endIndex = middleIndex;
				} else {
					//there should not be two indexes with the same timestamp
					break;
				}
			}
			return middleIndex;
		}

		public function dateCmp(i1:IndexValue, i2:IndexValue):Number {
			if (i1.timestamp.getTime() < i2.timestamp.getTime())
				return -1;
			else if (i1.timestamp.getTime() > i2.timestamp.getTime())
				return 1;
			return 0;
		}

		public function addIndexValue(timestamp:Date, averagePrice:Number, medianPrice:Number,
				minPrice:Number, maxPrice:Number, deviation:Number,  ticketCount:uint, minQuantity:uint,maxQuantity:uint,
				inTicketCount:uint, outTicketCount:uint, liquidityRatio:Number):void {
			indexValues.splice(getIndexPositionJustBefore(timestamp)+1, 0, new IndexValue(timestamp, averagePrice, medianPrice, minPrice,maxPrice, deviation, ticketCount, minQuantity, maxQuantity, inTicketCount, outTicketCount, liquidityRatio));
			indexValues.sort(dateCmp);
		}
				
	}
		
}