package com.admitone.tmat.ticketfetcher;

import java.net.InetAddress;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.enums.TicketDeliveryType;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.enums.TicketType;
import com.admitone.tmat.indexer.TicketHitIndexer;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;

/**
 * Fetcher to extract data from TicketNow website.
 * The ticket information are found in the javascript code in the page and are defined in JSON format. 
 */
public class TicketsNowTicketListingFetcher extends TicketListingFetcher{	
	private final Logger logger = LoggerFactory.getLogger(TicketsNowTicketListingFetcher.class);
//	private static final Pattern seatPattern = Pattern.compile("(\\d+)");	

	public TicketsNowTicketListingFetcher() {
	}

	@Override
	public boolean runFetchTicketListing(SimpleHttpClient httpClient, TicketHitIndexer ticketHitIndexer, TicketListingCrawl ticketListingCrawl) throws Exception {
		
		if (!(ticketListingCrawl.getSiteId().equals(Site.TICKETS_NOW))) {
			throw new Exception(this.getClass().getName() + ".fetchTicketListing parameter must be a ticketsnow crawl");
		}
		
		long startProcessingTime = System.currentTimeMillis(); 
		int counter=1;
		
		// to get the session in the cookie
		HttpGet httpGet = new HttpGet(ticketListingCrawl.getQueryUrl());
//		httpGet.addHeader("Host", "www.ticketsnow.com");				
		httpGet.addHeader("Accept", "application/json, text/javascript, */*");
		httpGet.addHeader("Accept-Encoding", "gzip,deflate");
		HttpResponse response = httpClient.execute(httpGet);		
		String content = EntityUtils.toString(HttpEntityHelper.getUncompressedEntity(response.getEntity()));
//		System.out.println(content.replaceAll("\\s+", " "));
		ticketListingCrawl.setFetchingTime(System.currentTimeMillis() - startProcessingTime);
		String proxy="";
		Object obj=httpClient.getParams().getParameter(ConnRoutePNames.DEFAULT_PROXY);
		if(obj!=null){
			proxy= obj.toString();
			proxy=proxy.split(":")[1].replaceAll("//","");
		}else{
			proxy=InetAddress.getLocalHost().getHostName();
		}
		saveCounterHistory(ticketListingCrawl, counter,proxy);
			
		
		int start = content.indexOf("var g_ticketData =") + "var g_ticketData =".length();
		int end = content.indexOf(";var", start);
		
		if (start <0 || end < 0) {
			return true;
		}
				
		String jsonContent = content.substring(start, end); 
		
		JSONArray jsonArray = new JSONArray(jsonContent);

		for (int i = 0 ; i < jsonArray.length() ; i++) {
			if  (jsonArray.get(i).equals(JSONObject.NULL)) {
				break;
			}
			JSONObject item = (JSONObject)jsonArray.get(i);
			
			// {"N":"","P":140,"Q":2,"S":"308","R":"Z","F":false,"T":35645404,"ANE":false,"SQ":2,"PP":0,"B":0,"BN":"","RT":false,"H":false,"LP":130.0000,"AP":123.50,"TS":1,"BSN":"","BPN":"","CP":0,"IS":false,"TN":"","SI":"-","LN":1,"FP":0}
			// seems to use the same format than eimp
			

			String itemId = item.getString("T");
			Boolean isPDF = Boolean.parseBoolean(item.getString("PDF"));
			Boolean isID = Boolean.parseBoolean(item.getString("ID"));
			String row = item.getString("R");
			String section = item.getString("S");
			Integer quantity = item.getInt("Q");
			Double currentPrice = item.getDouble("P");
			
			// seats:
			// 1 - 14
			// * - *
			String seats = item.getString("SI");
			// System.out.println("SEATS=" + seats + ",start=" + startSeat + ",end=" + endSeat);
			
			
			// TS=1 // Active
			// TS=2 // Inactive
			// TS=3 // Pending 
			// TS=4 // Deleted 
			// TS=5 // Exchausted

			// if ticket is not active, skip it
			int ticketStatus = item.getInt("TS");
			if (ticketStatus != 1) {
				continue;
			}
			
			// lot size depends on quantity:
			// Odd Qty => 1
			// Even Qty => 2
			// Qty >= 14 => 1

			int lotSize = 1;
			if ((quantity % 2) == 0 && quantity < 14) {
				lotSize = 2;
			}

			TicketHit ticketHit = new TicketHit(Site.TICKETS_NOW, itemId, 
					TicketType.REGULAR,
					quantity, lotSize,
					row, section, currentPrice, currentPrice, "TicketsNow", TicketStatus.ACTIVE);
			ticketHit.setTicketListingCrawl(ticketListingCrawl);
			ticketHit.setSeat((seats == null)?null:seats);
			if(isID){
				ticketHit.setTicketDeliveryType(TicketDeliveryType.INSTANT);
			}else if(isPDF){
				ticketHit.setTicketDeliveryType(TicketDeliveryType.ETICKETS);
			}
			
			long startIndexationTime = System.currentTimeMillis();
			indexTicketHit(ticketHitIndexer, ticketHit);
			ticketListingCrawl.setIndexationTime(ticketListingCrawl.getIndexationTime() + System.currentTimeMillis() - startIndexationTime);							
		}

		ticketListingCrawl.setExtractionTime(System.currentTimeMillis() - startProcessingTime - ticketListingCrawl.getFetchingTime() - ticketListingCrawl.getIndexationTime());
		return true;
	}

	@Override
	public boolean isValidUrl(String url) {
		// valid url: http://www.ticketsnow.com/InventoryBrowse/Super-Bowl-Tickets-at-Raymond-James-Stadium-in-Tampa?PID=596701
		return url.matches("^http://www.ticketsnow.com/InventoryBrowse/.*\\?PID=.*$") ||  url.matches("^http://www.ticketexchangebyticketmaster.com/.*/InventoryBrowse.*\\?PID=.*$");
	}

	@Override
	public String getTicketListingUrl(TicketListingCrawl ticketListingCrawl) {
		return ticketListingCrawl.getQueryUrl();
	}

	@Override
	public String getTicketItemUrl(TicketListingCrawl ticketListingCrawl, Ticket ticket) {
		return "http://www.ticketsnow.com/checkout/initializeorder.aspx?ID=" + ticket.getItemId()+ "&ReqQty=" + ticket.getLotSize();
	}	
}
