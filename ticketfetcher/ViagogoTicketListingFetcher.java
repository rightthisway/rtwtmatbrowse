package com.admitone.tmat.ticketfetcher;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.enums.TicketType;
import com.admitone.tmat.indexer.TicketHitIndexer;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;
import com.admitone.tmat.utils.httpclient.UncompressedHttpEntity;

/**
 * Viagogo Ticket Listing Fetcher. 
 */
public class ViagogoTicketListingFetcher extends TicketListingFetcher {	
	private final Logger logger = LoggerFactory.getLogger(ViagogoTicketListingFetcher.class);
	private static final Pattern dataPattern = Pattern.compile("[$]create\\(Viagogo.ClientGrid, (.*), null, null, [$]get\\(", Pattern.MULTILINE);
	
	private static final Pattern seatNumberPattern = Pattern.compile("(\\d+)");

	private static final Pattern seatsPattern = Pattern.compile("infoSeating.*?span>(.+?)</span>", Pattern.DOTALL);
	private static final Pattern trPattern = Pattern.compile("(<tr.*?/tr>.*?/tr>)", Pattern.DOTALL);
	private static final Pattern trUidPattern = Pattern.compile("<tr uid=\"(\\d+)\"", Pattern.DOTALL);

	private static final Pattern selectPattern = Pattern.compile("ddlQuantity\">(.*)</select>", Pattern.DOTALL);
	private static final Pattern optionValuePattern = Pattern.compile("value=\"(\\d+)\"", Pattern.DOTALL);

	private static final Pattern notePattern = Pattern.compile("go_hidden go_extra_info_popup\">.*?<li>(.*?)</li>", Pattern.DOTALL);
	
	// http://www.viagogo.es/Entradas-Conciertos/Flamenco/Farruquito-Entradas/E-240981
	private static final Pattern siteExtensionPattern = Pattern.compile("http://www.viagogo.([a-z]*?)/");
	
	
	public ViagogoTicketListingFetcher() {
	}
		
	@Override
	public boolean runFetchTicketListing(SimpleHttpClient httpClient, TicketHitIndexer ticketHitIndexer, TicketListingCrawl ticketListingCrawl) throws Exception {
		
		if (!(ticketListingCrawl.getSiteId().equals(Site.VIAGOGO))) {
			throw new Exception(this.getClass().getName() + ".fetchTicketListing parameter must be a viagogo crawl");
		}	
		
		Matcher siteExtensionMatcher = siteExtensionPattern.matcher(ticketListingCrawl.getQueryUrl());
		siteExtensionMatcher.find();
		
		String siteExtension = siteExtensionMatcher.group(1);
		// System.out.println("SITE EXTENSION=" + siteExtension);

		Long startProcessingTime = System.currentTimeMillis();
		HttpGet httpGet = new HttpGet(ticketListingCrawl.getQueryUrl());
		httpGet.addHeader("Host", "www.viagogo." + siteExtension);		
		httpGet.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
		httpGet.addHeader("Accept-Encoding", "gzip,deflate");		
		HttpResponse response = httpClient.execute(httpGet);
		UncompressedHttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
		String content = EntityUtils.toString(entity);
		
		ticketListingCrawl.setFetchedByteCount(ticketListingCrawl.getFetchedByteCount() + entity.getReadByteCount());		
		ticketListingCrawl.setFetchingTime(System.currentTimeMillis() - startProcessingTime);

		Matcher dataMatcher = dataPattern.matcher(content);
		if (!dataMatcher.find()) {
			return true;
		}
		String jsonData = dataMatcher.group(1);
		
		//System.out.println("JSONDATA=" + jsonData);
		JSONObject jsonObject = new JSONObject(jsonData);
		
		if (jsonObject.getString("id").contains("eventListPanel")) {
			return true;
		}

		// http://www.viagogo.com/Sports-Tickets/Football/NFL/Regular-Season-NFL/Cleveland-Browns-Tickets/E-196255
		
		// format id: [price, section, row, unknown, unknown, unknown, quantity, id] 
		// [290.6366,"GA FLOOR","GA",1,1,null,5,"2841994"]		
		// or
		// price, section, row, unknown, unknown, quantity, unknown, id 
		// [123.0064,"416","N",1,6,null,10,"s416","2831575"]
		JSONArray jsonArray = jsonObject.getJSONArray("records");
				
		Map<String, TicketHit> ticketHitByItemId = new HashMap<String, TicketHit>();
				
		for (int i = 0 ; i < jsonArray.length() ; i++) {
			JSONArray item = (JSONArray)jsonArray.get(i);
			
			Double price = item.getDouble(0);
			String section = item.getString(1);
			String row = item.getString(2);
			
			// in case row = CAT 3, section becomes CAT 3 and row becomes null
			if (row != null && (row.toUpperCase().contains("CAT") || row.toUpperCase().contains("KAT"))) {
				section = row;
				row = null;
			}

			int  quantity = item.getInt(6);			
			String itemId = item.getString(item.length() - 1);			
															
			TicketHit ticketHit = new TicketHit(Site.VIAGOGO, itemId, 
					TicketType.REGULAR,
					quantity, quantity,
					row, section, price, price, "Viagogo", TicketStatus.ACTIVE);
			ticketHit.setTicketListingCrawl(ticketListingCrawl);

			//System.out.println("TICKET HIT=" + ticketHit);
			ticketHitByItemId.put(itemId, ticketHit);
		}

		String[] tokens = ticketListingCrawl.getQueryUrl().split("/");
		String eventId = tokens[tokens.length - 1].split("-")[1];
				
		List<TicketHit> ticketHits = new ArrayList<TicketHit>(ticketHitByItemId.values());
		int ticketPacketSize = 50;
		
		for(int i = 0 ; i < ticketHits.size() ; i+= ticketPacketSize) {
			HttpPost httpPost = new HttpPost("http://www.viagogo." + siteExtension + "/WebServices/EventDetailListingGridWS.asmx/GetEventListingRows");
			httpPost.addHeader("Host", "www.viagogo." + siteExtension);		
			httpPost.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
			httpPost.addHeader("Accept-Encoding", "gzip,deflate");		
			httpPost.addHeader("Content-Type", "application/json; charset=utf-8");
			httpPost.addHeader("Referer", ticketListingCrawl.getQueryUrl());
			
			String recordIDs = "\"" + ticketHits.get(i).getItemId() + "\"";
			for(int j = i + 1; j < i + ticketPacketSize && j < ticketHits.size() ; j++) {
				recordIDs += ",\"" + ticketHits.get(j).getItemId() + "\"";
			}
			
			// System.out.println("POSTDATA=" + "{\"eventID\":" + eventId + ",\"renderMode\":1,\"recordIDs\":[" + recordIDs+ "]}");
			httpPost.setEntity(new StringEntity("{\"eventID\":" + eventId + ",\"renderMode\":1,\"recordIDs\":[" + recordIDs+ "]}"));
			
			long startFetchingTime = System.currentTimeMillis();
			response = httpClient.execute(httpPost);
			entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
			content = StringEscapeUtils.unescapeJavaScript(EntityUtils.toString(entity));
			ticketListingCrawl.setFetchedByteCount(ticketListingCrawl.getFetchedByteCount() + entity.getReadByteCount());
			ticketListingCrawl.setFetchingTime(ticketListingCrawl.getFetchingTime() + System.currentTimeMillis() - startFetchingTime);
			
			Matcher trMatcher = trPattern.matcher(content);
			while(trMatcher.find()) {
				
				String tr = trMatcher.group(1);
				
				Matcher trUidMatcher = trUidPattern.matcher(tr);
				trUidMatcher.find();
				String itemId = trUidMatcher.group(1);
				
				TicketHit ticketHit = ticketHitByItemId.get(itemId);							

				Matcher selectMatcher = selectPattern.matcher(tr); 
				if (selectMatcher.find()) {				
					String select = selectMatcher.group(1);
					
					int minQuantity = ticketHit.getQuantity();
					Matcher optionValueMatcher = optionValuePattern.matcher(select);
					while(optionValueMatcher.find()) {
						minQuantity = Math.min(minQuantity, Integer.parseInt(optionValueMatcher.group(1)));
					}
					ticketHit.setLotSize(minQuantity);
				} else {
					// lot size:
					// if odd lotSize=1
					// otherwise lotSize=2

					if ((ticketHit.getQuantity() % 2) == 0) {
						ticketHit.setLotSize(2);
					} else {
						ticketHit.setLotSize(1);						
					}
				}

				String seats = null;		
				Matcher seatsMatcher = seatsPattern.matcher(tr);
				if (seatsMatcher.find()) {
					seats = seatsMatcher.group(1);
				}
				
				ticketHit.setSeat((seats == null)?null:seats);				

				long startIndexationTime = System.currentTimeMillis();
				indexTicketHit(ticketHitIndexer, ticketHit);
				ticketListingCrawl.setIndexationTime(ticketListingCrawl.getIndexationTime() + System.currentTimeMillis() - startIndexationTime);
				
				//System.out.println("TICKET HIT=" + ticketHit);
			}
			
		}
		ticketListingCrawl.setExtractionTime(System.currentTimeMillis() - startProcessingTime - ticketListingCrawl.getFetchingTime() - ticketListingCrawl.getIndexationTime());
		
		return true;
	}
	
	@Override
	public boolean isValidUrl(String url) {
		// valid url:
		// http://www.viagogo.com/Sports-Tickets/Hockey/NHL-Regular-Season/Los-Angeles-Kings-Tickets/E-144496
		return url.matches("^http://www.viagogo.[a-z]*?/.*$");		
	}

	@Override
	public String getTicketListingUrl(TicketListingCrawl ticketListingCrawl) {
		return ticketListingCrawl.getQueryUrl();
	}

	@Override
	public String getTicketItemUrl(TicketListingCrawl ticketListingCrawl, Ticket ticket) {
		// https://www.viagogo.com/secure/pipeline/buy/CheckSeatAvailability?ListingID=2304451&EventID=149616&viagogoUrl=/Sports-Tickets/Basketball/NBA/NBA-Regular-Season/Cleveland-Cavaliers-Tickets/E-149616&Quantity=2
		// extract eventId from url
		String[] tokens = ticketListingCrawl.getQueryUrl().split("/");
		String eventId = tokens[tokens.length - 1].split("-")[1];
		
		Matcher siteExtensionMatcher = siteExtensionPattern.matcher(ticketListingCrawl.getQueryUrl());
		siteExtensionMatcher.find();		
		String siteExtension = siteExtensionMatcher.group(1);
		
		String path = ticketListingCrawl.getQueryUrl().substring(("http://www.viagogo." + siteExtension).length());
		
		// need to extract listing id from the web page
		return "https://www.viagogo." + siteExtension + "/secure/pipeline/buy/CheckSeatAvailability?ListingID=" + ticket.getItemId() + "&EventID=" + eventId + "&viagogoUrl=" + path + "&Quantity=" + ticket.getLotSize();
	}	
}
