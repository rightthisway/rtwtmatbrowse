package com.admitone.tmat.ticketfetcher;

/**
 * Fetch Ticket Listing from Ebay Website.
 */
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.indexer.TicketHitIndexer;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;
import com.admitone.tmat.utils.httpclient.UncompressedHttpEntity;

public class EBayWebTicketListingFetcher extends TicketListingFetcher {	
	private String ebayListUrl = "http://tickets.shop.ebay.com/items/?_nkw=%KEYWORDS%&_sacat=1305&_trksid=m270.l1313&_pgn=%PAGE%";
	private String ebayListUrlWithDate = "http://tickets.shop.ebay.com/items/Tickets__%KEYWORDS%?_catref=1&_dmpt=US_Tickets_all_in_one&_fln=1&_sacat=1305&_ssov=1&_trksid=p3286.c0.m282&_rnglo_Event%2520Date=%MONTH%%2F%DAY%%2F%YEAR%&_rnghi_Event%2520Date=%MONTH%%2F%DAY%%2F%YEAR%&_pgn=%PAGE%";
	private final Logger logger = LoggerFactory.getLogger(EBayWebTicketListingFetcher.class);
	private Pattern ebayItemPattern = Pattern.compile("ttl\"><a href=\"[^ ]+hash=item([0-9a-f]+)");
	
	public EBayWebTicketListingFetcher() {
	}

	@Override
	public boolean runFetchTicketListing(SimpleHttpClient httpClient, TicketHitIndexer ticketHitIndexer, TicketListingCrawl ticketListingCrawl) throws Exception {
		if (!(ticketListingCrawl.getSiteId().equals(Site.EBAY))) {
			throw new Exception(this.getClass().getName() + ".fetchTicketListing parameter must be an ebay crawl");
		}		
		
		long startProcessingTime = System.currentTimeMillis();

		Integer page = 0;

		String baseUrl = getTicketListingUrl(ticketListingCrawl);

		while(!ticketListingCrawl.isCancelled()) {
			page++;
			String url = baseUrl.replaceAll(" ", "+");
			
			long startFetchingTime = System.currentTimeMillis();
			HttpGet httpGet = new HttpGet(url); 
			HttpResponse response = httpClient.execute(httpGet); 			
			UncompressedHttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());			
			String content = EntityUtils.toString(entity);
			ticketListingCrawl.setFetchedByteCount(ticketListingCrawl.getFetchedByteCount() + entity.getReadByteCount());
			ticketListingCrawl.setFetchingTime(ticketListingCrawl.getFetchingTime() + System.currentTimeMillis() - startFetchingTime);
			
			Matcher matcher = ebayItemPattern.matcher(content);
			while (!ticketListingCrawl.isCancelled() && matcher.find()) {
				// convert itemId from hex to decimal
				String hexItemId = matcher.group(1);
				String itemId = Long.toString(Long.parseLong(hexItemId,16));
				
				ticketListingCrawl.setItemIndexed(ticketListingCrawl.getItemIndexed() + 1);
				
				TicketHit ticketHit = getTicketItemFetcher().fetchTicketItem(ticketListingCrawl, itemId);
				if (ticketHit == null) {
					continue;
				}
				ticketHit.setTicketListingCrawl(ticketListingCrawl);
				ticketListingCrawl.setItemIndexed(ticketListingCrawl.getItemIndexed() + 1);
				long startIndexationTime = System.currentTimeMillis();
				indexTicketHit(ticketHitIndexer, ticketHit);
				ticketListingCrawl.setIndexationTime(ticketListingCrawl.getIndexationTime() + System.currentTimeMillis() - startIndexationTime);
			}
			
			if (content.indexOf("class=\"enabled\">Next</a>") < 0 || content.indexOf("class=\"disabled\">Next</a>") > 0) {
				break;
			}			
		}
			
		ticketListingCrawl.setExtractionTime(System.currentTimeMillis() - startProcessingTime - ticketListingCrawl.getFetchingTime() - ticketListingCrawl.getIndexationTime());		
		return true;
	}

	public boolean isValidUrl() {
		return true;
	}
	
	@Override
	public String getTicketListingUrl(TicketListingCrawl ticketListingCrawl) {
	/**	String baseUrl = ebayListUrl;

		String year = ticketListingCrawl.getExtraParameter("queryYear");
		String month = ticketListingCrawl.getExtraParameter("queryMonth");
		String day = ticketListingCrawl.getExtraParameter("queryDay");

		String query = ticketListingCrawl.getExtraParameter("queryString");
		if (year != null & month != null && day != null) {
			baseUrl = ebayListUrlWithDate;
			baseUrl = baseUrl.replaceAll("%YEAR%", year);
			baseUrl = baseUrl.replaceAll("%MONTH%", month);
			baseUrl = baseUrl.replaceAll("%DAY%", day);
			query = query.trim().replaceAll(" ", "-");
		} else {
			baseUrl = ebayListUrl;
			query = query.trim().replaceAll(" ", "+");
		}
		baseUrl = baseUrl.replaceAll("%KEYWORDS%", query);**/
		
		return ticketListingCrawl.getQueryUrl();
	}

	@Override
	public String getTicketItemUrl(TicketListingCrawl ticketListingCrawl, Ticket ticket) {
		return "http://shop.ebay.com?_nkw=" + ticket.getItemId();		
	}
}
