package com.admitone.tmat.ticketfetcher;

import java.net.InetAddress;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.util.EntityUtils;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.enums.TicketType;
import com.admitone.tmat.indexer.TicketHitIndexer;
import com.admitone.tmat.utils.eimarketplace.EIMPCredential;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;
import com.admitone.tmat.utils.httpclient.UncompressedHttpEntity;

/**
 * EiMarketPlace Ticket Listing Fetcher. 
 */
public class EIMarketPlaceTicketListingFetcher extends TicketListingFetcher {
	
	private EIMPCredential credential;
	
	//private static Pattern ticketDataPattern = Pattern.compile("<tr id=\"rptTicketList_ctl\\d+_ucTicketList_rptTicketList_ctl\\d+_trInventoryRow\".*?\">.*?lblQuantity\">(.*?)</span></span></strong></span></td>.*?lblSection\">(.*?)</span></span></span></td>.*?lblRow\">(.*?)</span></span></span></td>.*?lblSeats\">(.*?)</span></span></span></td>.*?lblWholesale\">(.*?)</span></span></strong></span></td>.*?lblRetail\">(.*?)</span></span></strong></span></td>.*?lblTNowRetail\">(.*?)</span></span></strong></span></td>.*?lblInHandDate\">(.*?)</span></span></strong></span></td>.*?lblBroker\">.*?'/brokers/brokercontact.cfm[?]b=(\\d+).*?onmouseout.*?title=\".*?\">(.*?)</a></span></span></span></td>",  Pattern.DOTALL);	
	private static Pattern ticketDataPattern = Pattern.compile("<tr id=\"rptTicketList_ctl\\d+_ucTicketList_rptTicketList_ctl\\d+_trInventoryRow\".*?\">.*?lblQuantity\">(.*?)</span></span></strong></span></td>.*?lblSection\">(.*?)</span></span></span></td>.*?lblRow\">(.*?)</span></span></span></td>.*?lblSeats\">(.*?)</span></span></span></td>.*?lblWholesale\">(.*?)</span></span></strong></span></td>.*?lblTNowRetail\">(.*?)</span></span></strong></span></td>.*?lblInHandDate\">(.*?)</span></span></strong></span></td>.*?lblBroker\">.*?'/brokers/brokercontact.cfm[?]b=(\\d+).*?onmouseout.*?title=\".*?\">(.*?)</a></span></span></span></td>",  Pattern.DOTALL);
	//private static final Pattern seatPattern = Pattern.compile("(\\d+)");

	public EIMarketPlaceTicketListingFetcher() {
	}	
	
	@Override
	protected SimpleHttpClient createHttpClient(TicketListingCrawl crawl) throws Exception {
		SimpleHttpClient httpClient = HttpClientStore.createHttpClient(Site.EI_MARKETPLACE, credential.getUsername(), credential.getPassword());
		// Takes two hit one for index page and one for login page
		return httpClient;
	}

	@Override
	public boolean runFetchTicketListing(SimpleHttpClient httpClient, TicketHitIndexer ticketHitIndexer, TicketListingCrawl ticketListingCrawl) throws Exception {

		if (!(ticketListingCrawl.getSiteId().equals(Site.EI_MARKETPLACE))) {
			throw new Exception(this.getClass().getName() + ".fetchTicketListing parameter must be an eimarketplace crawl");
		}

		Long startProcessingTime = System.currentTimeMillis();

		httpClient.setRedirectHandler(null);
		
//		int counter=1;  // EIMP Index page ... why we need to go to index page first??..
//		counter++;   // for Login page
		int counter=1;  // According to new logic now we dont need to go to index page..
//		FIXME:  Redirect on their side... 
		counter+=2;   
		HttpGet httpGet = new HttpGet(getTicketListingUrl(ticketListingCrawl));		
		httpGet.addHeader("Host", "www.eimarketplace.com");
		httpGet.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
		httpGet.addHeader("Accept-Encoding", "gzip,deflate");
		httpGet.addHeader("Referer", getTicketListingUrl(ticketListingCrawl));
		HttpResponse response = httpClient.execute(httpGet);
		
		// counter which says how many time we hit eimp's web site
		counter++; 
		String proxy="";
		Object obj=httpClient.getParams().getParameter(ConnRoutePNames.DEFAULT_PROXY);
		if(obj!=null){
			proxy= obj.toString();
			proxy=proxy.split(":")[1].replaceAll("//","");
		}else{
			proxy=InetAddress.getLocalHost().getHostName();
		}
		saveCounterHistory(ticketListingCrawl, counter,proxy);
		
		UncompressedHttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
		String content = EntityUtils.toString(entity);
		ticketListingCrawl.setFetchedByteCount(ticketListingCrawl.getFetchedByteCount() + entity.getReadByteCount());		
		
		ticketListingCrawl.setFetchingTime(System.currentTimeMillis() - startProcessingTime);
				
		Matcher ticketJSONMatcher = ticketDataPattern.matcher(content);
		if (!ticketJSONMatcher.find()) {
			return true;
		}		
		
		Map<String, Boolean> usedItemIds = new HashMap<String, Boolean>(); 
		
		while(ticketJSONMatcher.find()){
			
			Integer quantity = Integer.parseInt(ticketJSONMatcher.group(1).trim());
			String section = ticketJSONMatcher.group(2);
			String row = ticketJSONMatcher.group(3);
			String seats = ticketJSONMatcher.group(4).replaceAll("\\s+|\\t+|\\n+|\\r+", "").trim();
			NumberFormat numberFormat = NumberFormat.getInstance();
			
			Double wholesalePrice = numberFormat.parse(ticketJSONMatcher.group(5).trim().substring(1)).doubleValue();
			/*
			 * Retail price removed from page - Nov 8 2010
			 * 
			Double retail = numberFormat.parse(ticketJSONMatcher.group(6).trim().substring(1)).doubleValue();
			*/
//			Double tNowPrice = numberFormat.parse(ticketJSONMatcher.group(6).trim().substring(1)).doubleValue();
			
			Double retailPrice = wholesalePrice;
			
//			String inhanddate = ticketJSONMatcher.group(7);
			String brokerId = ticketJSONMatcher.group(8);
			String brokerName = ticketJSONMatcher.group(9);
			
			//System.out.println("Qty: " + quantity + " Section " + section + " Row " + row + " Seats " + seats + " Wholesale Price " + wholesalePrice + " Retail Price " + retail + " T Now Price" + tNowPrice + " In hand date " + inhanddate + " broker id: " + brokerId + " broker " + brokerName);

			String itemId = ticketListingCrawl.getExtraParameter("queryEventId") + "-" + brokerId + "-" + section + "-" + row;
			
			int i = 1;
			while (usedItemIds.containsKey(itemId)) {
				itemId = itemId.split("@")[0] + "@" + i;
				i++;
			}
			
			usedItemIds.put(itemId, true);
			
			TicketHit ticketHit = new TicketHit(Site.EI_MARKETPLACE, itemId,
					TicketType.REGULAR,
					quantity,
					quantity,
					row, section, wholesalePrice, retailPrice, brokerName, TicketStatus.ACTIVE);
			
			ticketHit.setTicketListingCrawl(ticketListingCrawl);
			
			ticketHit.setSeat((seats == null)?null:seats);

			Long startIndexationTime = System.currentTimeMillis();
			indexTicketHit(ticketHitIndexer, ticketHit);
			ticketListingCrawl.setIndexationTime(ticketListingCrawl.getIndexationTime() + System.currentTimeMillis() - startIndexationTime);
			
		}
		
//		
//		JSONArray ticketJSONArray = new JSONArray(ticketJSONMatcher.group(1));
//		
//		for (int i = 0 ; i < ticketJSONArray.length() ; i++) {
//			JSONObject ticketJSON = ticketJSONArray.getJSONObject(i);
//			String itemId = ticketJSON.getString("T");
//			int quantity = Integer.parseInt(ticketJSON.getString("Q"));
//			String section = ticketJSON.getString("S");
//			String row = ticketJSON.getString("R");
//			String brokerName = ticketJSON.getString("BSN");
//			// String brokerId = ticketJSON.getString("B");
//			
//			// seats:
//			// 1 - 14
//			// * - *
//			String seats = ticketJSON.getString("SI");
//
//			// from the js file: https://www.eimarketplace.com/MarketPlace/ExternalScripts/TicketList.js
//			// TS=1 // Active
//			// TS=2 // Inactive
//			// TS=3 // Pending 
//			// TS=4 // Deleted 
//			// TS=5 // Exchausted
//			int ticketStatus = ticketJSON.getInt("TS");
//			
//			// if ticket is not active, skip it
//			if (ticketStatus != 1) {
//				continue;
//			}
//			
//			double wholesalePrice  = Double.parseDouble(ticketJSON.getString("LP"));
//			// double retailPrice  = Double.parseDouble(ticketJSON.getString("P"));
//			double retailPrice  = wholesalePrice;
//
//			// String itemId = ticketListingCrawl.getExtraParameter("queryEventId") + "-" + brokerId + "-" + section + "-" + row;
//			TicketHit ticketHit = new TicketHit(Site.EI_MARKETPLACE, itemId,
//					TicketType.REGULAR,
//					quantity,
//					quantity,
//					row, section, wholesalePrice, retailPrice, brokerName, TicketStatus.ACTIVE);
//			
//			ticketHit.setSeat((seats == null)?null:seats);
//			
//			// System.out.println("TICKET HIT=" + ticketHit);
//			ticketHit.setTicketListingCrawl(ticketListingCrawl);			
//
//			Long startIndexationTime = System.currentTimeMillis();
//			indexTicketHit(ticketHitIndexer, ticketHit);						
//			ticketListingCrawl.setIndexationTime(ticketListingCrawl.getIndexationTime() + System.currentTimeMillis() - startIndexationTime);				
//		}
		
		ticketListingCrawl.setExtractionTime(System.currentTimeMillis() - startProcessingTime - ticketListingCrawl.getFetchingTime() - ticketListingCrawl.getIndexationTime());
		
		return true;
	}
	
	@Override
	public String getTicketListingUrl(TicketListingCrawl ticketListingCrawl) {
		String queryEventId = ticketListingCrawl.getExtraParameter("queryEventId");
		//http://www.eimarketplace.com/MarketPlace/Inventory/TicketList.aspx?p=984813
		return "http://www.eimarketplace.com/MarketPlace/Inventory/TicketList.aspx?p=" + queryEventId;
	}
	
	public void setCredential(EIMPCredential credential) {
		this.credential = credential;
	}

}