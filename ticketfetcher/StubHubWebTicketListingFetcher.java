package com.admitone.tmat.ticketfetcher;

import java.net.InetAddress;
import java.net.URI;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ProtocolException;
import org.apache.http.client.RedirectHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.CrawlerHistoryManagement;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.enums.TicketDeliveryType;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.enums.TicketType;
import com.admitone.tmat.indexer.TicketHitIndexer;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;
import com.admitone.tmat.utils.httpclient.UncompressedHttpEntity;

import flex.messaging.io.ArrayCollection;
import flex.messaging.io.MessageIOConstants;
import flex.messaging.io.amf.ASObject;
import flex.messaging.io.amf.client.AMFConnection;
/**
 * StubHub Ticket Listing FetcherdoubleSectionPattern
 * Get tickets from web listing only (don't get end date and notes)
 * 
 */
public class StubHubWebTicketListingFetcher extends TicketListingFetcher {	
	private final Logger logger = LoggerFactory.getLogger(StubHubWebTicketListingFetcher.class);
	
	private Pattern xbidPattern = Pattern.compile("qsObj\\['xbid'\\]\\s*=\\s*([0-9]+);");
	private Pattern cobrandIdPattern = Pattern.compile("qsObj\\['cobrand'\\]\\s*=\\s*([0-9]+);");

	private Pattern rowNumberPattern = Pattern.compile("^(\\d+).*$");
	private Pattern rowActuallyPattern = Pattern.compile("\\(Actually Row (\\d+)\\)");
	
	private Pattern totalPattern = Pattern.compile("<total>(.*?)</total>");
	private Pattern ticketPattern = Pattern.compile("<ticket>(.*?)</ticket>", Pattern.DOTALL);
	private Pattern elementPattern = Pattern.compile("([a-zA-Z_]*?)>(.*?)</.*?>");
	private Pattern sectionPattern = Pattern.compile("(\\d+)([-\\/\\s])^([a-zA-Z]{1})$");
	private Pattern doubleSectionColonPattern = Pattern.compile("^.+:(.+)$");
	private Pattern doubleSectionNumberDashPattern = Pattern.compile("^(\\d+)-\\d+$");
	private Pattern doubleSectionStringNumberDashPattern = Pattern.compile("^(.+)-(\\d+)$");

	public StubHubWebTicketListingFetcher() {
	}

	@Override
	public boolean runFetchTicketListing(SimpleHttpClient httpClient, TicketHitIndexer ticketHitIndexer, TicketListingCrawl ticketListingCrawl) throws Exception {
		if (!(ticketListingCrawl.getSiteId().equals(Site.STUB_HUB))) {
			throw new Exception(this.getClass().getName() + ".fetchTicketListing parameter must be a stubhub crawl");
		}		
		
		//System.out.println("START FETCHER at " +new Date());
		//
		// EXTRACT EVENT ID
		//
		
		String proxy="";
		Object obj=httpClient.getParams().getParameter(ConnRoutePNames.DEFAULT_PROXY);
		if(obj!=null){
			proxy= obj.toString();
			proxy=proxy.split(":")[1].replaceAll("//","");
		}else{
			proxy=InetAddress.getLocalHost().getHostName();
		}
		
		
		StubHubRedirectHandler redirectHandler = new StubHubRedirectHandler();
		httpClient.setRedirectHandler(redirectHandler);
		
		long startProcessingTime = System.currentTimeMillis();
		
		String eventId;
		
		String url = ticketListingCrawl.getQueryUrl();
		int eventIdPos = url.indexOf("event_id="); 
		if (eventIdPos > 0) {
			eventId = url.substring(eventIdPos + 9, url.length());
		} else {
			String[] tokens = url.split("-");
			eventId = tokens[tokens.length - 1].split("/")[0];
		}

		// 
		// CHECK IF THE EVENT IS ACTIVE OR NOT
		//
		
		CrawlerHistoryManagement crawlerHistoryManagement = new CrawlerHistoryManagement(); 
		long startFetchingTime = System.currentTimeMillis();
		HttpPost httpPost = new HttpPost("http://partnerfeed.stubhub.com/listingCatalog/select/?q=%2BstubhubDocumentType%3Aevent%0D%0A%2Bevent_id%3A"
				+ eventId + "%0D%0A%0D%0A&version=2.2&fl=active&wt=json");
		httpPost.addHeader("Host", "partnerfeed.stubhub.com");		
		httpPost.addHeader("Accept", "application/json, text/javascript, */*");
		
		HttpResponse response = httpClient.execute(httpPost);			
		UncompressedHttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());			
		String content = EntityUtils.toString(entity);
		
 		ticketListingCrawl.setFetchedByteCount(ticketListingCrawl.getFetchedByteCount() + entity.getReadByteCount());						
		ticketListingCrawl.setFetchingTime(ticketListingCrawl.getFetchingTime() + System.currentTimeMillis() - startFetchingTime);
		int counter=1;
		if (content.indexOf("\"active\":\"0\"") >= 0) {
//			throw new Exception("The event seems to have been deleted on StubHub. You might probably delete the crawl.");
			saveCounterHistory(ticketListingCrawl, counter,proxy);
			return true;
		}
		
		int itemsPerPage = 1000;
		
		//
		// FETCH TICKET IDS FROM WEB LISTING
		//
		startFetchingTime = System.currentTimeMillis();
		HttpGet httpGet = new HttpGet("http://www.stubhub.com/?event_id=" + eventId);
		httpGet.addHeader("Host", "www.stubhub.com");		
		httpGet.addHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5 ");
		httpGet.addHeader("Accept", "application/json, text/javascript, */*");
		httpGet.addHeader("Accept-Language", "en-us,en;q=0.5");
		httpGet.addHeader("Accept-Encoding", "gzip,deflate");
		httpGet.addHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
		counter++;
		crawlerHistoryManagement.setCounter(counter);
		crawlerHistoryManagement.setSuccessfulHit(new Date());
		response = httpClient.execute(httpGet);
		entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
		content = EntityUtils.toString(entity);		
		ticketListingCrawl.setFetchingTime(ticketListingCrawl.getFetchingTime() + System.currentTimeMillis() - startFetchingTime);
 		ticketListingCrawl.setFetchedByteCount(ticketListingCrawl.getFetchedByteCount() + entity.getReadByteCount());
 		
 		// if the ticket page is a flash application, fetch ticket using the AMF protocol
		if (content.contains("FlashJsContainer")) {
			counter++;
			saveCounterHistory(ticketListingCrawl, counter,proxy);
			return fetchTicketListingFromAMF(Integer.parseInt(eventId), ticketHitIndexer, ticketListingCrawl);			
		}
//		DAORegistry.getCrawlerHistoryManagementDAO().save(crawlerHistoryManagement);

		// Extract xbid		
		Matcher xbidMatcher = xbidPattern.matcher(content);
		String xbid = "";
		if (!xbidMatcher.find()) {
			// System.out.println("DO NOT FIND XBID FOR EVENT " + eventId + " IN TICKET LISTING");
		} else {
			xbid = xbidMatcher.group(1).trim();
		}

		// Extract cobrandId		
		Matcher cobrandIdMatcher = cobrandIdPattern.matcher(content);
		String cobrandId = "";
		if (!cobrandIdMatcher.find()) {
			//System.out.println("DO NOT FIND COBRAND FOR EVENT " + eventId + " IN TICKET LISTING");			
		} else {
			cobrandId = cobrandIdMatcher.group(1).trim();			
		}

		
		int webOffset = 1;
		int webTotal = -1;
		
		Pattern piggbackPattern = Pattern.compile("[\\\\|,|/|&]");
		Matcher piggbackMatcher ;
		
		do {			
			startFetchingTime = System.currentTimeMillis();
			httpPost = new HttpPost("http://www.stubhub.com/listingCatalog/getTicketsByEvent?limit=" + itemsPerPage + "&offset=" + webOffset + "&sortby=p&orderby=a&eventId="
					+ eventId + "&xbid=" + xbid + "&cobrandId=" + cobrandId);
			//System.out.println("URL=" + ("http://www.stubhub.com/listingCatalog/getTicketsByEvent?limit=" + itemsPerPage + "&offset=" + webOffset + "&sortby=p&orderby=a&eventId="
			//		+ eventId + "&xbid=" + xbid + "&cobrandId=" + cobrandId));
			ticketListingCrawl.setFetchingTime(ticketListingCrawl.getFetchingTime() + System.currentTimeMillis() - startFetchingTime);		
			httpPost.addHeader("Host", "www.stubhub.com");		
			httpPost.addHeader("Accept", "application/json, text/javascript, */*");
			counter++;
			response = httpClient.execute(httpPost);			
			entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
			
			content = EntityUtils.toString(entity);
			ticketListingCrawl.setFetchingTime(ticketListingCrawl.getFetchingTime() + System.currentTimeMillis() - startFetchingTime);
	 		ticketListingCrawl.setFetchedByteCount(ticketListingCrawl.getFetchedByteCount() + entity.getReadByteCount());						
						
			if (webTotal == -1) {
				Matcher totalMatcher = totalPattern.matcher(content);
				if (!totalMatcher.find()) {
					return true;
				}
				webTotal = Integer.parseInt(totalMatcher.group(1));
			}

			Matcher ticketMatcher = ticketPattern.matcher(content);
			while(ticketMatcher.find()) {
				String section = null;
				String row = null;
				Double price = null;
				Integer quantity = null;
				Integer quantity_remain = null;
				Integer split = null;
				String ticketId = null;
				TicketType ticketType = TicketType.REGULAR;
				TicketDeliveryType ticketDeliveryType = null;
				
				Matcher elementMatcher = elementPattern.matcher(ticketMatcher.group(1)); 
				while(elementMatcher.find()) {
					String elementName = elementMatcher.group(1).trim();
					String elementValue = elementMatcher.group(2).trim();
					
					if (elementName.equals("section")) {
						Matcher sectionMatcher=sectionPattern.matcher(elementValue);
						if (sectionMatcher.find()){
							section=sectionMatcher.group(1)+sectionMatcher.group(3);
						}else{
							section = elementValue;
						}
					} else if (elementName.equals("row")) {
						row = elementValue;
					} else if (elementName.equals("price")) {
						price = Double.parseDouble(elementValue);
					} else if (elementName.equals("quantity")) {
						quantity = Integer.parseInt(elementValue);
					} else if (elementName.equals("quantity_remain")) {
						quantity_remain = Integer.parseInt(elementValue);
					} else if (elementName.equals("split")) {
						split = Integer.parseInt(elementValue);
					} else if (elementName.equals("id")) {
						ticketId = elementValue;
					} else if (elementName.equals("sale_method")) {
						if (elementValue.equals("0")) {
							ticketType = TicketType.AUCTION;
						}
					} else if (elementName.equals("ticket_medium_id")) {
						if (elementValue.equals("3")) {
							ticketDeliveryType = TicketDeliveryType.INSTANT;
						}
					} else if (elementName.equals("delivery_icon") && ticketDeliveryType == null) {
						String deliveryIcon = elementValue;
						if (deliveryIcon.contains("edelivery")) {
							ticketDeliveryType = TicketDeliveryType.EDELIVERY;
						}
					}
				}
				//	/* cs1
				// exclude ticket if Piggy back locations (eg :Row-(5,4)) excluding char are [,\/]
				piggbackMatcher = piggbackPattern.matcher(row);
				if(piggbackMatcher.find()){
//					System.out.println(rowActuallyMatcher.group());
					continue;
				}
				//	cs1 */
				Double buyItNowPrice;
				if (ticketType == TicketType.AUCTION) {
					// for auction, there is no buyItNow price
					buyItNowPrice = null;
				} else {
					// otherwise the buyItNowPrice is the current price
					buyItNowPrice = price;
				}

				TicketHit ticketHit = new TicketHit(Site.STUB_HUB, ticketId,
						ticketType,
						quantity, split,
						row, section, price, buyItNowPrice, "StubHub", TicketStatus.ACTIVE);

				ticketHit.setSoldQuantity(quantity - quantity_remain);
				ticketHit.setTicketListingCrawl(ticketListingCrawl);
				ticketHit.setTicketDeliveryType(ticketDeliveryType);

				long startIndexationTime = System.currentTimeMillis();
				indexTicketHit(ticketHitIndexer, ticketHit);
				ticketListingCrawl.setIndexationTime(ticketListingCrawl.getIndexationTime() + System.currentTimeMillis() - startIndexationTime);

			}
			webOffset += itemsPerPage;

		} while (webOffset <= webTotal);
		saveCounterHistory(ticketListingCrawl, counter,proxy);
		ticketListingCrawl.setExtractionTime(System.currentTimeMillis() - startProcessingTime - ticketListingCrawl.getFetchingTime() - ticketListingCrawl.getIndexationTime());
		return true;
	}
	

	private boolean fetchTicketListingFromAMF(int eventId, TicketHitIndexer ticketHitIndexer, TicketListingCrawl ticketListingCrawl) throws Exception {
		//System.out.println("START AMF CALL=" + new Date());
		AMFConnection connection = new AMFConnection();
		connection.setInstantiateTypes(false);
		connection.connect("http://tbds.stubhub.com/eventBlazeAPI/messagebroker/amf");
		connection.setObjectEncoding(MessageIOConstants.AMF3);
		ASObject response = (ASObject)connection.call("product.getEventTicketListings", eventId);
		ArrayCollection ticketListings = (ArrayCollection)response.get("ticketListings");
		
		Pattern piggbackPattern = Pattern.compile("[\\\\|,|/]");
		Matcher piggbackMatcher ;
		for(int i = 0 ; i < ticketListings.size() ; i++) {
			ASObject ticket = (ASObject)ticketListings.get(i);
			// System.out.println("TICKET=" + ticket);
			
			String ticketId = "" + ((Double)ticket.get("listingId")).longValue();
			
			int quantity = ((Double)ticket.get("quantity")).intValue();
			Double currentPrice = ((Double)ticket.get("currentPrice"));
			
			int split;
			if (ticket.get("splitQuantity") != null) {
				split = ((Double)ticket.get("splitQuantity")).intValue();
			} else {
				split = quantity;
			}
			 
			String section = ((String)ticket.get("section"));
			
			// if there are two numbers in the section separated by colon(1:200), takes the second one(200)
			Matcher doubleSectionColonMatcher = doubleSectionColonPattern.matcher(section);
			if(doubleSectionColonMatcher.find()) {
//				System.out.println("CONVERT SECTION FROM " + section + " TO " + doubleSectionMatcher.group(1));
				section = doubleSectionColonMatcher.group(1);
			}
			
			// if there are two numbers in the section separated by dash(104-1), takes the first one(104)
			// if the section is NN-8, then the section is NN8
			Matcher doubleSectionNumberDashMatcher = doubleSectionNumberDashPattern.matcher(section);
			Matcher doubleSectionStringNumberDashMatcher = doubleSectionStringNumberDashPattern.matcher(section);
			Matcher sectionMatcher=sectionPattern.matcher(section);
			if(doubleSectionNumberDashMatcher.find()) {
				// System.out.println("CONVERT SECTION FROM " + section + " TO " + doubleSectionDashMatcher.group(1));
				section = doubleSectionNumberDashMatcher.group(1);
			}	else if(doubleSectionStringNumberDashMatcher.find()){
				section = doubleSectionStringNumberDashMatcher.group(1) + doubleSectionStringNumberDashMatcher.group(2);
			}else if (sectionMatcher.find()){
				String temp=sectionMatcher.group(3);
				if(temp.length()<=2){
					section=sectionMatcher.group(1)+temp;
				}
			}

			String row = ((String)ticket.get("rowDesc"));
			//   /* cs1
			// exclude ticket if Piggy back locations (eg :Row-(5,4)) excluding char are [,\/]
			piggbackMatcher = piggbackPattern.matcher(row);
			if(piggbackMatcher.find()){
//				System.out.println(rowActuallyMatcher.group());
				continue;
			}
			
			// */   cs1 
			Matcher rowActuallyMatcher = rowActuallyPattern.matcher(row);
			if (rowActuallyMatcher.find()) {
				row = rowActuallyMatcher.group(1);
				// System.out.println(">>>  TRANSFORM " + oldRow + "=>" + row);
			} else {
				// if row starts with a number, just keep the number and discard the rest			 
				Matcher rowNumberMatcher = rowNumberPattern.matcher(row);
				if (rowNumberMatcher.find()) {
					row = rowNumberMatcher.group(1);
					// System.out.println(">>>  TRANSFORM " + oldRow + "=>" + row);
				}
			}
			
			Boolean isElectronicTicket = (Boolean)ticket.get("isElectronicTicket");
			TicketDeliveryType ticketDeliveryType = null;
			if (isElectronicTicket != null && isElectronicTicket) {
				ticketDeliveryType = TicketDeliveryType.EDELIVERY;
			}
			
			Boolean isInstanceDelivery = (Boolean)ticket.get("isInstanceDelivery");
			if (isInstanceDelivery != null && isInstanceDelivery) {
				ticketDeliveryType = TicketDeliveryType.INSTANT;
			}

			TicketHit ticketHit = new TicketHit(Site.STUB_HUB, ticketId,
					TicketType.REGULAR,
					quantity, split,
					row, section, currentPrice, currentPrice, "StubHub", TicketStatus.ACTIVE);

			ticketHit.setTicketDeliveryType(ticketDeliveryType);
			String seats = ((String)ticket.get("seats"));			
			if(seats != null && !seats.contains("Not provided")) {
				try {
					String[] tokens = seats.split(",");
					Integer lowSeat = Integer.MAX_VALUE;
					Integer highSeat = Integer.MIN_VALUE;
					
					for(String token: tokens) {
						int seat = Integer.parseInt(token);
						if (seat < lowSeat) {
							lowSeat = seat;
						}
	
						if (seat > highSeat) {
							highSeat = seat;
						}
					}
					
					if (quantity == 1) {
						ticketHit.setSeat("" + lowSeat);						
					} else {
						ticketHit.setSeat(lowSeat + "-" + highSeat);
					}
				} catch (Exception e) {}
			}
			
			ticketHit.setTicketListingCrawl(ticketListingCrawl);

			long startIndexationTime = System.currentTimeMillis();
//			System.out.println("TICKET HIT=" + ticketHit);
			indexTicketHit(ticketHitIndexer, ticketHit);
			ticketListingCrawl.setIndexationTime(ticketListingCrawl.getIndexationTime() + System.currentTimeMillis() - startIndexationTime);
		}
		
		//System.out.println("END AMF CALL=" + new Date());
		connection.close();

		return true;		
	}



	@Override
	public boolean isValidUrl(String url) {
		// valid url: http://www.stubhub.com/mlb-all-star-game-tickets/mlb-all-star-strip-7-11-2009-750829/
		// http://www.stubhub.com/jimmy-buffett-tickets/jimmy-buffett-honolulu-waikiki-shell-2-28-2009-790072/
		// http://www.stubhub.com/super-bowl-tickets/super-bowl-2-1-2009-565762/
		// http://www.stubhub.com?q=eventId=62789
		return url.matches("^http://www.stubhub.com/.*[0-9]*/$") || url.matches("^http://www.stubhub.com/\\?event_id=\\d+$");		
	}

	@Override
	public String getTicketListingUrl(TicketListingCrawl ticketListingCrawl) {
		return "http://www.stubhub.com/largesellers-ticketcenter/";
		//return ticketListingCrawl.getQueryUrl();
	}
	
	@Override
	public String getTicketItemUrl(TicketListingCrawl ticketListingCrawl, Ticket ticket) {
		return "http://www.stubhub.com/largesellers-ticketcenter/";
//		if (ticket.getTicketListingCrawl().getQueryUrl().indexOf("?") >= 0) {
//			return ticket.getTicketListingCrawl().getQueryUrl() + "&ticket_id=" + ticket.getItemId();
//		}
//
//		return ticket.getTicketListingCrawl().getQueryUrl() + "?ticket_id=" + ticket.getItemId();
	}
	
	/**
	 * Redirect Handler Class 
	 */
	private class StubHubRedirectHandler implements RedirectHandler{
		private String redirectUrl;
		
		public StubHubRedirectHandler() {
		}

		public URI getLocationURI(HttpResponse response, HttpContext context)
				throws ProtocolException {	
			//System.out.println("REDIRECT URL=" + redirectUrl);
			if (redirectUrl.startsWith("http://")) {
				return URI.create(redirectUrl);
			} else {
				return URI.create("http://www.stubhub.com" + redirectUrl);				
			}
		}

		public boolean isRedirectRequested(HttpResponse response, HttpContext context) {
			Header[] headers = response.getAllHeaders();
			for(Header header: headers) {
				// System.out.println("Header=" + header.getName() + "=" + header.getValue());
				if (header.getName().equalsIgnoreCase("Location")) {
					redirectUrl = header.getValue();
					return true;
				}
			}
			
			return false;
		}

	}	
	

	
}