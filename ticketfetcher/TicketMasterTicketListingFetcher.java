package com.admitone.tmat.ticketfetcher;

import java.io.StringReader;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.xalan.xsltc.trax.SAX2DOM;
import org.apache.xpath.XPathAPI;
import org.ccil.cowan.tagsoup.Parser;
import org.json.JSONObject;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.enums.TicketType;
import com.admitone.tmat.indexer.TicketHitIndexer;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;
import com.admitone.tmat.utils.httpclient.UncompressedHttpEntity;

/**
 * This fetcher fetch ticket data either from the regular ticketmaster website
 * or the teamexchange ticketmaster website
 */
public class TicketMasterTicketListingFetcher extends TicketListingFetcher {
	private Pattern ticketMasterItemIDPattern = Pattern.compile("/exchange/[A-F0-9]+/([A-F0-9]+)';");
	private Pattern teamExchangeItemIDPattern = Pattern.compile("sellID=([0-9]+)&");
	private Pattern teamExchangeEventIdPattern = Pattern.compile("EVNT=([0-9A-Z]+)&");
	private Pattern teamExchangeTeamIdPattern = Pattern.compile("team=(\\w+)");
	private Pattern teamExchangeMinPricePattern = Pattern.compile("var minSlider = ([0-9]+);", Pattern.MULTILINE);
	private Pattern teamExchangeMaxPricePattern = Pattern.compile("var maxSlider = ([0-9]+);", Pattern.MULTILINE);

	private static final Pattern seatPattern = Pattern.compile("(\\d+)");

	public TicketMasterTicketListingFetcher() {
	}
		
	private boolean runFetchTicketMasterTicketListing(SimpleHttpClient httpClient, TicketHitIndexer ticketHitIndexer, TicketListingCrawl ticketListingCrawl) throws Exception {
		
		long startProcessingTime = System.currentTimeMillis();

		// FIXME: this request might not be necessary
		HttpGet httpGet = new HttpGet(ticketListingCrawl.getQueryUrl());
		httpGet.addHeader("Host", "www.ticketmaster.com");				
		httpGet.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
		httpGet.addHeader("Accept-Encoding", "gzip,deflate");
		HttpResponse response = httpClient.execute(httpGet);
		response.getEntity().consumeContent();		
		ticketListingCrawl.setFetchingTime(System.currentTimeMillis() - startProcessingTime);
		
		// tranform url:
		// http://www.ticketmaster.com/exchange/1B00416C7A862395?tm_link=edp_buytix
		// to:
		// http://www.ticketmaster.com/exchange/1B00416C7A862395?page=all&sort=price
		
		String[] tokens = ticketListingCrawl.getQueryUrl().split("[?]");
		String url = tokens[0] + "?page=all&sort=price";

		long startFetchingTime = System.currentTimeMillis();
		httpGet = new HttpGet(url);
		httpGet.addHeader("Host", "www.ticketmaster.com");				
		httpGet.addHeader("Accept", "application/json, text/javascript, */*");
		httpGet.addHeader("Accept-Encoding", "gzip,deflate");
		httpGet.addHeader("Referer", ticketListingCrawl.getQueryUrl());
		response = httpClient.execute(httpGet);
		UncompressedHttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
		String content = EntityUtils.toString(entity);
		ticketListingCrawl.setFetchedByteCount(ticketListingCrawl.getFetchedByteCount() + entity.getReadByteCount());								
		ticketListingCrawl.setFetchingTime(ticketListingCrawl.getFetchingTime() + System.currentTimeMillis() - startFetchingTime);

		Parser parser = new Parser();
		parser.setFeature("http://xml.org/sax/features/namespace-prefixes", true);
		// to define the html: prefix (off by default)
		SAX2DOM sax2dom = new SAX2DOM();
		parser.setContentHandler(sax2dom);
		
		parser.parse(new InputSource(new StringReader(content)));
					
		Node document = sax2dom.getDOM();
		
		String venueName = XPathAPI.selectSingleNode(document, "//html:a[@id='artist_venue']").getTextContent();
		
		// New Orleans, LA
		String location = XPathAPI.selectSingleNode(document, "//html:span[@id='artist_location']").getTextContent();
		String venueCity = location.split(",")[0].trim();
		String venueState = location.split(",")[1].trim();
		
		String venueDateString = XPathAPI.selectSingleNode(document, "//html:span[@id='artist_event_date']").getTextContent();
				
		// Tue, Mar 3, 2009 08:00 PM
		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM dd, yyyy hh:mm aa");			
		Date venueDate = dateFormat.parse(venueDateString);
		
		// System.out.println("VENUE CITY = " + venueCity);
		// System.out.println("VENUE STATE = " + venueState);
		// System.out.println("VENUE DATE = " + venueDate);						
		
		NodeList trNodeList = XPathAPI.selectNodeList(document, "//html:tbody[@id='ex_ticket_list']/html:tr");		
		
		// skip first tr as it contains the table title
		for (int i = 1 ; i < trNodeList.getLength() ; i++) {
			Node trNode = trNodeList.item(i);
			
			String section = XPathAPI.selectSingleNode(trNode, "html:td[@class='section-col']").getTextContent();
			String row = XPathAPI.selectSingleNode(trNode, "html:td[@class='row-col']").getTextContent();
			String seats = XPathAPI.selectSingleNode(trNode, "html:td[@class='seats-col']").getTextContent();
			int quantity = Integer.parseInt(XPathAPI.selectSingleNode(trNode, "html:td[@class='qty-col']").getTextContent());

			String priceString = XPathAPI.selectSingleNode(trNode, "html:td[@class='price-col']/html:strong").getTextContent();
			NumberFormat numberFormat = NumberFormat.getInstance();
	        Number number = numberFormat.parse(priceString.split("[$]")[1]);
	        Double price = number.doubleValue();

			String href = XPathAPI.selectSingleNode(trNode, "html:td[@class='button-col']/html:input/@onclick").getTextContent();
			Matcher itemIDMatcher = ticketMasterItemIDPattern.matcher(href);
			itemIDMatcher.find();
			String itemId = itemIDMatcher.group(1);

			// System.out.println("ITEM ID=" + itemId);
			// System.out.println("QUANTITY=" + quantity);
			// System.out.println("SEATS=" + seats);
			// System.out.println("ROW=" + row);
			// System.out.println("SECTION=" + section);
			// System.out.println("PRICE=" + price);

			TicketHit ticketHit = new TicketHit(Site.TICKET_MASTER, itemId, 
					TicketType.REGULAR,
					quantity, quantity,
					row, section, price, price, "TicketMaster", TicketStatus.ACTIVE);
			ticketHit.setTicketListingCrawl(ticketListingCrawl);
			
			ticketHit.setEventDate(venueDate);
			ticketHit.setVenueState(venueState);
			ticketHit.setVenueCity(venueCity);
			ticketHit.setVenueName(venueName);
			
			long startIndexationTime = System.currentTimeMillis();
			indexTicketHit(ticketHitIndexer, ticketHit);			
			ticketListingCrawl.setIndexationTime(ticketListingCrawl.getIndexationTime() + System.currentTimeMillis() - startIndexationTime);
		}
		
		ticketListingCrawl.setExtractionTime(System.currentTimeMillis() - startProcessingTime - ticketListingCrawl.getFetchingTime() - ticketListingCrawl.getIndexationTime());		
		return true;
	}

	private boolean runFetchTeamExchangeSeatListTicketListing(SimpleHttpClient httpClient, TicketHitIndexer ticketHitIndexer, TicketListingCrawl ticketListingCrawl) throws Exception {		
		// https://teamexchange.ticketmaster.com/html/seatlist.htmI?l=EN&team=raptors&EVNT=R090412&CNTX=
		// https://teamexchange.ticketmaster.com/html/seatlist.htmI?l=EN&team=wachovia&EVNT=PEN0410&CNTX=		

		long startProcessingTime = System.currentTimeMillis();

		HttpGet httpGet = new HttpGet(ticketListingCrawl.getQueryUrl() + "&showAll=1");
		httpGet.addHeader("Host", "teamexchange.ticketmaster.com");				
		httpGet.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
		httpGet.addHeader("Accept-Encoding", "gzip,deflate");
		HttpResponse response = httpClient.execute(httpGet);
		UncompressedHttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
		String content = EntityUtils.toString(entity);
		ticketListingCrawl.setFetchedByteCount(ticketListingCrawl.getFetchedByteCount() + entity.getReadByteCount());		
		ticketListingCrawl.setFetchingTime(System.currentTimeMillis() - startProcessingTime);

		Parser parser = new Parser();
		parser.setFeature("http://xml.org/sax/features/namespace-prefixes", true);
		// to define the html: prefix (off by default)
		SAX2DOM sax2dom = new SAX2DOM();
		parser.setContentHandler(sax2dom);		
		parser.parse(new InputSource(new StringReader(content)));
		
		Node document = sax2dom.getDOM();		
		NodeList trNodeList = XPathAPI.selectNodeList(document, "//html:table[@id='tableListing']/html:tr");
		
		for (int i = 1 ; i < trNodeList.getLength() ; i++) {
			NodeList tdNodeList = XPathAPI.selectNodeList(trNodeList.item(i), "./html:td");
			
			if (tdNodeList.getLength() < 6) {
				continue;
			}
			
			// section, row, seats, quantity, price
			String section = tdNodeList.item(0).getTextContent();
			String row = tdNodeList.item(1).getTextContent();
			
			// 11-12
			// 2
			String seats = tdNodeList.item(2).getTextContent();

			int quantity = Integer.parseInt(tdNodeList.item(3).getTextContent());
			
			String priceString = tdNodeList.item(4).getTextContent().split("[$]")[1];
	        NumberFormat numberFormat = NumberFormat.getInstance();
	        Number number = numberFormat.parse(priceString);
			Double price = number.doubleValue();
			
			// href: https://teamexchange.ticketmaster.com/html/buyer_login.htmI?l=EN&team=raptors&sellID=2764275&CNTX=
			String href = tdNodeList.item(5).getChildNodes().item(0).getAttributes().getNamedItem("href").getTextContent();
			Matcher itemIDMatcher = teamExchangeItemIDPattern.matcher(href);
			itemIDMatcher.find();
			String itemId = itemIDMatcher.group(1);
						
			TicketHit ticketHit = new TicketHit(Site.TICKET_MASTER, itemId, 
					TicketType.REGULAR,
					quantity, quantity,
					row, section, price, price, "TicketMaster", TicketStatus.ACTIVE);
			ticketHit.setTicketListingCrawl(ticketListingCrawl);

			ticketHit.setSeat((seats == null)?null:seats);

			long startIndexationTime = System.currentTimeMillis();
			indexTicketHit(ticketHitIndexer, ticketHit);
			ticketListingCrawl.setIndexationTime(ticketListingCrawl.getIndexationTime() + System.currentTimeMillis() - startIndexationTime);
		}
		
		ticketListingCrawl.setExtractionTime(System.currentTimeMillis() - startProcessingTime - ticketListingCrawl.getFetchingTime() - ticketListingCrawl.getIndexationTime());
		return true;
	}

	private boolean runFetchTeamExchangePostingListTicketListing(SimpleHttpClient httpClient, TicketHitIndexer ticketHitIndexer, TicketListingCrawl ticketListingCrawl) throws Exception {
		// https://teamexchange.ticketmaster.com/html/postinglist.htmI?l=EN&team=hawks&EVNT=EZPH0319&CNTX=
		
		long startProcessingTime = System.currentTimeMillis();
		
		Matcher eventIdMatcher = teamExchangeEventIdPattern.matcher(ticketListingCrawl.getQueryUrl());
		eventIdMatcher.find();
		String eventId = eventIdMatcher.group(1);

		Matcher teamIdMatcher = teamExchangeTeamIdPattern.matcher(ticketListingCrawl.getQueryUrl());
		teamIdMatcher.find();
		String teamId = teamIdMatcher.group(1);

		long startFetchingTime = System.currentTimeMillis();
		HttpGet httpGet = new HttpGet(ticketListingCrawl.getQueryUrl());
		httpGet.addHeader("Host", "teamexchange.ticketmaster.com");				
		httpGet.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
		httpGet.addHeader("Accept-Encoding", "gzip,deflate");
		HttpResponse response = httpClient.execute(httpGet);		

		UncompressedHttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
		String content = EntityUtils.toString(entity);
		ticketListingCrawl.setFetchedByteCount(ticketListingCrawl.getFetchedByteCount() + entity.getReadByteCount());		
		ticketListingCrawl.setFetchingTime(System.currentTimeMillis() - startFetchingTime);
		
		Matcher minPriceMatcher = teamExchangeMinPricePattern.matcher(content);
		minPriceMatcher.find();
		String minPrice = minPriceMatcher.group(1);

		Matcher maxPriceMatcher = teamExchangeMaxPricePattern.matcher(content);
		maxPriceMatcher.find();
		String maxPrice = maxPriceMatcher.group(1);
		
		int offset = 0;
		
		int itemCount = 0;
		int ticketCount = 0;
		
		// for (Cookie cookie: httpClient.getCookieStore().getCookies()) {
		// 	System.out.println("cookie " + cookie.getName() + "=" + cookie.getValue());
		// }
		
		do {
			startFetchingTime = System.currentTimeMillis();
			HttpPost httpPost = new HttpPost("https://teamexchange.ticketmaster.com/html/asynchronous/asynchronous_dynmap.htmI");
			httpPost.addHeader("Host", "teamexchange.ticketmaster.com");				
			httpPost.addHeader("Accept", "text/javascript, text/html, application/xml, text/xml, */*");
			httpPost.addHeader("Accept-Encoding", "gzip,deflate");
			httpPost.addHeader("X-Requested-With", "XMLHttpRequest");
			httpPost.addHeader("X-Prototype-Version", "1.5.1_rc1");
			httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
			httpPost.addHeader("Referer", ticketListingCrawl.getQueryUrl());
			
			// POST REQUEST:
			// EVNT: EZPH0319
			// action: updateAll
			// maxPrice: 770
			// minPrice: 10
			// nbrRecs	: 40
			// sPrice: 1
			// startingPos: 0
			// teamID: hawks
			
			String postData = "EVNT=" + eventId
				+ "&action=" + "updateAll"
				+ ((offset == 0)?"":"&globalNbrPosts=" + itemCount + "&globalNbrTickets=" + ticketCount)
				+ "&maxPrice=" + maxPrice
				+ "&minPrice=" + minPrice
				+ "&nbrRecs=" + 40
				+ "&sPrice=" + 1
				+ "&startingPos=" + offset
				+ "&teamID=" + teamId;
			// System.out.println("POSTDATA=" + postData);			
			
			httpPost.setEntity(new StringEntity(postData));
			response = httpClient.execute(httpPost);
			
			entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
			content = EntityUtils.toString(entity);
			ticketListingCrawl.setFetchedByteCount(ticketListingCrawl.getFetchedByteCount() + entity.getReadByteCount());			
			ticketListingCrawl.setFetchingTime(ticketListingCrawl.getFetchingTime() + System.currentTimeMillis() - startFetchingTime);
			
			// System.out.println("POST RESPONSE=" + content);
			
			JSONObject rootJsonObject = new JSONObject(content);			
			
			if (rootJsonObject.getString("NbrPostings").equals("null")) {
				break;
			}
			itemCount = Integer.parseInt(rootJsonObject.getString("NbrPostings")); 
			ticketCount = Integer.parseInt(rootJsonObject.getString("NbrTickets")); 

			JSONObject dataJsonObject = rootJsonObject.getJSONObject("DATA");
			Iterator<String> iterator = dataJsonObject.keys();			

			while(true) {
				String key;
				try {
					key = iterator.next();
				} catch (NoSuchElementException e) {
					break;
				}
				
				// if the key is a number, then it is a ticket
				if (!key.matches("^[0-9]+$")) {
					continue;
				}
				
				// {"sellID":"2871668","teamID":"hawks","section":"303","row":"F","firstSeat":"3","seatIncrement":"1",
				// "nbrSeats":"1","lastSeat":"3","priceBySeat":"22.0000","seatURL":"","sectionLabel":"Section",
				// "rowLabel":"Row","seatLabel":"Seat","gaIndicator":"N","sellerComment":""}
				JSONObject ticketJson = dataJsonObject.getJSONObject(key);
				String itemId = ticketJson.getString("sellID");
				String section = ticketJson.getString("section");
				String row = ticketJson.getString("row");
				int lotSize = Integer.parseInt(ticketJson.getString("seatIncrement"));
				int quantity = Integer.parseInt(ticketJson.getString("nbrSeats"));
				
				String firstSeat = ticketJson.getString("firstSeat");
				String lastSeat = ticketJson.getString("lastSeat");
				
				NumberFormat numberFormat = NumberFormat.getInstance();
		        Number number = numberFormat.parse(ticketJson.getString("priceBySeat"));
		        Double price = number.doubleValue();

				TicketHit ticketHit = new TicketHit(Site.TICKET_MASTER, itemId,
						TicketType.REGULAR,
						quantity, lotSize,
						row, section, price, price, "TicketMaster", TicketStatus.ACTIVE);
				ticketHit.setTicketListingCrawl(ticketListingCrawl);
				ticketHit.setSeat(firstSeat + "-" + lastSeat);
				
				long startIndexationTime = System.currentTimeMillis();
				indexTicketHit(ticketHitIndexer, ticketHit);			
				ticketListingCrawl.setIndexationTime(ticketListingCrawl.getIndexationTime() + System.currentTimeMillis() - startIndexationTime);
			}
			
			offset += 40;
		} while (offset < itemCount);		

		ticketListingCrawl.setExtractionTime(System.currentTimeMillis() - startProcessingTime - ticketListingCrawl.getFetchingTime() - ticketListingCrawl.getIndexationTime());		

		return true;
		
	}
	
	@Override
	public boolean runFetchTicketListing(SimpleHttpClient httpClient, TicketHitIndexer ticketHitIndexer, TicketListingCrawl ticketListingCrawl) throws Exception {
		if (!(ticketListingCrawl.getSiteId().equals("ticketmaster"))) {
			throw new Exception(this.getClass().getName() + ".fetchTicketListing parameter must be a ticketmaster crawl");
		}		
		
		if (ticketListingCrawl.getQueryUrl().startsWith("http://www.ticketmaster.com/exchange/")) {
			return runFetchTicketMasterTicketListing(httpClient, ticketHitIndexer, ticketListingCrawl);			
		} else if (ticketListingCrawl.getQueryUrl().startsWith("https://teamexchange.ticketmaster.com/html/seatlist.htmI")) {
			return runFetchTeamExchangeSeatListTicketListing(httpClient, ticketHitIndexer, ticketListingCrawl);						
		} else {
			return runFetchTeamExchangePostingListTicketListing(httpClient, ticketHitIndexer, ticketListingCrawl);						
		}
	}

	@Override
	public boolean isValidUrl(String url) {
		// valid url: 
		// http://www.ticketmaster.com/exchange/1B00416C7A862395?tm_link=edp_buytix
		if (url.matches("^http://www.ticketmaster.com/exchange/.*tm_link=edp_buytix$")) {
			return true;
		}
		
		// or
		// https://teamexchange.ticketmaster.com/html/seatlist.htmI?l=EN&team=raptors&EVNT=R090412&CNTX=
		// https://teamexchange.ticketmaster.com/html/postinglist.htmI?l=EN&team=hawks&EVNT=EZPH0319&CNTX=		
		return url.matches("^https://teamexchange.ticketmaster.com/html/.*&team=.*$");		
	}

	@Override
	public String getTicketListingUrl(TicketListingCrawl ticketListingCrawl) {
		return ticketListingCrawl.getQueryUrl();
	}	

	@Override
	public String getTicketItemUrl(TicketListingCrawl ticketListingCrawl, Ticket ticket) {
		// for the regular ticketmaster site:
		if (ticketListingCrawl.getQueryUrl().matches("^http://www.ticketmaster.com/exchange/.*tm_link=edp_buytix$")) {
			return ticket.getTicketListingCrawl().getQueryUrl().split("[?]")[0] + "/" + ticket.getItemId();
		} else {
			// for teamexchange website
			// https://teamexchange.ticketmaster.com/html/buyer_login.htmI?l=EN&team=raptors&sellID=2229939&CNTX=
			
			// Pattern teamIdPattern = Pattern.compile("^.*team=(\\w+)&.*$");
			// Matcher teamIdMatcher = teamIdPattern.matcher(ticketListingCrawl.getQueryUrl());
			// teamIdMatcher.find();
			// String teamId = teamIdMatcher.group(1);			
			// return "https://teamexchange.ticketmaster.com/html/buyer_login.htmI?l=EN&team=" + teamId + "&sellID=" + ticket.getItemId() + "&CNTX=";
			
			// it uses the referrer field, so we cannot do anything here
			return null;			
		}
		
	}		
}
