package com.admitone.tmat.ticketfetcher;

import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.security.GeneralSecurityException;
import java.text.NumberFormat;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.enums.TicketDeliveryType;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.enums.TicketType;
import com.admitone.tmat.indexer.TicketHitIndexer;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;


/**
 * Ticket Evolution Ticket Listing Fetcher. 
 */
public class TicketEvolutionTicketListingFetcher extends TicketListingFetcher {
	
	
	private  String token="c517a07c3875093fed0c789df1aa0460";
	private String secrete="5nSPDnk4chgthfVsQDNO9vUqL+JFG3rjfqk+G5ME";	

	public TicketEvolutionTicketListingFetcher() {
	}	
	
	@Override
	protected SimpleHttpClient createHttpClient(TicketListingCrawl crawl) throws Exception {
		SimpleHttpClient httpClient = new SimpleHttpClient(Site.TICKET_EVOLUTION);
		// Takes two hit one for index page and one for login page
		return httpClient;
	}

	@Override
	public boolean runFetchTicketListing(SimpleHttpClient httpClient, TicketHitIndexer ticketHitIndexer, TicketListingCrawl ticketListingCrawl) throws Exception {

		if (!(ticketListingCrawl.getSiteId().equals(Site.TICKET_EVOLUTION))) {
			throw new Exception(this.getClass().getName() + ".fetchTicketListing parameter must be an ticketevolution crawl");
		}

		Long startProcessingTime = System.currentTimeMillis();

		httpClient.setRedirectHandler(null);

		int counter=1;  // According to new logic now we dont need to go to index page..
//		FIXME:  Redirect on their side... 
//		counter+=2;  
		//System.out.println(ticketListingCrawl.getQueryUrl().split("/events/")[1]);
		String ticketGroupURL="https://api.ticketevolution.com/v9/ticket_groups?event_id="+ticketListingCrawl.getQueryUrl().split("/events/")[1];
		try{
		//url for ticket group api.sandbox.ticketevolution.com/ticket_groups?event_id=114196
			String proxy="";
			Object obj=httpClient.getParams().getParameter(ConnRoutePNames.DEFAULT_PROXY);
			if(obj!=null){
				proxy= obj.toString();
				proxy=proxy.split(":")[1].replaceAll("//","");
			}else{
				proxy=InetAddress.getLocalHost().getHostName();
			}
			saveCounterHistory(ticketListingCrawl, counter,proxy);
		String ticketGroupConent = 	hitTicketEvolution(httpClient,ticketGroupURL);
		//System.out.println(ticketGroupConent);
		JSONObject ticketGroupJson = new JSONObject(ticketGroupConent);
				
			if(!ticketGroupJson.isNull("ticket_groups")){
			JSONArray ticketGroupArray =(JSONArray)ticketGroupJson.get("ticket_groups");				
			if(ticketGroupArray!=null && ticketGroupArray.length()>0){
				
				for(int index=0;index<ticketGroupArray.length();index++){
					JSONObject ticketJson= (JSONObject)ticketGroupArray.get(index);					
					
					if(!ticketJson.isNull("quantity") && !"0".equals(ticketJson.getString("quantity"))){
						Integer quantity = Integer.parseInt(ticketJson.getString("quantity"));
						String section = ticketJson.getString("section");
						String row =  ticketJson.getString("row");
						NumberFormat numberFormat = NumberFormat.getInstance();
						int lotSize = 1;
						if ((quantity % 2) == 0 && quantity < 14) {
							lotSize = 2;
						}
						Double wholesalePrice = numberFormat.parse(ticketJson.getString("wholesale_price")).doubleValue();
						//Double retailPrice = numberFormat.parse(ticketJson.getString("retail_price")).doubleValue();
						//retail price should be wholesale price,for browse ticket page
						Double retailPrice = wholesalePrice;
						String brokerId = ticketJson.getJSONObject("office").getJSONObject("brokerage").getString("id");
						String brokerName = ticketJson.getJSONObject("office").getJSONObject("brokerage").getString("name");
						
						//System.out.println("Qty: " + quantity + " Section " + section + " Row " + row + " Seats " + seats + " Wholesale Price " + wholesalePrice + " Retail Price " + retail + " T Now Price" + tNowPrice + " In hand date " + inhanddate + " broker id: " + brokerId + " broker " + brokerName);

						String itemId = "TE" + ticketJson.getString("id");
						String seats = ticketJson.getString("seats");
						if(seats!=null){
							seats=seats.replaceAll("\\[", "").replaceAll("\\]", "").replaceAll("\"", "");
							if(!seats.equals("")){
								String[] temp=seats.split(",");
								int high=0;
								int low=0;
								for(String seat:temp){
									int tempSeat =Integer.parseInt(seat) ;
									if(low == 0 || low>tempSeat){
										low=tempSeat;
									}
									if(high==0 || high<tempSeat){
										high=tempSeat;
									}
									
								}
								if(high==low){
									seats=high+"";
								}else{
									seats=low+"-"+high;
								}
							}
						}else{
							seats="";
						}
						boolean isETicket=ticketJson.getBoolean("eticket");
						
						
						TicketHit ticketHit = new TicketHit(Site.TICKET_EVOLUTION, itemId,
								TicketType.REGULAR,
								quantity,
								lotSize,
								row, section, wholesalePrice, retailPrice, brokerName, TicketStatus.ACTIVE);
						
						ticketHit.setTicketListingCrawl(ticketListingCrawl);
						ticketHit.setSeat(seats);
						
						if(isETicket){
							ticketHit.setTicketDeliveryType(TicketDeliveryType.ETICKETS);
						}
						
						Long startIndexationTime = System.currentTimeMillis();
						indexTicketHit(ticketHitIndexer, ticketHit);
						ticketListingCrawl.setIndexationTime(ticketListingCrawl.getIndexationTime() + System.currentTimeMillis() - startIndexationTime);
					}
					}
					
				}
			}
			
	     	
	}catch (Exception e) {
			e.printStackTrace();
	}
		
		ticketListingCrawl.setExtractionTime(System.currentTimeMillis() - startProcessingTime - ticketListingCrawl.getFetchingTime() - ticketListingCrawl.getIndexationTime());
		
		return true;
		// counter which says how many time we hit ticketevolution web site
		
		
	}
	//common method to hit url using httpget method and return response content
	public String hitTicketEvolution(HttpClient httpClient, String searchUrl)
			throws GeneralSecurityException, UnsupportedEncodingException {
			String content = "";
			String toSignIn = coumputeSignInURL(searchUrl);
			String signature = computeSignature(toSignIn,secrete);
			HttpGet httpGet = new HttpGet(searchUrl);
			httpGet.addHeader("Host", "api.ticketevolution.com");
			httpGet.addHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
			httpGet.addHeader("Accept", "application/vnd.ticketevolution.api+json; version=8");				
			httpGet.addHeader("X-Signature", signature);
			httpGet.addHeader("Accept-Encoding", "gzip,deflate");
			httpGet.addHeader("X-Token", token);
			httpGet.addHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
			httpGet.addHeader("Referer", "api.ticketevolution.com");
			try{				
				HttpResponse response = httpClient.execute(httpGet);
				HttpEntity ticeketevolutionEntity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
				content= EntityUtils.toString(ticeketevolutionEntity);
				//System.out.println(content);
			}
				catch (Exception e) {
					e.printStackTrace();
				}
			return content;
}
	//compute signature for each request 
	public  String computeSignature(String baseString, String keyString) throws GeneralSecurityException, UnsupportedEncodingException {

		SecretKey signingKey = new SecretKeySpec(keyString.getBytes(), "HMACSHA256");  
        Mac mac = Mac.getInstance("HMACSHA256");  
        mac.init(signingKey);  
        byte[] digest = mac.doFinal(baseString.getBytes("UTF-8"));     //output of sha256  
        
        //1  
        String encoded64 = new String(Base64.encodeBase64(digest));   // using it as an input  
        //System.out.println("direct base64 : "+encoded64);
        return encoded64;
	}
	
	public String coumputeSignInURL(String url){
		return url.replace("https://", "GET ");		
	}
	@Override
	public String getTicketItemUrl(TicketListingCrawl ticketListingCrawl,
			Ticket ticket) {
		// TODO Auto-generated method stub
		return super.getTicketItemUrl(ticketListingCrawl, ticket);
	}
		
	

}