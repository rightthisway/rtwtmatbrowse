package com.admitone.tmat.ticketfetcher;
/*
package com.yoonew.tixmarket.fetcher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

import com.ebay.sdk.ApiContext;
import com.ebay.sdk.call.GetSearchResultsCall;
import com.ebay.soap.eBLBaseComponents.DateType;
import com.ebay.soap.eBLBaseComponents.DetailLevelCodeType;
import com.ebay.soap.eBLBaseComponents.ItemType;
import com.ebay.soap.eBLBaseComponents.PaginationType;
import com.ebay.soap.eBLBaseComponents.SearchResultItemType;
import com.ebay.soap.eBLBaseComponents.SiteCodeType;
import com.ebay.soap.eBLBaseComponents.TicketDetailsType;
import com.yoonew.tixmarket.crawler.TicketListingCrawl;
import com.yoonew.tixmarket.dao.DAORegistry;
import com.yoonew.tixmarket.data.Ticket;
import com.yoonew.tixmarket.enums.TicketStatus;
import com.yoonew.tixmarket.indexer.TicketHitIndexer;

public class EBayTicketListingFetcher extends TicketListingFetcher implements InitializingBean {	
	private final Logger logger = LoggerFactory.getLogger(EBayTicketListingFetcher.class);

	private ApiContext ebayApiContext;

	public EBayTicketListingFetcher() {
	}
		
	@Override
	public void afterPropertiesSet() throws Exception {
		if (ebayApiContext == null) {
			throw new Exception("Property ebayApiContext must be set");
		}
	}

	@Override
	public boolean fetchTicketListing(TicketHitIndexer ticketHitIndexer, TicketListingCrawl ticketListingCrawl) throws Exception {

		if (!(ticketListingCrawl.getSiteId().equals(Site.EBAY))) {
			throw new Exception(this.getClass().getName() + ".fetchTicketListing parameter must be an ebay crawl");
		}		
		
		super.fetchTicketListing(ticketHitIndexer, ticketListingCrawl);
				
		// do not use detail DetailLevelCodeType.ITEM_RETURN_DESCRIPTION,it is too huge (a lot of HTML code)
		// DetailLevelCodeType.RETURN_ALL,

		DetailLevelCodeType[] searchDetailLevels = new DetailLevelCodeType[] {};		

		// these lines are to force SearchResult to return also the attributes (row, section, ...)
		TicketDetailsType ticketDetailsType = new TicketDetailsType();

		DateType dateType = new DateType();

		if (ticketListingCrawl.getExtraParameter("queryYear") != null) {
			Integer year = null;
			try {
				year = Integer.parseInt(ticketListingCrawl.getExtraParameter("queryYear"));
			} catch (Exception e) {
				// e.printStackTrace();
			}
			dateType.setYear(year);			
		}

		if (ticketListingCrawl.getExtraParameter("queryMonth") != null) {
			Integer month = null;
			try {
				month = Integer.parseInt(ticketListingCrawl.getExtraParameter("queryMonth"));
			} catch (Exception e) {
				// e.printStackTrace();
			}
			dateType.setMonth(month);			
		}
		
		if (ticketListingCrawl.getExtraParameter("queryDay") != null) {
			Integer day = null;
			try {
				day = Integer.parseInt(ticketListingCrawl.getExtraParameter("queryDay"));
			} catch (Exception e) {
				// e.printStackTrace();
			}
			dateType.setDay(day);			
		}
			
		ticketDetailsType.setEventDate(dateType);

		int pageNumber = 1;
		int pageCount = Integer.MAX_VALUE;
		
		
		while(pageNumber <= pageCount) {
			if (!running) {
				return false;
			}
			
			GetSearchResultsCall searchResultCall = new GetSearchResultsCall(ebayApiContext);
			searchResultCall.setSite(SiteCodeType.US);

			searchResultCall.setCategoryID("16122"); // category id of event ticket
			searchResultCall.setDetailLevel(searchDetailLevels);
			searchResultCall.setQuery(ticketListingCrawl.getExtraParameter("queryString"));

			searchResultCall.setTicketFinder(ticketDetailsType);

			PaginationType paginationType = new PaginationType();
			paginationType.setEntriesPerPage(100);
			paginationType.setPageNumber(pageNumber);
			
			searchResultCall.setPagination(paginationType);
			SearchResultItemType[] results = searchResultCall.getSearchResults();
			
			pageCount = searchResultCall.getResultPagination().getTotalNumberOfPages();

			logger.info("Fetched page " + pageNumber + "/" +  pageCount);

			if (results == null || results.length == 0) {
				logger.info("No search results for " + ticketListingCrawl.getId());
				return true;
			}
						
			for (SearchResultItemType resultItemType: results) {
				
				if (!running) {
					return false;
				}
								
				
				ItemType item = resultItemType.getItem();
				
				if (item == null) {
					continue;
				}
				
				// check if the item was already indexed,if so we only need to update the price
				// and no need to do a costly API call
				Ticket ticket = DAORegistry.getTicketDAO().loadById(ticketListingCrawl.getSiteId() + "-" + item.getItemID());
				
				TicketHit ticketHit = null;

				if (ticket != null) {
					double currentPrice = item.getSellingStatus().getCurrentPrice().getValue() / ticket.getQuantity();
					ticketHit = new TicketHit(ticket);
					ticketHit.setCurrentPrice(currentPrice);
					ticketHit.setTicketStatus(TicketStatus.ACTIVE);
					if (item.getSeller() != null) {
						ticketHit.setSeller(item.getSeller().getUserID());
					}
				} else {
					ticketHit = getTicketItemFetcher().fetchTicketItem(item.getItemID());
				}
				ticketHit.setTicketListingCrawl(ticketListingCrawl);

				indexTicketHit(ticketHitIndexer, ticketHit);
			}
			
			pageNumber++;			
		}
		return true;
	}

	public ApiContext getEbayApiContext() {
		return ebayApiContext;
	}

	public void setEbayApiContext(ApiContext ebayApiContext) {
		this.ebayApiContext = ebayApiContext;
	}
	
	public boolean isValidUrl() {
		return true;
	}
	
}
*/