package com.admitone.tmat.ticketfetcher;

import java.io.StringReader;
import java.net.URLDecoder;
import java.text.NumberFormat;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.xalan.xsltc.trax.SAX2DOM;
import org.apache.xpath.XPathAPI;
import org.ccil.cowan.tagsoup.Parser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.enums.TicketType;
import com.admitone.tmat.indexer.TicketHitIndexer;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;
import com.admitone.tmat.utils.httpclient.UncompressedHttpEntity;

/**
 * Fetcher to extract data from TicketNetwork website.
 * The ticket information are fetched through an AJAX call which returns the HTML code of the ticket listing table. 
 */
public class TicketNetworkTicketListingFetcher extends TicketListingFetcher {	
	private final Logger logger = LoggerFactory.getLogger(FanSnapTicketListingFetcher.class);
	private Pattern ticketNetworkEventIdPattern = Pattern.compile("-([0-9]+)\\.aspx");
	//private Pattern ticketNetworkDetailPattern = Pattern.compile("Details:(.*)");
	private Pattern ticketNetworkItemIDPattern = Pattern.compile("&tgid=([0-9]+)&");

//	private Pattern zeroMarkupTrPattern = Pattern.compile("(?:tn_results_standard_row|tn_results_alternate_row)\">(.*?</table>.*?)</tr>",  Pattern.DOTALL);
//	private Pattern zeroMarkupSectionPattern = Pattern.compile("tn_results_ticket_section_text\">(.*?)</span>");
//	private Pattern zeroMarkupRowPattern = Pattern.compile("tn_results_ticket_row_text\">(.*?)</span>");
//	// there can be some carriage returns in some notes
//	//private Pattern zeroMarkupNotePattern = Pattern.compile("tn_results_ticket_notes\">(.*?)</td>", Pattern.DOTALL);
//	private Pattern zeroMarkupPricePattern = Pattern.compile("tn_results_ticket_retail\">(.*?)</td>");
//	private Pattern zeroMarkupOptionPattern = Pattern.compile("<option>(\\d+)</option>");
//	private Pattern zeroMarkupItemIdPattern = Pattern.compile("SubmitPurchaseLink\\('\\w+', '(.+)'\\)");
	private Pattern zeroMarkupResultsNotFoundPattern = Pattern.compile("class=\"tn_results_notfound\"");
	
	//private Pattern zeroMarkupTicGrpPattern = Pattern.compile("ticGrps \\+= \">(.*?)<\"",  Pattern.DOTALL);
	
	Pattern zeroMarkupTicGrpPattern = Pattern.compile("var ticGrps = \"<>.<\"(.*?)</script><table class=\"tn_results_header\">",  Pattern.DOTALL);
	   
	Pattern zeroMarkupTicPattern = Pattern.compile("\\+.*?\">(.*?)<\"",  Pattern.DOTALL);

	public TicketNetworkTicketListingFetcher() {
	}
		
	private boolean runFetchTicketNetworkTicketListing(DefaultHttpClient httpClient, TicketHitIndexer ticketHitIndexer, TicketListingCrawl ticketListingCrawl) throws Exception {		
		if (!(ticketListingCrawl.getSiteId().equals(Site.TICKET_NETWORK))) {
			throw new Exception(this.getClass().getName() + ".fetchTicketListing parameter must be a ticketnetwork crawl");
		}
		
		long startProcessingTime = System.currentTimeMillis();
		
		Matcher eventIdMatcher = ticketNetworkEventIdPattern.matcher(ticketListingCrawl.getQueryUrl());
		eventIdMatcher.find();
		String eventId = eventIdMatcher.group(1).trim();
		
		// the data is in the ajax call:
		// http://www.ticketnetwork.com/ajax/ajxRTCall.aspx?fn=resultsTicket&evtID=975896

		HttpGet httpGet = new HttpGet("http://www.ticketnetwork.com/ajax/ajxRTCall.aspx?fn=resultsTicket&evtID=" + eventId);
		httpGet.addHeader("Host", "www.ticketnetwork.com");
		httpGet.addHeader("Accept", "application/json, text/javascript, */*");
		httpGet.addHeader("Accept-Encoding", "gzip,deflate");
		httpGet.addHeader("Referer", ticketListingCrawl.getQueryUrl());		
		HttpResponse response = httpClient.execute(httpGet);
		UncompressedHttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
		String content = EntityUtils.toString(entity);
		ticketListingCrawl.setFetchedByteCount(ticketListingCrawl.getFetchedByteCount() + entity.getReadByteCount());		
		ticketListingCrawl.setFetchingTime(System.currentTimeMillis() - startProcessingTime);		
		
		Parser parser = new Parser();
		parser.setFeature("http://xml.org/sax/features/namespace-prefixes", true);
		// to define the html: prefix (off by default)
		SAX2DOM sax2dom = new SAX2DOM();
		parser.setContentHandler(sax2dom);

		parser.parse(new InputSource(new StringReader(content)));

		Node document = sax2dom.getDOM();

		NodeList trNodes = XPathAPI.selectNodeList(document, "//html:tr[contains(@class,'even') or contains(@class,'odd')]");
		for (int i = 0 ; i < trNodes.getLength() ; i++) {
			Node node = trNodes.item(i);
			NodeList tdNodes = node.getChildNodes();
			if (tdNodes.getLength() < 4) {
				continue;
			}
			
			// TD 0: we can get the section and row from the first cell
			// <div><b>Section:</b> 400-LEVEL <b>Row:</b> CORNER</div>
			// or if there is a star, there is a <div class="tixStar"/>
			Node sectionRowNode = tdNodes.item(0).getChildNodes().item(0);
			if (sectionRowNode.getAttributes().getNamedItem("class") != null 
					&& sectionRowNode.getAttributes().getNamedItem("class").getTextContent().equals("tixStar")) {
				// skip it and get the following div
				sectionRowNode = tdNodes.item(0).getChildNodes().item(1);
			}
			NodeList sectionRowNodeList = sectionRowNode.getChildNodes();
			String section = sectionRowNodeList.item(1).getTextContent().trim();

			String row = sectionRowNodeList.item(3).getTextContent().trim();

			// TD 1
			NodeList optionNodeList = XPathAPI.selectNodeList(tdNodes.item(1), "html:select/html:option");
			int minQuantity = Integer.MAX_VALUE;
			int maxQuantity = Integer.MIN_VALUE;
			for (int j = 0 ; j < optionNodeList.getLength() ; j++) {
				Node option = optionNodeList.item(j);
				int c = Integer.parseInt(option.getTextContent());
				
				minQuantity = Math.min(minQuantity, c);
				maxQuantity = Math.max(maxQuantity, c);
			}
			
			// TD 2
			String priceString = tdNodes.item(2).getFirstChild().getTextContent().trim();
	        NumberFormat numberFormat = NumberFormat.getInstance();
	        Number number = numberFormat.parse(priceString.substring(1));
	        double price = number.doubleValue();
			
			// TD 3: <a class="RT" href="javascript:SubmitPurchaseLink('Ticks0', 'https://secure.ticketnetwork.com/checkout/Checkout.aspx?brokerid=1&sitenumber=3&tgid=597211949&evtID=756587&price=515.00')"> Buy Tickets </a>
			String href = XPathAPI.selectSingleNode(tdNodes.item(3), "html:a/@onclick").getTextContent();

			Matcher itemIDMatcher = ticketNetworkItemIDPattern.matcher(href);
			itemIDMatcher.find();
			String itemId = itemIDMatcher.group(1);
						
			TicketHit ticketHit = new TicketHit(Site.TICKET_NETWORK, itemId,
					TicketType.REGULAR,					
					maxQuantity, minQuantity,
					row, section, price, price, "TicketNetwork", TicketStatus.ACTIVE);
			ticketHit.setTicketListingCrawl(ticketListingCrawl);

			Long startIndexationTime = System.currentTimeMillis();			
			indexTicketHit(ticketHitIndexer, ticketHit);
			ticketListingCrawl.setIndexationTime(ticketListingCrawl.getIndexationTime() + System.currentTimeMillis() - startIndexationTime);						
		}
		
		ticketListingCrawl.setExtractionTime(System.currentTimeMillis() - startProcessingTime - ticketListingCrawl.getFetchingTime() - ticketListingCrawl.getIndexationTime());
		return true;
	}

	private boolean runFetchZeroMarkupTicketListing(DefaultHttpClient httpClient, TicketHitIndexer ticketHitIndexer, TicketListingCrawl ticketListingCrawl) throws Exception {

		if (!(ticketListingCrawl.getSiteId().equals(Site.TICKET_NETWORK))) {
			throw new Exception(this.getClass().getName() + ".fetchTicketListing parameter must be a ticketnetwork crawl");
		}

		Long startProcessingTime = System.currentTimeMillis();

		HttpGet httpGet = new HttpGet(ticketListingCrawl.getQueryUrl().replace(" ", "+"));
		httpGet.addHeader("Host", "zeromarkup.com");
		httpGet.addHeader("Accept", "application/json, text/javascript, */*");
		httpGet.addHeader("Accept-Encoding", "gzip,deflate");
		HttpResponse response = httpClient.execute(httpGet);

		// System.out.println("PROCESSING TIME=" + (System.currentTimeMillis() - startProcessingTime));
		
		UncompressedHttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
		String content = EntityUtils.toString(entity);		
		ticketListingCrawl.setFetchedByteCount(ticketListingCrawl.getFetchedByteCount() + entity.getReadByteCount());		
		ticketListingCrawl.setFetchingTime(System.currentTimeMillis() - startProcessingTime);

		// if no tickets are found, it display a table with the class tn_results_notfound		
		Matcher resultsNotFoundMatcher = zeroMarkupResultsNotFoundPattern.matcher(content);
				
		if (resultsNotFoundMatcher.find()) {
			return true;
		}
		
		Matcher ticGrpMatcher = zeroMarkupTicGrpPattern.matcher(content); 

		while(ticGrpMatcher.find()) {
			
			String ticGrpContent = ticGrpMatcher.group(1);
			Matcher ticMatcher = zeroMarkupTicPattern.matcher(ticGrpContent);
		    while(ticMatcher.find()) {
			    String ticContent = ticMatcher.group(1);
				
			    ticContent = ticContent.replaceAll(">>", "> >");
				System.out.println("********************************");
				System.out.println("Tr Content: " + ticContent);
				System.out.println("********************************");
	        	StringTokenizer stok = new StringTokenizer(ticContent, ">");
	    		String section = stok.nextToken();
	    		String row = stok.nextToken();
	    		String qty = stok.nextToken();
	    		String priceStr = stok.nextToken();
	    		String id = stok.nextToken();
	    		String note = stok.nextToken();
	    		String itemId = URLDecoder.decode(stok.nextToken(),"ISO-8859-1");
	    		String seatRange = stok.nextToken();
	    		
	    		Integer minQuantity = Integer.parseInt(qty);
	    		Integer maxQuantity = Integer.parseInt(qty);
	    		
	    		Double price = Double.parseDouble(priceStr);
	    		
				TicketHit ticketHit = new TicketHit(Site.TICKET_NETWORK, itemId,
						TicketType.REGULAR,					
						maxQuantity, minQuantity,
						row, section, price, price, "TicketNetwork", TicketStatus.ACTIVE);
				ticketHit.setTicketListingCrawl(ticketListingCrawl);
				
				Long startIndexationTime = System.currentTimeMillis();
				indexTicketHit(ticketHitIndexer, ticketHit);
				ticketListingCrawl.setIndexationTime(ticketListingCrawl.getIndexationTime() + System.currentTimeMillis() - startIndexationTime);
		    }
		}

		ticketListingCrawl.setExtractionTime(System.currentTimeMillis() - startProcessingTime - ticketListingCrawl.getFetchingTime() - ticketListingCrawl.getIndexationTime());

		return true;
	}

	@Override
	public boolean runFetchTicketListing(SimpleHttpClient httpClient, TicketHitIndexer ticketHitIndexer, TicketListingCrawl ticketListingCrawl) throws Exception {
		if (ticketListingCrawl.getQueryUrl().indexOf(Site.TICKET_NETWORK) >= 0) {
			return runFetchTicketNetworkTicketListing(httpClient, ticketHitIndexer, ticketListingCrawl);
		}

		// return runFetchZeroMarkupTicketListing(httpClient, ticketHitIndexer, ticketListingCrawl);
		return runFetchZeroMarkupTicketListing(httpClient, ticketHitIndexer, ticketListingCrawl);
	}
	
	
	@Override
	public boolean isValidUrl(String url) {
		// valid url: http://www.ticketnetwork.com/tix/super-bowl-xliii-arizona-cardinals-vs-pittsburgh-steelers-tickets-february-1-2009-tampa-fl-756587.aspx
		if (url.matches("^http://www.ticketnetwork.com/tix/.*\\.aspx$")){
			return true;
		}
		// we can also have: http://www.ticketnetwork.com/tix/u2-sunday-8-9-2009-tickets-1065872.aspx#tixListing
		if (url.matches("^http://www.ticketnetwork.com/tix/.*\\.aspx#.*$")){
			return true;
		}
		
		// http://zeromarkup.com/ResultsTicket.aspx?evtid=979135&event=Spring+Training%3a+San+Diego+Padres+vs.+Colorado+Rockies
		// http://www.zeromarkup.com/ResultsTicket.aspx?evtid=868113&event=A+Bronx+Tale
		return url.matches("^http://.*zeromarkup.com/ResultsTicket.aspx\\?evtid=.*$"); 		
	}

	@Override
	public String getTicketListingUrl(TicketListingCrawl ticketListingCrawl) {
		return ticketListingCrawl.getQueryUrl();
	}	

	@Override
	public String getTicketItemUrl(TicketListingCrawl ticketListingCrawl, Ticket ticket) {
		if (ticket.getTicketListingCrawl().getQueryUrl().indexOf("zeromarkup") >= 0) {
			// https://tickettransaction2.com/Checkout.aspx?e=YnJva2VyaWQ9OTgyJnNpdGVudW1iZXI9NSZ0Z2lkPTYxNjk3NTA5NiZldnRpZD05NjgzNDYmcHJpY2U9NTkuMDAwMA..!qNUjPcPUd2c.&treq=4&wcid=257&SessionId=TApyV&__utma=1.588180687.1236354578.1236359093.1236365208.3&__utmb=1&__utmc=1&__utmz=1.1236354578.1.1.utmccn=(direct)|utmcsr=(direct)|utmcmd=(none)&ip=10.230.78.114&hc=__utma%3D1.588180687.1236354578.1236359093.1236365208.3%3B%20__utmz%3D1.1236354578.1.1.utmccn%3D(direct)|utmcsr%3D(direct)|utmcmd%3D(none)%3B%20ASP.NET_SessionId%3Deyif5d55vpvqr32waal2jqvv%3B%20__utmb%3D1%3B%20__utmc%3D1&ha=Mozilla/5.0%20(X11%3B%20U%3B%20Linux%20i686%3B%20en-US%3B%20rv%3A1.9.0.6)%20Gecko/2009020911%20Ubuntu/8.10%20(intrepid)%20Firefox/3.0.6
			return "https://tickettransaction2.com/Checkout.aspx?e=" + ticket.getItemId() +"&treq=" + ticket.getLotSize() + "&wcid=257";							
		}						
		return null;
	}		
}
