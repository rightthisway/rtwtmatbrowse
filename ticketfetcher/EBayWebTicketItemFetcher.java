package com.admitone.tmat.ticketfetcher;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.enums.TicketType;
import com.admitone.tmat.utils.TextUtil;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.UncompressedHttpEntity;

public final class EBayWebTicketItemFetcher extends TicketItemFetcher{
	public static final String viewItemUrl = "http://cgi.ebay.com/ws/eBayISAPI.dll?ViewItem&item=";
	//private static final Pattern ebayTitlePattern = Pattern.compile("<title>(.+) - eBay.*</title>");
	private static final Pattern ebayEventNamePattern = Pattern.compile("Event Name:\\s*(?:</font>)?</[^/]+>([^/]+)</");
	private static final Pattern ebayVenueStatePattern = Pattern.compile("Venue State/Province:\\s*(?:</font>)?</[^/]+>([^/]+)</");
	private static final Pattern ebayVenueCityPattern = Pattern.compile("Venue City:\\s*(?:</font>)?</[^/]+>([^/]+)</");
	private static final Pattern ebayVenueNamePattern = Pattern.compile("Venue Name:\\s*(?:</font>)?</[^/]+>([^/]+)</");
	private static final Pattern ebayNumTicketsPattern = Pattern.compile("Number of Tickets:\\s*(?:</font>)?</[^/]+>([^/]+)</");
	private static final Pattern ebayQuantityPattern = Pattern.compile("Quantity:[^:]+[^\\d]([\\d]+) available");
	private static final Pattern ebaySectionPattern = Pattern.compile("Section:\\s*(?:</font>)?</[^/]+>([^/]+)</");
	private static final Pattern ebayRowPattern = Pattern.compile("Row:\\s*(?:</font>)?</[^/]+>([^/]+)</");
	private static final Pattern ebayYearPattern = Pattern.compile("Year:\\s*(?:</font>)?</[^/]+>([^/]+)</");
	private static final Pattern ebayMonthPattern = Pattern.compile("Month:\\s*(?:</font>)?</[^/]+>([^/]+)</");
	private static final Pattern ebayDayPattern = Pattern.compile("Day:\\s*(?:</font>)?</[^/]+>([^/]+)</");
	private static final Pattern ebayCurrentPricePattern = Pattern.compile("(?:DetailsCurrentBidValue\" class=\"sectiontitle\"><b>US \\$([\\d\\.,]+))|(?:bid:[^:]+US \\$([\\d\\.,]+)<)");
	private static final Pattern ebayBuyItNowPattern = Pattern.compile("(?:<span class=\"sectiontitle\"><b>US \\$([^<]+)<)|(?:Price:.+US \\$([^<]+)<.+Buy&nbsp;It&nbsp;Now)|(?:id=\"v4-\\d+\">US \\$([^<]+)<)");
	private static final Pattern ebayEndDatePattern = Pattern.compile("end time ([^)]+)\\)");
	private static final Pattern ebaySellerPattern = Pattern.compile("ViewFeedback&amp;userid=([^&]+)&amp");
	private static final int MAX_RETRY = 5;
	
	public EBayWebTicketItemFetcher() {
		super();
	}
	
	public String matchIfExists(Pattern pattern, String content) {
		Matcher m = pattern.matcher(content);
		if (m.find()) {
			for (int i = 1; i <= m.groupCount(); i++) {
				if (m.group(i) != null) {
					return m.group(i).trim();
				}
			}
		}
		
		return null;
	}
	
	@Override
	public TicketHit runFetchTicketItem(DefaultHttpClient httpClient, TicketListingCrawl crawl, String itemId) throws Exception {
		String row = null;
		String section = null;
		String venueCity = null;
		String venueState = null;
		String venueName = null;
		
		Date eventDate = null;

		long startFetchingTime = System.currentTimeMillis();
		HttpGet httpGet = new HttpGet(viewItemUrl + itemId);
		
		// cannot parse using XML because there are malformed tags :-(
		
		String response = null;
		int i = 0;
		for (i = 0; i < MAX_RETRY; i++) {
			try {
				HttpResponse httpResponse = httpClient.execute(httpGet);				
				UncompressedHttpEntity entity = HttpEntityHelper.getUncompressedEntity(httpResponse.getEntity());				
				response = EntityUtils.toString(entity);
				crawl.setFetchedByteCount(crawl.getFetchedByteCount() + entity.getReadByteCount());
				break;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		crawl.setFetchingTime(crawl.getFetchingTime() + System.currentTimeMillis() - startFetchingTime);		

		if (i >= MAX_RETRY) {
			return null;
		}
		
		//System.out.println("TITLE: "  + title);

		String eventName = matchIfExists(ebayEventNamePattern, response);
		//System.out.println("EVENT NAME: "  + eventName);

		venueName = matchIfExists(ebayVenueNamePattern, response);
		//System.out.println("VENUE NAME: "  + venueName);

		venueCity= matchIfExists(ebayVenueCityPattern, response);
		//System.out.println("VENUE CITY: "  + venueCity);

		venueState= matchIfExists(ebayVenueStatePattern, response);
		//System.out.println("VENUE STATE: "  + venueState);

		String locationFilter = null;
		if (crawl.getEvent() != null && crawl.getEvent().getVenue() != null) {
			locationFilter = crawl.getEvent().getVenue().getBuilding();
		}			

		String location = venueName + ", " + venueCity + ", " + venueState;
		if (locationFilter != null && !locationFilter.isEmpty() && !TextUtil.isSimilar(locationFilter, location, 2)) {
			return null;
		}
		
		String lotSizeStr = matchIfExists(ebayNumTicketsPattern, response);
		Integer lotSize = 1;
		if (lotSizeStr != null) {
			try {
				lotSize = Integer.parseInt(lotSizeStr);
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}

		//System.out.println("NUM TIX: "  + quantity);

		section = matchIfExists(ebaySectionPattern, response);
//		System.out.println("SECTION: "  + section);

		row = matchIfExists(ebayRowPattern, response);
//		System.out.println("ROW: "  + row);

		Integer quantity = 1;
		
		String quantityStr = matchIfExists(ebayQuantityPattern, response);
		if (quantityStr != null) {
			try {	
				quantity = Integer.parseInt(quantityStr);
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}
		
//		System.out.println("IT: " + itemId + "  QTY: "  + quantityStr);

		String year = matchIfExists(ebayYearPattern, response);
//		System.out.println("YEAR: "  + year);

		String month = matchIfExists(ebayMonthPattern, response);
//		System.out.println("MONTH: "  + month);

		String day = matchIfExists(ebayDayPattern, response);
//		System.out.println("DAY: "  + day);

		String buyItNowString = matchIfExists(ebayBuyItNowPattern, response);
		Double buyItNowPrice = 0D;
		if (buyItNowString != null) {
			buyItNowPrice = Double.parseDouble(buyItNowString.replace(",", ""));
		}

		String priceString = matchIfExists(ebayCurrentPricePattern, response);
//		System.out.println("PRICE: "  + priceString);
		Double currentPrice = buyItNowPrice;
		if (priceString != null) {
			currentPrice = Double.parseDouble(priceString.replace(",", ""));
		}

		try {
			DateFormat format = new SimpleDateFormat("yyyy-MMMMM-d");
			format.setTimeZone(TimeZone.getTimeZone("UTC"));
			eventDate = format.parse(year + "-" + month + "-" + day);
		} catch (Exception e) {
			System.out.println("Error parsing date for ebay item: " + itemId);
			return null;
		}
			
		String endDateString = matchIfExists(ebayEndDatePattern, response);
		DateFormat endDateFormat = new SimpleDateFormat("MMM-dd-yy HH:mm:ss zzz");
		Date endDate = endDateFormat.parse(endDateString);

		String seller = matchIfExists(ebaySellerPattern, response);
		
		TicketStatus ticketStatus;
		
		if (endDate.before(new Date())) {
			ticketStatus = TicketStatus.EXPIRED;
		} else {
			ticketStatus = TicketStatus.ACTIVE;			
		}

		TicketHit ticketHit = new TicketHit(Site.EBAY, itemId, 
				TicketType.AUCTION,
				quantity * lotSize,
				lotSize,
				row, section, currentPrice / lotSize, buyItNowPrice / lotSize, seller, ticketStatus);

		ticketHit.setEventDate(eventDate);
		ticketHit.setEndDate(endDate);
		ticketHit.setVenueState(venueState);
		ticketHit.setVenueCity(venueCity);
		ticketHit.setVenueName(venueName);

		return ticketHit;
	}
}
