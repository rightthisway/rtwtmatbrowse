package com.admitone.tmat.ticketfetcher;

import java.net.InetAddress;
import java.net.URI;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ProtocolException;
import org.apache.http.client.RedirectHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.enums.TicketType;
import com.admitone.tmat.indexer.TicketHitIndexer;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;
import com.admitone.tmat.utils.httpclient.UncompressedHttpEntity;

public class AOTicketListingFetcher extends TicketListingFetcher {

//	private Pattern eventIdPattern = Pattern.compile("[&?]e=([0-9]*)&");
//	private Pattern ticketPattern = Pattern.compile("&ticket=([0-9]+)&");
	
	public AOTicketListingFetcher() {
	}
	

	/*public static void main(String[] args) {
		AOTicketListingFetcher aoTicketListingFetcher = new AOTicketListingFetcher();
		ticketList
		aoTicketListingFetcher.runFetchTicketListing(new SimpleHttpClient("admitone"), new PreviewTicketHitIndexer(), ticketListingCrawl);
	}*/
	@Override
	public boolean runFetchTicketListing(SimpleHttpClient httpClient, TicketHitIndexer ticketHitIndexer, TicketListingCrawl ticketListingCrawl) throws Exception {

		if (!(ticketListingCrawl.getSiteId().equals(Site.AOP))) {
			throw new Exception(this.getClass().getName() + ".fetchTicketListing parameter must be an eventinventory crawl");
		}
		
		if(ticketListingCrawl.getId()==null){
/**
 * 			Commented to avoid unnecessary hits to EI
 */
			/*
			String queryUrl=ticketListingCrawl.getQueryUrl();
			String url[]=queryUrl.split("\\?");
			HttpPost hp = new HttpPost(url[0]);
			List<NameValuePair> parameters = new ArrayList<NameValuePair>();
			
			NameValuePair nameValuePair1 = null;
			for(String param:url[1].split("&")){
				String[] paramPair=param.split("=");
				nameValuePair1 = new BasicNameValuePair(paramPair[0], paramPair[1]);
				parameters.add(nameValuePair1);
			}
			HttpClient hc = HttpClientStore.createHttpClient(Site.AOP);// new DefaultHttpClient();
			UrlEncodedFormEntity entity = new UrlEncodedFormEntity(parameters);
			hp.setEntity(entity);
			HttpResponse response = hc.execute(hp);
			UncompressedHttpEntity entityResult = HttpEntityHelper.getUncompressedEntity(response.getEntity());			
			String content = EntityUtils.toString(entityResult).replaceAll("\\s++", " ");
			System.out.println(content);
			Pattern ticketDetailPattern=Pattern.compile("\"Radio\"(.*?)</form>");
			Matcher ticketDetailMatcher = ticketDetailPattern.matcher(content);
			Pattern sectionPattern= Pattern.compile("SEC: <strong> (\\d++) <");
			Pattern rowPattern = Pattern.compile("ROW: <strong>(.*?)<");
			Pattern pricePattern= Pattern.compile("\\$(.*?)<");
			Pattern quantityPattern = Pattern.compile("selected>(.*?)<");
			Pattern ticketIdPattern = Pattern.compile("ticket=(\\d++)");
			Matcher rowMatcher=null;
			Matcher sectionMatcher= null;
			Matcher priceMatcher=null;
			Matcher quantityMatcher= null;
			Matcher ticketIdMatcher=null;
			String section="";
			String row="";
			double price=0;
			int quantity=0;
			String ticketId="";
			while(ticketDetailMatcher.find()){
				String rowData=ticketDetailMatcher.group();
				System.out.println(rowData);
				sectionMatcher=sectionPattern.matcher(rowData);
				if(sectionMatcher.find()){
					section=sectionMatcher.group().replaceAll("[\":a-zA-Z=><\\s++]", "");
				}
				rowMatcher=rowPattern.matcher(rowData);
				if(rowMatcher.find()){
					row=rowMatcher.group().replaceAll("ROW: <strong> ", "").replaceAll("<", "").replaceAll("\\s++", "");
				}
				priceMatcher=pricePattern.matcher(rowData);
				if(priceMatcher.find()){
					price=Double.parseDouble(priceMatcher.group().replaceAll("[$,<\\s++]", ""));
				}
				quantityMatcher=quantityPattern.matcher(rowData);
				if(quantityMatcher.find()){
					quantity=Integer.parseInt(quantityMatcher.group().replaceAll("[a-zA-Z><\\s++]", ""));
				}
				
				ticketIdMatcher=ticketIdPattern.matcher(rowData);
				if(ticketIdMatcher.find()){
					ticketId=ticketIdMatcher.group().replaceAll("[=a-zA-Z\\s++]", "");
				}
				if(quantity==0 || "".equals(ticketId) || "".equals(row) || "".equals(section) || price==0){
					continue;
				}
				int lotSize = 1;
				if ((quantity % 2) == 0 && quantity < 14) {
					lotSize = 2;
				}
				TicketHit ticketHit = new TicketHit(Site.AOP, ticketId, 
						TicketType.REGULAR,
						quantity, lotSize,
						row, section, price, price, "AdmitoneEI", TicketStatus.ACTIVE);
				ticketHit.setTicketListingCrawl(ticketListingCrawl);
//				ticketHit.setSeat((seats == null)?null:seats);

				long startIndexationTime = System.currentTimeMillis();
				indexTicketHit(ticketHitIndexer, ticketHit);
				ticketListingCrawl.setIndexationTime(ticketListingCrawl.getIndexationTime() + System.currentTimeMillis() - startIndexationTime);
				
			}*/
		}else{
			/***
			 Test FOR EI
			 We do additional strike to aop-bronws.blogsopt to track EI crawls 
			 commented to make process faster and seems it is not necessary
			 */
			/*HttpGet httpGet1 =  new HttpGet("http://aop-browns.blogspot.com");
			httpGet1.setHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
			try {
				HttpResponse response = httpClient.execute(httpGet1);
				HttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
				String content=EntityUtils.toString(entity);
				Pattern pattern= Pattern.compile("var ip =(.*?);");
				Matcher match=pattern.matcher(content);
				if(match.find()){
					String main=match.group();
//					System.out.println("Your Ip:" + main);
//					logger.info("Your Ip:" + main);
				}
			} catch (Exception e) {
				System.err.println(e.fillInStackTrace());
			}
			*/
			/**
			 *  Ends here
			 */
			
			String proxy="";
			Object obj=httpClient.getParams().getParameter(ConnRoutePNames.DEFAULT_PROXY);
			if(obj!=null){
				proxy= obj.toString();
				proxy=proxy.split(":")[1].replaceAll("//","");
			}else{
				proxy=InetAddress.getLocalHost().getHostName();
			}
			Map<String, String> map=ticketListingCrawl.getExtraParameterMap();
			String tempKeyword=map.get("keywords");
			String tempArray[] = tempKeyword.split("\\+");
			String keywords = "";
			String location = "";
			keywords=tempArray[0];
			if(tempArray.length==2){
				location=tempArray[1];
			}
			
			String patternParameters[]=keywords.split("vs");
			
			String temp="";
			int counter=0;
			String content ="";
			String pattern="";
			Pattern tourIdPattern = null;
			Pattern parameterPattern=Pattern.compile("cfid(.*?)=\"");
			try{
				for(String keyword:patternParameters[0].split(" ")){
					temp+=keyword+" ";//toCamelCase(keyword)+" ";
				}
			pattern= "=\"(\\d++)\">(.*?)<";
			tourIdPattern= Pattern.compile(pattern);
			
			HttpGet hp = new HttpGet("http://www.eventinventory.com/search/byevent.cfm?restart=Interface4&next=byevent.cfm&ref=&id=-1&client=4583&k="+keywords.replaceAll(" ", "%20"));
			hp.setHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
			HttpResponse response = httpClient.execute(hp);
			counter++;
			UncompressedHttpEntity entityResult = HttpEntityHelper.getUncompressedEntity(response.getEntity());			
			content = EntityUtils.toString(entityResult).replaceAll("\\s++", " ");
			System.out.println(content);
			}catch (Exception ex) {
				System.out.println(ex.fillInStackTrace());
				saveCounterHistory(ticketListingCrawl, counter,proxy);
				HttpClientStore.increamentErrorCount();
				throw new Exception(ex.getMessage());
			}
			if(content.contains("ERR_ACCESS_DENIED")){
				saveCounterHistory(ticketListingCrawl, counter,proxy);
				HttpClientStore.increamentErrorCount();
				throw new Exception("Access denied for proxy:" + obj.toString().split("//")[1]);
			}
			Matcher tourIdMatcher = tourIdPattern.matcher(content);
			String stringTourId=null;
			Integer tourId=null;
			String url=ticketListingCrawl.getQueryUrl();
			String eventString="";
			for(String tempParams:url.split("&")){
				if(tempParams.contains("e=")){
					eventString=tempParams.split("=")[1];
					break;
				}
			}
			Event event =ticketListingCrawl.getEvent();
			boolean outerFalg= false;
			
			while(tourIdMatcher.find()){
				System.err.println(tourIdMatcher.group(0).toLowerCase());
				String tempKeywords[] =temp.split(" ");
				int ii=0;
				while(ii<tempKeywords.length){
					if(!(tourIdMatcher.group(0).toLowerCase()).contains(tempKeywords[ii].trim().toLowerCase())){
						outerFalg=true;
						break;
					}
					ii++;
				}
				if(outerFalg){
					outerFalg=false;
					continue;
				}
				try{
					System.out.println(tourIdMatcher.group(0));
//					stringTourId=tourIdMatcher.group(0).replaceAll("[\"a-zA-Z=>]", "").trim();
					stringTourId=tourIdMatcher.group(1);
					if(!stringTourId.equals(eventString)){
						continue;
					}
					System.out.println(stringTourId);
					tourId=Integer.parseInt(stringTourId);
					if(tourId!=null){
						Matcher parameterMatcher = parameterPattern.matcher(content);
						if(parameterMatcher.find()){
							System.out.println(parameterMatcher.group(0));
							String cfParams[]=parameterMatcher.group().split("&");
							String cfid=cfParams[0].split("=")[1];
							String cftoken=cfParams[1].split("=")[1];
							String cfuser=cfParams[2].split("=")[1];
							String tempUrl=("http://www.eventinventory.com/search/pubsearch.cfm?client=4583&id=10&e="+eventString+"&cfid" + cfid + "&cftoken=" + cftoken + "&cfuser=" + cfuser).replaceAll("\"", "").replaceAll("#", "%5F").replaceAll("-", "%2D");
//							System.out.println("tempUrl:" + tempUrl);
							HttpGet hp1 = new HttpGet(tempUrl);
							hp1.setHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
							HttpResponse response1 = httpClient.execute(hp1);
							counter++;
							UncompressedHttpEntity entityTicketsResult = HttpEntityHelper.getUncompressedEntity(response1.getEntity());			
							String content1 = EntityUtils.toString(entityTicketsResult).replaceAll("\\s++", " ");
							
							if(content1.contains("Please SELECT A THEATER from the list below")){
								Pattern venuePattern = Pattern.compile("href(.*?)font>");
								if(location==null){
									location="location not specified..";
								}
								boolean found=false;
								Matcher venueMatcher=venuePattern.matcher(content1);
								while(venueMatcher.find()){
									if((venueMatcher.group(0).toLowerCase()).contains(location.toLowerCase())){
										List<NameValuePair> pList = new ArrayList<NameValuePair>();
										NameValuePair nvp;
										HttpPost hp2 = new HttpPost("http://www.eventinventory.com/search/pubsearch.cfm");
										Pattern urlPattern= Pattern.compile("\\?(.*?)\"");
										Matcher urlMatcher=urlPattern.matcher(venueMatcher.group());
										while(urlMatcher.find()){
											String[] params=urlMatcher.group(1).split("&");
											for(String param:params){
												String[] tempParam=param.split("=");
												if(tempParam.length==2){
													nvp = new BasicNameValuePair(tempParam[0], tempParam[1]);
												}else{
													nvp = new BasicNameValuePair(tempParam[0], "");
												}
												pList.add(nvp);
											}
											UrlEncodedFormEntity urlEntity = new UrlEncodedFormEntity(pList);
											hp2.setEntity(urlEntity);
											HttpResponse response2 = httpClient.execute(hp2);
											UncompressedHttpEntity urlResult = HttpEntityHelper.getUncompressedEntity(response2.getEntity());			
											content1 = EntityUtils.toString(urlResult).replaceAll("\\s++", " ");
											found=true;
											break;
										}
									}
									if(found){
										break;
									}
								}
							}
							
							Pattern tablePattern= Pattern.compile("<table width=\"100%\" cellspacing=\"1\"(.*?)table>");
							Matcher tableMatcher=tablePattern.matcher(content1);
							
							
							if(tableMatcher.find()){
								Pattern rowPattern= Pattern.compile("<tr(.*?)tr>");
								Matcher rowMatcher=rowPattern.matcher(tableMatcher.group());
								DateFormat df = new SimpleDateFormat("MMMMMM d, yyyy");
								DateFormat timeFormat = new SimpleDateFormat("h:mm aa");
								
								String stringToDate = "TBD";
								if(event.getDate()!=null){
									stringToDate = df.format(event.getDate());
								}
								String stringToTime = "TBD";
								if(event.getTime()!=null){ 
									stringToTime  = timeFormat.format(event.getTime());
								}
								
								while(rowMatcher.find()){
									if (rowMatcher.group().contains(stringToDate) && rowMatcher.group().contains(stringToTime)){
//										System.out.println(rowMatcher.group());
										Pattern urlPattern= Pattern.compile("href=\"(.*?)\"");
										Matcher urlMatcher=urlPattern.matcher(rowMatcher.group());
										if(urlMatcher.find()){
											String queryUrl="http://www.eventinventory.com" + urlMatcher.group().replaceAll("href=\"", "");
											queryUrl=queryUrl.replaceAll("\"", "").replaceAll("#", "%5F").replaceAll("-", "%2D");
//											System.out.println(queryUrl);
											HttpGet httpPost = new HttpGet(queryUrl);
											httpPost.setHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
											HttpResponse response2 = httpClient.execute(httpPost);
											counter++;
											UncompressedHttpEntity entityResult1 = HttpEntityHelper.getUncompressedEntity(response2.getEntity());			
											String finalContent = EntityUtils.toString(entityResult1).replaceAll("\\s++", " ");
											System.out.println(finalContent);
											Pattern ticketDetailPattern=Pattern.compile("<tr valign=\"middle\"(.*?)</form>");
											Matcher ticketDetailMatcher = ticketDetailPattern.matcher(finalContent);
											Pattern sectionPattern= Pattern.compile("SEC: <strong> (.*?) <");
											Pattern rowPattern1 = Pattern.compile("ROW: <strong>(.*?)<");
											Pattern pricePattern= Pattern.compile("<strong>\\$(.*?)<");
											Pattern quantityPattern = Pattern.compile("selected>(.*?)<");
											Pattern ticketIdPattern = Pattern.compile("ticket=(\\d++)");
											Matcher rowMatcher1=null;
											Matcher sectionMatcher= null;
											Matcher priceMatcher=null;
											Matcher quantityMatcher= null;
											Matcher ticketIdMatcher=null;
											String section="";
											String row="";
											double price=0;
											int quantity=0;
											String ticketId="";
											while(ticketDetailMatcher.find()){
												section="";
												row="";
												price=0;
												quantity=0;
												ticketId="";
												String rowData=ticketDetailMatcher.group();
//												System.out.println(rowData);
												sectionMatcher=sectionPattern.matcher(rowData);
												if(sectionMatcher.find()){
													section=sectionMatcher.group().replace("SEC: <strong>","").replaceAll("[\":=><]", "");
												}
												rowMatcher1=rowPattern1.matcher(rowData);
												if(rowMatcher1.find()){
													row=rowMatcher1.group().replaceAll("ROW: <strong> ", "").replaceAll("<", "");//.replaceAll("\\s++", "");
												}
												priceMatcher=pricePattern.matcher(rowData);
												if(priceMatcher.find()){
													price=Double.parseDouble(priceMatcher.group().replaceAll("[a-zA-z$,<>\\s++]", ""));
												}
												quantityMatcher=quantityPattern.matcher(rowData);
												if(quantityMatcher.find()){
													quantity=Integer.parseInt(quantityMatcher.group().replaceAll("[a-zA-Z><\\s++]", ""));
												}
												
												ticketIdMatcher=ticketIdPattern.matcher(rowData);
												if(ticketIdMatcher.find()){
													ticketId=ticketIdMatcher.group().replaceAll("[a-zA-Z\\s++]", "");
												}
												if(quantity==0 || "".equals(ticketId) || "".equals(row) || "".equals(section) || price==0){
													continue;
												}
												int lotSize = 1;
												if ((quantity % 2) == 0 && quantity < 14) {
													lotSize = 2;
												}
												TicketHit ticketHit = new TicketHit(Site.AOP, ticketId, 
														TicketType.REGULAR,
														quantity, lotSize,
														row, section, price, price, "AdmitoneEI", TicketStatus.ACTIVE);
												ticketHit.setTicketListingCrawl(ticketListingCrawl);

												long startIndexationTime = System.currentTimeMillis();
												indexTicketHit(ticketHitIndexer, ticketHit);
												ticketListingCrawl.setIndexationTime(ticketListingCrawl.getIndexationTime() + System.currentTimeMillis() - startIndexationTime);
											}
										}
										break;
									}
									
								}
								
							}
							
						}
					}
				}catch(Exception ex){
					System.out.println(ex.fillInStackTrace());
					saveCounterHistory(ticketListingCrawl, counter,proxy);
					HttpClientStore.increamentErrorCount();
					throw new Exception(ex.getMessage());
				}
				saveCounterHistory(ticketListingCrawl, counter,proxy);
			}
			/*****/

		}
		return true;
		
	}
	
	@Override
	public boolean isValidUrl(String url) {
		// valid url: 
		// http://www.eventinventory.com/search/pubsearch.cfm?cfid=101056146&cftoken=4fda0bf-d30b7501-700c-4052-a85f-44ba0692ab09&cfuser=5514FDF5-08B7-4B40-B909702D85FAED05&client=1337&e=52&id=10&cfid=101056146&cftoken=4fda0bf-d30b7501-700c-4052-a85f-44ba0692ab09&cfuser=5514FDF5-08B7-4B40-B909702D85FAED05&RefList=  
		// http://www.eventinventory.com/search/results.cfm?restart=yes&client=1337&e=594&v=386&s=1&month=2&day=10&year=2009&p=692415&cfid=101288044&cftoken=4a64c19-49694649-a304-486c-ab3a-3eaa3190835e&cfuser=67440E7B-DC11-427D-860CEA70F4B30B5A&RefList=#search_top 
		if (url.matches("^http://www.eventinventory.com/search/pubsearch.cfm.*[&?]e=.*$")) {
			return true;
		}
		return url.matches("^http://www.eventinventory.com/search/results.cfm.*[&?]e=.*$"); 
	}
	
	@Override
	public String getTicketListingUrl(TicketListingCrawl ticketListingCrawl) {
		return ticketListingCrawl.getQueryUrl();
	}	
	
	public String toCamelCase(String keyword)
	{
	    StringBuilder keywordBuilder = new StringBuilder(keyword.length());
	    	keywordBuilder.append(keyword.substring(0, 1).toUpperCase());
	    	keywordBuilder.append(keyword.substring(1));
	    return keywordBuilder.toString(); // join
	
	}
	
	public static class EventInventoryRedirectHandler implements RedirectHandler{
		private String redirectUrl;
		
		public EventInventoryRedirectHandler() {
		}

		public URI getLocationURI(HttpResponse response, HttpContext context)
				throws ProtocolException {
			if (redirectUrl.startsWith("http")) {
				return URI.create(redirectUrl);
			} else {
				return URI.create("http://www.eventinventory.com" + redirectUrl);
			}
		}

		public boolean isRedirectRequested(HttpResponse response, HttpContext context) {
			Header[] headers = response.getAllHeaders();
			for(Header header: headers) {
				// System.out.println("Header=" + header.getName() + "=" + header.getValue());
				if (header.getName().equalsIgnoreCase("Location")) {
					redirectUrl = header.getValue();
					return true;
				}
			}
			
			return false;
		}

		public String getRedirectUrl() {
			return redirectUrl;
		}		
	}

}
