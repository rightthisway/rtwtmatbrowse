package com.admitone.tmat.ticketfetcher;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.enums.TicketType;
import com.admitone.tmat.indexer.TicketHitIndexer;
import com.admitone.tmat.utils.TicketUtil;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;
import com.admitone.tmat.utils.httpclient.UncompressedHttpEntity;

/**
 * Seat Wave Ticket Web Services Listing Fetcher. 
 */
public class SeatWaveWebServicesTicketListingFetcher extends TicketListingFetcher {	
	private final Logger logger = LoggerFactory.getLogger(SeatWaveWebServicesTicketListingFetcher.class);
	
	private static final String seatwaveComAuthId = "00000000011";
	private static final String seatwaveDeAuthId = "00000000022";
	private static final String seatwaveNlAuthId = "00000000033";
	
	private Pattern eventIdPattern = Pattern.compile("/(\\d+)$");

	public SeatWaveWebServicesTicketListingFetcher() {
	}
		
	@Override
	public boolean runFetchTicketListing(SimpleHttpClient httpClient, TicketHitIndexer ticketHitIndexer, TicketListingCrawl ticketListingCrawl) throws Exception {
		
		if (!(ticketListingCrawl.getSiteId().equals(Site.SEATWAVE))) {
			throw new Exception(this.getClass().getName() + ".fetchTicketListing parameter must be a seatwave crawl");
		}	

		Long startProcessingTime = System.currentTimeMillis();
		
		Matcher eventIdMatcher = eventIdPattern.matcher(ticketListingCrawl.getQueryUrl());
		eventIdMatcher.find();
		String eventId = eventIdMatcher.group(1);
		
		// try with seatwave.com first, then seatwave.de and finally with seatwave.nl
		for(String authId: new String[]{seatwaveComAuthId, seatwaveDeAuthId, seatwaveNlAuthId}) {
			// System.out.println("TRY WITH AUTHID=" + authId);
			
			HttpGet httpGet = new HttpGet(
					String.format("http://api.seatwave.com/service.svc/v1/json/%1$s/seasons/0/performances/%2$s/Tickets",
							authId, eventId
					)
			);
	
	
			httpGet.addHeader("Host", "api.seatwave.com");		
			httpGet.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
			httpGet.addHeader("Accept-Encoding", "gzip,deflate");
			
			HttpResponse response = httpClient.execute(httpGet);
			
			int statusCode = response.getStatusLine().getStatusCode();
			if (statusCode != 200) {
				return true; 
			}
			// System.out.println("RESPONSE=" + response.getStatusLine());
			
			UncompressedHttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
			
			String content = EntityUtils.toString(entity);
			// System.out.println("CONTENT=" + content);
			
			ticketListingCrawl.setFetchedByteCount(ticketListingCrawl.getFetchedByteCount() + entity.getReadByteCount());
			ticketListingCrawl.setFetchingTime(System.currentTimeMillis() - startProcessingTime);
			
	
			JSONArray jsonArray = new JSONArray(content);
			
			for (int i = 0 ; i < jsonArray.length() ; i++) {
				JSONObject ticketJSON = jsonArray.getJSONObject(i);
				
				int splitNumber = ticketJSON.getInt("SplNumber");
	
				String buyURL = ticketJSON.getString("BuyURL");
				String ticketId = ticketJSON.getString("TsgID");
				if (buyURL.contains("PrvPstID")) {
					ticketId = "P" + ticketId;
				}
				
				
				Integer quantity = ticketJSON.getInt("Quantity");
				
				String section = ticketJSON.getString("Section");
				String row = ticketJSON.getString("RowName");
				
				Double initialPrice = ticketJSON.getDouble("InitialPrice");
				
				String currencySymbol = ticketJSON.getString("CurrencySymbol");
							
				// if equals to GBP symbol
				if ((int)currencySymbol.charAt(0) == 163) {
					initialPrice = initialPrice * TicketUtil.getGBPConversionRate();
					// if equals to EURO symbol
				} else if ((int)currencySymbol.charAt(0) == 8364) {
					initialPrice = initialPrice * TicketUtil.getEURConversionRate();				
				}
				
				String typeName = ticketJSON.getString("TypeName");
				
				if (section.length() > 0 && typeName.length() > 0) {
					section = typeName + " - " + section;
				} else if(typeName.length() > 0 ) {
					section = typeName;
				}
	
				TicketHit ticketHit = new TicketHit(Site.SEATWAVE, ticketId,
						TicketType.REGULAR,					
						quantity, splitNumber,
						row, section, initialPrice, initialPrice, "SeatWave", TicketStatus.ACTIVE);
				
				ticketHit.setTicketListingCrawl(ticketListingCrawl);
				
				long startIndexationTime = System.currentTimeMillis();
				indexTicketHit(ticketHitIndexer, ticketHit);
				ticketListingCrawl.setIndexationTime(ticketListingCrawl.getIndexationTime() + System.currentTimeMillis() - startIndexationTime);
			}
			
			// we try first with seatwave.com. If there are some tickets, we are done.
			// otherwise try with seatwave.de, then with seatwave.nl
			if (jsonArray.length() > 0) {
				break;
			}
			
		}
		
		ticketListingCrawl.setExtractionTime(System.currentTimeMillis() - startProcessingTime - ticketListingCrawl.getIndexationTime() - ticketListingCrawl.getFetchingTime());
		return true;
	}
	
	@Override
	public boolean isValidUrl(String url) {
		// valid url:
		// http://www.seatwave.com/beyoncetickets/metro-radio-arena-tickets/22-may-2009/perf/198403
		// http://www.seatwave.com/billy-elliot-tickets/victoria-palace-theatre-tickets/19-march-2009/perf/141225?sw_source=mytown
		// http://www.seatwave.com/rock-am-ring-tickets/season
		return url.matches("^http://www.seatwave.[a-z]*/.*$");		
	}

	@Override
	public String getTicketListingUrl(TicketListingCrawl ticketListingCrawl) {
		return ticketListingCrawl.getQueryUrl();
	}
	
	@Override
	public String getTicketItemUrl(TicketListingCrawl ticketListingCrawl, Ticket ticket) {
		if (ticket.getItemId().startsWith("P")) {
			return "https://www.seatwave.com/buy/ticketpage.aspx?PrvPstID=" + ticket.getItemId().substring(1) + "&receipt=yes&sm=yes";			
		} else {
			return "https://www.seatwave.com/buy/ticketpage.aspx?tsgID=" + ticket.getItemId() + "&TicLumpID=1&receipt=yes&sm=yes";
		}
	}		
}
