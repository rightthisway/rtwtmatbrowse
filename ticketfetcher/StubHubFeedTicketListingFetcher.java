package com.admitone.tmat.ticketfetcher;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.enums.TicketDeliveryType;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.enums.TicketType;
import com.admitone.tmat.indexer.TicketHitIndexer;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;
import com.admitone.tmat.utils.httpclient.UncompressedHttpEntity;

/**
 * StubHub Ticket Listing Fetcher
 * 
 * more info at: http://stubhubapi.nobisi.com/index.php?title=Ticket_Index_Document
 */
public class StubHubFeedTicketListingFetcher extends TicketListingFetcher {	
	private final Logger logger = LoggerFactory.getLogger(StubHubFeedTicketListingFetcher.class);
	
	// for ticket which has a section: 100:112 => 112
	private static final Pattern sectionPattern = Pattern.compile("^\\d+:(\\d+)$");

	private static final Pattern seatPattern = Pattern.compile("(\\d+)");
	
	private Pattern timeLeftPattern = Pattern.compile(">(.*)</");

	public StubHubFeedTicketListingFetcher() {
	}

	@Override
	public boolean runFetchTicketListing(SimpleHttpClient httpClient, TicketHitIndexer ticketHitIndexer, TicketListingCrawl ticketListingCrawl) throws Exception {
		if (!(ticketListingCrawl.getSiteId().equals(Site.STUB_HUB) || ticketListingCrawl.getSiteId().equals(Site.STUBHUB_FEED))) {
			throw new Exception(this.getClass().getName() + ".fetchTicketListing parameter must be a stubhub crawl");
		}		
				
		//http://www.stubhub.com/super-bowl-tickets/super-bowl-2-1-2009-565762/
		// previous feed that we were using
		// http://www.stubhub.com/listingCatalog/getTicketsByEvent?limit=80&offset=1&sortby=p&orderby=a&eventId=565762&xbid=4981&cobrandId=47
		// new feed  for stubhub that we are using
		// http://partnerfeed.stubhub.com/listingCatalog/select/?q=%2B+stubhubDocumentType:ticket%0D%0A%2B+showTicketKeyStr:ticketACTIVE690326&version=2.2&start=0&rows=10000&indent=off&fl=curr_price+event_id+id+quantity+row_desc+section+sale_method+comments+time_left
		
		long startProcessingTime = System.currentTimeMillis();
		
		String eventId;
		
		// Extract EventId
		String url = ticketListingCrawl.getQueryUrl();
		int eventIdPos = url.indexOf("event_id="); 
		if (eventIdPos > 0) {
			eventId = url.substring(eventIdPos + 9, url.length());
		} else {
			String[] tokens = url.split("-");
			eventId = tokens[tokens.length - 1].split("/")[0];
		}
		
		int start = 0;
		int total;
		
		int itemsPerPage = 1000;
		
		// check if the event is active or not
		long startFetchingTime = System.currentTimeMillis();
		HttpPost httpPost = new HttpPost("http://partnerfeed.stubhub.com/listingCatalog/select/?q=%2BstubhubDocumentType%3Aevent%0D%0A%2Bevent_id%3A"
				+ eventId + "%0D%0A%0D%0A&version=2.2&fl=active&wt=json");
		httpPost.addHeader("Host", "partnerfeed.stubhub.com");		
		httpPost.addHeader("Accept", "application/json, text/javascript, */*");
		
		HttpResponse response = httpClient.execute(httpPost);			
		UncompressedHttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());			
		String content = EntityUtils.toString(entity);
		ticketListingCrawl.setFetchedByteCount(ticketListingCrawl.getFetchedByteCount() + entity.getReadByteCount());						
		ticketListingCrawl.setFetchingTime(ticketListingCrawl.getFetchingTime() + System.currentTimeMillis() - startFetchingTime);		
		
		if (content.indexOf("\"active\":\"0\"") >= 0) {
			throw new Exception("The event seems to have been deleted on StubHub. You might probably delete the crawl.");
		}
		
		do {
			
			// http://partnerfeed.stubhub.com/listingCatalog/static/schema.xml
			// ticket_medium_id:
			// - 1 (normal)
			// - 3 (instant)
			//
			// delivery_icon
			// - edelivery
			// - drop
			// - fedex
			//
			// rules:
			// if ticket_medium_id = 2 => instant
			// else if delivery_icon = edelivery =>edelivery
			
			
			startFetchingTime = System.currentTimeMillis();
			
			httpPost = new HttpPost(
					"http://partnerfeed.stubhub.com/listingCatalog/select/?q=%2B+stubhubDocumentType:ticket%0D%0A%2B+showTicketKeyStr:ticketACTIVE" + eventId
					+ "&version=2.2&start=" + start
					+ "&rows=" + itemsPerPage
					+ "&indent=off&fl=curr_price+event_id+id+quantity+row_desc+section+sale_method+comments+time_left+quantity_remain+ticket_split+seats+ticket_medium_id+delivery_icon"
					+ "&wt=json"
			);
			
			// System.out.println("http://partnerfeed.stubhub.com/listingCatalog/select/?q=%2B+stubhubDocumentType:ticket%0D%0A%2B+showTicketKeyStr:ticketACTIVE" + eventId
			// 		+ "&version=2.2&start=" + start
			// 		+ "&rows=" + itemsPerPage
			// 		+ "&indent=off&fl=curr_price+event_id+id+quantity+row_desc+section+sale_method+comments+time_left+quantity_remain+ticket_split+seats+ticket_medium_id+delivery_icon"
			// 		+ "&wt=json");
			
			httpPost.addHeader("Host", "partnerfeed.stubhub.com");		
			httpPost.addHeader("Accept", "application/json, text/javascript, */*");
			httpPost.addHeader("Accept-Encoding", "gzip,deflate");
			// httpPost.addHeader("Keep-Alive", "300");
			// httpPost.addHeader("Connection", "keep-alive");
			
			response = httpClient.execute(httpPost);			
			entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());			
			content = EntityUtils.toString(entity);
			
			// System.out.println("CONTENT=" + content);
			 
			ticketListingCrawl.setFetchedByteCount(ticketListingCrawl.getFetchedByteCount() + entity.getReadByteCount());						
			ticketListingCrawl.setFetchingTime(ticketListingCrawl.getFetchingTime() + System.currentTimeMillis() - startFetchingTime);		
			
			JSONObject jsonObject = new JSONObject(content);
			
			total = jsonObject.getJSONObject("response").getInt("numFound");
						
			JSONArray docsJSONArray = jsonObject.getJSONObject("response").getJSONArray("docs");
			
			for (int i = 0 ; i < docsJSONArray.length() ; i++) {
				JSONObject docJSONObject = docsJSONArray.getJSONObject(i);

				String section = docJSONObject.getString("section");
				
				// if we have 100:122 => returns 122
				Matcher matcher = sectionPattern.matcher(section);
				if (matcher.find()) {
					section = matcher.group(1);
				}

				String row = docJSONObject.getString("row_desc");
				Double price = docJSONObject.getDouble("curr_price");
				int quantity = docJSONObject.getInt("quantity");
				int quantity_remain = docJSONObject.getInt("quantity_remain");
				int split = docJSONObject.getInt("ticket_split");
				String id = docJSONObject.getString("id");				

				String timeLeftString = docJSONObject.getString("time_left");
				
				// if time is less than one hour, we have:
				// 	<span style="color=#FF0000">54 min</span>
																
				Matcher timeLeftMatcher = timeLeftPattern.matcher(timeLeftString);
				if (timeLeftMatcher.find()) {
					timeLeftString = timeLeftMatcher.group(1);
				}				

				// Time Left to Purchase: 1d 1h 38m
				// Time Left to Purchase: 15 min
				long endTime = new Date().getTime();
				
				TicketStatus ticketStatus = null;
				Date endDate = null;
				
				if (timeLeftString.contains("Ended")) {
					ticketStatus = TicketStatus.SOLD;
				} else {
					ticketStatus = TicketStatus.ACTIVE;
					
					String[] timeTokens = timeLeftString.split(" ");
					
					if (timeTokens.length == 2 && timeTokens[1].equals("min")) {
						endTime += Integer.parseInt(timeTokens[0]) * 60L * 1000L;							
					} else {						
						for (String timeToken: timeTokens) {
							int timeValue = Integer.parseInt(timeToken.substring(0, timeToken.length() - 1));
							String timeUnit = timeToken.substring(timeToken.length() - 1);
							
							if (timeUnit.equals("d")) {
								endTime += timeValue * 24L * 60L * 60L * 1000L;
							} else if (timeUnit.equals("h")) {
								endTime += timeValue * 60L * 60L * 1000L;								
							} else if (timeUnit.equals("m")) {
								endTime += timeValue * 60L * 1000L;																
							}
						}
					}
					endDate = new Date(endTime);
				}
				
				// Sale Method:
				// 0 - Auction listing
				// 1 - Fixed price listing
				// 2 - Declining price listing
				
				boolean isAuction = docJSONObject.getString("sale_method").equals("0");
				
				Double buyItNowPrice;
				TicketType ticketType;
				
				if (isAuction) {
					// for auction, there is no buyItNow price
					buyItNowPrice = null;
					ticketType = TicketType.AUCTION;
				} else {
					// otherwise the buyItNowPrice is the current price
					buyItNowPrice = price;
					ticketType = TicketType.REGULAR;
				}
				
				String ticketMediumId = docJSONObject.getString("ticket_medium_id");
				String deliveryIcon = docJSONObject.getString("delivery_icon");
				TicketDeliveryType ticketDeliveryType = null;
				if (ticketMediumId.equals("3") || ticketMediumId.equals("2")) {
					ticketDeliveryType = TicketDeliveryType.INSTANT;
				} else if (deliveryIcon.equals("edelivery")){
					// if the icon is edelivery or the medium id is 2(PDF Ticket)
					ticketDeliveryType = TicketDeliveryType.EDELIVERY;					
				}
				
				// seats can be:
				// - 3,4,5,6
				// - 15,16
				// - ga-5,ga-6,ga-7,ga-8
				// - General Admission
				String ticketHitSeats = null;
				if (docJSONObject.has("seats")) {
					Integer startSeat = null;
					Integer endSeat = null;		
					String seats = docJSONObject.getString("seats");
					String[] seatTokens = seats.split(",");
					
					for(String seatToken: seatTokens) {
						Matcher seatMatcher = seatPattern.matcher(seatToken);
						if (seatMatcher.find()) {
							int seat = Integer.parseInt(seatMatcher.group(1));
							if (startSeat == null || seat < startSeat) {
								startSeat = seat;
							}
	
							if (endSeat == null || seat > endSeat) {
								endSeat = seat;							
							}
						}
					}
					
					if (startSeat == null) {						
					} else if (startSeat == endSeat) {
						ticketHitSeats = "" + startSeat;
					} else {
						ticketHitSeats = startSeat + "-" + endSeat;
					}
					// System.out.println("SEATS=" + seats + ",start=" + startSeat + ",end=" + endSeat);
				}
				
				TicketHit ticketHit = new TicketHit(Site.STUB_HUB, id,
						ticketType,
						quantity, split,
						row, section, price, buyItNowPrice, "StubHub", ticketStatus);


				ticketHit.setTicketListingCrawl(ticketListingCrawl);
				ticketHit.setEndDate(endDate);
				ticketHit.setTicketDeliveryType(ticketDeliveryType);
				ticketHit.setSoldQuantity(quantity - quantity_remain);
				
				ticketHit.setSeat(ticketHitSeats);

				long startIndexationTime = System.currentTimeMillis();
				indexTicketHit(ticketHitIndexer, ticketHit);
				ticketListingCrawl.setIndexationTime(ticketListingCrawl.getIndexationTime() + System.currentTimeMillis() - startIndexationTime);
				
				// System.out.println("TICKET HIT=" + ticketHit);
			}
			start += itemsPerPage;
		}	while (start < total);
		
		ticketListingCrawl.setExtractionTime(System.currentTimeMillis() - startProcessingTime - ticketListingCrawl.getFetchingTime() - ticketListingCrawl.getIndexationTime());

		return true;
	}

	@Override
	public boolean isValidUrl(String url) {
		// valid url: http://www.stubhub.com/mlb-all-star-game-tickets/mlb-all-star-strip-7-11-2009-750829/
		// http://www.stubhub.com/jimmy-buffett-tickets/jimmy-buffett-honolulu-waikiki-shell-2-28-2009-790072/
		// http://www.stubhub.com/super-bowl-tickets/super-bowl-2-1-2009-565762/
		// http://www.stubhub.com?q=eventId=62789
		return url.matches("^http://www.stubhub.com/.*[0-9]*/$") || url.matches("^http://www.stubhub.com/\\?event_id=\\d+$");		
	}

	@Override
	public String getTicketListingUrl(TicketListingCrawl ticketListingCrawl) {
		return "http://www.stubhub.com/largesellers-ticketcenter/";
		//return ticketListingCrawl.getQueryUrl();
	}
	
	@Override
	public String getTicketItemUrl(TicketListingCrawl ticketListingCrawl, Ticket ticket) {
		return "http://www.stubhub.com/largesellers-ticketcenter/";
//		if (ticket.getTicketListingCrawl().getQueryUrl().indexOf("?") >= 0) {
//			return ticket.getTicketListingCrawl().getQueryUrl() + "&ticket_id=" + ticket.getItemId();
//		}
//
//		return ticket.getTicketListingCrawl().getQueryUrl() + "?ticket_id=" + ticket.getItemId();
	}
}