package com.admitone.tmat.ticketfetcher;
/*
package com.yoonew.tixmarket.fetcher;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.springframework.beans.factory.InitializingBean;

import com.ebay.sdk.ApiContext;
import com.ebay.sdk.call.GetItemCall;
import com.ebay.soap.eBLBaseComponents.AttributeSetArrayType;
import com.ebay.soap.eBLBaseComponents.AttributeSetType;
import com.ebay.soap.eBLBaseComponents.AttributeType;
import com.ebay.soap.eBLBaseComponents.DetailLevelCodeType;
import com.ebay.soap.eBLBaseComponents.ItemType;
import com.ebay.soap.eBLBaseComponents.ListingStatusCodeType;
import com.yoonew.tixmarket.enums.TicketStatus;

public class EBayTicketItemFetcher extends TicketItemFetcher implements InitializingBean {
	private ApiContext ebayApiContext;
	
	public EBayTicketItemFetcher() {		
	}
	
	@Override
	public TicketHit fetchTicketItem(String itemId) throws Exception {
		
		DetailLevelCodeType[] itemDetailLevels = new DetailLevelCodeType[] {
				DetailLevelCodeType.ITEM_RETURN_ATTRIBUTES
		};

		GetItemCall itemCall = new GetItemCall(ebayApiContext);
		itemCall.setDetailLevel(itemDetailLevels);
		itemCall.setIncludeItemSpecifics(true);
		
		ItemType itemType = itemCall.getItem(itemId);

		AttributeSetArrayType attributeSetArrayType = itemType.getAttributeSetArray();
		
		String row = null;
		String section = null;
		String venueCity = null;
		String venueState = null;
		String venueName = null;
		int lotSize = 1;
		
		Date eventDate = null;
		
		if (attributeSetArrayType != null) {
			for (AttributeSetType attributeSetType: attributeSetArrayType.getAttributeSet()) {
				for(AttributeType attributeType: attributeSetType.getAttribute()) {
					
					switch(attributeType.getAttributeID()) {

					case 1:
						lotSize = Integer.parseInt(attributeType.getValue(0).getValueLiteral());
						break;

					case 10400:
						section = attributeType.getValue(0).getValueLiteral();
						break;
						
					case 10399:
						row = attributeType.getValue(0).getValueLiteral();
						break;

					case 10:
						venueCity = attributeType.getValue(0).getValueLiteral();
						break;

					case 63:
						venueName = attributeType.getValue(0).getValueLiteral();
						break;

					case 9:
						venueState = attributeType.getValue(0).getValueLiteral();
						break;

					case 48587:
						 // format YYYYMMDD. e.g. 20090323
						String formattedDate = attributeType.getValue(0).getValueLiteral();
						DateFormat format = new SimpleDateFormat("yyyyMMdd");
						format.setTimeZone(TimeZone.getTimeZone("UTC"));
						
						eventDate = format.parse(formattedDate);
						break;
					}
				}
			}
		}
				
		double currentPrice = itemType.getSellingStatus().getCurrentPrice().getValue() / lotSize;
		Double buyItNowPrice = null;
		if (itemType.getBuyItNowPrice() != null) {
			buyItNowPrice = itemType.getBuyItNowPrice().getValue() / lotSize;
		}
		
		Date endDate = itemType.getListingDetails().getEndTime().getTime();
		
		TicketStatus ticketStatus;
		ListingStatusCodeType listingStatusCodeType = itemType.getSellingStatus().getListingStatus();
		if (listingStatusCodeType.equals(ListingStatusCodeType.ACTIVE)) {
			ticketStatus = TicketStatus.ACTIVE;
		} else {
			// two cases: the ticket can have been sold or not
			if (itemType.getSellingStatus().getQuantitySold() > 0) {
				ticketStatus = TicketStatus.SOLD;							
			} else {
				ticketStatus = TicketStatus.EXPIRED;											
			}
		}
		
		TicketHit ticketHit = new TicketHit(Site.EBAY, itemType.getItemID(), 
				itemType.getQuantity() * lotSize, itemType.getSellingStatus().getQuantitySold() * lotSize,
				"no description",
				venueState,  venueCity, venueName,
				row, section, eventDate,	
				endDate, currentPrice, buyItNowPrice, ticketStatus);
		ticketHit.setLotSize(lotSize);

		if (itemType.getSeller() != null) {
			ticketHit.setSeller(itemType.getSeller().getUserID());
		}
		return ticketHit;
	}
	
	@Override
	public void afterPropertiesSet() throws Exception {
		if (ebayApiContext == null) {
			throw new Exception("Property ebayApiContext must be set");
		}		
	}

	public ApiContext getEbayApiContext() {
		return ebayApiContext;
	}

	public void setEbayApiContext(ApiContext ebayApiContext) {
		this.ebayApiContext = ebayApiContext;
	}


}
*/