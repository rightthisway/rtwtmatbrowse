package com.admitone.tmat.ticketfetcher;

import java.io.Serializable;
import java.util.Date;

import org.apache.http.client.HttpClient;
import org.apache.http.conn.params.ConnRoutePNames;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.CrawlerHistoryManagement;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.indexer.TicketHitIndexer;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;

/**
 * Abstract parent class for all the ticket listing fetchers.
 */
public abstract class TicketListingFetcher implements Cloneable, Serializable {
	private TicketItemFetcher ticketItemFetcher;
		
	public TicketListingFetcher() {
	}
	
	public abstract boolean runFetchTicketListing(SimpleHttpClient httpClient, TicketHitIndexer ticketHitIndexer, TicketListingCrawl ticketListingCrawl) throws Exception;

	public boolean fetchTicketListing(TicketHitIndexer ticketHitIndexer, TicketListingCrawl ticketListingCrawl) throws Exception {
		ticketListingCrawl.setCancelled(false);
		ticketListingCrawl.resetStats();
		
		SimpleHttpClient httpClient = null;
		try {
			httpClient = createHttpClient(ticketListingCrawl);
			return runFetchTicketListing(httpClient, ticketHitIndexer, ticketListingCrawl);
		} finally {
			releaseHttpClient(httpClient);
		}
	}
	
	protected SimpleHttpClient createHttpClient(TicketListingCrawl crawl) throws Exception {
		return HttpClientStore.createHttpClient(crawl.getSiteId());
	}

	protected void releaseHttpClient(SimpleHttpClient httpClient) {
		HttpClientStore.releaseHttpClient(httpClient);
	}

	public TicketItemFetcher getTicketItemFetcher() {
		return ticketItemFetcher;
	}

	public void setTicketItemFetcher(TicketItemFetcher ticketItemFetcher) {
		this.ticketItemFetcher = ticketItemFetcher;
	}
	
	public void saveCounterHistory(TicketListingCrawl ticketListingCrawl,int counter,String  proxy){
		try{
			if(ticketListingCrawl.getId()!=null){
				CrawlerHistoryManagement crawlerHistoryManagement = new CrawlerHistoryManagement();
				crawlerHistoryManagement.setCrawlId(ticketListingCrawl.getId());
				crawlerHistoryManagement.setSiteId(ticketListingCrawl.getSiteId());
				crawlerHistoryManagement.setCounter(counter);
				crawlerHistoryManagement.setSuccessfulHit(new Date());
				crawlerHistoryManagement.setSystem(proxy);   // for EBAY system
				DAORegistry.getCrawlerHistoryManagementDAO().save(crawlerHistoryManagement);
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	public void indexTicketHit(TicketHitIndexer ticketHitIndexer, TicketHit ticketHit) {
		int result = ticketHitIndexer.indexTicketHit(ticketHit);
		TicketListingCrawl crawl = ticketHit.getTicketListingCrawl();
		
		if (result == TicketHitIndexer.NEW_ITEM) {
			crawl.setNewItemIndexed(crawl.getNewItemIndexed() + 1);
			crawl.setItemIndexed(crawl.getItemIndexed() + 1);
		} else if (result == TicketHitIndexer.EXISTING_ITEM) {
			crawl.setItemIndexed(crawl.getItemIndexed() + 1);
		} else if (result == TicketHitIndexer.ERROR) {
			crawl.setItemErrorIndexed(crawl.getItemErrorIndexed() + 1);					
		}		
	}
	
	public boolean isValidUrl(String url) {
		return true;
	}
	
	public String getTicketListingUrl(TicketListingCrawl ticketListingCrawl) {
		return null;
	}
	
	public String getTicketItemUrl(TicketListingCrawl ticketListingCrawl, Ticket ticket) {
		return null;
	}

	public Object clone() throws CloneNotSupportedException {
		TicketListingFetcher copy = (TicketListingFetcher)super.clone();
		copy.setTicketItemFetcher(ticketItemFetcher);
		return copy;
	}		
}
