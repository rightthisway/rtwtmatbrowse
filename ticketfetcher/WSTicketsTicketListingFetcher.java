package com.admitone.tmat.ticketfetcher;

import java.io.InputStream;
import java.io.StringReader;
import java.net.URI;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ProtocolException;
import org.apache.http.client.RedirectHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.apache.xalan.xsltc.trax.SAX2DOM;
import org.apache.xpath.XPathAPI;
import org.apache.xpath.objects.XObject;
import org.ccil.cowan.tagsoup.Parser;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.enums.TicketType;
import com.admitone.tmat.indexer.TicketHitIndexer;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;
import com.admitone.tmat.utils.httpclient.UncompressedHttpEntity;

/**
 * WSTickets Ticket Listing Fetcher. 
 */
public class WSTicketsTicketListingFetcher extends TicketListingFetcher{
    private Pattern iFrameUrlEventPattern = Pattern.compile("EVID=([0-9]*)");
	private Pattern sectionPattern1 = Pattern.compile("Sec: (.*)\\s-");
	private Pattern sectionPattern2 = Pattern.compile("Sec: (.*)");
	private Pattern sectionPattern3 = Pattern.compile("Sections: (.*)");
	
	public WSTicketsTicketListingFetcher() {
	}
		
	@Override
	public boolean runFetchTicketListing(SimpleHttpClient httpClient, TicketHitIndexer ticketHitIndexer, TicketListingCrawl ticketListingCrawl) throws Exception {
		
		if (!(ticketListingCrawl.getSiteId().equals(Site.WS_TICKETS))) {
			throw new Exception(this.getClass().getName() + ".fetchTicketListing parameter must be a wstickets crawl");
		}
		
		long startProcessingTime = System.currentTimeMillis();
		long startFetchingTime;

		WSRedirectHandler redirectHandler = new WSRedirectHandler();
		httpClient.setRedirectHandler(redirectHandler);
		
		HttpGet httpGet;
		HttpResponse response;
		UncompressedHttpEntity entity;
		Parser parser;
		SAX2DOM sax2dom;
		Node document;
		
		// if the url is like:
		// http://tickets.wstickets.com/feed/tickets.asp?SITEID=1&EVID=101590
		// http://tickets.wstickets.com/feed/tickets.asp?Siteid=1&EVID=100335
		String iframeUrl;
		String eventId = null;
		if (ticketListingCrawl.getQueryUrl().indexOf("tickets.wstickets.com/feed/tickets.asp") >= 0) {
			iframeUrl = ticketListingCrawl.getQueryUrl();

//			System.out.println("WS IFRAME URL=" + iframeUrl);
			Matcher eventMatcher = iFrameUrlEventPattern.matcher(iframeUrl);
			eventMatcher.find();
			eventId = eventMatcher.group(1);
		} else {		
			// to get the iframe url which contains the ticket list
			startFetchingTime = System.currentTimeMillis();
			httpGet = new HttpGet(ticketListingCrawl.getQueryUrl().replace(" ", "+"));
			httpGet.addHeader("Host", "www.wstickets.com");		
			httpGet.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
			httpGet.addHeader("Accept-Encoding", "gzip,deflate");
			httpClient.execute(httpGet);
	
			response = httpClient.execute(httpGet);
			entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
			String content =EntityUtils.toString(entity);
			ticketListingCrawl.setFetchedByteCount(ticketListingCrawl.getFetchedByteCount() + entity.getReadByteCount());			
			ticketListingCrawl.setFetchingTime(ticketListingCrawl.getFetchingTime() + System.currentTimeMillis() - startFetchingTime);
			
			parser = new Parser();
			parser.setFeature("http://xml.org/sax/features/namespace-prefixes", true);
			// to define the html: prefix (off by default)
			sax2dom = new SAX2DOM();
			parser.setContentHandler(sax2dom);			
			parser.parse(new InputSource(new StringReader(content)));			
			document = sax2dom.getDOM();		
			iframeUrl = XPathAPI.selectSingleNode(document, "//html:iframe[@id='FEEDWIN']/@src").getTextContent();
			// System.out.println("IFRAME=" + iframeUrl);
		}

		// System.out.println("IFRAME SRC=" + iframeUrl);
		if (iframeUrl.indexOf("www.eventinventory.com") >= 0
				|| (redirectHandler.getRedirectUrl() != null && redirectHandler.getRedirectUrl().indexOf("www.eventinventory.com") >= 0)) {
			System.out.println("ERROR: not crawl");
			throw new Exception("Use the EventInventory Crawl  to handle this event");
		}

		// to get the iframe url which contains the ticket list
		startFetchingTime = System.currentTimeMillis();
		httpGet = new HttpGet(iframeUrl.replace(" ", "+"));
		httpGet.addHeader("Host", "www.wstickets.com");		
		httpGet.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
		httpGet.addHeader("Accept-Encoding", "gzip,deflate");
		httpGet.addHeader("Referer", ticketListingCrawl.getQueryUrl());				
		response = httpClient.execute(httpGet);
		entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());		
		String content = EntityUtils.toString(entity);
		ticketListingCrawl.setFetchedByteCount(ticketListingCrawl.getFetchedByteCount() + entity.getReadByteCount());			
		ticketListingCrawl.setFetchingTime(ticketListingCrawl.getFetchingTime() + System.currentTimeMillis() - startFetchingTime);		
		
		if (eventId == null) {
			// System.out.println("Redirect URL=" + redirectHandler.getRedirectUrl());
			if (redirectHandler.getRedirectUrl() == null || redirectHandler.getRedirectUrl().contains("www.eventinventory.com")) {
				throw new Exception("Use the EventInventory crawl to handle this event");
			}
			Matcher eventMatcher = iFrameUrlEventPattern.matcher(redirectHandler.getRedirectUrl());
			eventMatcher.find();
			eventId = eventMatcher.group(1);
		}
		
		parser = new Parser();
		parser.setFeature("http://xml.org/sax/features/namespace-prefixes", true);
		// to define the html: prefix (off by default)
		sax2dom = new SAX2DOM();
		parser.setContentHandler(sax2dom);			
		parser.parse(new InputSource(new StringReader(content)));
		document = sax2dom.getDOM();
	    
		NodeList trNodeList = XPathAPI.selectNodeList(document, "//html:table[@id='PriceTable']/html:tr");
		
		// System.out.println("TRLENGTH=" + trNodeList.getLength());
		for (int i = 0 ; i < trNodeList.getLength() ; i++) {
			Node trNode = trNodeList.item(i);
		//	String title = XPathAPI.selectSingleNode(trNode, "html:td/html:b").getTextContent();
			
			// for MLB: Sec: 501-511, 589-597
			// for SB: Sec: 302-306, 315-319, 327-331, 340-344 - all rows
			// for CBB: Sec: 315-325, 337-347
			String section = null;
			Node sectionRowNode = XPathAPI.selectSingleNode(trNode, "html:td/html:span");
			if (sectionRowNode != null) {
				String sectionRow = sectionRowNode.getTextContent();
			
				Matcher sectionMatcher = sectionPattern1.matcher(sectionRow);
				if (sectionMatcher.find()) {
					section = sectionMatcher.group(1);
				} else {				
					sectionMatcher = sectionPattern2.matcher(sectionRow);
					if (sectionMatcher.find()) {
						section = sectionMatcher.group(1);
					} else {
						sectionMatcher = sectionPattern3.matcher(sectionRow);					
						if (sectionMatcher.find()) {
							section = sectionMatcher.group(1);
						}
					}
				}
			}
			
			// System.out.println("SECTION =" + section);

			Double price = Double.parseDouble(XPathAPI.selectSingleNode(trNode, "html:form/html:input[@name='price']/@value").getTextContent());

			NodeList nodeList = XPathAPI.selectNodeList(trNode, "//html:option[@class='QtyOpt']");
			int minQuantity = Integer.MAX_VALUE;
			int maxQuantity = Integer.MIN_VALUE;
			
			for (int j = 0 ; j < nodeList.getLength() ; j++) {
				Node node = nodeList.item(j);
				int quantity;
				if (node.getTextContent().equals("Pairs Only")) {
					quantity = 2;
				} else {
					quantity = Integer.parseInt(node.getTextContent());
				}
				
				minQuantity = Math.min(quantity, minQuantity);
				maxQuantity = Math.max(quantity, maxQuantity);
			}
			
			// http://tickets.wstickets.com/feed/tickets.asp?Siteid=1&EVID=93635
			// build id: EVID + SEAT CAT
			String seatCat = XPathAPI.selectSingleNode(trNode, "html:form/html:input[@name='seat_cat']/@value").getTextContent();
			
			String itemId = eventId + "-" + seatCat;
				

			TicketHit ticketHit = new TicketHit(Site.WS_TICKETS, itemId, 
					TicketType.REGULAR,
					maxQuantity, minQuantity, 
					null, section, price, price, "wstickets", TicketStatus.ACTIVE);
			ticketHit.setTicketListingCrawl(ticketListingCrawl);

			long startIndexationTime = System.currentTimeMillis();
			indexTicketHit(ticketHitIndexer, ticketHit);						
			ticketListingCrawl.setIndexationTime(ticketListingCrawl.getIndexationTime() + System.currentTimeMillis() - startIndexationTime);
		}

		ticketListingCrawl.setExtractionTime(System.currentTimeMillis() - startProcessingTime - ticketListingCrawl.getFetchingTime() - ticketListingCrawl.getIndexationTime());		
		return true;
	}

	@Override
	public boolean isValidUrl(String url) {
		// valid url: http://www.wstickets.com/football/nfl/Super_Bowl_Tickets.html
		// http://wstickets.com/baseball/mlb/mlb_all_star_game_tickets.html
		return url.matches("^http://.*wstickets.com/.*$");		
	}

	
	public static class WSRedirectHandler implements RedirectHandler{
		private String redirectUrl;
		
		public WSRedirectHandler() {
		}

		public URI getLocationURI(HttpResponse response, HttpContext context)
				throws ProtocolException {			
			return URI.create("http://tickets.wstickets.com/feed/" +  redirectUrl);
		}

		public boolean isRedirectRequested(HttpResponse response, HttpContext context) {
			Header[] headers = response.getAllHeaders();
			for(Header header: headers) {
				// System.out.println("Header=" + header.getName() + "=" + header.getValue());
				if (header.getName().equalsIgnoreCase("Location")) {
					redirectUrl = header.getValue();
					return true;
				}
			}
			
			return false;
		}

		public String getRedirectUrl() {
			return redirectUrl;
		}		
	}

	public String getTicketListingUrl(TicketListingCrawl ticketListingCrawl) {
		return ticketListingCrawl.getQueryUrl();
	}	
}
