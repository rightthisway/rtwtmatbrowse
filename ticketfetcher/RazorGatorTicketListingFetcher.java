package com.admitone.tmat.ticketfetcher;

import java.text.NumberFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.enums.TicketType;
import com.admitone.tmat.indexer.TicketHitIndexer;
import com.admitone.tmat.utils.TextUtil;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;
import com.admitone.tmat.utils.httpclient.UncompressedHttpEntity;

/**
 * Razor Gator Ticket Listing Fetcher. 
 * @author frederic
 */
public class RazorGatorTicketListingFetcher extends TicketListingFetcher {	
	private final Logger logger = LoggerFactory.getLogger(RazorGatorTicketListingFetcher.class);
	
	private Pattern recordPattern = Pattern.compile("class=\"recordcount\".*?of\\s+(\\d+)\\s+Tickets");
	private Pattern ticketTablePattern = Pattern.compile("<table class=\"list\"(.*?)</table>", Pattern.DOTALL);
	private Pattern trPattern = Pattern.compile("<tr>(.*?)</tr>", Pattern.DOTALL);
	private Pattern sectionPattern = Pattern.compile("tdSection.*?<p>(.*?)</p>", Pattern.DOTALL);
	//private Pattern notePattern = Pattern.compile("noteLine\">(.*?)</span>", Pattern.DOTALL);
	private Pattern rowPattern = Pattern.compile("tdRow\".*?<p>(.*?)</p>");
	private Pattern optionPattern = Pattern.compile("<option.*?>(\\d+)</option>");
	private Pattern pricePattern = Pattern.compile("tdPrice.*?<p>(.*?)</p>", Pattern.DOTALL);
	
	private Pattern selectItemIdPattern = Pattern.compile("<select id=\"Quant(-?\\d+)\"");

	public RazorGatorTicketListingFetcher() {
	}
		
	/**
	 * Fetch the razorgator ticket listing and extract the data. 
	 */
	@Override
	public boolean runFetchTicketListing(SimpleHttpClient httpClient, TicketHitIndexer ticketHitIndexer, TicketListingCrawl ticketListingCrawl) throws Exception {
		
		if (!(ticketListingCrawl.getSiteId().equals(Site.RAZOR_GATOR))) {
			throw new Exception(this.getClass().getName() + ".fetchTicketListing parameter must be a razorgator crawl");
		}
		
		// http://www.razorgator.com/tickets/sports/basketball/nba/atlanta-hawks-tickets/?performance=6595247|1

		int page = 0;
		int pageCount = -1;
		int pageSize = 1000;

		Long startProcessingTime = System.currentTimeMillis();
		long fetchingTime = 0;
		long indexationTime = 0;
		
		try {
			do {
				Long startFetchingTime = System.currentTimeMillis();
				HttpGet httpGet = new HttpGet(ticketListingCrawl.getQueryUrl().replace("|", "%7C") + "&Sort=TicketType&PageSize=" + pageSize + "&Page=" + page + "&SortDirection=Ascending");
				
				HttpResponse response = httpClient.execute(httpGet, false);
				UncompressedHttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());			
				String content = EntityUtils.toString(entity);
				ticketListingCrawl.setFetchedByteCount(ticketListingCrawl.getFetchedByteCount() + entity.getReadByteCount());					

				fetchingTime += System.currentTimeMillis() - startFetchingTime;
	
				if (pageCount == -1) {				
					Matcher recordMatcher = recordPattern.matcher(content);
					if (!recordMatcher.find()) {
						return true;
					}
					int recordCount = Integer.parseInt(recordMatcher.group(1)); 
					
					pageCount = (recordCount + pageSize - 1) / pageSize;
					// System.out.println("PAGE_COUNT=" + pageCount);
				}			
				page++;
				
				Matcher ticketTableMatcher = ticketTablePattern.matcher(content);			
				if (!ticketTableMatcher.find()) {
					return true;
				}
				
				String ticketTable = ticketTableMatcher.group(1);
	
				Matcher trMatcher = trPattern.matcher(ticketTable);
				while(trMatcher.find()) {
					String tr = trMatcher.group(1);
					// if in table header, skip
					if (!tr.contains("<td")) {
						continue;
					}
					
					Matcher sectionMatcher = sectionPattern.matcher(tr);
					// there can be a unique cell that takes 6 columns
					if (!sectionMatcher.find()) {
						continue;
					}
					String section = sectionMatcher.group(1);				
					// System.out.println("SECTION=" + section);
					
					//Matcher noteMatcher = notePattern.matcher(tr);
					//noteMatcher.find();
					//String note = noteMatcher.group(1);
					// System.out.println("NOTE=" + note);
	
					String row = null;
					Matcher rowMatcher = rowPattern.matcher(tr);
					if (rowMatcher.find()) {
						row = TextUtil.removeExtraWhitespaces(StringEscapeUtils.unescapeHtml(rowMatcher.group(1).replace("&nbsp;", " "))).trim();
					}
					// System.out.println("ROW=" + row);
	
					Matcher optionMatcher = optionPattern.matcher(tr);
					int minQuantity = Integer.MAX_VALUE;
					int maxQuantity = Integer.MIN_VALUE;
					while(optionMatcher.find()) {
						int quantity = Integer.parseInt(optionMatcher.group(1));
						minQuantity = Math.min(quantity, minQuantity);
						maxQuantity = Math.max(quantity, maxQuantity);					
					}
					// System.out.println("MIN QTY=" + minQuantity);
					// System.out.println("MAX QTY=" + maxQuantity);
	
					Matcher priceMatcher = pricePattern.matcher(tr);
					priceMatcher.find();
					NumberFormat numberFormat = NumberFormat.getInstance();
			        Number number = numberFormat.parse(priceMatcher.group(1).trim().substring(1));
					Double price = number.doubleValue();
					// System.out.println("PRICE=" + price);
					
					Matcher itemIdMatcher = selectItemIdPattern.matcher(tr);
					itemIdMatcher.find();
					String itemId = itemIdMatcher.group(1); 
					// System.out.println("ITEMID=" + itemId);				
					
					TicketHit ticketHit = new TicketHit(Site.RAZOR_GATOR, itemId,
							TicketType.REGULAR,
							maxQuantity, minQuantity,
							row, section, price, price, "RazorGator", TicketStatus.ACTIVE);
	
					ticketHit.setTicketListingCrawl(ticketListingCrawl);

					Long startIndexationTime = System.currentTimeMillis();
					indexTicketHit(ticketHitIndexer, ticketHit);					
					indexationTime += System.currentTimeMillis() - startIndexationTime;
					
				}
			} while (page < pageCount);
			
			return true;
		} finally {
			long totalProcessingTime = System.currentTimeMillis() - startProcessingTime;
			ticketListingCrawl.setFetchingTime(fetchingTime);
			ticketListingCrawl.setExtractionTime(totalProcessingTime - fetchingTime - indexationTime);
			ticketListingCrawl.setIndexationTime(indexationTime);
		}
	}
	
	/**
	 * Return true if the ticket listing url is valid.
	 */
	@Override
	public boolean isValidUrl(String url) {
		// valid url: 
		// http://www.razorgator.com/tickets/sports/basketball/nba/atlanta-hawks-tickets/?performance=6595315|1
		// http://www.razorgator.com/tickets/concerts/rock-pop/bonnaroo-music-festival-tickets/?performance=6959009|1
		// http://www.razorgator.com/tickets/sports/basketball/ncaa-tournaments/acc-tournament-tickets/?performance=6240533|1
		return url.matches("^http://www.razorgator.com/tickets/.*performance=.*$");		
	}

	/**
	 * Return the web ticket listing url from the razorgator crawl.
	 */
	@Override
	public String getTicketListingUrl(TicketListingCrawl ticketListingCrawl) {
		return ticketListingCrawl.getQueryUrl();
	}	
}
