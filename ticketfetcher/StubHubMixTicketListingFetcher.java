package com.admitone.tmat.ticketfetcher;

import java.io.ByteArrayInputStream;
import java.net.URI;
import java.text.NumberFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.ProtocolException;
import org.apache.http.client.RedirectHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.apache.xpath.XPathAPI;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.enums.TicketDeliveryType;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.enums.TicketType;
import com.admitone.tmat.indexer.TicketHitIndexer;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;
import com.admitone.tmat.utils.httpclient.UncompressedHttpEntity;

/**
 * StubHub Ticket Listing Fetcher
 * The stubhub feed does not contain the latest data. There are maybe a delay of 5 to 10mn compared to the data on the website.
 * Some tickets which appears on the listing on the website are not in the feed, also some tickets appear in the feed whereas they are already expired.
 * 
 * On one hand, we cannot get the ticket listing from the website easily because we would need to fetch the ticket listing web page and also
 * all individual page of each ticket because some informations such as the quantity and notes are not present in the ticket listing page itself.
 * So for a listing of 1000 items, we would need to fetch 1001 web pages which is impracticable.
 *
 * On the other hand, we cannot use the feed listing because the data are out of date.
 *
 * To solve this, we will fetch the ticket listing from the website (ticket ids only) and the ticket listing feed (ticket id, qty, seat, row, section, ...). Let's call the first one TW and the second one TF.
 * There are two cases:
 * - a ticket which is not in TW and not in TF will be ignored
 * - a ticket which is not in TW and in TF will be ignored
 * - a ticket which is in TW and in TF will be indexed
 * - a ticket which is in TW but not in TF. We would need to fetch the individual ticket page based on the ticket id on the website(qty, section, row, notes). And then index the ticket.
 */
public class StubHubMixTicketListingFetcher extends TicketListingFetcher {	
	private final Logger logger = LoggerFactory.getLogger(StubHubWebTicketListingFetcher.class);
	
	// for ticket which has a section: 100:112 => 112
	private static final Pattern sectionPattern = Pattern.compile("^\\d+:(\\d+)$");

	private static final Pattern seatPattern = Pattern.compile("(\\d+)");
	
	private Pattern timeLeftPattern = Pattern.compile(">(.*)</");
	
	private Pattern xbidPattern = Pattern.compile("qsObj\\['xbid'\\]\\s*=\\s*([0-9]+);");
	private Pattern cobrandIdPattern = Pattern.compile("qsObj\\['cobrand'\\]\\s*=\\s*([0-9]+);");

	private Pattern stubHubAuctionPattern = Pattern.compile("<div class=\"left\">Auction</div>");
	private Pattern stubHubEDeliveryPattern = Pattern.compile("<b>Delivery Options:</b> eDelivery");
	private Pattern stubHubInstantEDeliveryPattern = Pattern.compile("<b>Delivery Options:</b> Instant eDelivery");
	//private Pattern stubHubNotePattern = Pattern.compile("<b>Seller comments:</b>\\s*(.*?)</p>");
	private Pattern stubHubTimeLeftPattern = Pattern.compile("Time Left:</div><div class=\"content\">(.*?)</div>");	
	private Pattern stubHubSectionPattern = Pattern.compile("<div class=\"left\">Section:</div><div class=\"content\">(.*?)</div>");	
	private Pattern stubHubRowPattern = Pattern.compile("<div id=\"row\" class=\"row\"><div class=\"left\">Row:</div><div class=\"content\">(.*?)</div>");	
	private Pattern stubHubPricePattern = Pattern.compile("EVID-[0-9]+\">[$]([0-9.]*)\\s*");	
	private Pattern stubHubQuantitySelectPattern = Pattern.compile("<select name=\"quantity_selected\">(.*?)</select>", Pattern.DOTALL);	
	private Pattern stubHubQuantityOptionPattern = Pattern.compile("<option value=\"(\\d+)\">");	
	
	
	public StubHubMixTicketListingFetcher() {
	}

	@Override
	public boolean runFetchTicketListing(SimpleHttpClient httpClient, TicketHitIndexer ticketHitIndexer, TicketListingCrawl ticketListingCrawl) throws Exception {
		if (!(ticketListingCrawl.getSiteId().equals(Site.STUB_HUB))) {
			throw new Exception(this.getClass().getName() + ".fetchTicketListing parameter must be a stubhub crawl");
		}		
		
		Set<String> ticketIdsInWeb = new HashSet<String>();
		Set<String> ticketIdsInFeed = new HashSet<String>();

		//
		// EXTRACT EVENT ID
		//
		StubHubRedirectHandler redirectHandler = new StubHubRedirectHandler();
		httpClient.setRedirectHandler(redirectHandler);
		
		long startProcessingTime = System.currentTimeMillis();
		
		String eventId;
		
		String url = ticketListingCrawl.getQueryUrl();
		int eventIdPos = url.indexOf("event_id="); 
		if (eventIdPos > 0) {
			eventId = url.substring(eventIdPos + 9, url.length());
		} else {
			String[] tokens = url.split("-");
			eventId = tokens[tokens.length - 1].split("/")[0];
		}

		// 
		// CHECK IF THE EVENT IS ACTIVE OR NOT
		//
		long startFetchingTime = System.currentTimeMillis();
		HttpPost httpPost = new HttpPost("http://partnerfeed.stubhub.com/listingCatalog/select/?q=%2BstubhubDocumentType%3Aevent%0D%0A%2Bevent_id%3A"
				+ eventId + "%0D%0A%0D%0A&version=2.2&fl=active&wt=json");
		httpPost.addHeader("Host", "partnerfeed.stubhub.com");		
		httpPost.addHeader("Accept", "application/json, text/javascript, */*");
		
		HttpResponse response = httpClient.execute(httpPost);			
		UncompressedHttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());			
		String content = EntityUtils.toString(entity);
		
 		ticketListingCrawl.setFetchedByteCount(ticketListingCrawl.getFetchedByteCount() + entity.getReadByteCount());						
		ticketListingCrawl.setFetchingTime(ticketListingCrawl.getFetchingTime() + System.currentTimeMillis() - startFetchingTime);		
		
		if (content.indexOf("\"active\":\"0\"") >= 0) {
			throw new Exception("The event seems to have been deleted on StubHub. You might probably delete the crawl.");
		}
		
		int start = 0;
		int total;
		
		int itemsPerPage = 1000;
		
		//
		// FETCH TICKET IDS FROM WEB LISTING
		//
		startFetchingTime = System.currentTimeMillis();
		HttpGet httpGet = new HttpGet("http://www.stubhub.com/?event_id=" + eventId);
		httpGet.addHeader("Host", "www.stubhub.com");		
		httpGet.addHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5 ");
		httpGet.addHeader("Accept", "application/json, text/javascript, */*");
		httpGet.addHeader("Accept-Language", "en-us,en;q=0.5");
		httpGet.addHeader("Accept-Encoding", "gzip,deflate");
		httpGet.addHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");

		response = httpClient.execute(httpGet);
		entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
		content = EntityUtils.toString(entity);		
		ticketListingCrawl.setFetchingTime(ticketListingCrawl.getFetchingTime() + System.currentTimeMillis() - startFetchingTime);
 		ticketListingCrawl.setFetchedByteCount(ticketListingCrawl.getFetchedByteCount() + entity.getReadByteCount());
 		
 		String eventPageUrl = redirectHandler.getRedirectUrl();
		
		// Extract xbid		
		Matcher xbidMatcher = xbidPattern.matcher(content);
		String xbid = "";
		if (!xbidMatcher.find()) {
			System.out.println("DO NOT FIND XBID FOR EVENT " + eventId + " IN TICKET LISTING");
		} else {
			xbid = xbidMatcher.group(1).trim();
		}

		// Extract cobrandId		
		Matcher cobrandIdMatcher = cobrandIdPattern.matcher(content);
		String cobrandId = "";
		if (!cobrandIdMatcher.find()) {
			System.out.println("DO NOT FIND COBRAND FOR EVENT " + eventId + " IN TICKET LISTING");			
		} else {
			cobrandId = cobrandIdMatcher.group(1).trim();			
		}


		int webOffset = 1;
		int webTotal = -1;
		
		do {			
			startFetchingTime = System.currentTimeMillis();
			httpPost = new HttpPost("http://www.stubhub.com/listingCatalog/getTicketsByEvent?limit=" + itemsPerPage + "&offset=" + webOffset + "&sortby=p&orderby=a&eventId="
					+ eventId + "&xbid=" + xbid + "&cobrandId=" + cobrandId);
			System.out.println("URL=" + ("http://www.stubhub.com/listingCatalog/getTicketsByEvent?limit=" + itemsPerPage + "&offset=" + webOffset + "&sortby=p&orderby=a&eventId="
					+ eventId + "&xbid=" + xbid + "&cobrandId=" + cobrandId));
			ticketListingCrawl.setFetchingTime(ticketListingCrawl.getFetchingTime() + System.currentTimeMillis() - startFetchingTime);		
			httpPost.addHeader("Host", "www.stubhub.com");		
			httpPost.addHeader("Accept", "application/json, text/javascript, */*");

			response = httpClient.execute(httpPost);			
			entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
			
			content = EntityUtils.toString(entity);
			ticketListingCrawl.setFetchingTime(ticketListingCrawl.getFetchingTime() + System.currentTimeMillis() - startFetchingTime);
	 		ticketListingCrawl.setFetchedByteCount(ticketListingCrawl.getFetchedByteCount() + entity.getReadByteCount());						
			
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
			Document xmlDocument = documentBuilder.parse(new ByteArrayInputStream(content.getBytes()));
			
			if (webTotal == -1) {
				Node detailsNode = XPathAPI.selectSingleNode(xmlDocument, "/tickets/details");
				if (detailsNode == null) {
					throw new Exception("StubHub Web Listing do not contain tickets/details tag");
				}
				webTotal = Integer.parseInt(detailsNode.getAttributes().getNamedItem("total").getTextContent());
			}
			
			NodeList ticketNodeList = XPathAPI.selectNodeList(xmlDocument, "//ticket");
			for (int i = 0 ; i < ticketNodeList.getLength() ; i++) {
				Node ticketNode = ticketNodeList.item(i);
				for (int j = 0 ; j < ticketNode.getChildNodes().getLength() ; j++) {
					Node child = ticketNode.getChildNodes().item(j);
					
					if (child.getNodeName().equals("id")) {
						ticketIdsInWeb.add(child.getTextContent());
						break;
					}
				}
			}
			webOffset += itemsPerPage;

		} while (webOffset <= webTotal);

		
//		System.out.println("TICKET IN WEB=" + ticketIdsInWeb.size());

		//
		// FETCH TICKETS FROM FEED
		// Tickets not in web listing are ignored. Otherwise they are indexed.
		//
		do {
			
			// http://partnerfeed.stubhub.com/listingCatalog/static/schema.xml
			// ticket_medium_id:
			// - 1 (normal)
			// - 3 (instant)
			//
			// delivery_icon
			// - edelivery
			// - drop
			// - fedex
			//
			// rules:
			// if ticket_medium_id = 2 => instant
			// else if delivery_icon = edelivery =>edelivery
			 
			
			startFetchingTime = System.currentTimeMillis();
			
			httpPost = new HttpPost(
					"http://partnerfeed.stubhub.com/listingCatalog/select/?q=%2B+stubhubDocumentType:ticket%0D%0A%2B+showTicketKeyStr:ticketACTIVE" + eventId
					+ "&version=2.2&start=" + start
					+ "&rows=" + itemsPerPage
					+ "&indent=off&fl=curr_price+event_id+id+quantity+row_desc+section+sale_method+comments+time_left+quantity_remain+ticket_split+seats+ticket_medium_id+delivery_icon"
					+ "&wt=json"
			);
			
			httpPost.addHeader("Host", "partnerfeed.stubhub.com");		
			httpPost.addHeader("Accept", "application/json, text/javascript, */*");
			httpPost.addHeader("Accept-Encoding", "gzip,deflate");
			// httpPost.addHeader("Keep-Alive", "300");
			// httpPost.addHeader("Connection", "keep-alive");
			
			response = httpClient.execute(httpPost);			
			entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());			
			content = EntityUtils.toString(entity);
			
			// System.out.println("CONTENT=" + content);
			ticketListingCrawl.setFetchedByteCount(ticketListingCrawl.getFetchedByteCount() + entity.getReadByteCount());						
			ticketListingCrawl.setFetchingTime(ticketListingCrawl.getFetchingTime() + System.currentTimeMillis() - startFetchingTime);		
			
			JSONObject jsonObject = new JSONObject(content);
			
			total = jsonObject.getJSONObject("response").getInt("numFound");
						
			JSONArray docsJSONArray = jsonObject.getJSONObject("response").getJSONArray("docs");
			
			for (int i = 0 ; i < docsJSONArray.length() ; i++) {
				JSONObject docJSONObject = docsJSONArray.getJSONObject(i);

				String section = docJSONObject.getString("section");
				
				// if we have 100:122 => returns 122
				Matcher matcher = sectionPattern.matcher(section);
				if (matcher.find()) {
					section = matcher.group(1);
				}

				String row = docJSONObject.getString("row_desc");
				Double price = docJSONObject.getDouble("curr_price");
				int quantity = docJSONObject.getInt("quantity");
				int quantity_remain = docJSONObject.getInt("quantity_remain");
				int split = docJSONObject.getInt("ticket_split");
				String id = docJSONObject.getString("id");
				
				String timeLeftString = docJSONObject.getString("time_left");
				
				// skip tickets which are not in the web listing
				if (!ticketIdsInWeb.contains(id)) {
					// System.out.println("SKIP WEB=" + id);
					continue;
				}

				// skip tickets which are appearing several times in the feed
				if (ticketIdsInFeed.contains(id)) {
					// System.out.println("SKIP FEED=" + id);
					continue;
				}
				
				// if time is less than one hour, we have:
				// 	<span style="color=#FF0000">54 min</span>
																
				Matcher timeLeftMatcher = timeLeftPattern.matcher(timeLeftString);
				if (timeLeftMatcher.find()) {
					timeLeftString = timeLeftMatcher.group(1);
					// System.out.println("SKIP TIME=" + id);
				}				

				// Time Left to Purchase: 1d 1h 38m
				// Time Left to Purchase: 15 min
				long endTime = new Date().getTime();
				
				TicketStatus ticketStatus = null;
				Date endDate = null;
				
				if (timeLeftString.contains("Ended")) {
					ticketStatus = TicketStatus.SOLD;
				} else {
					ticketStatus = TicketStatus.ACTIVE;
					
					String[] timeTokens = timeLeftString.split(" ");
					
					if (timeTokens.length == 2 && timeTokens[1].equals("min")) {
						endTime += Integer.parseInt(timeTokens[0]) * 60L * 1000L;							
					} else {						
						for (String timeToken: timeTokens) {
							int timeValue = Integer.parseInt(timeToken.substring(0, timeToken.length() - 1));
							String timeUnit = timeToken.substring(timeToken.length() - 1);
							
							if (timeUnit.equals("d")) {
								endTime += timeValue * 24L * 60L * 60L * 1000L;
							} else if (timeUnit.equals("h")) {
								endTime += timeValue * 60L * 60L * 1000L;								
							} else if (timeUnit.equals("m")) {
								endTime += timeValue * 60L * 1000L;																
							}
						}
					}
					endDate = new Date(endTime);
				}
				
				// Sale Method:
				// 0 - Auction listing
				// 1 - Fixed price listing
				// 2 - Declining price listing
				
				boolean isAuction = docJSONObject.getString("sale_method").equals("0");
				
				Double buyItNowPrice;
				TicketType ticketType;
				
				if (isAuction) {
					// for auction, there is no buyItNow price
					buyItNowPrice = null;
					ticketType = TicketType.AUCTION;
				} else {
					// otherwise the buyItNowPrice is the current price
					buyItNowPrice = price;
					ticketType = TicketType.REGULAR;
				}
				
				String ticketMediumId = docJSONObject.getString("ticket_medium_id");
				String deliveryIcon = docJSONObject.getString("delivery_icon");
				TicketDeliveryType ticketDeliveryType = null;
				if (ticketMediumId.equals("3")) {
					ticketDeliveryType = TicketDeliveryType.INSTANT;
				} else if (deliveryIcon.equals("edelivery")){
					ticketDeliveryType = TicketDeliveryType.EDELIVERY;					
				}
				
				// seats can be:
				// - 3,4,5,6
				// - 15,16
				// - ga-5,ga-6,ga-7,ga-8
				// - General Admission
				String seats = docJSONObject.getString("seats");
				String[] seatTokens = seats.split(",");
				Integer startSeat = null;
				Integer endSeat = null;				
				
				for(String seatToken: seatTokens) {
					Matcher seatMatcher = seatPattern.matcher(seatToken);
					if (seatMatcher.find()) {
						int seat = Integer.parseInt(seatMatcher.group(1));
						if (startSeat == null || seat < startSeat) {
							startSeat = seat;
						}

						if (endSeat == null || seat > endSeat) {
							endSeat = seat;							
						}
					}
				}
					
				TicketHit ticketHit = new TicketHit(Site.STUB_HUB, id,
						ticketType,
						quantity, split,
						row, section, price, buyItNowPrice, "StubHub", ticketStatus);


				ticketHit.setTicketListingCrawl(ticketListingCrawl);
				ticketHit.setEndDate(endDate);
				ticketHit.setTicketDeliveryType(ticketDeliveryType);
				ticketHit.setSoldQuantity(quantity - quantity_remain);
				
				ticketHit.setSeat((seats == null)?null:startSeat + "-" + endSeat);
	
				long startIndexationTime = System.currentTimeMillis();
				indexTicketHit(ticketHitIndexer, ticketHit);
				ticketListingCrawl.setIndexationTime(ticketListingCrawl.getIndexationTime() + System.currentTimeMillis() - startIndexationTime);
				
				ticketIdsInFeed.add(ticketHit.getItemId());
			}
			start += itemsPerPage;
		}	while (start < total);

//		System.out.println("TICKET IN FEED=" + ticketIdsInFeed.size());

		ticketListingCrawl.setExtractionTime(System.currentTimeMillis() - startProcessingTime - ticketListingCrawl.getFetchingTime() - ticketListingCrawl.getIndexationTime());

		//
		// FETCH TICKETS FROM WEB WHICH ARE IN WEB BUT NOT IN FEED.
		// Need to fetch individual ticket page to get the required information(qty,row, section, split,...)
		//
		ticketIdsInWeb.removeAll(ticketIdsInFeed);
		for (String ticketId: ticketIdsInWeb) {
			System.out.println("MISSING ID=" + ticketId);
			httpGet = new HttpGet(eventPageUrl + "?ticket_id=" + ticketId);
			httpGet.addHeader("Host", "www.stubhub.com");		
			httpGet.addHeader("Accept-Language", "en-us,en;q=0.5");
			httpGet.addHeader("Accept-Encoding", "gzip,deflate");
			httpGet.addHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
			httpGet.addHeader("Referer", eventPageUrl);
			
			response = httpClient.execute(httpGet);					
			
			// some items has expired and redirect to error page
			if (redirectHandler.getRedirectUrl() != null && redirectHandler.getRedirectUrl().indexOf("error.html") >= 0) {
				httpGet.abort();
				continue;
			}
			
			entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());

			String pageContent = EntityUtils.toString(entity);					
			
			// auction item url
			// http://www.stubhub.com/new-york-yankees-tickets/yankees-vs-rays-5-7-2009-720475/?ticket_id=186010228
			
			// http://www.stubhub.com/st-johns-red-storm-baseball-tickets/st-johns-red-storm-vs-georgetown-hoyas-3-29-2009-802507/
			// for auction listing, get remaining date

			Matcher priceMatcher = stubHubPricePattern.matcher(pageContent);
			priceMatcher.find();
			String priceString = priceMatcher.group(1);
	        NumberFormat numberFormat = NumberFormat.getInstance();
	        Double price = numberFormat.parse(priceString).doubleValue();

			TicketType ticketType;			
			Date endDate;			
			Double buyItNowPrice;

			Matcher auctionMatcher = stubHubAuctionPattern.matcher(pageContent);			
			
			if (auctionMatcher.find()) {
				long endTime = new Date().getTime();
				Matcher timeLeftMatcher = stubHubTimeLeftPattern.matcher(pageContent);
				timeLeftMatcher.find();
				String timeLeftString = timeLeftMatcher.group(1);
				System.out.println("TILE LEFT STRING=" + timeLeftString);

				// Time Left to Purchase: 1d 1h 38m
				// Time Left to Purchase: 15 min
				
				String[] timeTokens = timeLeftString.split(" ");
				
				if (timeTokens.length == 2 && timeTokens[1].equals("min")) {
					endTime += Integer.parseInt(timeTokens[0]) * 60L * 1000L;							
				} else {						
					for (String timeToken: timeTokens) {
						int timeValue = Integer.parseInt(timeToken.substring(0, timeToken.length() - 1));
						String timeUnit = timeToken.substring(timeToken.length() - 1);
						
						if (timeUnit.equals("d")) {
							endTime += timeValue * 24L * 60L * 60L * 1000L;
						} else if (timeUnit.equals("h")) {
							endTime += timeValue * 60L * 60L * 1000L;								
						} else if (timeUnit.equals("m")) {
							endTime += timeValue * 60L * 1000L;																
						}
					}
				}
				endDate = new Date(endTime);
				ticketType = TicketType.AUCTION;
				buyItNowPrice = null;
			} else {
				endDate = null;
				ticketType = TicketType.REGULAR;
				buyItNowPrice = price;
			}
			
			TicketDeliveryType ticketDeliveryType = null;

			// check if the ticket is edelivery or instanteDelivery
			Matcher eDeliveryMatcher = stubHubEDeliveryPattern.matcher(pageContent);
			if (eDeliveryMatcher.find()) {
				ticketDeliveryType = TicketDeliveryType.EDELIVERY;
			} else {
				Matcher instantMatcher = stubHubInstantEDeliveryPattern.matcher(pageContent);
				if (instantMatcher.find()) {
					ticketDeliveryType = TicketDeliveryType.INSTANT;					
				}
			}
			
			Matcher rowMatcher = stubHubRowPattern.matcher(pageContent);
			rowMatcher.find();
			String row = rowMatcher.group(1);

			Matcher sectionMatcher = stubHubSectionPattern.matcher(pageContent);
			sectionMatcher.find();
			String section = sectionMatcher.group(1);
						
			Matcher quantitySelectMatcher = stubHubQuantitySelectPattern.matcher(pageContent);
			quantitySelectMatcher.find();
			String quantitySelect = quantitySelectMatcher.group(1);
			
			Matcher quantityOptionMatcher = stubHubQuantityOptionPattern.matcher(quantitySelect);
			Integer minQuantity = Integer.MAX_VALUE;
			Integer maxQuantity = Integer.MIN_VALUE;
			while(quantityOptionMatcher.find()) {
				String quantityOptionString = quantityOptionMatcher.group(1);
				Integer q = Integer.parseInt(quantityOptionString);
				minQuantity = Math.min(minQuantity, q); 
				maxQuantity = Math.max(maxQuantity, q); 
			}
						
			// System.out.println("section=" + section);
			// System.out.println("row=" + row);
			// System.out.println("PRICE=" + price);
			// System.out.println("minQuant=" + minQuantity);
			// System.out.println("maxQuant=" + maxQuantity);
			// System.out.println("TicketType=" + ticketType);
			// System.out.println("ticketDeliveryType=" + ticketDeliveryType);
			// System.out.println("note=" + note);
			// System.out.println("endDate=" + endDate);

			TicketHit ticketHit = new TicketHit(Site.STUB_HUB, ticketId,
					ticketType,
					maxQuantity, minQuantity,
					row, section, price, buyItNowPrice, "StubHub", TicketStatus.ACTIVE);


			ticketHit.setTicketListingCrawl(ticketListingCrawl);
			ticketHit.setEndDate(endDate);
			ticketHit.setTicketDeliveryType(ticketDeliveryType);

			// System.out.println("TICKET HIT=" + ticketHit);

			long startIndexationTime = System.currentTimeMillis();
			indexTicketHit(ticketHitIndexer, ticketHit);
			ticketListingCrawl.setIndexationTime(ticketListingCrawl.getIndexationTime() + System.currentTimeMillis() - startIndexationTime);
		}
		return true;
	}
	
	@Override
	public boolean isValidUrl(String url) {
		// valid url: http://www.stubhub.com/mlb-all-star-game-tickets/mlb-all-star-strip-7-11-2009-750829/
		// http://www.stubhub.com/jimmy-buffett-tickets/jimmy-buffett-honolulu-waikiki-shell-2-28-2009-790072/
		// http://www.stubhub.com/super-bowl-tickets/super-bowl-2-1-2009-565762/
		// http://www.stubhub.com?q=eventId=62789
		return url.matches("^http://www.stubhub.com/.*[0-9]*/$") || url.matches("^http://www.stubhub.com/\\?event_id=\\d+$");		
	}

	@Override
	public String getTicketListingUrl(TicketListingCrawl ticketListingCrawl) {
		return "http://www.stubhub.com/largesellers-ticketcenter/";
		//return ticketListingCrawl.getQueryUrl();
	}
	
	@Override
	public String getTicketItemUrl(TicketListingCrawl ticketListingCrawl, Ticket ticket) {
		return "http://www.stubhub.com/largesellers-ticketcenter/";
//		if (ticket.getTicketListingCrawl().getQueryUrl().indexOf("?") >= 0) {
//			return ticket.getTicketListingCrawl().getQueryUrl().replaceFirst("stubhub.com", "stubhub.com/largesellers-ticketcenter") + "&ticket_id=" + ticket.getItemId();
//		}
//
//		return ticket.getTicketListingCrawl().getQueryUrl() + "?ticket_id=" + ticket.getItemId();
	}
	
	/**
	 * Redirect Handler Class 
	 */
	private class StubHubRedirectHandler implements RedirectHandler{
		private String redirectUrl;
		
		public StubHubRedirectHandler() {
		}

		public URI getLocationURI(HttpResponse response, HttpContext context)
				throws ProtocolException {	
			
			System.out.println("REDIRECT URL=" + redirectUrl);
			if (redirectUrl.startsWith("http://")) {
				return URI.create(redirectUrl);
			} else {
				return URI.create("http://www.stubhub.com" + redirectUrl);				
			}
		}

		public boolean isRedirectRequested(HttpResponse response, HttpContext context) {
			Header[] headers = response.getAllHeaders();
			for(Header header: headers) {
				// System.out.println("Header=" + header.getName() + "=" + header.getValue());
				if (header.getName().equalsIgnoreCase("Location")) {
					redirectUrl = header.getValue();
					return true;
				}
			}
			
			return false;
		}

		public void setRedirectUrl(String redirectUrl) {
			this.redirectUrl = redirectUrl;
		}		

		public String getRedirectUrl() {
			return redirectUrl;
		}		
	}	
	
}